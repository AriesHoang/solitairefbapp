window.Utils = {};
window.Utils.cloneObject = function(objSrc){
    return JSON.parse(JSON.stringify(objSrc));
};
window.Utils.isEmpty = function (ob) {
    return (ob === null || ob === undefined || ob === "");
};
window.Utils.removeItemInArray = function(item, array){
    let idxItem = array.indexOf(item);
    if(idxItem > -1)
        array.splice(idxItem, 1);
    return array;
}
window.Utils.convertTime = function(iTime){
    iTime = Math.floor(iTime);
    let min = Math.floor(iTime/60);
    let sec = iTime%60
    return ((min < 10 ? "0" : "")  + min + ":" + (sec < 10 ? "0" : "") + sec);
}


window.SoundEffect = {
    DEAL_SOUND: 0,
    FLIP_SOUND: 1,
    CLICK_SOUND: 2,
    DEFEAT_SOUND: 3
}