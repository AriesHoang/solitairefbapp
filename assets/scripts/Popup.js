cc.Class({
    extends: cc.Component,

    properties: {
        lblContent: cc.Label
    },
    showPopup(bShow, strContent){
        if(bShow){
            this.lblContent.string = strContent
            this.node.opacity = 0;
            this.node.scale = 0;
            this.node.active = true;
            cc.tween(this.node)
                .to(0.2, {scale: 1.0, opacity: 255})
                .start();
        }else{
            cc.tween(this.node)
                .to(0.2, {scale: 0, opacity: 0})
                .call(()=>{this.node.active = false})
                .start();
        }

    },
    btnStartNewGame(){
        this.gameScene.btnNewGameClick();
        this.showPopup(false);
    },
    btnCloseClick(){
        this.showPopup(false);
    },
    ctor () {
    },

    // update (dt) {},
});
