function Decks(){
    this._arrCards = [];
    for(let i = 0; i < 52; i++){
        this._arrCards.push(i);
    }
}

Decks.prototype.reset = function(){
    this._arrCards.sort(function(){
        return 0.5 - Math.random();
    });
}

Decks.prototype.dealCard = function(){
    // let _arrCardOnTable = [51,50,45,43,49,46,40,48,47,41,39,44,42,36,35,28,37,34,29,27,20,19,38,32,30,24,23,16,15];
    // for(let i = 0; i < _arrCardOnTable.length; i++){
    //     let idx = this._arrCards.indexOf(_arrCardOnTable[i])
    //     this._arrCards.splice(idx, 1);
    // }
    // cc.log("_arrCards: " + JSON.stringify(this._arrCards));
    // this._arrCards = this._arrCards.concat(_arrCardOnTable);



    this.reset();
    return this._arrCards;
}

module.exports = Decks;