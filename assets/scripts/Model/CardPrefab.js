const arrSuit = ["B", "T", "R", "C"];
const arrRank = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"];
cc.Class({
    extends: cc.Component,

    properties: {
        btnCard: cc.Button,
        sprCard: cc.Sprite,
        animCard: cc.Animation,
        sprBackCard: cc.SpriteFrame,
        sprBorder: cc.Node,
        effectCard: cc.Animation
    },

    ctor() {
        this.idCard = -1;
        this.groupId = -1;
        this.bTouch = false;
        this.oriPos = cc.v2(0, 0);
        this.oriIndex = 0;
        this.spriteFrameCard = null;
    },
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {
    },
    setSpriteCard(idCard, sprCard){
        // cc.log("setSpriteCard: " + idCard);
        this.showBorder(false);
        this.bTouch = false;
        this.bActive = false
        this.idCard = idCard;
        this.spriteFrameCard = sprCard;
        this.btnCard.interactable = false;
    },
    showCard(bAnim){
        this.bActive = true;
        if(bAnim){
            cc.tween(this.node)
                .to(0.2, {scaleX:0})
                .call(()=>{
                    this.sprCard.spriteFrame = this.spriteFrameCard;
                    this.btnCard.interactable = true;
                    this.bTouch = true;
                })
                .to(0.2, {scaleX:1.0})
                .start();
        }else{
            this.sprCard.spriteFrame = this.spriteFrameCard;
            this.btnCard.interactable = false;
            this.bTouch = false;
        }
    },
    showBorder(bShow){
        this.sprBorder.active = bShow;
    },
    shakeCard(){
        this.animCard.play();
    },
    showEffectCard(bShow = true){
        this.effectCard.node.active = bShow;
        if(bShow){
            this.effectCard.play();
        }
    },
    endEffectCard(){
        this.showEffectCard(false);
    },
    resetCard(){
        this.idCard = -1;
        this.bTouch = false;
        this.bActive = false;
        this.sprCard.spriteFrame = this.sprBackCard;
        this.showBorder(false);
        this.effectCard.node.active = false;
    }
});
