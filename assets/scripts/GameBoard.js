var Decks = require("Decks");
var CardPrefab = require("CardPrefab");
var FBSDK = require("FbSdk");
const arrPosAnim = [[cc.v2(-200, 0), cc.v2(200, 0), cc.v2(-200, -250), cc.v2(200, -250), cc.v2(-200, -500), cc.v2(200, -500)],
    [cc.v2(0, 0), cc.v2(150, -550), cc.v2(-300, -250), cc.v2(300, -250), cc.v2(-150, -550)],
    [cc.v2(-350, -100), cc.v2(-200, -450), cc.v2(0, -100), cc.v2(200, -450), cc.v2(350, -100)]]
const typeAction = {
    GET_CARD_ON_DECK: 0,
    MOVE_CARD_DECK_TO_HOLDER: 1,
    MOVE_CARD_DECK_TO_TABLEGROUP: 2,
    MOVE_CARD_HOLDER_TO_TABLEGROUP: 3,
    MOVE_CARD_TABLE_TO_HOLDER:4,
    MOVE_CARD_TABLE_TO_TABLE: 5
}

cc.Class({
    extends: cc.Component,

    properties: {
        btnAutoComplete: cc.Node,
        btnHint: cc.Node,
        btnUndo: cc.Button,
        prefabCard: cc.Prefab,
        srpCardOnDeck: cc.Node,
        cardNodePool:{
            default: null,
            hidden: false,
            type: cc.NodePool
        },
        cardHolderAnchor: [cc.Node],
        cardOnTableAnchor: [cc.Node],
        cardDeckAnchor:cc.Node,
        nodeCardContainer: cc.Node,
        btnGetCard: cc.Button,
        arrResourcesCard: [cc.SpriteFrame],


        listCardHolder: [],
        listIdCardHolder:[],

        listCardDeck: [],
        listIdCardDeck: [],

        listCardOnTable: [],
        listIdCardOnTable: [],
        arrCardMove:[],
    },

    // LIFE-CYCLE CALLBACKS:
    ctor(){
        this.gameScene = null;
        this.decks = new Decks();
        this.listCardHolder = [[],[],[],[]];
        this.listIdCardHolder = [[],[],[],[]];

        this.listCardDeck = [];
        this.listIdCardDeck = [];

        this.listCardOnTable = [];
        this.listIdCardOnTable = [];
        this.bAutoComplete = false;

        this.iScore = 0;
        this.iMove = 0;
        this.historyAction = [];
    },

    start () {
        this.init();
    },
    init(){
        this.srpCardOnDeck.zIndex = 100;
        this.cardNodePool = new cc.NodePool();
        for(let i = 0; i < 52; i++){
            let card = cc.instantiate(this.prefabCard);
            this.cardNodePool.put(card);
        }
        this.resetGame();
        this.fbsdk = new FBSDK();
        this.fbsdk.init();
    },
    resetGame(){
        this.nodeCardContainer.removeAllChildren(true);

        this.listCardHolder = [[],[],[],[]];
        this.listIdCardHolder = [[],[],[],[]];

        this.listCardDeck = [];
        this.listIdCardDeck = [];

        this.listIdCardOnTable = [[],[],[],[],[],[],[]];
        this.listCardOnTable = [[],[],[],[],[],[],[]];

        this.iHiddenCard = 21;
        this.idxCurrentDeck = 0;
        this.arrGroupCardOnTable = [];
        // this.btnNewGame.node.active = true;
        this.btnGetCard.node.active = true;
        this.btnGetCard.interactable = false;
        this.setStateGetCard(false);
        this.bGameStart = false;
        this.bAutoComplete = false;
        this.btnAutoComplete.active = false;
        this.srpCardOnDeck.active = false;
        this.iScore = 1000;
        this.iMove = 0;
        // this.btnUndo.interactable = false;
        this.historyAction = [];
    },
    btnGetCardClick(){
        if(this.bGameStart){
            this.gameScene.playEffect(SoundEffect.CLICK_SOUND);
            this.processGetCard();
        }
    },
    processGetCard(sourcePos){
        // cc.log("btnGetCardClick :" + this.listIdCardDeck);
        // cc.log("this.listCardDeck: " + this.listCardDeck.length);

        if(this.listIdCardDeck.length < 1) return;
        this.gameScene.playEffect(SoundEffect.FLIP_SOUND);
        this.setStateGetCard(this.idxCurrentDeck < this.listIdCardDeck.length - 1);
        if(this.idxCurrentDeck === this.listIdCardDeck.length) {
            this.idxCurrentDeck = 0;
            this.setStateGetCard(true);
            for (let i = 0; i < this.listCardDeck.length; i++) {
                this.listCardDeck[i].node.removeFromParent(true);
            }
            this.listCardDeck = [];
        }
        // }else{
        let objectHistory = {};
        objectHistory.typeAction = typeAction.GET_CARD_ON_DECK;
        this.historyAction.push(objectHistory);
        this.iMove ++;
        this.iScore -= 5;
            let card = null;
            let cardId = this.listIdCardDeck[this.idxCurrentDeck];
            if(this.listCardDeck.length === 3){
                for(let i = 1; i < 3; i++){
                    let cardId2 = this.listIdCardDeck[this.idxCurrentDeck - i];
                    this.listCardDeck[2-i].setSpriteCard(cardId2, this.arrResourcesCard[cardId2]);
                    this.listCardDeck[2-i].showCard(false);
                }
                card = this.listCardDeck[2];
                card.resetCard();
                card.setSpriteCard(cardId, this.arrResourcesCard[cardId]);
            }else{
                card = this.getCard(cardId);
                this.nodeCardContainer.addChild(card.node);
                if(this.listCardDeck.length > 0){
                    this.listCardDeck[this.listCardDeck.length - 1].bTouch = false;
                }

                this.listCardDeck.push(card);

            }
            card.node.position = this.btnGetCard.node.getPosition();
            if(!Utils.isEmpty(sourcePos))
                card.node.position = sourcePos;
            card.node.zIndex = this.listCardDeck.length;
            let posX = this.cardDeckAnchor.getPosition().x + 50*(this.listCardDeck.length - 1);
            let pos = cc.v2(posX, this.cardDeckAnchor.getPosition().y);
            card.oriPos = pos;
            // cc.log("cardDeck_" + this.listCardDeck.length + " has oriPos: " + pos.x + "-" + pos.y);
            card.oriIndex = this.listCardDeck.length;
            let iTime = this.bAutoComplete ? 0.05 : 0.1;
            cc.tween(card.node)
                .to(iTime, {position: pos})
                .call(()=>{
                    card.showCard(true);
                    if(this.bAutoComplete)
                        this.autoComplete();
                })
                .start();

            this.idxCurrentDeck++;
        this.btnGetCard.node.active = !(this.listIdCardDeck.length < 2);
        // cc.log("btnGetCardClick :" + this.listIdCardDeck);
        // cc.log("this.listCardDeck: " + this.listCardDeck.length);
        // }
    },
    btnHintClick(){
        this.gameScene.playEffect(SoundEffect.CLICK_SOUND);
        // let self = this;
        // this.fbsdk.showRewardVideo(()=>{
        //     if(self.bGameStart)
        //         self.checkHint();
        // });
        if(this.bGameStart)
            this.checkHint();

    },
    btnAutoCompleteClick(){
        if(this.bGameStart){
            this.gameScene.playEffect(SoundEffect.CLICK_SOUND);
            this.bAutoComplete = true;
            this.btnAutoComplete.active = false;
            this.autoComplete();
        }
        
    },
    btnUndoClick(){
        cc.log("========== Undo Click:" + this.historyAction.length);
        this.gameScene.playEffect(SoundEffect.CLICK_SOUND);
        if(this.historyAction.length > 0 && this.bGameStart){
            this.iMove++;
            let lastAction = this.historyAction[this.historyAction.length - 1];

            if(lastAction.hasOwnProperty("cardIdActiveAfterMove") && !Utils.isEmpty(lastAction.cardIdActiveAfterMove)){
                let arrGroupCard = this.listCardOnTable[lastAction.idxSource];
                let listIdCardOfGroup = this.listIdCardOnTable[lastAction.idxSource];
                let card = arrGroupCard[arrGroupCard.length - 1];
                card.resetCard();
                card.idCard = lastAction.cardIdActiveAfterMove;
                this.iHiddenCard++;
                listIdCardOfGroup.splice(listIdCardOfGroup.length - 1, 1);
            }

            switch (lastAction.typeAction){
                case typeAction.GET_CARD_ON_DECK:
                    this.idxCurrentDeck--;
                    if (this.listCardDeck.length > 0){
                        this.listCardDeck[this.listCardDeck.length - 1].node.removeFromParent(true);
                        this.listCardDeck.splice(this.listCardDeck.length - 1, 1);
                    }
                    if(this.listCardDeck.length > 2){
                        for(let i = this.listCardDeck.length, j = 0; i > 0; i--, j++) {
                            let cardId = this.listIdCardDeck[this.idxCurrentDeck - 1 - j];
                            this.listCardDeck[i - 1].setSpriteCard(cardId, this.arrResourcesCard[cardId]);
                            this.listCardDeck[i - 1].showCard(false);
                        }
                    }else if (this.listCardDeck.length > 0){
                        // this.listCardDeck[this.listCardDeck.length - 1].node.removeFromParent(true);
                        // this.listCardDeck.splice(this.listCardDeck.length - 1, 1);
                    }
                    if(this.listCardDeck.length > 0){
                        this.listCardDeck[this.listCardDeck.length - 1].bTouch = true;
                    }
                    this.setStateGetCard(true);
                    this.btnGetCard.node.active = true;
                    break;
                case typeAction.MOVE_CARD_DECK_TO_HOLDER:
                    let card = lastAction.arrCardMove[0];
                    this.listIdCardDeck.splice(this.idxCurrentDeck, 0, card.idCard);
                    this.processGetCard(card.oriPos)
                    this.historyAction.splice(this.historyAction.length - 1, 1);


                    // cc.log(this.idxCurrentDeck + " - this.listIdCardDeck undo: " + JSON.stringify(this.listIdCardDeck));
                    this.listCardHolder[card.groupId - 100][this.listCardHolder[card.groupId - 100].length - 1].node.removeFromParent(true);
                    this.listCardHolder[card.groupId - 100].splice(this.listCardHolder[card.groupId - 100].length - 1, 1);
                    this.listIdCardHolder[card.groupId - 100].splice(this.listCardHolder[card.groupId - 100].length - 1, 1);
                    break;
                case typeAction.MOVE_CARD_DECK_TO_TABLEGROUP:
                    let card2 = lastAction.arrCardMove[0];
                    this.listIdCardDeck.splice(this.idxCurrentDeck, 0, card2.idCard);
                    // cc.log(this.idxCurrentDeck + " - this.listIdCardDeck undo: " + JSON.stringify(this.listIdCardDeck));
                    this.processGetCard(card2.oriPos);
                    this.historyAction.splice(this.historyAction.length - 1, 1);

                    this.listCardOnTable[card2.groupId][this.listCardOnTable[card2.groupId].length - 1].node.removeFromParent(true);
                    this.listCardOnTable[card2.groupId].splice(this.listCardOnTable[card2.groupId].length - 1, 1);
                    this.listIdCardOnTable[card2.groupId].splice(this.listIdCardOnTable[card2.groupId].length - 1, 1);
                    break;
                case typeAction.MOVE_CARD_HOLDER_TO_TABLEGROUP:
                    this.moveCardToHolder(lastAction.arrCardMove, lastAction.idxSource);
                    this.historyAction.splice(this.historyAction.length - 1, 1);
                    break;
                case typeAction.MOVE_CARD_TABLE_TO_HOLDER:
                    this.moveCardOnTables(lastAction.arrCardMove, lastAction.idxSource);
                    this.historyAction.splice(this.historyAction.length - 1, 1);
                    
                    break;
                case typeAction.MOVE_CARD_TABLE_TO_TABLE:
                    this.moveCardOnTables(lastAction.arrCardMove, lastAction.idxSource);
                    this.historyAction.splice(this.historyAction.length - 1, 1);
                    break;
            }

            this.historyAction.splice(this.historyAction.length - 1, 1);
            // cc.log("this.historyAction after:" + this.historyAction.length);
        }
    },
    showCardDeck(){
        this.idxCurrentDeck--;
        if(this.idxCurrentDeck > 2 && this.listCardDeck.length > 1){
            for(let i = 0; i < this.listCardDeck.length; i++){
                let cardId = this.listIdCardDeck[this.idxCurrentDeck - 3 + i];
                this.listCardDeck[i].setSpriteCard(cardId, this.arrResourcesCard[cardId]);
                this.listCardDeck[i].showCard(false);
                this.listCardDeck[i].bTouch = false;
            }
            let card = this.getCard(this.listIdCardDeck[this.idxCurrentDeck - 1]);
            this.nodeCardContainer.addChild(card.node);
            this.listCardDeck.push(card);
            let posX = this.cardDeckAnchor.getPosition().x + 50*(this.listCardDeck.length - 1);
            card.oriPos = cc.v2(posX, this.cardDeckAnchor.getPosition().y);
            card.node.position = cc.v2(posX, this.cardDeckAnchor.getPosition().y);
            card.oriIndex = this.listCardDeck.length;
            card.node.zIndex = this.listCardDeck.length;
            card.showCard(false);
            card.bTouch = true;
        }else if (this.listCardDeck.length > 0){
            this.listCardDeck[this.listCardDeck.length - 1].bTouch = true;

        }
    },
    setStateGetCard(bState){// true: getCard | false: Reload Deck
        this.btnGetCard.node.getComponent(cc.Sprite).spriteFrame = this.arrResourcesCard[bState ? 52 : 53];
    },
    startNewGame(){
        this.fbsdk.showInterstitial();
        // this.fbsdk.showBannerAds();
        this.resetGame();
        this.bGameStart = true;
        this.btnGetCard.interactable = true;
        this.setStateGetCard(true);
        let newDeck = this.decks.dealCard();
        // newDeck = [46,15,47,39,42,2,1,34,41,4,32,16,38,24,14,18,19,28,8,23,37,20,0,33,0,21,4,13,27,8,12,50,7,1,22,17,36,3,10,31,40,48,44,30,43,5,26,9,25,6,51,35];
        // cc.log("newDeck: " + newDeck);
        this.listIdCardDeck = newDeck.slice(0, 24);
        this.idxCurrentDeck = 0;
        let listIdCardOnTable = newDeck.slice(24, newDeck.length);

        for(let i = 0, idx = 0; i < 7; i++){
            for(let j = 0; j < i+1; j++){
                let cardId = listIdCardOnTable[idx]
                idx++;

                let card = this.getCard(cardId);
                card.node.position = this.btnGetCard.node.getPosition();
                this.nodeCardContainer.addChild(card.node);
                card.node.zIndex = j;
                let _posX = this.cardOnTableAnchor[i].getPosition().x;
                let _posY = this.cardOnTableAnchor[i].getPosition().y - 25*j;
                let _pos = cc.v2(_posX, _posY);
                card.oriPos = _pos;
                card.oriIndex = j;
                card.groupId = i;
                this.listCardOnTable[i].push(card);
                cc.tween(card.node)
                    .delay(0.3)
                    .to(0.3, {position: _pos})
                    .delay(0.1*j)
                    .call(()=>{
                        if(i === 0 && j === 0){
                            this.gameScene.playEffect(SoundEffect.DEAL_SOUND);
                        }
                        if(j === i) {
                        // if(true) {
                            card.showCard(true);
                            this.listIdCardOnTable[i].push(cardId);
                        }
                    })
                    .start();
            }
        }
    },
    getCard(cardId){
        let card = this.cardNodePool.get();
        if(Utils.isEmpty(card)){
            let cardPlus = cc.instantiate(this.prefabCard);
            this.cardNodePool.put(cardPlus);
            card = this.cardNodePool.get();
        }
        card.on(cc.Node.EventType.TOUCH_START, this.onBeginTouch, this, true);
        card.on(cc.Node.EventType.TOUCH_MOVE, this.onMoveCard, this, true);
        card.on(cc.Node.EventType.TOUCH_END, this.onEndTouch, this, true);

        card = card.getComponent(CardPrefab);
        card.setSpriteCard(cardId, this.arrResourcesCard[cardId]);
        return card;
    },
    onBeginTouch(event){
        this.gameScene.playEffect(SoundEffect.CLICK_SOUND);
        // this.arrCardMove = [];
        let cardSelect = event.target.getComponent(CardPrefab);
        if(!cardSelect.bTouch || this.arrCardMove.length > 0) {
            cc.log("double click....")
            for(let i = 0; i < this.arrCardMove.length; i++){
                let card = this.arrCardMove[i];
                card.node.position = card.oriPos;
                card.node.zIndex = card.oriIndex;
            }
            this.arrCardMove = [];
            return;
        }
        if(cardSelect.groupId > -1 && cardSelect.groupId < 100){//Card On Table
            let idxCardSelect = this.listCardOnTable[cardSelect.groupId].indexOf(cardSelect);
            for(let i = idxCardSelect; i < this.listCardOnTable[cardSelect.groupId].length; i++){
                let card = this.listCardOnTable[cardSelect.groupId][i];
                this.arrCardMove.push(card);
                card.node.zIndex = 100 + i;
            }
        }else{// Card On Deck or Holder
            this.arrCardMove.push(cardSelect);
            cc.log("cardSelect: " + 100)
            cardSelect.node.zIndex = 100;
        }
        // cc.log("event.target: " + cardSelect.idCard);
    },
    onMoveCard(event){
        this.bMoveCard = true;
        let cardComp = event.target.getComponent(CardPrefab);
        if(!cardComp.bTouch) return;
        let delta = event.touch.getDelta();
        // cc.log("onMoveCard: " + event);
        if(this.arrCardMove.length > 0){
            for(let i = 0; i < this.arrCardMove.length; i++){
                let card = this.arrCardMove[i];
                card.node.x += delta.x;
                card.node.y += delta.y;
            }
        }
    },
    onEndTouch(event){
        let cardComp = event.target.getComponent(CardPrefab);
        if(!cardComp.bTouch) return;
        // cc.log("cardComp: " + cardComp.idCard);
        // cc.log("this.arrCardMove: " + this.arrCardMove.length);
        if(this.arrCardMove.length > 0){
            if(this.bMoveCard && this.checkPosCardMoved(this.arrCardMove)){

            }else if(!this.bMoveCard && this.autoMoveCard()){

            }else{
                for(let i = 0; i < this.arrCardMove.length; i++){
                    let card = this.arrCardMove[i];
                    // cc.log("card.oriIndex: " + card.oriIndex)
                    card.node.position = card.oriPos;
                    card.node.zIndex = card.oriIndex;
                    card.shakeCard();
                }
            }
        }
        this.arrCardMove = [];
        this.bMoveCard = false;
    },
    checkPosCardMoved(arrCardMoved){
        if(this.arrCardMove.length < 0) return false;
        let posEndTouch = arrCardMoved[0].node.getPosition();
        let rectCardMove = arrCardMoved[0].node.getBoundingBox();
        //bounding Box Holder
        for(let i = 0; i < this.cardHolderAnchor.length; i++){
            let rectHolder = this.cardHolderAnchor[i].getBoundingBox();
            if(rectHolder.contains(posEndTouch)){
                let idxHolder = i;
                if(this.checkWithListHolder(arrCardMoved[0].idCard, idxHolder) !== -1 && arrCardMoved.length === 1 && arrCardMoved[0].groupId < 100){
                    this.moveCardToHolder(arrCardMoved[0], idxHolder);
                    return true;
                }
                return false;
            }
        }
        //BoudingBox Group On Table
        for(let j = 0; j < this.listCardOnTable.length; j++){
            let rectOfLastCard = null;
            if(this.listCardOnTable[j].length === 0)
                rectOfLastCard = this.cardOnTableAnchor[j].getBoundingBox();
            else
                rectOfLastCard = this.listCardOnTable[j][this.listCardOnTable[j].length - 1].node.getBoundingBox();
            if(rectOfLastCard.contains(posEndTouch) && j !== arrCardMoved[0].groupId){
                let idxGroup = j;
                if(this.checkWithCardOnTable(arrCardMoved[0].idCard, idxGroup) !== -1){
                    this.moveCardOnTables(arrCardMoved, idxGroup);
                    return true;
                }
                return false;
            }
        }

        return false;
    },
    autoMoveCard(){
        if(this.arrCardMove.length < 0) return false;
        let holderValidate = this.checkWithListHolder(this.arrCardMove[0].idCard);
        // cc.log("holderValidate: " + holderValidate);
        let groupValidate = this.checkWithCardOnTable(this.arrCardMove[0].idCard);
        // cc.log("groupValidate: " + groupValidate);
        if(holderValidate !== -1 && this.arrCardMove.length === 1 && this.arrCardMove[0].groupId < 100){
            this.moveCardToHolder(this.arrCardMove[0], holderValidate);
            return true;
        }else if(groupValidate !== -1 && groupValidate !== this.arrCardMove[0].groupId){
            this.moveCardOnTables(this.arrCardMove, groupValidate);
            return true;
        }
        return false;
    },
    moveCardToHolder(cardMove, idxHolder){
        this.gameScene.playEffect(SoundEffect.FLIP_SOUND);
        if(cardMove.groupId > 99) return;//Card FromHolder
        this.iScore += 100;
        this.iMove ++;
        let objectHistory = {};
        let bCardFromDeck = (cardMove.groupId === -1);

        if(bCardFromDeck){
            objectHistory.typeAction = typeAction.MOVE_CARD_DECK_TO_HOLDER;
            // cc.log("this.listCardDeck:" + this.listCardDeck.length);
            objectHistory.idxSource = -1;
        } else{
            objectHistory.typeAction = typeAction.MOVE_CARD_TABLE_TO_HOLDER;
            // cc.log("this.listCardOnTable[" + cardMove.groupId + "] : " + this.listCardOnTable[cardMove.groupId].length);
            objectHistory.idxSource = cardMove.groupId;
        }
        objectHistory.idxDes = idxHolder;
        objectHistory.arrCardMove = [cardMove];
        
        let pos = this.cardHolderAnchor[idxHolder].getPosition()
        let iTime = this.bAutoComplete ? 0.1 : 0.2;
        cc.tween(cardMove.node)
            .delay(0.1)
            .to(iTime, {position: pos})
            .call(()=>{
                cardMove.showEffectCard();
                cardMove.node.zIndex = cardMove.oriIndex;
                if(this.bAutoComplete)
                    this.autoComplete();
                this.checkFinishGame();
            })
            .start();

        // cardMove.node.position = this.cardHolderAnchor[idxHolder].getPosition();

        this.listCardHolder[idxHolder].push(cardMove);
        this.listIdCardHolder[idxHolder].push(cardMove.idCard);

        cardMove.oriIndex = this.listCardHolder[idxHolder].length;
        cardMove.oriPos = this.cardHolderAnchor[idxHolder].getPosition();

        let activeCardInGroup = null;
        if(!bCardFromDeck){
            this.listCardOnTable[cardMove.groupId] = Utils.removeItemInArray(cardMove, this.listCardOnTable[cardMove.groupId]);
            this.listIdCardOnTable[cardMove.groupId] = Utils.removeItemInArray(cardMove.idCard, this.listIdCardOnTable[cardMove.groupId]);
            activeCardInGroup = this.checkCardActiveInGroup(cardMove.groupId);
        }else{
            this.listCardDeck = Utils.removeItemInArray(cardMove, this.listCardDeck);
            this.listIdCardDeck = Utils.removeItemInArray(cardMove.idCard, this.listIdCardDeck);
            let self = this;
            setTimeout(function (){
                self.showCardDeck();
            }, 200);
        }

        objectHistory.cardIdActiveAfterMove = activeCardInGroup;
        this.historyAction.push(objectHistory);
        cardMove.groupId = 100 + idxHolder;
    },
    moveCardOnTables(arrSource, idxGroup){
        this.gameScene.playEffect(SoundEffect.FLIP_SOUND);
        this.iScore -= 5;
        this.iMove ++;
        let objectHistory = {};
        let bCardFromDeck = (arrSource[0].groupId === -1);
        let bCardFromHolder = (arrSource[0].groupId > 99);
        if(bCardFromDeck){
            // cc.log("this.listCardDeck:" + this.listCardDeck.length);
            objectHistory.typeAction = typeAction.MOVE_CARD_DECK_TO_TABLEGROUP;
        }else if(bCardFromHolder){
            objectHistory.typeAction = typeAction.MOVE_CARD_HOLDER_TO_TABLEGROUP;
        }else{
            objectHistory.typeAction = typeAction.MOVE_CARD_TABLE_TO_TABLE;
        }
        objectHistory.idxSource = arrSource[0].groupId;
        objectHistory.idxDes = idxGroup;
        objectHistory.arrCardMove = arrSource;
    

        // cc.log("moveCardOnTables to: " + idxGroup)
        let desPos = null;
        if(this.listCardOnTable[idxGroup].length === 0)
            desPos = this.cardOnTableAnchor[idxGroup].getPosition();
        else{
            let lastCardPos = this.listCardOnTable[idxGroup][this.listCardOnTable[idxGroup].length - 1].node.getPosition();
            desPos = cc.v2(lastCardPos.x, lastCardPos.y - 50);
        }
        let activeCardInGroup = null;
        for(let i = 0; i < arrSource.length ; i++){
            let card = arrSource[i];
            // cc.log("cardId: " + card.idCard);
            let pos = cc.v2(desPos.x, desPos.y - 50*i);
            cc.tween(card.node)
                .to(0.2, {position: pos})
                .call(()=>{
                    card.node.zIndex = card.oriIndex;
                    if(this.bAutoComplete && (i === arrSource.length - 1))
                        this.autoComplete();
                })
                .start();

            if(bCardFromDeck) {
                this.listCardDeck = Utils.removeItemInArray(card, this.listCardDeck);
                this.listIdCardDeck = Utils.removeItemInArray(card.idCard, this.listIdCardDeck);
                let self = this;
                setTimeout(function (){
                    self.showCardDeck();
                }, 200);

            }else if (bCardFromHolder) {
                this.listCardHolder[card.groupId - 100] = Utils.removeItemInArray(card, this.listCardHolder[card.groupId - 100]);
                this.listIdCardHolder[card.groupId - 100] = Utils.removeItemInArray(card.idCard, this.listIdCardHolder[card.groupId - 100]);
            }else{
                this.listCardOnTable[card.groupId] = Utils.removeItemInArray(card, this.listCardOnTable[card.groupId]);
                this.listIdCardOnTable[card.groupId] = Utils.removeItemInArray(card.idCard, this.listIdCardOnTable[card.groupId]);
                if(i === arrSource.length - 1){
                    activeCardInGroup = this.checkCardActiveInGroup(card.groupId);
                }
                    
            }
            // card.node.zIndex = this.listCardOnTable[idxGroup].length + i;
            card.oriIndex = this.listCardOnTable[idxGroup].length;
            this.listCardOnTable[idxGroup].push(card);
            this.listIdCardOnTable[idxGroup].push(card.idCard);

            card.oriPos = pos;
            card.groupId = idxGroup;
        }
        objectHistory.cardIdActiveAfterMove = activeCardInGroup;
        this.historyAction.push(objectHistory);
    },
    checkWithListHolder(cardId, idxHolder){
        // cc.log("checkWithListHolder: " + cardId + " - " + idxHolder)
        if(!Utils.isEmpty(idxHolder)){
            if(CardUtils.validateCardHolder(cardId, this.listIdCardHolder[idxHolder])){
                return idxHolder;
            }
        }else{
            for(let i = 0; i < this.listIdCardHolder.length; i++){
                if(CardUtils.validateCardHolder(cardId, this.listIdCardHolder[i]))
                    return i;
            }
        }
        return -1;
    },
    checkWithCardOnTable(cardId, idxGroup){
        if(!Utils.isEmpty(idxGroup)){
            if(CardUtils.validateCardOnBoard(cardId, this.listIdCardOnTable[idxGroup]))
                return idxGroup;
        }else{
            for(let i = 0; i < this.listIdCardOnTable.length; i++){
                if(CardUtils.validateCardOnBoard(cardId, this.listIdCardOnTable[i]))
                    return i;
            }
        }
        return -1;
    },
    checkCardActiveInGroup(idxGroup){
        // cc.log("checkCardActiveInGroup: " + idxGroup);
        if(idxGroup > -1 && idxGroup < 8){
            let arrCard = this.listCardOnTable[idxGroup];
            let card = arrCard[arrCard.length - 1];
            if(arrCard.length > 0 && !card.bActive) {
                card.showCard(true);
                this.listIdCardOnTable[idxGroup].push(card.idCard);
                this.iHiddenCard--;
                if (this.iHiddenCard === 0){
                    // cc.log("You are Victory");
                    this.btnAutoComplete.active = true;
                }
                return card.idCard;
            }
        }
        return null;
    },



//    Check Hint
    checkHint(){
        this.iScore -= 10;
        // Check card On Table
        for(let i = 0; i < this.listCardOnTable.length; i++){
            let listCardOfGroup = this.listCardOnTable[i];
            if(listCardOfGroup.length > 0){
                //Check lastCard of Group match with Holder
                let idxHolder = this.checkWithListHolder(listCardOfGroup[listCardOfGroup.length - 1].idCard);
                if(idxHolder !== -1){
                    let card = listCardOfGroup[listCardOfGroup.length - 1];
                    let pos = this.cardHolderAnchor[idxHolder].getPosition();
                    this.moveCardHint([card], pos);
                    return;
                }
                // Check card On Table Match each other
                //If FirstCardOfGroup is King -> return
                if(listCardOfGroup[0].bActive && listCardOfGroup[0].idCard > 47 && listCardOfGroup[0].idCard < 52)
                    continue;

                let arrCard = [];

                for(let j = 0; j < listCardOfGroup.length; j++){
                    if(listCardOfGroup[j].bActive)
                        arrCard.push(listCardOfGroup[j]);
                }
                let idxGroupValidate = this.checkWithCardOnTable(arrCard[0].idCard);
                if(idxGroupValidate !== -1){
                    let desPos = null;
                    if(this.listCardOnTable[idxGroupValidate].length === 0)
                        desPos = this.cardOnTableAnchor[idxGroupValidate].getPosition();
                    else{
                        let lastCardPos = this.listCardOnTable[idxGroupValidate][this.listCardOnTable[idxGroupValidate].length - 1].node.getPosition();
                        desPos = cc.v2(lastCardPos.x, lastCardPos.y - 50);
                    }
                    this.moveCardHint(arrCard, desPos);
                    return;
                }
            }
        }
        // Check card On Deck
        if(this.listCardDeck.length > 0){
            let card = this.listCardDeck[this.listCardDeck.length - 1];
            //Check card Deck with Holder
            let idxHolder = this.checkWithListHolder(this.listCardDeck[this.listCardDeck.length - 1].idCard);
            if(idxHolder !== -1){
                let posHolder = this.cardHolderAnchor[idxHolder].getPosition();
                this.moveCardHint([card], posHolder);
                return;
            }
            //Check card Deck with All group
            let idxGroupValidate = this.checkWithCardOnTable(this.listCardDeck[this.listCardDeck.length - 1].idCard);
            if(idxGroupValidate !== -1){
                let desPos = null;
                if(this.listCardOnTable[idxGroupValidate].length === 0)
                    desPos = this.cardOnTableAnchor[idxGroupValidate].getPosition();
                else{
                    let lastCardPos = this.listCardOnTable[idxGroupValidate][this.listCardOnTable[idxGroupValidate].length - 1].node.getPosition();
                    desPos = cc.v2(lastCardPos.x, lastCardPos.y - 50);
                }
                this.moveCardHint([card], desPos);
                return;
            }
        }
        //Check card On List Card Deck
        if(this.listIdCardDeck.length > 0){
            for(let i = 0; i < this.listIdCardDeck.length; i++){
                let idxHolderValidate = this.checkWithListHolder(this.listIdCardDeck[i]);
                let idxGroupvalidate = this.checkWithCardOnTable(this.listIdCardDeck[i]);
                if(idxGroupvalidate !== -1 || idxHolderValidate !== -1){
                    let desPosX = this.cardDeckAnchor.getPosition().x + 50*(this.listCardDeck.length > 2 ? 2 : this.listCardDeck.length);
                    this.srpCardOnDeck.active = true;
                    cc.tween(this.srpCardOnDeck)
                        .to(0.2, {position:cc.v2(desPosX, this.cardDeckAnchor.getPosition().y)})
                        .delay(0.2)
                        .to(0.2, {position: this.btnGetCard.node.getPosition()})
                        .call(()=>{this.srpCardOnDeck.active = false})
                        .start();
                    return;
                }
            }
        }
        this.gameScene.popup.showPopup(true, "No card can be moved!")
    },
    moveCardHint(arrMoved, firstDesPos){
        for(let i = 0; i < arrMoved.length; i++){
            let card = arrMoved[i];
            let desPos = cc.v2(firstDesPos.x, firstDesPos.y - 50*i);
            cc.tween(card.node)
                .call(()=>{
                    card.showBorder(true)
                    card.node.zIndex = 100 + i;
                })
                .to(0.2, {position:desPos})
                .delay(0.2)
                .to(0.2, {position:card.oriPos})
                .call(()=>{
                    card.showBorder(false);
                    card.node.zIndex = card.oriIndex;
                })
                .start();
        }
    },
//    AutoComplete
    autoComplete(){
        //Check last card on table
        for(let i = 0; i < this.listCardOnTable.length; i++){
            let listCardOfGroup = this.listCardOnTable[i];
            if(listCardOfGroup.length > 0){
                //Check lastCard of Group match with Holder
                let card = listCardOfGroup[listCardOfGroup.length - 1];
                let idxHolder = this.checkWithListHolder(card.idCard);
                if(idxHolder != -1){
                    card.node.zIndex = 100;
                    this.moveCardToHolder(card, idxHolder);
                    return;
                    // this.autoComplete();
                }
            }
        }
        //Check last card of list Deck
        if(this.listCardDeck.length > 0) {
            let card = this.listCardDeck[this.listCardDeck.length - 1];
            //Check card Deck with Holder
            let idxHolder = this.checkWithListHolder(card.idCard);
            if (idxHolder !== -1) {
                card.node.zIndex = 100;
                this.moveCardToHolder(card, idxHolder);
                // this.autoComplete();
                return;
            }
            let idxGroupValidate = this.checkWithCardOnTable(card.idCard);
            if(idxGroupValidate !== -1){
                card.node.zIndex = 100;
                this.moveCardOnTables([card], idxGroupValidate);
                return;
            }
        }
        if(this.listIdCardDeck.length > 0){
            this.btnGetCardClick();
            return;
        }
        this.checkFinishGame();
    },
    checkFinishGame(){
        for(let i = 0; i < this.listCardHolder.length; i++) {
            if (this.listCardHolder[i].length < 13){
                return;
            }
        }
        this.bGameStart = false;
        this.processEndGame();
    },
    processEndGame(){
        let randomPosAnim = arrPosAnim[Math.floor(Math.random()*3)];
        for(let i = 0; i < this.listCardHolder.length; i++){
            for(let j = 0; j < this.listCardHolder[i].length; j++){
                let card = this.listCardHolder[i][j];
                let firstPos = randomPosAnim[(j+i)%randomPosAnim.length];
                let secondPos = randomPosAnim[(j+i+1)%randomPosAnim.length];
                let thirdPos = randomPosAnim[(j+i+2)%randomPosAnim.length];
                let fourthPos = randomPosAnim[(j+i+3)%randomPosAnim.length];
                let fifthPos = randomPosAnim[(j+i+4)%randomPosAnim.length];

                cc.tween(card.node)
                    .delay(2.0 + 0.1*j + 0.2*i)
                    .to(0.2, {position:firstPos})
                    .delay(0.2*(14 - j))
                    .to(0.4, {position:secondPos})
                    .to(0.4, {position:thirdPos})
                    .to(0.4, {position:fourthPos})
                    .to(0.4, {position:fifthPos})
                    .delay(0.2*j + 0.1*i)
                    .to(0.2 + (0.01*j), {position:card.oriPos})
                    .call(()=>{
                        if(i === this.listCardHolder.length - 1 && j === this.listCardHolder[i].length - 1 && !this.gameScene.popup.node.active){
                            // cc.log("You are Victory")
                            this.gameScene.popup.showPopup(true, "You are Victory!")
                        }
                            
                    })
                    .start();

            }
        }
    }
});
