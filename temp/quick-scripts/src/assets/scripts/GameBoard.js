"use strict";
cc._RF.push(module, '0f28aaIbo5MN6bLMv5aY3rC', 'GameBoard');
// scripts/GameBoard.js

"use strict";

var Decks = require("Decks");

var CardPrefab = require("CardPrefab");

var FBSDK = require("FbSdk");

var arrPosAnim = [[cc.v2(-200, 0), cc.v2(200, 0), cc.v2(-200, -250), cc.v2(200, -250), cc.v2(-200, -500), cc.v2(200, -500)], [cc.v2(0, 0), cc.v2(150, -550), cc.v2(-300, -250), cc.v2(300, -250), cc.v2(-150, -550)], [cc.v2(-350, -100), cc.v2(-200, -450), cc.v2(0, -100), cc.v2(200, -450), cc.v2(350, -100)]];
var typeAction = {
  GET_CARD_ON_DECK: 0,
  MOVE_CARD_DECK_TO_HOLDER: 1,
  MOVE_CARD_DECK_TO_TABLEGROUP: 2,
  MOVE_CARD_HOLDER_TO_TABLEGROUP: 3,
  MOVE_CARD_TABLE_TO_HOLDER: 4,
  MOVE_CARD_TABLE_TO_TABLE: 5
};
cc.Class({
  "extends": cc.Component,
  properties: {
    btnAutoComplete: cc.Node,
    btnHint: cc.Node,
    btnUndo: cc.Button,
    prefabCard: cc.Prefab,
    srpCardOnDeck: cc.Node,
    cardNodePool: {
      "default": null,
      hidden: false,
      type: cc.NodePool
    },
    cardHolderAnchor: [cc.Node],
    cardOnTableAnchor: [cc.Node],
    cardDeckAnchor: cc.Node,
    nodeCardContainer: cc.Node,
    btnGetCard: cc.Button,
    arrResourcesCard: [cc.SpriteFrame],
    listCardHolder: [],
    listIdCardHolder: [],
    listCardDeck: [],
    listIdCardDeck: [],
    listCardOnTable: [],
    listIdCardOnTable: [],
    arrCardMove: []
  },
  // LIFE-CYCLE CALLBACKS:
  ctor: function ctor() {
    this.gameScene = null;
    this.decks = new Decks();
    this.listCardHolder = [[], [], [], []];
    this.listIdCardHolder = [[], [], [], []];
    this.listCardDeck = [];
    this.listIdCardDeck = [];
    this.listCardOnTable = [];
    this.listIdCardOnTable = [];
    this.bAutoComplete = false;
    this.iScore = 0;
    this.iMove = 0;
    this.historyAction = [];
  },
  start: function start() {
    this.init();
  },
  init: function init() {
    this.srpCardOnDeck.zIndex = 100;
    this.cardNodePool = new cc.NodePool();

    for (var i = 0; i < 52; i++) {
      var card = cc.instantiate(this.prefabCard);
      this.cardNodePool.put(card);
    }

    this.resetGame();
    this.fbsdk = new FBSDK();
    this.fbsdk.init();
  },
  resetGame: function resetGame() {
    this.nodeCardContainer.removeAllChildren(true);
    this.listCardHolder = [[], [], [], []];
    this.listIdCardHolder = [[], [], [], []];
    this.listCardDeck = [];
    this.listIdCardDeck = [];
    this.listIdCardOnTable = [[], [], [], [], [], [], []];
    this.listCardOnTable = [[], [], [], [], [], [], []];
    this.iHiddenCard = 21;
    this.idxCurrentDeck = 0;
    this.arrGroupCardOnTable = []; // this.btnNewGame.node.active = true;

    this.btnGetCard.node.active = true;
    this.btnGetCard.interactable = false;
    this.setStateGetCard(false);
    this.bGameStart = false;
    this.bAutoComplete = false;
    this.btnAutoComplete.active = false;
    this.srpCardOnDeck.active = false;
    this.iScore = 1000;
    this.iMove = 0; // this.btnUndo.interactable = false;

    this.historyAction = [];
  },
  btnGetCardClick: function btnGetCardClick() {
    if (this.bGameStart) {
      this.gameScene.playEffect(SoundEffect.CLICK_SOUND);
      this.processGetCard();
    }
  },
  processGetCard: function processGetCard(sourcePos) {
    var _this = this;

    // cc.log("btnGetCardClick :" + this.listIdCardDeck);
    // cc.log("this.listCardDeck: " + this.listCardDeck.length);
    if (this.listIdCardDeck.length < 1) return;
    this.gameScene.playEffect(SoundEffect.FLIP_SOUND);
    this.setStateGetCard(this.idxCurrentDeck < this.listIdCardDeck.length - 1);

    if (this.idxCurrentDeck === this.listIdCardDeck.length) {
      this.idxCurrentDeck = 0;
      this.setStateGetCard(true);

      for (var i = 0; i < this.listCardDeck.length; i++) {
        this.listCardDeck[i].node.removeFromParent(true);
      }

      this.listCardDeck = [];
    } // }else{


    var objectHistory = {};
    objectHistory.typeAction = typeAction.GET_CARD_ON_DECK;
    this.historyAction.push(objectHistory);
    this.iMove++;
    this.iScore -= 5;
    var card = null;
    var cardId = this.listIdCardDeck[this.idxCurrentDeck];

    if (this.listCardDeck.length === 3) {
      for (var _i = 1; _i < 3; _i++) {
        var cardId2 = this.listIdCardDeck[this.idxCurrentDeck - _i];

        this.listCardDeck[2 - _i].setSpriteCard(cardId2, this.arrResourcesCard[cardId2]);

        this.listCardDeck[2 - _i].showCard(false);
      }

      card = this.listCardDeck[2];
      card.resetCard();
      card.setSpriteCard(cardId, this.arrResourcesCard[cardId]);
    } else {
      card = this.getCard(cardId);
      this.nodeCardContainer.addChild(card.node);

      if (this.listCardDeck.length > 0) {
        this.listCardDeck[this.listCardDeck.length - 1].bTouch = false;
      }

      this.listCardDeck.push(card);
    }

    card.node.position = this.btnGetCard.node.getPosition();
    if (!Utils.isEmpty(sourcePos)) card.node.position = sourcePos;
    card.node.zIndex = this.listCardDeck.length;
    var posX = this.cardDeckAnchor.getPosition().x + 50 * (this.listCardDeck.length - 1);
    var pos = cc.v2(posX, this.cardDeckAnchor.getPosition().y);
    card.oriPos = pos; // cc.log("cardDeck_" + this.listCardDeck.length + " has oriPos: " + pos.x + "-" + pos.y);

    card.oriIndex = this.listCardDeck.length;
    var iTime = this.bAutoComplete ? 0.05 : 0.1;
    cc.tween(card.node).to(iTime, {
      position: pos
    }).call(function () {
      card.showCard(true);
      if (_this.bAutoComplete) _this.autoComplete();
    }).start();
    this.idxCurrentDeck++;
    this.btnGetCard.node.active = !(this.listIdCardDeck.length < 2); // cc.log("btnGetCardClick :" + this.listIdCardDeck);
    // cc.log("this.listCardDeck: " + this.listCardDeck.length);
    // }
  },
  btnHintClick: function btnHintClick() {
    this.gameScene.playEffect(SoundEffect.CLICK_SOUND); // let self = this;
    // this.fbsdk.showRewardVideo(()=>{
    //     if(self.bGameStart)
    //         self.checkHint();
    // });

    if (this.bGameStart) this.checkHint();
  },
  btnAutoCompleteClick: function btnAutoCompleteClick() {
    if (this.bGameStart) {
      this.gameScene.playEffect(SoundEffect.CLICK_SOUND);
      this.bAutoComplete = true;
      this.btnAutoComplete.active = false;
      this.autoComplete();
    }
  },
  btnUndoClick: function btnUndoClick() {
    cc.log("========== Undo Click:" + this.historyAction.length);
    this.gameScene.playEffect(SoundEffect.CLICK_SOUND);

    if (this.historyAction.length > 0 && this.bGameStart) {
      this.iMove++;
      var lastAction = this.historyAction[this.historyAction.length - 1];

      if (lastAction.hasOwnProperty("cardIdActiveAfterMove") && !Utils.isEmpty(lastAction.cardIdActiveAfterMove)) {
        var arrGroupCard = this.listCardOnTable[lastAction.idxSource];
        var listIdCardOfGroup = this.listIdCardOnTable[lastAction.idxSource];
        var card = arrGroupCard[arrGroupCard.length - 1];
        card.resetCard();
        card.idCard = lastAction.cardIdActiveAfterMove;
        this.iHiddenCard++;
        listIdCardOfGroup.splice(listIdCardOfGroup.length - 1, 1);
      }

      switch (lastAction.typeAction) {
        case typeAction.GET_CARD_ON_DECK:
          this.idxCurrentDeck--;

          if (this.listCardDeck.length > 0) {
            this.listCardDeck[this.listCardDeck.length - 1].node.removeFromParent(true);
            this.listCardDeck.splice(this.listCardDeck.length - 1, 1);
          }

          if (this.listCardDeck.length > 2) {
            for (var i = this.listCardDeck.length, j = 0; i > 0; i--, j++) {
              var cardId = this.listIdCardDeck[this.idxCurrentDeck - 1 - j];
              this.listCardDeck[i - 1].setSpriteCard(cardId, this.arrResourcesCard[cardId]);
              this.listCardDeck[i - 1].showCard(false);
            }
          } else if (this.listCardDeck.length > 0) {// this.listCardDeck[this.listCardDeck.length - 1].node.removeFromParent(true);
            // this.listCardDeck.splice(this.listCardDeck.length - 1, 1);
          }

          if (this.listCardDeck.length > 0) {
            this.listCardDeck[this.listCardDeck.length - 1].bTouch = true;
          }

          this.setStateGetCard(true);
          this.btnGetCard.node.active = true;
          break;

        case typeAction.MOVE_CARD_DECK_TO_HOLDER:
          var _card = lastAction.arrCardMove[0];
          this.listIdCardDeck.splice(this.idxCurrentDeck, 0, _card.idCard);
          this.processGetCard(_card.oriPos);
          this.historyAction.splice(this.historyAction.length - 1, 1); // cc.log(this.idxCurrentDeck + " - this.listIdCardDeck undo: " + JSON.stringify(this.listIdCardDeck));

          this.listCardHolder[_card.groupId - 100][this.listCardHolder[_card.groupId - 100].length - 1].node.removeFromParent(true);
          this.listCardHolder[_card.groupId - 100].splice(this.listCardHolder[_card.groupId - 100].length - 1, 1);
          this.listIdCardHolder[_card.groupId - 100].splice(this.listCardHolder[_card.groupId - 100].length - 1, 1);
          break;

        case typeAction.MOVE_CARD_DECK_TO_TABLEGROUP:
          var card2 = lastAction.arrCardMove[0];
          this.listIdCardDeck.splice(this.idxCurrentDeck, 0, card2.idCard); // cc.log(this.idxCurrentDeck + " - this.listIdCardDeck undo: " + JSON.stringify(this.listIdCardDeck));

          this.processGetCard(card2.oriPos);
          this.historyAction.splice(this.historyAction.length - 1, 1);
          this.listCardOnTable[card2.groupId][this.listCardOnTable[card2.groupId].length - 1].node.removeFromParent(true);
          this.listCardOnTable[card2.groupId].splice(this.listCardOnTable[card2.groupId].length - 1, 1);
          this.listIdCardOnTable[card2.groupId].splice(this.listIdCardOnTable[card2.groupId].length - 1, 1);
          break;

        case typeAction.MOVE_CARD_HOLDER_TO_TABLEGROUP:
          this.moveCardToHolder(lastAction.arrCardMove, lastAction.idxSource);
          this.historyAction.splice(this.historyAction.length - 1, 1);
          break;

        case typeAction.MOVE_CARD_TABLE_TO_HOLDER:
          this.moveCardOnTables(lastAction.arrCardMove, lastAction.idxSource);
          this.historyAction.splice(this.historyAction.length - 1, 1);
          break;

        case typeAction.MOVE_CARD_TABLE_TO_TABLE:
          this.moveCardOnTables(lastAction.arrCardMove, lastAction.idxSource);
          this.historyAction.splice(this.historyAction.length - 1, 1);
          break;
      }

      this.historyAction.splice(this.historyAction.length - 1, 1); // cc.log("this.historyAction after:" + this.historyAction.length);
    }
  },
  showCardDeck: function showCardDeck() {
    this.idxCurrentDeck--;

    if (this.idxCurrentDeck > 2 && this.listCardDeck.length > 1) {
      for (var i = 0; i < this.listCardDeck.length; i++) {
        var cardId = this.listIdCardDeck[this.idxCurrentDeck - 3 + i];
        this.listCardDeck[i].setSpriteCard(cardId, this.arrResourcesCard[cardId]);
        this.listCardDeck[i].showCard(false);
        this.listCardDeck[i].bTouch = false;
      }

      var card = this.getCard(this.listIdCardDeck[this.idxCurrentDeck - 1]);
      this.nodeCardContainer.addChild(card.node);
      this.listCardDeck.push(card);
      var posX = this.cardDeckAnchor.getPosition().x + 50 * (this.listCardDeck.length - 1);
      card.oriPos = cc.v2(posX, this.cardDeckAnchor.getPosition().y);
      card.node.position = cc.v2(posX, this.cardDeckAnchor.getPosition().y);
      card.oriIndex = this.listCardDeck.length;
      card.node.zIndex = this.listCardDeck.length;
      card.showCard(false);
      card.bTouch = true;
    } else if (this.listCardDeck.length > 0) {
      this.listCardDeck[this.listCardDeck.length - 1].bTouch = true;
    }
  },
  setStateGetCard: function setStateGetCard(bState) {
    // true: getCard | false: Reload Deck
    this.btnGetCard.node.getComponent(cc.Sprite).spriteFrame = this.arrResourcesCard[bState ? 52 : 53];
  },
  startNewGame: function startNewGame() {
    var _this2 = this;

    this.fbsdk.showInterstitial(); // this.fbsdk.showBannerAds();

    this.resetGame();
    this.bGameStart = true;
    this.btnGetCard.interactable = true;
    this.setStateGetCard(true);
    var newDeck = this.decks.dealCard(); // newDeck = [46,15,47,39,42,2,1,34,41,4,32,16,38,24,14,18,19,28,8,23,37,20,0,33,0,21,4,13,27,8,12,50,7,1,22,17,36,3,10,31,40,48,44,30,43,5,26,9,25,6,51,35];
    // cc.log("newDeck: " + newDeck);

    this.listIdCardDeck = newDeck.slice(0, 24);
    this.idxCurrentDeck = 0;
    var listIdCardOnTable = newDeck.slice(24, newDeck.length);

    var _loop = function _loop(i, _idx) {
      var _loop2 = function _loop2(j) {
        var cardId = listIdCardOnTable[_idx];
        _idx++;

        var card = _this2.getCard(cardId);

        card.node.position = _this2.btnGetCard.node.getPosition();

        _this2.nodeCardContainer.addChild(card.node);

        card.node.zIndex = j;

        var _posX = _this2.cardOnTableAnchor[i].getPosition().x;

        var _posY = _this2.cardOnTableAnchor[i].getPosition().y - 25 * j;

        var _pos = cc.v2(_posX, _posY);

        card.oriPos = _pos;
        card.oriIndex = j;
        card.groupId = i;

        _this2.listCardOnTable[i].push(card);

        cc.tween(card.node).delay(0.3).to(0.3, {
          position: _pos
        }).delay(0.1 * j).call(function () {
          if (i === 0 && j === 0) {
            _this2.gameScene.playEffect(SoundEffect.DEAL_SOUND);
          }

          if (j === i) {
            // if(true) {
            card.showCard(true);

            _this2.listIdCardOnTable[i].push(cardId);
          }
        }).start();
      };

      for (var j = 0; j < i + 1; j++) {
        _loop2(j);
      }

      idx = _idx;
    };

    for (var i = 0, idx = 0; i < 7; i++) {
      _loop(i, idx);
    }
  },
  getCard: function getCard(cardId) {
    var card = this.cardNodePool.get();

    if (Utils.isEmpty(card)) {
      var cardPlus = cc.instantiate(this.prefabCard);
      this.cardNodePool.put(cardPlus);
      card = this.cardNodePool.get();
    }

    card.on(cc.Node.EventType.TOUCH_START, this.onBeginTouch, this, true);
    card.on(cc.Node.EventType.TOUCH_MOVE, this.onMoveCard, this, true);
    card.on(cc.Node.EventType.TOUCH_END, this.onEndTouch, this, true);
    card = card.getComponent(CardPrefab);
    card.setSpriteCard(cardId, this.arrResourcesCard[cardId]);
    return card;
  },
  onBeginTouch: function onBeginTouch(event) {
    this.gameScene.playEffect(SoundEffect.CLICK_SOUND); // this.arrCardMove = [];

    var cardSelect = event.target.getComponent(CardPrefab);

    if (!cardSelect.bTouch || this.arrCardMove.length > 0) {
      cc.log("double click....");

      for (var i = 0; i < this.arrCardMove.length; i++) {
        var card = this.arrCardMove[i];
        card.node.position = card.oriPos;
        card.node.zIndex = card.oriIndex;
      }

      this.arrCardMove = [];
      return;
    }

    if (cardSelect.groupId > -1 && cardSelect.groupId < 100) {
      //Card On Table
      var idxCardSelect = this.listCardOnTable[cardSelect.groupId].indexOf(cardSelect);

      for (var _i2 = idxCardSelect; _i2 < this.listCardOnTable[cardSelect.groupId].length; _i2++) {
        var _card2 = this.listCardOnTable[cardSelect.groupId][_i2];
        this.arrCardMove.push(_card2);
        _card2.node.zIndex = 100 + _i2;
      }
    } else {
      // Card On Deck or Holder
      this.arrCardMove.push(cardSelect);
      cc.log("cardSelect: " + 100);
      cardSelect.node.zIndex = 100;
    } // cc.log("event.target: " + cardSelect.idCard);

  },
  onMoveCard: function onMoveCard(event) {
    this.bMoveCard = true;
    var cardComp = event.target.getComponent(CardPrefab);
    if (!cardComp.bTouch) return;
    var delta = event.touch.getDelta(); // cc.log("onMoveCard: " + event);

    if (this.arrCardMove.length > 0) {
      for (var i = 0; i < this.arrCardMove.length; i++) {
        var card = this.arrCardMove[i];
        card.node.x += delta.x;
        card.node.y += delta.y;
      }
    }
  },
  onEndTouch: function onEndTouch(event) {
    var cardComp = event.target.getComponent(CardPrefab);
    if (!cardComp.bTouch) return; // cc.log("cardComp: " + cardComp.idCard);
    // cc.log("this.arrCardMove: " + this.arrCardMove.length);

    if (this.arrCardMove.length > 0) {
      if (this.bMoveCard && this.checkPosCardMoved(this.arrCardMove)) {} else if (!this.bMoveCard && this.autoMoveCard()) {} else {
        for (var i = 0; i < this.arrCardMove.length; i++) {
          var card = this.arrCardMove[i]; // cc.log("card.oriIndex: " + card.oriIndex)

          card.node.position = card.oriPos;
          card.node.zIndex = card.oriIndex;
          card.shakeCard();
        }
      }
    }

    this.arrCardMove = [];
    this.bMoveCard = false;
  },
  checkPosCardMoved: function checkPosCardMoved(arrCardMoved) {
    if (this.arrCardMove.length < 0) return false;
    var posEndTouch = arrCardMoved[0].node.getPosition();
    var rectCardMove = arrCardMoved[0].node.getBoundingBox(); //bounding Box Holder

    for (var i = 0; i < this.cardHolderAnchor.length; i++) {
      var rectHolder = this.cardHolderAnchor[i].getBoundingBox();

      if (rectHolder.contains(posEndTouch)) {
        var idxHolder = i;

        if (this.checkWithListHolder(arrCardMoved[0].idCard, idxHolder) !== -1 && arrCardMoved.length === 1 && arrCardMoved[0].groupId < 100) {
          this.moveCardToHolder(arrCardMoved[0], idxHolder);
          return true;
        }

        return false;
      }
    } //BoudingBox Group On Table


    for (var j = 0; j < this.listCardOnTable.length; j++) {
      var rectOfLastCard = null;
      if (this.listCardOnTable[j].length === 0) rectOfLastCard = this.cardOnTableAnchor[j].getBoundingBox();else rectOfLastCard = this.listCardOnTable[j][this.listCardOnTable[j].length - 1].node.getBoundingBox();

      if (rectOfLastCard.contains(posEndTouch) && j !== arrCardMoved[0].groupId) {
        var idxGroup = j;

        if (this.checkWithCardOnTable(arrCardMoved[0].idCard, idxGroup) !== -1) {
          this.moveCardOnTables(arrCardMoved, idxGroup);
          return true;
        }

        return false;
      }
    }

    return false;
  },
  autoMoveCard: function autoMoveCard() {
    if (this.arrCardMove.length < 0) return false;
    var holderValidate = this.checkWithListHolder(this.arrCardMove[0].idCard); // cc.log("holderValidate: " + holderValidate);

    var groupValidate = this.checkWithCardOnTable(this.arrCardMove[0].idCard); // cc.log("groupValidate: " + groupValidate);

    if (holderValidate !== -1 && this.arrCardMove.length === 1 && this.arrCardMove[0].groupId < 100) {
      this.moveCardToHolder(this.arrCardMove[0], holderValidate);
      return true;
    } else if (groupValidate !== -1 && groupValidate !== this.arrCardMove[0].groupId) {
      this.moveCardOnTables(this.arrCardMove, groupValidate);
      return true;
    }

    return false;
  },
  moveCardToHolder: function moveCardToHolder(cardMove, idxHolder) {
    var _this3 = this;

    this.gameScene.playEffect(SoundEffect.FLIP_SOUND);
    if (cardMove.groupId > 99) return; //Card FromHolder

    this.iScore += 100;
    this.iMove++;
    var objectHistory = {};
    var bCardFromDeck = cardMove.groupId === -1;

    if (bCardFromDeck) {
      objectHistory.typeAction = typeAction.MOVE_CARD_DECK_TO_HOLDER; // cc.log("this.listCardDeck:" + this.listCardDeck.length);

      objectHistory.idxSource = -1;
    } else {
      objectHistory.typeAction = typeAction.MOVE_CARD_TABLE_TO_HOLDER; // cc.log("this.listCardOnTable[" + cardMove.groupId + "] : " + this.listCardOnTable[cardMove.groupId].length);

      objectHistory.idxSource = cardMove.groupId;
    }

    objectHistory.idxDes = idxHolder;
    objectHistory.arrCardMove = [cardMove];
    var pos = this.cardHolderAnchor[idxHolder].getPosition();
    var iTime = this.bAutoComplete ? 0.1 : 0.2;
    cc.tween(cardMove.node).delay(0.1).to(iTime, {
      position: pos
    }).call(function () {
      cardMove.showEffectCard();
      cardMove.node.zIndex = cardMove.oriIndex;
      if (_this3.bAutoComplete) _this3.autoComplete();

      _this3.checkFinishGame();
    }).start(); // cardMove.node.position = this.cardHolderAnchor[idxHolder].getPosition();

    this.listCardHolder[idxHolder].push(cardMove);
    this.listIdCardHolder[idxHolder].push(cardMove.idCard);
    cardMove.oriIndex = this.listCardHolder[idxHolder].length;
    cardMove.oriPos = this.cardHolderAnchor[idxHolder].getPosition();
    var activeCardInGroup = null;

    if (!bCardFromDeck) {
      this.listCardOnTable[cardMove.groupId] = Utils.removeItemInArray(cardMove, this.listCardOnTable[cardMove.groupId]);
      this.listIdCardOnTable[cardMove.groupId] = Utils.removeItemInArray(cardMove.idCard, this.listIdCardOnTable[cardMove.groupId]);
      activeCardInGroup = this.checkCardActiveInGroup(cardMove.groupId);
    } else {
      this.listCardDeck = Utils.removeItemInArray(cardMove, this.listCardDeck);
      this.listIdCardDeck = Utils.removeItemInArray(cardMove.idCard, this.listIdCardDeck);
      var self = this;
      setTimeout(function () {
        self.showCardDeck();
      }, 200);
    }

    objectHistory.cardIdActiveAfterMove = activeCardInGroup;
    this.historyAction.push(objectHistory);
    cardMove.groupId = 100 + idxHolder;
  },
  moveCardOnTables: function moveCardOnTables(arrSource, idxGroup) {
    var _this4 = this;

    this.gameScene.playEffect(SoundEffect.FLIP_SOUND);
    this.iScore -= 5;
    this.iMove++;
    var objectHistory = {};
    var bCardFromDeck = arrSource[0].groupId === -1;
    var bCardFromHolder = arrSource[0].groupId > 99;

    if (bCardFromDeck) {
      // cc.log("this.listCardDeck:" + this.listCardDeck.length);
      objectHistory.typeAction = typeAction.MOVE_CARD_DECK_TO_TABLEGROUP;
    } else if (bCardFromHolder) {
      objectHistory.typeAction = typeAction.MOVE_CARD_HOLDER_TO_TABLEGROUP;
    } else {
      objectHistory.typeAction = typeAction.MOVE_CARD_TABLE_TO_TABLE;
    }

    objectHistory.idxSource = arrSource[0].groupId;
    objectHistory.idxDes = idxGroup;
    objectHistory.arrCardMove = arrSource; // cc.log("moveCardOnTables to: " + idxGroup)

    var desPos = null;
    if (this.listCardOnTable[idxGroup].length === 0) desPos = this.cardOnTableAnchor[idxGroup].getPosition();else {
      var lastCardPos = this.listCardOnTable[idxGroup][this.listCardOnTable[idxGroup].length - 1].node.getPosition();
      desPos = cc.v2(lastCardPos.x, lastCardPos.y - 50);
    }
    var activeCardInGroup = null;

    var _loop3 = function _loop3(i) {
      var card = arrSource[i]; // cc.log("cardId: " + card.idCard);

      var pos = cc.v2(desPos.x, desPos.y - 50 * i);
      cc.tween(card.node).to(0.2, {
        position: pos
      }).call(function () {
        card.node.zIndex = card.oriIndex;
        if (_this4.bAutoComplete && i === arrSource.length - 1) _this4.autoComplete();
      }).start();

      if (bCardFromDeck) {
        _this4.listCardDeck = Utils.removeItemInArray(card, _this4.listCardDeck);
        _this4.listIdCardDeck = Utils.removeItemInArray(card.idCard, _this4.listIdCardDeck);
        var self = _this4;
        setTimeout(function () {
          self.showCardDeck();
        }, 200);
      } else if (bCardFromHolder) {
        _this4.listCardHolder[card.groupId - 100] = Utils.removeItemInArray(card, _this4.listCardHolder[card.groupId - 100]);
        _this4.listIdCardHolder[card.groupId - 100] = Utils.removeItemInArray(card.idCard, _this4.listIdCardHolder[card.groupId - 100]);
      } else {
        _this4.listCardOnTable[card.groupId] = Utils.removeItemInArray(card, _this4.listCardOnTable[card.groupId]);
        _this4.listIdCardOnTable[card.groupId] = Utils.removeItemInArray(card.idCard, _this4.listIdCardOnTable[card.groupId]);

        if (i === arrSource.length - 1) {
          activeCardInGroup = _this4.checkCardActiveInGroup(card.groupId);
        }
      } // card.node.zIndex = this.listCardOnTable[idxGroup].length + i;


      card.oriIndex = _this4.listCardOnTable[idxGroup].length;

      _this4.listCardOnTable[idxGroup].push(card);

      _this4.listIdCardOnTable[idxGroup].push(card.idCard);

      card.oriPos = pos;
      card.groupId = idxGroup;
    };

    for (var i = 0; i < arrSource.length; i++) {
      _loop3(i);
    }

    objectHistory.cardIdActiveAfterMove = activeCardInGroup;
    this.historyAction.push(objectHistory);
  },
  checkWithListHolder: function checkWithListHolder(cardId, idxHolder) {
    // cc.log("checkWithListHolder: " + cardId + " - " + idxHolder)
    if (!Utils.isEmpty(idxHolder)) {
      if (CardUtils.validateCardHolder(cardId, this.listIdCardHolder[idxHolder])) {
        return idxHolder;
      }
    } else {
      for (var i = 0; i < this.listIdCardHolder.length; i++) {
        if (CardUtils.validateCardHolder(cardId, this.listIdCardHolder[i])) return i;
      }
    }

    return -1;
  },
  checkWithCardOnTable: function checkWithCardOnTable(cardId, idxGroup) {
    if (!Utils.isEmpty(idxGroup)) {
      if (CardUtils.validateCardOnBoard(cardId, this.listIdCardOnTable[idxGroup])) return idxGroup;
    } else {
      for (var i = 0; i < this.listIdCardOnTable.length; i++) {
        if (CardUtils.validateCardOnBoard(cardId, this.listIdCardOnTable[i])) return i;
      }
    }

    return -1;
  },
  checkCardActiveInGroup: function checkCardActiveInGroup(idxGroup) {
    // cc.log("checkCardActiveInGroup: " + idxGroup);
    if (idxGroup > -1 && idxGroup < 8) {
      var arrCard = this.listCardOnTable[idxGroup];
      var card = arrCard[arrCard.length - 1];

      if (arrCard.length > 0 && !card.bActive) {
        card.showCard(true);
        this.listIdCardOnTable[idxGroup].push(card.idCard);
        this.iHiddenCard--;

        if (this.iHiddenCard === 0) {
          // cc.log("You are Victory");
          this.btnAutoComplete.active = true;
        }

        return card.idCard;
      }
    }

    return null;
  },
  //    Check Hint
  checkHint: function checkHint() {
    var _this5 = this;

    this.iScore -= 10; // Check card On Table

    for (var i = 0; i < this.listCardOnTable.length; i++) {
      var listCardOfGroup = this.listCardOnTable[i];

      if (listCardOfGroup.length > 0) {
        //Check lastCard of Group match with Holder
        var idxHolder = this.checkWithListHolder(listCardOfGroup[listCardOfGroup.length - 1].idCard);

        if (idxHolder !== -1) {
          var card = listCardOfGroup[listCardOfGroup.length - 1];
          var pos = this.cardHolderAnchor[idxHolder].getPosition();
          this.moveCardHint([card], pos);
          return;
        } // Check card On Table Match each other
        //If FirstCardOfGroup is King -> return


        if (listCardOfGroup[0].bActive && listCardOfGroup[0].idCard > 47 && listCardOfGroup[0].idCard < 52) continue;
        var arrCard = [];

        for (var j = 0; j < listCardOfGroup.length; j++) {
          if (listCardOfGroup[j].bActive) arrCard.push(listCardOfGroup[j]);
        }

        var idxGroupValidate = this.checkWithCardOnTable(arrCard[0].idCard);

        if (idxGroupValidate !== -1) {
          var desPos = null;
          if (this.listCardOnTable[idxGroupValidate].length === 0) desPos = this.cardOnTableAnchor[idxGroupValidate].getPosition();else {
            var lastCardPos = this.listCardOnTable[idxGroupValidate][this.listCardOnTable[idxGroupValidate].length - 1].node.getPosition();
            desPos = cc.v2(lastCardPos.x, lastCardPos.y - 50);
          }
          this.moveCardHint(arrCard, desPos);
          return;
        }
      }
    } // Check card On Deck


    if (this.listCardDeck.length > 0) {
      var _card3 = this.listCardDeck[this.listCardDeck.length - 1]; //Check card Deck with Holder

      var _idxHolder = this.checkWithListHolder(this.listCardDeck[this.listCardDeck.length - 1].idCard);

      if (_idxHolder !== -1) {
        var posHolder = this.cardHolderAnchor[_idxHolder].getPosition();

        this.moveCardHint([_card3], posHolder);
        return;
      } //Check card Deck with All group


      var _idxGroupValidate = this.checkWithCardOnTable(this.listCardDeck[this.listCardDeck.length - 1].idCard);

      if (_idxGroupValidate !== -1) {
        var _desPos = null;
        if (this.listCardOnTable[_idxGroupValidate].length === 0) _desPos = this.cardOnTableAnchor[_idxGroupValidate].getPosition();else {
          var _lastCardPos = this.listCardOnTable[_idxGroupValidate][this.listCardOnTable[_idxGroupValidate].length - 1].node.getPosition();

          _desPos = cc.v2(_lastCardPos.x, _lastCardPos.y - 50);
        }
        this.moveCardHint([_card3], _desPos);
        return;
      }
    } //Check card On List Card Deck


    if (this.listIdCardDeck.length > 0) {
      for (var _i3 = 0; _i3 < this.listIdCardDeck.length; _i3++) {
        var idxHolderValidate = this.checkWithListHolder(this.listIdCardDeck[_i3]);
        var idxGroupvalidate = this.checkWithCardOnTable(this.listIdCardDeck[_i3]);

        if (idxGroupvalidate !== -1 || idxHolderValidate !== -1) {
          var desPosX = this.cardDeckAnchor.getPosition().x + 50 * (this.listCardDeck.length > 2 ? 2 : this.listCardDeck.length);
          this.srpCardOnDeck.active = true;
          cc.tween(this.srpCardOnDeck).to(0.2, {
            position: cc.v2(desPosX, this.cardDeckAnchor.getPosition().y)
          }).delay(0.2).to(0.2, {
            position: this.btnGetCard.node.getPosition()
          }).call(function () {
            _this5.srpCardOnDeck.active = false;
          }).start();
          return;
        }
      }
    }

    this.gameScene.popup.showPopup(true, "No card can be moved!");
  },
  moveCardHint: function moveCardHint(arrMoved, firstDesPos) {
    var _loop4 = function _loop4(i) {
      var card = arrMoved[i];
      var desPos = cc.v2(firstDesPos.x, firstDesPos.y - 50 * i);
      cc.tween(card.node).call(function () {
        card.showBorder(true);
        card.node.zIndex = 100 + i;
      }).to(0.2, {
        position: desPos
      }).delay(0.2).to(0.2, {
        position: card.oriPos
      }).call(function () {
        card.showBorder(false);
        card.node.zIndex = card.oriIndex;
      }).start();
    };

    for (var i = 0; i < arrMoved.length; i++) {
      _loop4(i);
    }
  },
  //    AutoComplete
  autoComplete: function autoComplete() {
    //Check last card on table
    for (var i = 0; i < this.listCardOnTable.length; i++) {
      var listCardOfGroup = this.listCardOnTable[i];

      if (listCardOfGroup.length > 0) {
        //Check lastCard of Group match with Holder
        var card = listCardOfGroup[listCardOfGroup.length - 1];
        var idxHolder = this.checkWithListHolder(card.idCard);

        if (idxHolder != -1) {
          card.node.zIndex = 100;
          this.moveCardToHolder(card, idxHolder);
          return; // this.autoComplete();
        }
      }
    } //Check last card of list Deck


    if (this.listCardDeck.length > 0) {
      var _card4 = this.listCardDeck[this.listCardDeck.length - 1]; //Check card Deck with Holder

      var _idxHolder2 = this.checkWithListHolder(_card4.idCard);

      if (_idxHolder2 !== -1) {
        _card4.node.zIndex = 100;
        this.moveCardToHolder(_card4, _idxHolder2); // this.autoComplete();

        return;
      }

      var idxGroupValidate = this.checkWithCardOnTable(_card4.idCard);

      if (idxGroupValidate !== -1) {
        _card4.node.zIndex = 100;
        this.moveCardOnTables([_card4], idxGroupValidate);
        return;
      }
    }

    if (this.listIdCardDeck.length > 0) {
      this.btnGetCardClick();
      return;
    }

    this.checkFinishGame();
  },
  checkFinishGame: function checkFinishGame() {
    for (var i = 0; i < this.listCardHolder.length; i++) {
      if (this.listCardHolder[i].length < 13) {
        return;
      }
    }

    this.bGameStart = false;
    this.processEndGame();
  },
  processEndGame: function processEndGame() {
    var _this6 = this;

    var randomPosAnim = arrPosAnim[Math.floor(Math.random() * 3)];

    var _loop5 = function _loop5(i) {
      var _loop6 = function _loop6(j) {
        var card = _this6.listCardHolder[i][j];
        var firstPos = randomPosAnim[(j + i) % randomPosAnim.length];
        var secondPos = randomPosAnim[(j + i + 1) % randomPosAnim.length];
        var thirdPos = randomPosAnim[(j + i + 2) % randomPosAnim.length];
        var fourthPos = randomPosAnim[(j + i + 3) % randomPosAnim.length];
        var fifthPos = randomPosAnim[(j + i + 4) % randomPosAnim.length];
        cc.tween(card.node).delay(2.0 + 0.1 * j + 0.2 * i).to(0.2, {
          position: firstPos
        }).delay(0.2 * (14 - j)).to(0.4, {
          position: secondPos
        }).to(0.4, {
          position: thirdPos
        }).to(0.4, {
          position: fourthPos
        }).to(0.4, {
          position: fifthPos
        }).delay(0.2 * j + 0.1 * i).to(0.2 + 0.01 * j, {
          position: card.oriPos
        }).call(function () {
          if (i === _this6.listCardHolder.length - 1 && j === _this6.listCardHolder[i].length - 1 && !_this6.gameScene.popup.node.active) {
            // cc.log("You are Victory")
            _this6.gameScene.popup.showPopup(true, "You are Victory!");
          }
        }).start();
      };

      for (var j = 0; j < _this6.listCardHolder[i].length; j++) {
        _loop6(j);
      }
    };

    for (var i = 0; i < this.listCardHolder.length; i++) {
      _loop5(i);
    }
  }
});

cc._RF.pop();