
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/CardUtils.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'ef78dkcymJO2onYHMezrQIz', 'CardUtils');
// scripts/CardUtils.js

"use strict";

window.CardUtils = {};

window.CardUtils.getRankCard = function (idxCard) {
  // cc.log(idxCard + " - getRankCard: " + (idxCard - idxCard%4)/4);
  return (idxCard - idxCard % 4) / 4;
};

window.CardUtils.getSuitCard = function (idxCard) {
  // cc.log(idxCard + " - getSuitCard: " + idxCard%4);
  return idxCard % 4;
};

window.CardUtils.checkSameColor = function (idxCard1, idxCard2) {
  return CardUtils.getSuitCard(idxCard1) > 1 && CardUtils.getSuitCard(idxCard2) > 1 || CardUtils.getSuitCard(idxCard1) < 2 && CardUtils.getSuitCard(idxCard2) < 2;
};

window.CardUtils.checkSameSuit = function (idxCard1, idxCard2) {
  return CardUtils.getSuitCard(idxCard1) === CardUtils.getSuitCard(idxCard2);
};

window.CardUtils.validateCardHolder = function (cardId, arrHolder) {
  if (arrHolder.length === 0 && CardUtils.getRankCard(cardId) === 0) return true;

  if (arrHolder.length > 0) {
    var lastCardHolder = arrHolder[arrHolder.length - 1]; //Check same suit and continue rank

    if (CardUtils.checkSameSuit(cardId, lastCardHolder) && CardUtils.getRankCard(cardId) === CardUtils.getRankCard(lastCardHolder) + 1) return true;
  }

  return false;
};

window.CardUtils.validateCardOnBoard = function (idCardSource, arrCard) {
  if (idCardSource > 47 && arrCard.length === 0) return true;
  var lastCardOfGroup = arrCard[arrCard.length - 1]; //Check different color and continue rank

  return !CardUtils.checkSameColor(idCardSource, lastCardOfGroup) && CardUtils.getRankCard(idCardSource) + 1 === CardUtils.getRankCard(lastCardOfGroup);
};

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9zY3JpcHRzL0NhcmRVdGlscy5qcyJdLCJuYW1lcyI6WyJ3aW5kb3ciLCJDYXJkVXRpbHMiLCJnZXRSYW5rQ2FyZCIsImlkeENhcmQiLCJnZXRTdWl0Q2FyZCIsImNoZWNrU2FtZUNvbG9yIiwiaWR4Q2FyZDEiLCJpZHhDYXJkMiIsImNoZWNrU2FtZVN1aXQiLCJ2YWxpZGF0ZUNhcmRIb2xkZXIiLCJjYXJkSWQiLCJhcnJIb2xkZXIiLCJsZW5ndGgiLCJsYXN0Q2FyZEhvbGRlciIsInZhbGlkYXRlQ2FyZE9uQm9hcmQiLCJpZENhcmRTb3VyY2UiLCJhcnJDYXJkIiwibGFzdENhcmRPZkdyb3VwIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBQSxNQUFNLENBQUNDLFNBQVAsR0FBbUIsRUFBbkI7O0FBQ0FELE1BQU0sQ0FBQ0MsU0FBUCxDQUFpQkMsV0FBakIsR0FBK0IsVUFBVUMsT0FBVixFQUFrQjtBQUM3QztBQUNBLFNBQU8sQ0FBQ0EsT0FBTyxHQUFHQSxPQUFPLEdBQUMsQ0FBbkIsSUFBc0IsQ0FBN0I7QUFDSCxDQUhEOztBQUlBSCxNQUFNLENBQUNDLFNBQVAsQ0FBaUJHLFdBQWpCLEdBQStCLFVBQVVELE9BQVYsRUFBa0I7QUFDN0M7QUFDQSxTQUFPQSxPQUFPLEdBQUMsQ0FBZjtBQUNILENBSEQ7O0FBSUFILE1BQU0sQ0FBQ0MsU0FBUCxDQUFpQkksY0FBakIsR0FBa0MsVUFBVUMsUUFBVixFQUFvQkMsUUFBcEIsRUFBOEI7QUFDNUQsU0FBU04sU0FBUyxDQUFDRyxXQUFWLENBQXNCRSxRQUF0QixJQUFrQyxDQUFsQyxJQUF1Q0wsU0FBUyxDQUFDRyxXQUFWLENBQXNCRyxRQUF0QixJQUFrQyxDQUExRSxJQUNITixTQUFTLENBQUNHLFdBQVYsQ0FBc0JFLFFBQXRCLElBQWtDLENBQWxDLElBQXVDTCxTQUFTLENBQUNHLFdBQVYsQ0FBc0JHLFFBQXRCLElBQWtDLENBRDlFO0FBRUgsQ0FIRDs7QUFJQVAsTUFBTSxDQUFDQyxTQUFQLENBQWlCTyxhQUFqQixHQUFpQyxVQUFVRixRQUFWLEVBQW9CQyxRQUFwQixFQUE4QjtBQUMzRCxTQUFRTixTQUFTLENBQUNHLFdBQVYsQ0FBc0JFLFFBQXRCLE1BQW9DTCxTQUFTLENBQUNHLFdBQVYsQ0FBc0JHLFFBQXRCLENBQTVDO0FBQ0gsQ0FGRDs7QUFHQVAsTUFBTSxDQUFDQyxTQUFQLENBQWlCUSxrQkFBakIsR0FBc0MsVUFBU0MsTUFBVCxFQUFpQkMsU0FBakIsRUFBMkI7QUFDN0QsTUFBR0EsU0FBUyxDQUFDQyxNQUFWLEtBQXFCLENBQXJCLElBQTBCWCxTQUFTLENBQUNDLFdBQVYsQ0FBc0JRLE1BQXRCLE1BQWtDLENBQS9ELEVBQWtFLE9BQU8sSUFBUDs7QUFDbEUsTUFBR0MsU0FBUyxDQUFDQyxNQUFWLEdBQW1CLENBQXRCLEVBQXdCO0FBQ3BCLFFBQUlDLGNBQWMsR0FBR0YsU0FBUyxDQUFDQSxTQUFTLENBQUNDLE1BQVYsR0FBbUIsQ0FBcEIsQ0FBOUIsQ0FEb0IsQ0FFcEI7O0FBQ0EsUUFBR1gsU0FBUyxDQUFDTyxhQUFWLENBQXdCRSxNQUF4QixFQUFnQ0csY0FBaEMsS0FDSFosU0FBUyxDQUFDQyxXQUFWLENBQXNCUSxNQUF0QixNQUFrQ1QsU0FBUyxDQUFDQyxXQUFWLENBQXNCVyxjQUF0QixJQUF3QyxDQUQxRSxFQUVJLE9BQU8sSUFBUDtBQUNQOztBQUNELFNBQU8sS0FBUDtBQUNILENBVkQ7O0FBV0FiLE1BQU0sQ0FBQ0MsU0FBUCxDQUFpQmEsbUJBQWpCLEdBQXVDLFVBQVNDLFlBQVQsRUFBdUJDLE9BQXZCLEVBQStCO0FBQ2xFLE1BQUdELFlBQVksR0FBRyxFQUFmLElBQXFCQyxPQUFPLENBQUNKLE1BQVIsS0FBbUIsQ0FBM0MsRUFBOEMsT0FBTyxJQUFQO0FBRTlDLE1BQUlLLGVBQWUsR0FBR0QsT0FBTyxDQUFDQSxPQUFPLENBQUNKLE1BQVIsR0FBaUIsQ0FBbEIsQ0FBN0IsQ0FIa0UsQ0FJdEU7O0FBQ0ksU0FBUSxDQUFDWCxTQUFTLENBQUNJLGNBQVYsQ0FBeUJVLFlBQXpCLEVBQXVDRSxlQUF2QyxDQUFELElBQ0poQixTQUFTLENBQUNDLFdBQVYsQ0FBc0JhLFlBQXRCLElBQXNDLENBQXRDLEtBQTRDZCxTQUFTLENBQUNDLFdBQVYsQ0FBc0JlLGVBQXRCLENBRGhEO0FBR0gsQ0FSRCIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsid2luZG93LkNhcmRVdGlscyA9IHt9O1xud2luZG93LkNhcmRVdGlscy5nZXRSYW5rQ2FyZCA9IGZ1bmN0aW9uIChpZHhDYXJkKXtcbiAgICAvLyBjYy5sb2coaWR4Q2FyZCArIFwiIC0gZ2V0UmFua0NhcmQ6IFwiICsgKGlkeENhcmQgLSBpZHhDYXJkJTQpLzQpO1xuICAgIHJldHVybiAoaWR4Q2FyZCAtIGlkeENhcmQlNCkvNDtcbn07XG53aW5kb3cuQ2FyZFV0aWxzLmdldFN1aXRDYXJkID0gZnVuY3Rpb24gKGlkeENhcmQpe1xuICAgIC8vIGNjLmxvZyhpZHhDYXJkICsgXCIgLSBnZXRTdWl0Q2FyZDogXCIgKyBpZHhDYXJkJTQpO1xuICAgIHJldHVybiBpZHhDYXJkJTQ7XG59O1xud2luZG93LkNhcmRVdGlscy5jaGVja1NhbWVDb2xvciA9IGZ1bmN0aW9uIChpZHhDYXJkMSwgaWR4Q2FyZDIpIHtcbiAgICByZXR1cm4gKChDYXJkVXRpbHMuZ2V0U3VpdENhcmQoaWR4Q2FyZDEpID4gMSAmJiBDYXJkVXRpbHMuZ2V0U3VpdENhcmQoaWR4Q2FyZDIpID4gMSkgfHxcbiAgICAgICAgKENhcmRVdGlscy5nZXRTdWl0Q2FyZChpZHhDYXJkMSkgPCAyICYmIENhcmRVdGlscy5nZXRTdWl0Q2FyZChpZHhDYXJkMikgPCAyKSlcbn1cbndpbmRvdy5DYXJkVXRpbHMuY2hlY2tTYW1lU3VpdCA9IGZ1bmN0aW9uIChpZHhDYXJkMSwgaWR4Q2FyZDIpIHtcbiAgICByZXR1cm4gKENhcmRVdGlscy5nZXRTdWl0Q2FyZChpZHhDYXJkMSkgPT09IENhcmRVdGlscy5nZXRTdWl0Q2FyZChpZHhDYXJkMikpXG59XG53aW5kb3cuQ2FyZFV0aWxzLnZhbGlkYXRlQ2FyZEhvbGRlciA9IGZ1bmN0aW9uKGNhcmRJZCwgYXJySG9sZGVyKXtcbiAgICBpZihhcnJIb2xkZXIubGVuZ3RoID09PSAwICYmIENhcmRVdGlscy5nZXRSYW5rQ2FyZChjYXJkSWQpID09PSAwKSByZXR1cm4gdHJ1ZTtcbiAgICBpZihhcnJIb2xkZXIubGVuZ3RoID4gMCl7XG4gICAgICAgIGxldCBsYXN0Q2FyZEhvbGRlciA9IGFyckhvbGRlclthcnJIb2xkZXIubGVuZ3RoIC0gMV07XG4gICAgICAgIC8vQ2hlY2sgc2FtZSBzdWl0IGFuZCBjb250aW51ZSByYW5rXG4gICAgICAgIGlmKENhcmRVdGlscy5jaGVja1NhbWVTdWl0KGNhcmRJZCwgbGFzdENhcmRIb2xkZXIpICYmXG4gICAgICAgIENhcmRVdGlscy5nZXRSYW5rQ2FyZChjYXJkSWQpID09PSBDYXJkVXRpbHMuZ2V0UmFua0NhcmQobGFzdENhcmRIb2xkZXIpICsgMSlcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgIH1cbiAgICByZXR1cm4gZmFsc2U7XG59XG53aW5kb3cuQ2FyZFV0aWxzLnZhbGlkYXRlQ2FyZE9uQm9hcmQgPSBmdW5jdGlvbihpZENhcmRTb3VyY2UsIGFyckNhcmQpe1xuICAgIGlmKGlkQ2FyZFNvdXJjZSA+IDQ3ICYmIGFyckNhcmQubGVuZ3RoID09PSAwKSByZXR1cm4gdHJ1ZTtcblxuICAgIGxldCBsYXN0Q2FyZE9mR3JvdXAgPSBhcnJDYXJkW2FyckNhcmQubGVuZ3RoIC0gMV07XG4vL0NoZWNrIGRpZmZlcmVudCBjb2xvciBhbmQgY29udGludWUgcmFua1xuICAgIHJldHVybiAoIUNhcmRVdGlscy5jaGVja1NhbWVDb2xvcihpZENhcmRTb3VyY2UsIGxhc3RDYXJkT2ZHcm91cCkgJiZcbiAgICAgICAgQ2FyZFV0aWxzLmdldFJhbmtDYXJkKGlkQ2FyZFNvdXJjZSkgKyAxID09PSBDYXJkVXRpbHMuZ2V0UmFua0NhcmQobGFzdENhcmRPZkdyb3VwKSlcblxufVxuIl19