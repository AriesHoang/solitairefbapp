
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/HelpPopup.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'e0c61rpkHBJH6XQgn736VGo', 'HelpPopup');
// scripts/HelpPopup.js

"use strict";

var arrDesc = ["To win a game, you need to complete 4 pile of the same suit from Ace to King.", "Cards can only be moved to different-color cards ranked one higher.", "Any empty column can only placed by a Kind or a column starting with a King.", "When you cannot move any cards in the deck, you can click the stockpile to deal one or three cards"];
cc.Class({
  "extends": cc.Component,
  properties: {
    nodeContent: cc.Node,
    sprCurrentGuide: cc.Sprite,
    arrSpr: [cc.SpriteFrame],
    arrIdxDesc: [cc.Node],
    btnNext: cc.Button,
    btnPrev: cc.Button,
    lblGuideContent: cc.Label
  },
  ctor: function ctor() {
    this.iCurrentGuide = 0;
  },
  show: function show(bShow) {
    var _this = this;

    if (bShow) {
      this.resetGuide();
      this.node.opacity = 0;
      this.node.scale = 0;
      this.node.active = true;
      cc.tween(this.node).to(0.2, {
        scale: 1.0,
        opacity: 255
      }).start();
    } else {
      cc.tween(this.node).to(0.2, {
        scale: 0,
        opacity: 0
      }).call(function () {
        _this.node.active = false;
      }).start();
    }
  },
  resetGuide: function resetGuide() {
    this.iCurrentGuide = 0;
    this.processGuide();
  },
  processGuide: function processGuide(idxGuide) {
    if (idxGuide === void 0) {
      idxGuide = 0;
    }

    cc.log("processGuide: " + idxGuide);

    for (var i = 0; i < this.arrIdxDesc.length; i++) {
      if (i == idxGuide) {
        this.arrIdxDesc[idxGuide].opacity = 255;
      } else {
        this.arrIdxDesc[i].opacity = 100;
      }
    }

    this.btnNext.node.opacity = this.iCurrentGuide === 3 ? 100 : 255;
    this.btnPrev.node.opacity = this.iCurrentGuide === 0 ? 100 : 255;
    this.sprCurrentGuide.spriteFrame = this.arrSpr[this.iCurrentGuide];
    this.lblGuideContent.string = arrDesc[this.iCurrentGuide];
  },
  btnNextClick: function btnNextClick() {
    if (this.iCurrentGuide < 3) {
      this.iCurrentGuide++;
      this.processGuide(this.iCurrentGuide);
    }
  },
  btnPrevClick: function btnPrevClick() {
    if (this.iCurrentGuide > 0) {
      this.iCurrentGuide--;
      this.processGuide(this.iCurrentGuide);
    }
  },
  btnCloseClick: function btnCloseClick() {
    this.show(false);
  },
  start: function start() {} // update (dt) {},

});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9zY3JpcHRzL0hlbHBQb3B1cC5qcyJdLCJuYW1lcyI6WyJhcnJEZXNjIiwiY2MiLCJDbGFzcyIsIkNvbXBvbmVudCIsInByb3BlcnRpZXMiLCJub2RlQ29udGVudCIsIk5vZGUiLCJzcHJDdXJyZW50R3VpZGUiLCJTcHJpdGUiLCJhcnJTcHIiLCJTcHJpdGVGcmFtZSIsImFycklkeERlc2MiLCJidG5OZXh0IiwiQnV0dG9uIiwiYnRuUHJldiIsImxibEd1aWRlQ29udGVudCIsIkxhYmVsIiwiY3RvciIsImlDdXJyZW50R3VpZGUiLCJzaG93IiwiYlNob3ciLCJyZXNldEd1aWRlIiwibm9kZSIsIm9wYWNpdHkiLCJzY2FsZSIsImFjdGl2ZSIsInR3ZWVuIiwidG8iLCJzdGFydCIsImNhbGwiLCJwcm9jZXNzR3VpZGUiLCJpZHhHdWlkZSIsImxvZyIsImkiLCJsZW5ndGgiLCJzcHJpdGVGcmFtZSIsInN0cmluZyIsImJ0bk5leHRDbGljayIsImJ0blByZXZDbGljayIsImJ0bkNsb3NlQ2xpY2siXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsSUFBSUEsT0FBTyxHQUFHLENBQ1YsK0VBRFUsRUFFVixxRUFGVSxFQUdWLDhFQUhVLEVBSVYsb0dBSlUsQ0FBZDtBQU1BQyxFQUFFLENBQUNDLEtBQUgsQ0FBUztBQUNMLGFBQVNELEVBQUUsQ0FBQ0UsU0FEUDtBQUdMQyxFQUFBQSxVQUFVLEVBQUU7QUFDUkMsSUFBQUEsV0FBVyxFQUFFSixFQUFFLENBQUNLLElBRFI7QUFFUkMsSUFBQUEsZUFBZSxFQUFFTixFQUFFLENBQUNPLE1BRlo7QUFHUkMsSUFBQUEsTUFBTSxFQUFFLENBQUNSLEVBQUUsQ0FBQ1MsV0FBSixDQUhBO0FBSVJDLElBQUFBLFVBQVUsRUFBRSxDQUFDVixFQUFFLENBQUNLLElBQUosQ0FKSjtBQUtSTSxJQUFBQSxPQUFPLEVBQUVYLEVBQUUsQ0FBQ1ksTUFMSjtBQU1SQyxJQUFBQSxPQUFPLEVBQUViLEVBQUUsQ0FBQ1ksTUFOSjtBQU9SRSxJQUFBQSxlQUFlLEVBQUVkLEVBQUUsQ0FBQ2U7QUFQWixHQUhQO0FBYUxDLEVBQUFBLElBYkssa0JBYUM7QUFDRixTQUFLQyxhQUFMLEdBQXFCLENBQXJCO0FBQ0gsR0FmSTtBQWdCTEMsRUFBQUEsSUFoQkssZ0JBZ0JBQyxLQWhCQSxFQWdCTTtBQUFBOztBQUNQLFFBQUdBLEtBQUgsRUFBUztBQUNMLFdBQUtDLFVBQUw7QUFDQSxXQUFLQyxJQUFMLENBQVVDLE9BQVYsR0FBb0IsQ0FBcEI7QUFDQSxXQUFLRCxJQUFMLENBQVVFLEtBQVYsR0FBa0IsQ0FBbEI7QUFDQSxXQUFLRixJQUFMLENBQVVHLE1BQVYsR0FBbUIsSUFBbkI7QUFDQXhCLE1BQUFBLEVBQUUsQ0FBQ3lCLEtBQUgsQ0FBUyxLQUFLSixJQUFkLEVBQ0tLLEVBREwsQ0FDUSxHQURSLEVBQ2E7QUFBQ0gsUUFBQUEsS0FBSyxFQUFFLEdBQVI7QUFBYUQsUUFBQUEsT0FBTyxFQUFFO0FBQXRCLE9BRGIsRUFFS0ssS0FGTDtBQUdILEtBUkQsTUFRSztBQUNEM0IsTUFBQUEsRUFBRSxDQUFDeUIsS0FBSCxDQUFTLEtBQUtKLElBQWQsRUFDS0ssRUFETCxDQUNRLEdBRFIsRUFDYTtBQUFDSCxRQUFBQSxLQUFLLEVBQUUsQ0FBUjtBQUFXRCxRQUFBQSxPQUFPLEVBQUU7QUFBcEIsT0FEYixFQUVLTSxJQUZMLENBRVUsWUFBSTtBQUFDLFFBQUEsS0FBSSxDQUFDUCxJQUFMLENBQVVHLE1BQVYsR0FBbUIsS0FBbkI7QUFBeUIsT0FGeEMsRUFHS0csS0FITDtBQUlIO0FBRUosR0FoQ0k7QUFpQ0xQLEVBQUFBLFVBakNLLHdCQWlDTztBQUNSLFNBQUtILGFBQUwsR0FBcUIsQ0FBckI7QUFDQSxTQUFLWSxZQUFMO0FBQ0gsR0FwQ0k7QUFxQ0xBLEVBQUFBLFlBckNLLHdCQXFDUUMsUUFyQ1IsRUFxQ3FCO0FBQUEsUUFBYkEsUUFBYTtBQUFiQSxNQUFBQSxRQUFhLEdBQUYsQ0FBRTtBQUFBOztBQUN0QjlCLElBQUFBLEVBQUUsQ0FBQytCLEdBQUgsQ0FBTyxtQkFBbUJELFFBQTFCOztBQUNBLFNBQUksSUFBSUUsQ0FBQyxHQUFHLENBQVosRUFBZUEsQ0FBQyxHQUFHLEtBQUt0QixVQUFMLENBQWdCdUIsTUFBbkMsRUFBMkNELENBQUMsRUFBNUMsRUFBK0M7QUFDM0MsVUFBR0EsQ0FBQyxJQUFJRixRQUFSLEVBQWlCO0FBQ2IsYUFBS3BCLFVBQUwsQ0FBZ0JvQixRQUFoQixFQUEwQlIsT0FBMUIsR0FBb0MsR0FBcEM7QUFDSCxPQUZELE1BRUs7QUFDRCxhQUFLWixVQUFMLENBQWdCc0IsQ0FBaEIsRUFBbUJWLE9BQW5CLEdBQTZCLEdBQTdCO0FBQ0g7QUFDSjs7QUFFRCxTQUFLWCxPQUFMLENBQWFVLElBQWIsQ0FBa0JDLE9BQWxCLEdBQTZCLEtBQUtMLGFBQUwsS0FBdUIsQ0FBeEIsR0FBNkIsR0FBN0IsR0FBbUMsR0FBL0Q7QUFDQSxTQUFLSixPQUFMLENBQWFRLElBQWIsQ0FBa0JDLE9BQWxCLEdBQTZCLEtBQUtMLGFBQUwsS0FBdUIsQ0FBeEIsR0FBNkIsR0FBN0IsR0FBbUMsR0FBL0Q7QUFDQSxTQUFLWCxlQUFMLENBQXFCNEIsV0FBckIsR0FBbUMsS0FBSzFCLE1BQUwsQ0FBWSxLQUFLUyxhQUFqQixDQUFuQztBQUNBLFNBQUtILGVBQUwsQ0FBcUJxQixNQUFyQixHQUE4QnBDLE9BQU8sQ0FBQyxLQUFLa0IsYUFBTixDQUFyQztBQUNILEdBbkRJO0FBb0RMbUIsRUFBQUEsWUFwREssMEJBb0RTO0FBRVYsUUFBRyxLQUFLbkIsYUFBTCxHQUFxQixDQUF4QixFQUEwQjtBQUN0QixXQUFLQSxhQUFMO0FBQ0EsV0FBS1ksWUFBTCxDQUFrQixLQUFLWixhQUF2QjtBQUNIO0FBQ0osR0ExREk7QUEyRExvQixFQUFBQSxZQTNESywwQkEyRFM7QUFDVixRQUFHLEtBQUtwQixhQUFMLEdBQXFCLENBQXhCLEVBQTBCO0FBQ3RCLFdBQUtBLGFBQUw7QUFDQSxXQUFLWSxZQUFMLENBQWtCLEtBQUtaLGFBQXZCO0FBQ0g7QUFDSixHQWhFSTtBQWtFTHFCLEVBQUFBLGFBbEVLLDJCQWtFVTtBQUNiLFNBQUtwQixJQUFMLENBQVUsS0FBVjtBQUNELEdBcEVJO0FBcUVMUyxFQUFBQSxLQXJFSyxtQkFxRUksQ0FFUixDQXZFSSxDQXlFTDs7QUF6RUssQ0FBVCIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsidmFyIGFyckRlc2MgPSBbXG4gICAgXCJUbyB3aW4gYSBnYW1lLCB5b3UgbmVlZCB0byBjb21wbGV0ZSA0IHBpbGUgb2YgdGhlIHNhbWUgc3VpdCBmcm9tIEFjZSB0byBLaW5nLlwiLFxuICAgIFwiQ2FyZHMgY2FuIG9ubHkgYmUgbW92ZWQgdG8gZGlmZmVyZW50LWNvbG9yIGNhcmRzIHJhbmtlZCBvbmUgaGlnaGVyLlwiLFxuICAgIFwiQW55IGVtcHR5IGNvbHVtbiBjYW4gb25seSBwbGFjZWQgYnkgYSBLaW5kIG9yIGEgY29sdW1uIHN0YXJ0aW5nIHdpdGggYSBLaW5nLlwiLFxuICAgIFwiV2hlbiB5b3UgY2Fubm90IG1vdmUgYW55IGNhcmRzIGluIHRoZSBkZWNrLCB5b3UgY2FuIGNsaWNrIHRoZSBzdG9ja3BpbGUgdG8gZGVhbCBvbmUgb3IgdGhyZWUgY2FyZHNcIlxuXTtcbmNjLkNsYXNzKHtcbiAgICBleHRlbmRzOiBjYy5Db21wb25lbnQsXG5cbiAgICBwcm9wZXJ0aWVzOiB7XG4gICAgICAgIG5vZGVDb250ZW50OiBjYy5Ob2RlLFxuICAgICAgICBzcHJDdXJyZW50R3VpZGU6IGNjLlNwcml0ZSxcbiAgICAgICAgYXJyU3ByOiBbY2MuU3ByaXRlRnJhbWVdLFxuICAgICAgICBhcnJJZHhEZXNjOiBbY2MuTm9kZV0sXG4gICAgICAgIGJ0bk5leHQ6IGNjLkJ1dHRvbixcbiAgICAgICAgYnRuUHJldjogY2MuQnV0dG9uLFxuICAgICAgICBsYmxHdWlkZUNvbnRlbnQ6IGNjLkxhYmVsXG4gICAgfSxcblxuICAgIGN0b3IoKXtcbiAgICAgICAgdGhpcy5pQ3VycmVudEd1aWRlID0gMDtcbiAgICB9LFxuICAgIHNob3coYlNob3cpe1xuICAgICAgICBpZihiU2hvdyl7XG4gICAgICAgICAgICB0aGlzLnJlc2V0R3VpZGUoKTtcbiAgICAgICAgICAgIHRoaXMubm9kZS5vcGFjaXR5ID0gMDtcbiAgICAgICAgICAgIHRoaXMubm9kZS5zY2FsZSA9IDA7XG4gICAgICAgICAgICB0aGlzLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgICAgIGNjLnR3ZWVuKHRoaXMubm9kZSlcbiAgICAgICAgICAgICAgICAudG8oMC4yLCB7c2NhbGU6IDEuMCwgb3BhY2l0eTogMjU1fSlcbiAgICAgICAgICAgICAgICAuc3RhcnQoKTtcbiAgICAgICAgfWVsc2V7XG4gICAgICAgICAgICBjYy50d2Vlbih0aGlzLm5vZGUpXG4gICAgICAgICAgICAgICAgLnRvKDAuMiwge3NjYWxlOiAwLCBvcGFjaXR5OiAwfSlcbiAgICAgICAgICAgICAgICAuY2FsbCgoKT0+e3RoaXMubm9kZS5hY3RpdmUgPSBmYWxzZX0pXG4gICAgICAgICAgICAgICAgLnN0YXJ0KCk7XG4gICAgICAgIH1cblxuICAgIH0sXG4gICAgcmVzZXRHdWlkZSgpe1xuICAgICAgICB0aGlzLmlDdXJyZW50R3VpZGUgPSAwO1xuICAgICAgICB0aGlzLnByb2Nlc3NHdWlkZSgpO1xuICAgIH0sXG4gICAgcHJvY2Vzc0d1aWRlKGlkeEd1aWRlID0gMCl7XG4gICAgICAgIGNjLmxvZyhcInByb2Nlc3NHdWlkZTogXCIgKyBpZHhHdWlkZSk7XG4gICAgICAgIGZvcihsZXQgaSA9IDA7IGkgPCB0aGlzLmFycklkeERlc2MubGVuZ3RoOyBpKyspe1xuICAgICAgICAgICAgaWYoaSA9PSBpZHhHdWlkZSl7XG4gICAgICAgICAgICAgICAgdGhpcy5hcnJJZHhEZXNjW2lkeEd1aWRlXS5vcGFjaXR5ID0gMjU1O1xuICAgICAgICAgICAgfWVsc2V7XG4gICAgICAgICAgICAgICAgdGhpcy5hcnJJZHhEZXNjW2ldLm9wYWNpdHkgPSAxMDA7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLmJ0bk5leHQubm9kZS5vcGFjaXR5ID0gKHRoaXMuaUN1cnJlbnRHdWlkZSA9PT0gMykgPyAxMDAgOiAyNTU7XG4gICAgICAgIHRoaXMuYnRuUHJldi5ub2RlLm9wYWNpdHkgPSAodGhpcy5pQ3VycmVudEd1aWRlID09PSAwKSA/IDEwMCA6IDI1NTtcbiAgICAgICAgdGhpcy5zcHJDdXJyZW50R3VpZGUuc3ByaXRlRnJhbWUgPSB0aGlzLmFyclNwclt0aGlzLmlDdXJyZW50R3VpZGVdO1xuICAgICAgICB0aGlzLmxibEd1aWRlQ29udGVudC5zdHJpbmcgPSBhcnJEZXNjW3RoaXMuaUN1cnJlbnRHdWlkZV07XG4gICAgfSxcbiAgICBidG5OZXh0Q2xpY2soKXtcblxuICAgICAgICBpZih0aGlzLmlDdXJyZW50R3VpZGUgPCAzKXtcbiAgICAgICAgICAgIHRoaXMuaUN1cnJlbnRHdWlkZSsrO1xuICAgICAgICAgICAgdGhpcy5wcm9jZXNzR3VpZGUodGhpcy5pQ3VycmVudEd1aWRlKTtcbiAgICAgICAgfVxuICAgIH0sXG4gICAgYnRuUHJldkNsaWNrKCl7XG4gICAgICAgIGlmKHRoaXMuaUN1cnJlbnRHdWlkZSA+IDApe1xuICAgICAgICAgICAgdGhpcy5pQ3VycmVudEd1aWRlLS07XG4gICAgICAgICAgICB0aGlzLnByb2Nlc3NHdWlkZSh0aGlzLmlDdXJyZW50R3VpZGUpO1xuICAgICAgICB9XG4gICAgfSxcblxuICAgIGJ0bkNsb3NlQ2xpY2soKXtcbiAgICAgIHRoaXMuc2hvdyhmYWxzZSk7XG4gICAgfSxcbiAgICBzdGFydCAoKSB7XG5cbiAgICB9LFxuXG4gICAgLy8gdXBkYXRlIChkdCkge30sXG59KTtcbiJdfQ==