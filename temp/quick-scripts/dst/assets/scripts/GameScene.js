
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/GameScene.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'e9959FDf5RBfKVjy79pNx3r', 'GameScene');
// scripts/GameScene.js

"use strict";

var GameBoard = require("GameBoard");

var Popup = require("Popup");

var HelpPopup = require("HelpPopup");

cc.Class({
  "extends": cc.Component,
  properties: {
    btnSound: cc.Sprite,
    arrSprSound: [cc.SpriteFrame],
    audioClips: [cc.AudioClip],
    lblScore: cc.Label,
    lblMoves: cc.Label,
    lblTime: cc.Label,
    btnNewGame: cc.Button,
    gameBoard: GameBoard,
    popup: Popup,
    helpPopup: HelpPopup
  },
  onStart: function onStart() {
    this.iTime = 0;
  },
  onLoad: function onLoad() {
    this.gameBoard.gameScene = this;
    this.popup.gameScene = this;
    this.bMuteSound = false;
    this.backgroundSound = cc.audioEngine.play(this.getComponent(cc.AudioSource).clip, true, 0.5);
    this.popup.showPopup(true, "Start A New Game!");
  },
  btnMuteClick: function btnMuteClick() {
    this.playEffect(SoundEffect.CLICK_SOUND);
    this.bMuteSound = !this.bMuteSound;

    if (this.bMuteSound) {
      this.btnSound.spriteFrame = this.arrSprSound[0];
      cc.audioEngine.pause(this.backgroundSound);
    } else {
      this.btnSound.spriteFrame = this.arrSprSound[1];
      cc.audioEngine.resume(this.backgroundSound);
    }
  },
  btnHelpCliclk: function btnHelpCliclk() {
    this.helpPopup.show(true);
  },
  btnNewGameClick: function btnNewGameClick() {
    this.playEffect(SoundEffect.CLICK_SOUND);
    this.iTime = 0;
    this.gameBoard.startNewGame();
  },
  playEffect: function playEffect(iEffect) {
    if (!this.bMuteSound) {
      cc.audioEngine.playEffect(this.audioClips[iEffect], false, 1);
    }
  },
  update: function update(dt) {
    if (this.gameBoard.bGameStart) {
      this.iTime += dt;
      this.lblTime.string = "Time " + Utils.convertTime(this.iTime);
      this.lblScore.string = "Score:" + this.gameBoard.iScore;
      this.lblMoves.string = "Moves: " + this.gameBoard.iMove;
    } else {
      this.iTime = 0;
    }
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9zY3JpcHRzL0dhbWVTY2VuZS5qcyJdLCJuYW1lcyI6WyJHYW1lQm9hcmQiLCJyZXF1aXJlIiwiUG9wdXAiLCJIZWxwUG9wdXAiLCJjYyIsIkNsYXNzIiwiQ29tcG9uZW50IiwicHJvcGVydGllcyIsImJ0blNvdW5kIiwiU3ByaXRlIiwiYXJyU3ByU291bmQiLCJTcHJpdGVGcmFtZSIsImF1ZGlvQ2xpcHMiLCJBdWRpb0NsaXAiLCJsYmxTY29yZSIsIkxhYmVsIiwibGJsTW92ZXMiLCJsYmxUaW1lIiwiYnRuTmV3R2FtZSIsIkJ1dHRvbiIsImdhbWVCb2FyZCIsInBvcHVwIiwiaGVscFBvcHVwIiwib25TdGFydCIsImlUaW1lIiwib25Mb2FkIiwiZ2FtZVNjZW5lIiwiYk11dGVTb3VuZCIsImJhY2tncm91bmRTb3VuZCIsImF1ZGlvRW5naW5lIiwicGxheSIsImdldENvbXBvbmVudCIsIkF1ZGlvU291cmNlIiwiY2xpcCIsInNob3dQb3B1cCIsImJ0bk11dGVDbGljayIsInBsYXlFZmZlY3QiLCJTb3VuZEVmZmVjdCIsIkNMSUNLX1NPVU5EIiwic3ByaXRlRnJhbWUiLCJwYXVzZSIsInJlc3VtZSIsImJ0bkhlbHBDbGljbGsiLCJzaG93IiwiYnRuTmV3R2FtZUNsaWNrIiwic3RhcnROZXdHYW1lIiwiaUVmZmVjdCIsInVwZGF0ZSIsImR0IiwiYkdhbWVTdGFydCIsInN0cmluZyIsIlV0aWxzIiwiY29udmVydFRpbWUiLCJpU2NvcmUiLCJpTW92ZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxJQUFJQSxTQUFTLEdBQUdDLE9BQU8sQ0FBQyxXQUFELENBQXZCOztBQUNBLElBQUlDLEtBQUssR0FBR0QsT0FBTyxDQUFDLE9BQUQsQ0FBbkI7O0FBQ0EsSUFBSUUsU0FBUyxHQUFHRixPQUFPLENBQUMsV0FBRCxDQUF2Qjs7QUFDQUcsRUFBRSxDQUFDQyxLQUFILENBQVM7QUFDTCxhQUFTRCxFQUFFLENBQUNFLFNBRFA7QUFHTEMsRUFBQUEsVUFBVSxFQUFFO0FBQ1JDLElBQUFBLFFBQVEsRUFBR0osRUFBRSxDQUFDSyxNQUROO0FBRVJDLElBQUFBLFdBQVcsRUFBRyxDQUFDTixFQUFFLENBQUNPLFdBQUosQ0FGTjtBQUdSQyxJQUFBQSxVQUFVLEVBQUMsQ0FBQ1IsRUFBRSxDQUFDUyxTQUFKLENBSEg7QUFJUkMsSUFBQUEsUUFBUSxFQUFFVixFQUFFLENBQUNXLEtBSkw7QUFLUkMsSUFBQUEsUUFBUSxFQUFFWixFQUFFLENBQUNXLEtBTEw7QUFNUkUsSUFBQUEsT0FBTyxFQUFFYixFQUFFLENBQUNXLEtBTko7QUFPUkcsSUFBQUEsVUFBVSxFQUFFZCxFQUFFLENBQUNlLE1BUFA7QUFRUkMsSUFBQUEsU0FBUyxFQUFFcEIsU0FSSDtBQVNScUIsSUFBQUEsS0FBSyxFQUFFbkIsS0FUQztBQVVSb0IsSUFBQUEsU0FBUyxFQUFFbkI7QUFWSCxHQUhQO0FBZUxvQixFQUFBQSxPQWZLLHFCQWVJO0FBQ0wsU0FBS0MsS0FBTCxHQUFhLENBQWI7QUFDSCxHQWpCSTtBQWtCTEMsRUFBQUEsTUFsQkssb0JBa0JHO0FBQ0osU0FBS0wsU0FBTCxDQUFlTSxTQUFmLEdBQTJCLElBQTNCO0FBQ0EsU0FBS0wsS0FBTCxDQUFXSyxTQUFYLEdBQXVCLElBQXZCO0FBQ0EsU0FBS0MsVUFBTCxHQUFrQixLQUFsQjtBQUNBLFNBQUtDLGVBQUwsR0FBdUJ4QixFQUFFLENBQUN5QixXQUFILENBQWVDLElBQWYsQ0FBb0IsS0FBS0MsWUFBTCxDQUFrQjNCLEVBQUUsQ0FBQzRCLFdBQXJCLEVBQWtDQyxJQUF0RCxFQUE0RCxJQUE1RCxFQUFrRSxHQUFsRSxDQUF2QjtBQUNBLFNBQUtaLEtBQUwsQ0FBV2EsU0FBWCxDQUFxQixJQUFyQixFQUEyQixtQkFBM0I7QUFDSCxHQXhCSTtBQXlCTEMsRUFBQUEsWUF6QkssMEJBeUJTO0FBQ1YsU0FBS0MsVUFBTCxDQUFnQkMsV0FBVyxDQUFDQyxXQUE1QjtBQUNBLFNBQUtYLFVBQUwsR0FBa0IsQ0FBQyxLQUFLQSxVQUF4Qjs7QUFDQSxRQUFHLEtBQUtBLFVBQVIsRUFBbUI7QUFDZixXQUFLbkIsUUFBTCxDQUFjK0IsV0FBZCxHQUE0QixLQUFLN0IsV0FBTCxDQUFpQixDQUFqQixDQUE1QjtBQUNBTixNQUFBQSxFQUFFLENBQUN5QixXQUFILENBQWVXLEtBQWYsQ0FBcUIsS0FBS1osZUFBMUI7QUFDSCxLQUhELE1BSUk7QUFDQSxXQUFLcEIsUUFBTCxDQUFjK0IsV0FBZCxHQUE0QixLQUFLN0IsV0FBTCxDQUFpQixDQUFqQixDQUE1QjtBQUNBTixNQUFBQSxFQUFFLENBQUN5QixXQUFILENBQWVZLE1BQWYsQ0FBc0IsS0FBS2IsZUFBM0I7QUFDSDtBQUNKLEdBcENJO0FBcUNMYyxFQUFBQSxhQXJDSywyQkFxQ1U7QUFDWCxTQUFLcEIsU0FBTCxDQUFlcUIsSUFBZixDQUFvQixJQUFwQjtBQUNILEdBdkNJO0FBeUNMQyxFQUFBQSxlQXpDSyw2QkF5Q1k7QUFDYixTQUFLUixVQUFMLENBQWdCQyxXQUFXLENBQUNDLFdBQTVCO0FBQ0EsU0FBS2QsS0FBTCxHQUFhLENBQWI7QUFDQSxTQUFLSixTQUFMLENBQWV5QixZQUFmO0FBQ0gsR0E3Q0k7QUE4Q0xULEVBQUFBLFVBOUNLLHNCQThDTVUsT0E5Q04sRUE4Q2M7QUFDZixRQUFHLENBQUMsS0FBS25CLFVBQVQsRUFBb0I7QUFDaEJ2QixNQUFBQSxFQUFFLENBQUN5QixXQUFILENBQWVPLFVBQWYsQ0FBMEIsS0FBS3hCLFVBQUwsQ0FBZ0JrQyxPQUFoQixDQUExQixFQUFvRCxLQUFwRCxFQUEyRCxDQUEzRDtBQUNIO0FBQ0osR0FsREk7QUFtRExDLEVBQUFBLE1BbkRLLGtCQW1ERUMsRUFuREYsRUFtREs7QUFDTixRQUFHLEtBQUs1QixTQUFMLENBQWU2QixVQUFsQixFQUE2QjtBQUN6QixXQUFLekIsS0FBTCxJQUFjd0IsRUFBZDtBQUNBLFdBQUsvQixPQUFMLENBQWFpQyxNQUFiLEdBQXNCLFVBQVVDLEtBQUssQ0FBQ0MsV0FBTixDQUFrQixLQUFLNUIsS0FBdkIsQ0FBaEM7QUFDQSxXQUFLVixRQUFMLENBQWNvQyxNQUFkLEdBQXVCLFdBQVcsS0FBSzlCLFNBQUwsQ0FBZWlDLE1BQWpEO0FBQ0EsV0FBS3JDLFFBQUwsQ0FBY2tDLE1BQWQsR0FBdUIsWUFBWSxLQUFLOUIsU0FBTCxDQUFla0MsS0FBbEQ7QUFDSCxLQUxELE1BS0s7QUFDRCxXQUFLOUIsS0FBTCxHQUFhLENBQWI7QUFDSDtBQUNKO0FBNURJLENBQVQiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbInZhciBHYW1lQm9hcmQgPSByZXF1aXJlKFwiR2FtZUJvYXJkXCIpO1xudmFyIFBvcHVwID0gcmVxdWlyZShcIlBvcHVwXCIpO1xudmFyIEhlbHBQb3B1cCA9IHJlcXVpcmUoXCJIZWxwUG9wdXBcIik7XG5jYy5DbGFzcyh7XG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxuXG4gICAgcHJvcGVydGllczoge1xuICAgICAgICBidG5Tb3VuZCA6IGNjLlNwcml0ZSxcbiAgICAgICAgYXJyU3ByU291bmQgOiBbY2MuU3ByaXRlRnJhbWVdLFxuICAgICAgICBhdWRpb0NsaXBzOltjYy5BdWRpb0NsaXBdLFxuICAgICAgICBsYmxTY29yZTogY2MuTGFiZWwsXG4gICAgICAgIGxibE1vdmVzOiBjYy5MYWJlbCxcbiAgICAgICAgbGJsVGltZTogY2MuTGFiZWwsXG4gICAgICAgIGJ0bk5ld0dhbWU6IGNjLkJ1dHRvbixcbiAgICAgICAgZ2FtZUJvYXJkOiBHYW1lQm9hcmQsXG4gICAgICAgIHBvcHVwOiBQb3B1cCxcbiAgICAgICAgaGVscFBvcHVwOiBIZWxwUG9wdXBcbiAgICB9LFxuICAgIG9uU3RhcnQoKXtcbiAgICAgICAgdGhpcy5pVGltZSA9IDA7XG4gICAgfSxcbiAgICBvbkxvYWQoKXtcbiAgICAgICAgdGhpcy5nYW1lQm9hcmQuZ2FtZVNjZW5lID0gdGhpcztcbiAgICAgICAgdGhpcy5wb3B1cC5nYW1lU2NlbmUgPSB0aGlzO1xuICAgICAgICB0aGlzLmJNdXRlU291bmQgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5iYWNrZ3JvdW5kU291bmQgPSBjYy5hdWRpb0VuZ2luZS5wbGF5KHRoaXMuZ2V0Q29tcG9uZW50KGNjLkF1ZGlvU291cmNlKS5jbGlwLCB0cnVlLCAwLjUpO1xuICAgICAgICB0aGlzLnBvcHVwLnNob3dQb3B1cCh0cnVlLCBcIlN0YXJ0IEEgTmV3IEdhbWUhXCIpO1xuICAgIH0sXG4gICAgYnRuTXV0ZUNsaWNrKCl7XG4gICAgICAgIHRoaXMucGxheUVmZmVjdChTb3VuZEVmZmVjdC5DTElDS19TT1VORCk7XG4gICAgICAgIHRoaXMuYk11dGVTb3VuZCA9ICF0aGlzLmJNdXRlU291bmQ7XG4gICAgICAgIGlmKHRoaXMuYk11dGVTb3VuZCl7XG4gICAgICAgICAgICB0aGlzLmJ0blNvdW5kLnNwcml0ZUZyYW1lID0gdGhpcy5hcnJTcHJTb3VuZFswXTtcbiAgICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBhdXNlKHRoaXMuYmFja2dyb3VuZFNvdW5kKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNle1xuICAgICAgICAgICAgdGhpcy5idG5Tb3VuZC5zcHJpdGVGcmFtZSA9IHRoaXMuYXJyU3ByU291bmRbMV07XG4gICAgICAgICAgICBjYy5hdWRpb0VuZ2luZS5yZXN1bWUodGhpcy5iYWNrZ3JvdW5kU291bmQpO1xuICAgICAgICB9XG4gICAgfSxcbiAgICBidG5IZWxwQ2xpY2xrKCl7XG4gICAgICAgIHRoaXMuaGVscFBvcHVwLnNob3codHJ1ZSk7XG4gICAgfSxcblxuICAgIGJ0bk5ld0dhbWVDbGljaygpe1xuICAgICAgICB0aGlzLnBsYXlFZmZlY3QoU291bmRFZmZlY3QuQ0xJQ0tfU09VTkQpO1xuICAgICAgICB0aGlzLmlUaW1lID0gMDtcbiAgICAgICAgdGhpcy5nYW1lQm9hcmQuc3RhcnROZXdHYW1lKCk7XG4gICAgfSxcbiAgICBwbGF5RWZmZWN0KGlFZmZlY3Qpe1xuICAgICAgICBpZighdGhpcy5iTXV0ZVNvdW5kKXtcbiAgICAgICAgICAgIGNjLmF1ZGlvRW5naW5lLnBsYXlFZmZlY3QodGhpcy5hdWRpb0NsaXBzW2lFZmZlY3RdLCBmYWxzZSwgMSk7XG4gICAgICAgIH1cbiAgICB9LFxuICAgIHVwZGF0ZShkdCl7XG4gICAgICAgIGlmKHRoaXMuZ2FtZUJvYXJkLmJHYW1lU3RhcnQpe1xuICAgICAgICAgICAgdGhpcy5pVGltZSArPSBkdDtcbiAgICAgICAgICAgIHRoaXMubGJsVGltZS5zdHJpbmcgPSBcIlRpbWUgXCIgKyBVdGlscy5jb252ZXJ0VGltZSh0aGlzLmlUaW1lKTtcbiAgICAgICAgICAgIHRoaXMubGJsU2NvcmUuc3RyaW5nID0gXCJTY29yZTpcIiArIHRoaXMuZ2FtZUJvYXJkLmlTY29yZTtcbiAgICAgICAgICAgIHRoaXMubGJsTW92ZXMuc3RyaW5nID0gXCJNb3ZlczogXCIgKyB0aGlzLmdhbWVCb2FyZC5pTW92ZVxuICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgIHRoaXMuaVRpbWUgPSAwO1xuICAgICAgICB9XG4gICAgfVxufSk7XG4iXX0=