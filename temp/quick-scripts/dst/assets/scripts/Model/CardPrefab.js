
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/Model/CardPrefab.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, 'da8d01jkcRK47ZhDCADmmrf', 'CardPrefab');
// scripts/Model/CardPrefab.js

"use strict";

var arrSuit = ["B", "T", "R", "C"];
var arrRank = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"];
cc.Class({
  "extends": cc.Component,
  properties: {
    btnCard: cc.Button,
    sprCard: cc.Sprite,
    animCard: cc.Animation,
    sprBackCard: cc.SpriteFrame,
    sprBorder: cc.Node,
    effectCard: cc.Animation
  },
  ctor: function ctor() {
    this.idCard = -1;
    this.groupId = -1;
    this.bTouch = false;
    this.oriPos = cc.v2(0, 0);
    this.oriIndex = 0;
    this.spriteFrameCard = null;
  },
  // LIFE-CYCLE CALLBACKS:
  // onLoad () {},
  start: function start() {},
  setSpriteCard: function setSpriteCard(idCard, sprCard) {
    // cc.log("setSpriteCard: " + idCard);
    this.showBorder(false);
    this.bTouch = false;
    this.bActive = false;
    this.idCard = idCard;
    this.spriteFrameCard = sprCard;
    this.btnCard.interactable = false;
  },
  showCard: function showCard(bAnim) {
    var _this = this;

    this.bActive = true;

    if (bAnim) {
      cc.tween(this.node).to(0.2, {
        scaleX: 0
      }).call(function () {
        _this.sprCard.spriteFrame = _this.spriteFrameCard;
        _this.btnCard.interactable = true;
        _this.bTouch = true;
      }).to(0.2, {
        scaleX: 1.0
      }).start();
    } else {
      this.sprCard.spriteFrame = this.spriteFrameCard;
      this.btnCard.interactable = false;
      this.bTouch = false;
    }
  },
  showBorder: function showBorder(bShow) {
    this.sprBorder.active = bShow;
  },
  shakeCard: function shakeCard() {
    this.animCard.play();
  },
  showEffectCard: function showEffectCard(bShow) {
    if (bShow === void 0) {
      bShow = true;
    }

    this.effectCard.node.active = bShow;

    if (bShow) {
      this.effectCard.play();
    }
  },
  endEffectCard: function endEffectCard() {
    this.showEffectCard(false);
  },
  resetCard: function resetCard() {
    this.idCard = -1;
    this.bTouch = false;
    this.bActive = false;
    this.sprCard.spriteFrame = this.sprBackCard;
    this.showBorder(false);
    this.effectCard.node.active = false;
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9zY3JpcHRzL01vZGVsL0NhcmRQcmVmYWIuanMiXSwibmFtZXMiOlsiYXJyU3VpdCIsImFyclJhbmsiLCJjYyIsIkNsYXNzIiwiQ29tcG9uZW50IiwicHJvcGVydGllcyIsImJ0bkNhcmQiLCJCdXR0b24iLCJzcHJDYXJkIiwiU3ByaXRlIiwiYW5pbUNhcmQiLCJBbmltYXRpb24iLCJzcHJCYWNrQ2FyZCIsIlNwcml0ZUZyYW1lIiwic3ByQm9yZGVyIiwiTm9kZSIsImVmZmVjdENhcmQiLCJjdG9yIiwiaWRDYXJkIiwiZ3JvdXBJZCIsImJUb3VjaCIsIm9yaVBvcyIsInYyIiwib3JpSW5kZXgiLCJzcHJpdGVGcmFtZUNhcmQiLCJzdGFydCIsInNldFNwcml0ZUNhcmQiLCJzaG93Qm9yZGVyIiwiYkFjdGl2ZSIsImludGVyYWN0YWJsZSIsInNob3dDYXJkIiwiYkFuaW0iLCJ0d2VlbiIsIm5vZGUiLCJ0byIsInNjYWxlWCIsImNhbGwiLCJzcHJpdGVGcmFtZSIsImJTaG93IiwiYWN0aXZlIiwic2hha2VDYXJkIiwicGxheSIsInNob3dFZmZlY3RDYXJkIiwiZW5kRWZmZWN0Q2FyZCIsInJlc2V0Q2FyZCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxJQUFNQSxPQUFPLEdBQUcsQ0FBQyxHQUFELEVBQU0sR0FBTixFQUFXLEdBQVgsRUFBZ0IsR0FBaEIsQ0FBaEI7QUFDQSxJQUFNQyxPQUFPLEdBQUcsQ0FBQyxHQUFELEVBQU0sR0FBTixFQUFXLEdBQVgsRUFBZ0IsR0FBaEIsRUFBcUIsR0FBckIsRUFBMEIsR0FBMUIsRUFBK0IsR0FBL0IsRUFBb0MsR0FBcEMsRUFBeUMsR0FBekMsRUFBOEMsSUFBOUMsRUFBb0QsR0FBcEQsRUFBeUQsR0FBekQsRUFBOEQsR0FBOUQsQ0FBaEI7QUFDQUMsRUFBRSxDQUFDQyxLQUFILENBQVM7QUFDTCxhQUFTRCxFQUFFLENBQUNFLFNBRFA7QUFHTEMsRUFBQUEsVUFBVSxFQUFFO0FBQ1JDLElBQUFBLE9BQU8sRUFBRUosRUFBRSxDQUFDSyxNQURKO0FBRVJDLElBQUFBLE9BQU8sRUFBRU4sRUFBRSxDQUFDTyxNQUZKO0FBR1JDLElBQUFBLFFBQVEsRUFBRVIsRUFBRSxDQUFDUyxTQUhMO0FBSVJDLElBQUFBLFdBQVcsRUFBRVYsRUFBRSxDQUFDVyxXQUpSO0FBS1JDLElBQUFBLFNBQVMsRUFBRVosRUFBRSxDQUFDYSxJQUxOO0FBTVJDLElBQUFBLFVBQVUsRUFBRWQsRUFBRSxDQUFDUztBQU5QLEdBSFA7QUFZTE0sRUFBQUEsSUFaSyxrQkFZRTtBQUNILFNBQUtDLE1BQUwsR0FBYyxDQUFDLENBQWY7QUFDQSxTQUFLQyxPQUFMLEdBQWUsQ0FBQyxDQUFoQjtBQUNBLFNBQUtDLE1BQUwsR0FBYyxLQUFkO0FBQ0EsU0FBS0MsTUFBTCxHQUFjbkIsRUFBRSxDQUFDb0IsRUFBSCxDQUFNLENBQU4sRUFBUyxDQUFULENBQWQ7QUFDQSxTQUFLQyxRQUFMLEdBQWdCLENBQWhCO0FBQ0EsU0FBS0MsZUFBTCxHQUF1QixJQUF2QjtBQUNILEdBbkJJO0FBb0JMO0FBRUE7QUFFQUMsRUFBQUEsS0F4QkssbUJBd0JJLENBQ1IsQ0F6Qkk7QUEwQkxDLEVBQUFBLGFBMUJLLHlCQTBCU1IsTUExQlQsRUEwQmlCVixPQTFCakIsRUEwQnlCO0FBQzFCO0FBQ0EsU0FBS21CLFVBQUwsQ0FBZ0IsS0FBaEI7QUFDQSxTQUFLUCxNQUFMLEdBQWMsS0FBZDtBQUNBLFNBQUtRLE9BQUwsR0FBZSxLQUFmO0FBQ0EsU0FBS1YsTUFBTCxHQUFjQSxNQUFkO0FBQ0EsU0FBS00sZUFBTCxHQUF1QmhCLE9BQXZCO0FBQ0EsU0FBS0YsT0FBTCxDQUFhdUIsWUFBYixHQUE0QixLQUE1QjtBQUNILEdBbENJO0FBbUNMQyxFQUFBQSxRQW5DSyxvQkFtQ0lDLEtBbkNKLEVBbUNVO0FBQUE7O0FBQ1gsU0FBS0gsT0FBTCxHQUFlLElBQWY7O0FBQ0EsUUFBR0csS0FBSCxFQUFTO0FBQ0w3QixNQUFBQSxFQUFFLENBQUM4QixLQUFILENBQVMsS0FBS0MsSUFBZCxFQUNLQyxFQURMLENBQ1EsR0FEUixFQUNhO0FBQUNDLFFBQUFBLE1BQU0sRUFBQztBQUFSLE9BRGIsRUFFS0MsSUFGTCxDQUVVLFlBQUk7QUFDTixRQUFBLEtBQUksQ0FBQzVCLE9BQUwsQ0FBYTZCLFdBQWIsR0FBMkIsS0FBSSxDQUFDYixlQUFoQztBQUNBLFFBQUEsS0FBSSxDQUFDbEIsT0FBTCxDQUFhdUIsWUFBYixHQUE0QixJQUE1QjtBQUNBLFFBQUEsS0FBSSxDQUFDVCxNQUFMLEdBQWMsSUFBZDtBQUNILE9BTkwsRUFPS2MsRUFQTCxDQU9RLEdBUFIsRUFPYTtBQUFDQyxRQUFBQSxNQUFNLEVBQUM7QUFBUixPQVBiLEVBUUtWLEtBUkw7QUFTSCxLQVZELE1BVUs7QUFDRCxXQUFLakIsT0FBTCxDQUFhNkIsV0FBYixHQUEyQixLQUFLYixlQUFoQztBQUNBLFdBQUtsQixPQUFMLENBQWF1QixZQUFiLEdBQTRCLEtBQTVCO0FBQ0EsV0FBS1QsTUFBTCxHQUFjLEtBQWQ7QUFDSDtBQUNKLEdBcERJO0FBcURMTyxFQUFBQSxVQXJESyxzQkFxRE1XLEtBckROLEVBcURZO0FBQ2IsU0FBS3hCLFNBQUwsQ0FBZXlCLE1BQWYsR0FBd0JELEtBQXhCO0FBQ0gsR0F2REk7QUF3RExFLEVBQUFBLFNBeERLLHVCQXdETTtBQUNQLFNBQUs5QixRQUFMLENBQWMrQixJQUFkO0FBQ0gsR0ExREk7QUEyRExDLEVBQUFBLGNBM0RLLDBCQTJEVUosS0EzRFYsRUEyRHVCO0FBQUEsUUFBYkEsS0FBYTtBQUFiQSxNQUFBQSxLQUFhLEdBQUwsSUFBSztBQUFBOztBQUN4QixTQUFLdEIsVUFBTCxDQUFnQmlCLElBQWhCLENBQXFCTSxNQUFyQixHQUE4QkQsS0FBOUI7O0FBQ0EsUUFBR0EsS0FBSCxFQUFTO0FBQ0wsV0FBS3RCLFVBQUwsQ0FBZ0J5QixJQUFoQjtBQUNIO0FBQ0osR0FoRUk7QUFpRUxFLEVBQUFBLGFBakVLLDJCQWlFVTtBQUNYLFNBQUtELGNBQUwsQ0FBb0IsS0FBcEI7QUFDSCxHQW5FSTtBQW9FTEUsRUFBQUEsU0FwRUssdUJBb0VNO0FBQ1AsU0FBSzFCLE1BQUwsR0FBYyxDQUFDLENBQWY7QUFDQSxTQUFLRSxNQUFMLEdBQWMsS0FBZDtBQUNBLFNBQUtRLE9BQUwsR0FBZSxLQUFmO0FBQ0EsU0FBS3BCLE9BQUwsQ0FBYTZCLFdBQWIsR0FBMkIsS0FBS3pCLFdBQWhDO0FBQ0EsU0FBS2UsVUFBTCxDQUFnQixLQUFoQjtBQUNBLFNBQUtYLFVBQUwsQ0FBZ0JpQixJQUFoQixDQUFxQk0sTUFBckIsR0FBOEIsS0FBOUI7QUFDSDtBQTNFSSxDQUFUIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCBhcnJTdWl0ID0gW1wiQlwiLCBcIlRcIiwgXCJSXCIsIFwiQ1wiXTtcbmNvbnN0IGFyclJhbmsgPSBbXCJBXCIsIFwiMlwiLCBcIjNcIiwgXCI0XCIsIFwiNVwiLCBcIjZcIiwgXCI3XCIsIFwiOFwiLCBcIjlcIiwgXCIxMFwiLCBcIkpcIiwgXCJRXCIsIFwiS1wiXTtcbmNjLkNsYXNzKHtcbiAgICBleHRlbmRzOiBjYy5Db21wb25lbnQsXG5cbiAgICBwcm9wZXJ0aWVzOiB7XG4gICAgICAgIGJ0bkNhcmQ6IGNjLkJ1dHRvbixcbiAgICAgICAgc3ByQ2FyZDogY2MuU3ByaXRlLFxuICAgICAgICBhbmltQ2FyZDogY2MuQW5pbWF0aW9uLFxuICAgICAgICBzcHJCYWNrQ2FyZDogY2MuU3ByaXRlRnJhbWUsXG4gICAgICAgIHNwckJvcmRlcjogY2MuTm9kZSxcbiAgICAgICAgZWZmZWN0Q2FyZDogY2MuQW5pbWF0aW9uXG4gICAgfSxcblxuICAgIGN0b3IoKSB7XG4gICAgICAgIHRoaXMuaWRDYXJkID0gLTE7XG4gICAgICAgIHRoaXMuZ3JvdXBJZCA9IC0xO1xuICAgICAgICB0aGlzLmJUb3VjaCA9IGZhbHNlO1xuICAgICAgICB0aGlzLm9yaVBvcyA9IGNjLnYyKDAsIDApO1xuICAgICAgICB0aGlzLm9yaUluZGV4ID0gMDtcbiAgICAgICAgdGhpcy5zcHJpdGVGcmFtZUNhcmQgPSBudWxsO1xuICAgIH0sXG4gICAgLy8gTElGRS1DWUNMRSBDQUxMQkFDS1M6XG5cbiAgICAvLyBvbkxvYWQgKCkge30sXG5cbiAgICBzdGFydCAoKSB7XG4gICAgfSxcbiAgICBzZXRTcHJpdGVDYXJkKGlkQ2FyZCwgc3ByQ2FyZCl7XG4gICAgICAgIC8vIGNjLmxvZyhcInNldFNwcml0ZUNhcmQ6IFwiICsgaWRDYXJkKTtcbiAgICAgICAgdGhpcy5zaG93Qm9yZGVyKGZhbHNlKTtcbiAgICAgICAgdGhpcy5iVG91Y2ggPSBmYWxzZTtcbiAgICAgICAgdGhpcy5iQWN0aXZlID0gZmFsc2VcbiAgICAgICAgdGhpcy5pZENhcmQgPSBpZENhcmQ7XG4gICAgICAgIHRoaXMuc3ByaXRlRnJhbWVDYXJkID0gc3ByQ2FyZDtcbiAgICAgICAgdGhpcy5idG5DYXJkLmludGVyYWN0YWJsZSA9IGZhbHNlO1xuICAgIH0sXG4gICAgc2hvd0NhcmQoYkFuaW0pe1xuICAgICAgICB0aGlzLmJBY3RpdmUgPSB0cnVlO1xuICAgICAgICBpZihiQW5pbSl7XG4gICAgICAgICAgICBjYy50d2Vlbih0aGlzLm5vZGUpXG4gICAgICAgICAgICAgICAgLnRvKDAuMiwge3NjYWxlWDowfSlcbiAgICAgICAgICAgICAgICAuY2FsbCgoKT0+e1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnNwckNhcmQuc3ByaXRlRnJhbWUgPSB0aGlzLnNwcml0ZUZyYW1lQ2FyZDtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5idG5DYXJkLmludGVyYWN0YWJsZSA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYlRvdWNoID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC50bygwLjIsIHtzY2FsZVg6MS4wfSlcbiAgICAgICAgICAgICAgICAuc3RhcnQoKTtcbiAgICAgICAgfWVsc2V7XG4gICAgICAgICAgICB0aGlzLnNwckNhcmQuc3ByaXRlRnJhbWUgPSB0aGlzLnNwcml0ZUZyYW1lQ2FyZDtcbiAgICAgICAgICAgIHRoaXMuYnRuQ2FyZC5pbnRlcmFjdGFibGUgPSBmYWxzZTtcbiAgICAgICAgICAgIHRoaXMuYlRvdWNoID0gZmFsc2U7XG4gICAgICAgIH1cbiAgICB9LFxuICAgIHNob3dCb3JkZXIoYlNob3cpe1xuICAgICAgICB0aGlzLnNwckJvcmRlci5hY3RpdmUgPSBiU2hvdztcbiAgICB9LFxuICAgIHNoYWtlQ2FyZCgpe1xuICAgICAgICB0aGlzLmFuaW1DYXJkLnBsYXkoKTtcbiAgICB9LFxuICAgIHNob3dFZmZlY3RDYXJkKGJTaG93ID0gdHJ1ZSl7XG4gICAgICAgIHRoaXMuZWZmZWN0Q2FyZC5ub2RlLmFjdGl2ZSA9IGJTaG93O1xuICAgICAgICBpZihiU2hvdyl7XG4gICAgICAgICAgICB0aGlzLmVmZmVjdENhcmQucGxheSgpO1xuICAgICAgICB9XG4gICAgfSxcbiAgICBlbmRFZmZlY3RDYXJkKCl7XG4gICAgICAgIHRoaXMuc2hvd0VmZmVjdENhcmQoZmFsc2UpO1xuICAgIH0sXG4gICAgcmVzZXRDYXJkKCl7XG4gICAgICAgIHRoaXMuaWRDYXJkID0gLTE7XG4gICAgICAgIHRoaXMuYlRvdWNoID0gZmFsc2U7XG4gICAgICAgIHRoaXMuYkFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICB0aGlzLnNwckNhcmQuc3ByaXRlRnJhbWUgPSB0aGlzLnNwckJhY2tDYXJkO1xuICAgICAgICB0aGlzLnNob3dCb3JkZXIoZmFsc2UpO1xuICAgICAgICB0aGlzLmVmZmVjdENhcmQubm9kZS5hY3RpdmUgPSBmYWxzZTtcbiAgICB9XG59KTtcbiJdfQ==