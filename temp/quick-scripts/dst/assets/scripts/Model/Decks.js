
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/Model/Decks.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '6a163iFs4ZEPaYAQL7vYg3k', 'Decks');
// scripts/Model/Decks.js

"use strict";

function Decks() {
  this._arrCards = [];

  for (var i = 0; i < 52; i++) {
    this._arrCards.push(i);
  }
}

Decks.prototype.reset = function () {
  this._arrCards.sort(function () {
    return 0.5 - Math.random();
  });
};

Decks.prototype.dealCard = function () {
  // let _arrCardOnTable = [51,50,45,43,49,46,40,48,47,41,39,44,42,36,35,28,37,34,29,27,20,19,38,32,30,24,23,16,15];
  // for(let i = 0; i < _arrCardOnTable.length; i++){
  //     let idx = this._arrCards.indexOf(_arrCardOnTable[i])
  //     this._arrCards.splice(idx, 1);
  // }
  // cc.log("_arrCards: " + JSON.stringify(this._arrCards));
  // this._arrCards = this._arrCards.concat(_arrCardOnTable);
  this.reset();
  return this._arrCards;
};

module.exports = Decks;

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9zY3JpcHRzL01vZGVsL0RlY2tzLmpzIl0sIm5hbWVzIjpbIkRlY2tzIiwiX2FyckNhcmRzIiwiaSIsInB1c2giLCJwcm90b3R5cGUiLCJyZXNldCIsInNvcnQiLCJNYXRoIiwicmFuZG9tIiwiZGVhbENhcmQiLCJtb2R1bGUiLCJleHBvcnRzIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLFNBQVNBLEtBQVQsR0FBZ0I7QUFDWixPQUFLQyxTQUFMLEdBQWlCLEVBQWpCOztBQUNBLE9BQUksSUFBSUMsQ0FBQyxHQUFHLENBQVosRUFBZUEsQ0FBQyxHQUFHLEVBQW5CLEVBQXVCQSxDQUFDLEVBQXhCLEVBQTJCO0FBQ3ZCLFNBQUtELFNBQUwsQ0FBZUUsSUFBZixDQUFvQkQsQ0FBcEI7QUFDSDtBQUNKOztBQUVERixLQUFLLENBQUNJLFNBQU4sQ0FBZ0JDLEtBQWhCLEdBQXdCLFlBQVU7QUFDOUIsT0FBS0osU0FBTCxDQUFlSyxJQUFmLENBQW9CLFlBQVU7QUFDMUIsV0FBTyxNQUFNQyxJQUFJLENBQUNDLE1BQUwsRUFBYjtBQUNILEdBRkQ7QUFHSCxDQUpEOztBQU1BUixLQUFLLENBQUNJLFNBQU4sQ0FBZ0JLLFFBQWhCLEdBQTJCLFlBQVU7QUFDakM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFJQSxPQUFLSixLQUFMO0FBQ0EsU0FBTyxLQUFLSixTQUFaO0FBQ0gsQ0FiRDs7QUFlQVMsTUFBTSxDQUFDQyxPQUFQLEdBQWlCWCxLQUFqQiIsInNvdXJjZVJvb3QiOiIvIiwic291cmNlc0NvbnRlbnQiOlsiZnVuY3Rpb24gRGVja3MoKXtcbiAgICB0aGlzLl9hcnJDYXJkcyA9IFtdO1xuICAgIGZvcihsZXQgaSA9IDA7IGkgPCA1MjsgaSsrKXtcbiAgICAgICAgdGhpcy5fYXJyQ2FyZHMucHVzaChpKTtcbiAgICB9XG59XG5cbkRlY2tzLnByb3RvdHlwZS5yZXNldCA9IGZ1bmN0aW9uKCl7XG4gICAgdGhpcy5fYXJyQ2FyZHMuc29ydChmdW5jdGlvbigpe1xuICAgICAgICByZXR1cm4gMC41IC0gTWF0aC5yYW5kb20oKTtcbiAgICB9KTtcbn1cblxuRGVja3MucHJvdG90eXBlLmRlYWxDYXJkID0gZnVuY3Rpb24oKXtcbiAgICAvLyBsZXQgX2FyckNhcmRPblRhYmxlID0gWzUxLDUwLDQ1LDQzLDQ5LDQ2LDQwLDQ4LDQ3LDQxLDM5LDQ0LDQyLDM2LDM1LDI4LDM3LDM0LDI5LDI3LDIwLDE5LDM4LDMyLDMwLDI0LDIzLDE2LDE1XTtcbiAgICAvLyBmb3IobGV0IGkgPSAwOyBpIDwgX2FyckNhcmRPblRhYmxlLmxlbmd0aDsgaSsrKXtcbiAgICAvLyAgICAgbGV0IGlkeCA9IHRoaXMuX2FyckNhcmRzLmluZGV4T2YoX2FyckNhcmRPblRhYmxlW2ldKVxuICAgIC8vICAgICB0aGlzLl9hcnJDYXJkcy5zcGxpY2UoaWR4LCAxKTtcbiAgICAvLyB9XG4gICAgLy8gY2MubG9nKFwiX2FyckNhcmRzOiBcIiArIEpTT04uc3RyaW5naWZ5KHRoaXMuX2FyckNhcmRzKSk7XG4gICAgLy8gdGhpcy5fYXJyQ2FyZHMgPSB0aGlzLl9hcnJDYXJkcy5jb25jYXQoX2FyckNhcmRPblRhYmxlKTtcblxuXG5cbiAgICB0aGlzLnJlc2V0KCk7XG4gICAgcmV0dXJuIHRoaXMuX2FyckNhcmRzO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IERlY2tzOyJdfQ==