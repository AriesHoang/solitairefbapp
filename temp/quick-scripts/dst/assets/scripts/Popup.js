
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/Popup.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '7d6acgRVhxJD4vuJB+z6hJz', 'Popup');
// scripts/Popup.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {
    lblContent: cc.Label
  },
  showPopup: function showPopup(bShow, strContent) {
    var _this = this;

    if (bShow) {
      this.lblContent.string = strContent;
      this.node.opacity = 0;
      this.node.scale = 0;
      this.node.active = true;
      cc.tween(this.node).to(0.2, {
        scale: 1.0,
        opacity: 255
      }).start();
    } else {
      cc.tween(this.node).to(0.2, {
        scale: 0,
        opacity: 0
      }).call(function () {
        _this.node.active = false;
      }).start();
    }
  },
  btnStartNewGame: function btnStartNewGame() {
    this.gameScene.btnNewGameClick();
    this.showPopup(false);
  },
  btnCloseClick: function btnCloseClick() {
    this.showPopup(false);
  },
  ctor: function ctor() {} // update (dt) {},

});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9zY3JpcHRzL1BvcHVwLmpzIl0sIm5hbWVzIjpbImNjIiwiQ2xhc3MiLCJDb21wb25lbnQiLCJwcm9wZXJ0aWVzIiwibGJsQ29udGVudCIsIkxhYmVsIiwic2hvd1BvcHVwIiwiYlNob3ciLCJzdHJDb250ZW50Iiwic3RyaW5nIiwibm9kZSIsIm9wYWNpdHkiLCJzY2FsZSIsImFjdGl2ZSIsInR3ZWVuIiwidG8iLCJzdGFydCIsImNhbGwiLCJidG5TdGFydE5ld0dhbWUiLCJnYW1lU2NlbmUiLCJidG5OZXdHYW1lQ2xpY2siLCJidG5DbG9zZUNsaWNrIiwiY3RvciJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQUEsRUFBRSxDQUFDQyxLQUFILENBQVM7QUFDTCxhQUFTRCxFQUFFLENBQUNFLFNBRFA7QUFHTEMsRUFBQUEsVUFBVSxFQUFFO0FBQ1JDLElBQUFBLFVBQVUsRUFBRUosRUFBRSxDQUFDSztBQURQLEdBSFA7QUFNTEMsRUFBQUEsU0FOSyxxQkFNS0MsS0FOTCxFQU1ZQyxVQU5aLEVBTXVCO0FBQUE7O0FBQ3hCLFFBQUdELEtBQUgsRUFBUztBQUNMLFdBQUtILFVBQUwsQ0FBZ0JLLE1BQWhCLEdBQXlCRCxVQUF6QjtBQUNBLFdBQUtFLElBQUwsQ0FBVUMsT0FBVixHQUFvQixDQUFwQjtBQUNBLFdBQUtELElBQUwsQ0FBVUUsS0FBVixHQUFrQixDQUFsQjtBQUNBLFdBQUtGLElBQUwsQ0FBVUcsTUFBVixHQUFtQixJQUFuQjtBQUNBYixNQUFBQSxFQUFFLENBQUNjLEtBQUgsQ0FBUyxLQUFLSixJQUFkLEVBQ0tLLEVBREwsQ0FDUSxHQURSLEVBQ2E7QUFBQ0gsUUFBQUEsS0FBSyxFQUFFLEdBQVI7QUFBYUQsUUFBQUEsT0FBTyxFQUFFO0FBQXRCLE9BRGIsRUFFS0ssS0FGTDtBQUdILEtBUkQsTUFRSztBQUNEaEIsTUFBQUEsRUFBRSxDQUFDYyxLQUFILENBQVMsS0FBS0osSUFBZCxFQUNLSyxFQURMLENBQ1EsR0FEUixFQUNhO0FBQUNILFFBQUFBLEtBQUssRUFBRSxDQUFSO0FBQVdELFFBQUFBLE9BQU8sRUFBRTtBQUFwQixPQURiLEVBRUtNLElBRkwsQ0FFVSxZQUFJO0FBQUMsUUFBQSxLQUFJLENBQUNQLElBQUwsQ0FBVUcsTUFBVixHQUFtQixLQUFuQjtBQUF5QixPQUZ4QyxFQUdLRyxLQUhMO0FBSUg7QUFFSixHQXRCSTtBQXVCTEUsRUFBQUEsZUF2QkssNkJBdUJZO0FBQ2IsU0FBS0MsU0FBTCxDQUFlQyxlQUFmO0FBQ0EsU0FBS2QsU0FBTCxDQUFlLEtBQWY7QUFDSCxHQTFCSTtBQTJCTGUsRUFBQUEsYUEzQkssMkJBMkJVO0FBQ1gsU0FBS2YsU0FBTCxDQUFlLEtBQWY7QUFDSCxHQTdCSTtBQThCTGdCLEVBQUFBLElBOUJLLGtCQThCRyxDQUNQLENBL0JJLENBaUNMOztBQWpDSyxDQUFUIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJjYy5DbGFzcyh7XG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxuXG4gICAgcHJvcGVydGllczoge1xuICAgICAgICBsYmxDb250ZW50OiBjYy5MYWJlbFxuICAgIH0sXG4gICAgc2hvd1BvcHVwKGJTaG93LCBzdHJDb250ZW50KXtcbiAgICAgICAgaWYoYlNob3cpe1xuICAgICAgICAgICAgdGhpcy5sYmxDb250ZW50LnN0cmluZyA9IHN0ckNvbnRlbnRcbiAgICAgICAgICAgIHRoaXMubm9kZS5vcGFjaXR5ID0gMDtcbiAgICAgICAgICAgIHRoaXMubm9kZS5zY2FsZSA9IDA7XG4gICAgICAgICAgICB0aGlzLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgICAgIGNjLnR3ZWVuKHRoaXMubm9kZSlcbiAgICAgICAgICAgICAgICAudG8oMC4yLCB7c2NhbGU6IDEuMCwgb3BhY2l0eTogMjU1fSlcbiAgICAgICAgICAgICAgICAuc3RhcnQoKTtcbiAgICAgICAgfWVsc2V7XG4gICAgICAgICAgICBjYy50d2Vlbih0aGlzLm5vZGUpXG4gICAgICAgICAgICAgICAgLnRvKDAuMiwge3NjYWxlOiAwLCBvcGFjaXR5OiAwfSlcbiAgICAgICAgICAgICAgICAuY2FsbCgoKT0+e3RoaXMubm9kZS5hY3RpdmUgPSBmYWxzZX0pXG4gICAgICAgICAgICAgICAgLnN0YXJ0KCk7XG4gICAgICAgIH1cblxuICAgIH0sXG4gICAgYnRuU3RhcnROZXdHYW1lKCl7XG4gICAgICAgIHRoaXMuZ2FtZVNjZW5lLmJ0bk5ld0dhbWVDbGljaygpO1xuICAgICAgICB0aGlzLnNob3dQb3B1cChmYWxzZSk7XG4gICAgfSxcbiAgICBidG5DbG9zZUNsaWNrKCl7XG4gICAgICAgIHRoaXMuc2hvd1BvcHVwKGZhbHNlKTtcbiAgICB9LFxuICAgIGN0b3IgKCkge1xuICAgIH0sXG5cbiAgICAvLyB1cGRhdGUgKGR0KSB7fSxcbn0pO1xuIl19