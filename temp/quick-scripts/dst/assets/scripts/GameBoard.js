
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/GameBoard.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '0f28aaIbo5MN6bLMv5aY3rC', 'GameBoard');
// scripts/GameBoard.js

"use strict";

var Decks = require("Decks");

var CardPrefab = require("CardPrefab");

var FBSDK = require("FbSdk");

var arrPosAnim = [[cc.v2(-200, 0), cc.v2(200, 0), cc.v2(-200, -250), cc.v2(200, -250), cc.v2(-200, -500), cc.v2(200, -500)], [cc.v2(0, 0), cc.v2(150, -550), cc.v2(-300, -250), cc.v2(300, -250), cc.v2(-150, -550)], [cc.v2(-350, -100), cc.v2(-200, -450), cc.v2(0, -100), cc.v2(200, -450), cc.v2(350, -100)]];
var typeAction = {
  GET_CARD_ON_DECK: 0,
  MOVE_CARD_DECK_TO_HOLDER: 1,
  MOVE_CARD_DECK_TO_TABLEGROUP: 2,
  MOVE_CARD_HOLDER_TO_TABLEGROUP: 3,
  MOVE_CARD_TABLE_TO_HOLDER: 4,
  MOVE_CARD_TABLE_TO_TABLE: 5
};
cc.Class({
  "extends": cc.Component,
  properties: {
    btnAutoComplete: cc.Node,
    btnHint: cc.Node,
    btnUndo: cc.Button,
    prefabCard: cc.Prefab,
    srpCardOnDeck: cc.Node,
    cardNodePool: {
      "default": null,
      hidden: false,
      type: cc.NodePool
    },
    cardHolderAnchor: [cc.Node],
    cardOnTableAnchor: [cc.Node],
    cardDeckAnchor: cc.Node,
    nodeCardContainer: cc.Node,
    btnGetCard: cc.Button,
    arrResourcesCard: [cc.SpriteFrame],
    listCardHolder: [],
    listIdCardHolder: [],
    listCardDeck: [],
    listIdCardDeck: [],
    listCardOnTable: [],
    listIdCardOnTable: [],
    arrCardMove: []
  },
  // LIFE-CYCLE CALLBACKS:
  ctor: function ctor() {
    this.gameScene = null;
    this.decks = new Decks();
    this.listCardHolder = [[], [], [], []];
    this.listIdCardHolder = [[], [], [], []];
    this.listCardDeck = [];
    this.listIdCardDeck = [];
    this.listCardOnTable = [];
    this.listIdCardOnTable = [];
    this.bAutoComplete = false;
    this.iScore = 0;
    this.iMove = 0;
    this.historyAction = [];
  },
  start: function start() {
    this.init();
  },
  init: function init() {
    this.srpCardOnDeck.zIndex = 100;
    this.cardNodePool = new cc.NodePool();

    for (var i = 0; i < 52; i++) {
      var card = cc.instantiate(this.prefabCard);
      this.cardNodePool.put(card);
    }

    this.resetGame();
    this.fbsdk = new FBSDK();
    this.fbsdk.init();
  },
  resetGame: function resetGame() {
    this.nodeCardContainer.removeAllChildren(true);
    this.listCardHolder = [[], [], [], []];
    this.listIdCardHolder = [[], [], [], []];
    this.listCardDeck = [];
    this.listIdCardDeck = [];
    this.listIdCardOnTable = [[], [], [], [], [], [], []];
    this.listCardOnTable = [[], [], [], [], [], [], []];
    this.iHiddenCard = 21;
    this.idxCurrentDeck = 0;
    this.arrGroupCardOnTable = []; // this.btnNewGame.node.active = true;

    this.btnGetCard.node.active = true;
    this.btnGetCard.interactable = false;
    this.setStateGetCard(false);
    this.bGameStart = false;
    this.bAutoComplete = false;
    this.btnAutoComplete.active = false;
    this.srpCardOnDeck.active = false;
    this.iScore = 1000;
    this.iMove = 0; // this.btnUndo.interactable = false;

    this.historyAction = [];
  },
  btnGetCardClick: function btnGetCardClick() {
    if (this.bGameStart) {
      this.gameScene.playEffect(SoundEffect.CLICK_SOUND);
      this.processGetCard();
    }
  },
  processGetCard: function processGetCard(sourcePos) {
    var _this = this;

    // cc.log("btnGetCardClick :" + this.listIdCardDeck);
    // cc.log("this.listCardDeck: " + this.listCardDeck.length);
    if (this.listIdCardDeck.length < 1) return;
    this.gameScene.playEffect(SoundEffect.FLIP_SOUND);
    this.setStateGetCard(this.idxCurrentDeck < this.listIdCardDeck.length - 1);

    if (this.idxCurrentDeck === this.listIdCardDeck.length) {
      this.idxCurrentDeck = 0;
      this.setStateGetCard(true);

      for (var i = 0; i < this.listCardDeck.length; i++) {
        this.listCardDeck[i].node.removeFromParent(true);
      }

      this.listCardDeck = [];
    } // }else{


    var objectHistory = {};
    objectHistory.typeAction = typeAction.GET_CARD_ON_DECK;
    this.historyAction.push(objectHistory);
    this.iMove++;
    this.iScore -= 5;
    var card = null;
    var cardId = this.listIdCardDeck[this.idxCurrentDeck];

    if (this.listCardDeck.length === 3) {
      for (var _i = 1; _i < 3; _i++) {
        var cardId2 = this.listIdCardDeck[this.idxCurrentDeck - _i];

        this.listCardDeck[2 - _i].setSpriteCard(cardId2, this.arrResourcesCard[cardId2]);

        this.listCardDeck[2 - _i].showCard(false);
      }

      card = this.listCardDeck[2];
      card.resetCard();
      card.setSpriteCard(cardId, this.arrResourcesCard[cardId]);
    } else {
      card = this.getCard(cardId);
      this.nodeCardContainer.addChild(card.node);

      if (this.listCardDeck.length > 0) {
        this.listCardDeck[this.listCardDeck.length - 1].bTouch = false;
      }

      this.listCardDeck.push(card);
    }

    card.node.position = this.btnGetCard.node.getPosition();
    if (!Utils.isEmpty(sourcePos)) card.node.position = sourcePos;
    card.node.zIndex = this.listCardDeck.length;
    var posX = this.cardDeckAnchor.getPosition().x + 50 * (this.listCardDeck.length - 1);
    var pos = cc.v2(posX, this.cardDeckAnchor.getPosition().y);
    card.oriPos = pos; // cc.log("cardDeck_" + this.listCardDeck.length + " has oriPos: " + pos.x + "-" + pos.y);

    card.oriIndex = this.listCardDeck.length;
    var iTime = this.bAutoComplete ? 0.05 : 0.1;
    cc.tween(card.node).to(iTime, {
      position: pos
    }).call(function () {
      card.showCard(true);
      if (_this.bAutoComplete) _this.autoComplete();
    }).start();
    this.idxCurrentDeck++;
    this.btnGetCard.node.active = !(this.listIdCardDeck.length < 2); // cc.log("btnGetCardClick :" + this.listIdCardDeck);
    // cc.log("this.listCardDeck: " + this.listCardDeck.length);
    // }
  },
  btnHintClick: function btnHintClick() {
    this.gameScene.playEffect(SoundEffect.CLICK_SOUND); // let self = this;
    // this.fbsdk.showRewardVideo(()=>{
    //     if(self.bGameStart)
    //         self.checkHint();
    // });

    if (this.bGameStart) this.checkHint();
  },
  btnAutoCompleteClick: function btnAutoCompleteClick() {
    if (this.bGameStart) {
      this.gameScene.playEffect(SoundEffect.CLICK_SOUND);
      this.bAutoComplete = true;
      this.btnAutoComplete.active = false;
      this.autoComplete();
    }
  },
  btnUndoClick: function btnUndoClick() {
    cc.log("========== Undo Click:" + this.historyAction.length);
    this.gameScene.playEffect(SoundEffect.CLICK_SOUND);

    if (this.historyAction.length > 0 && this.bGameStart) {
      this.iMove++;
      var lastAction = this.historyAction[this.historyAction.length - 1];

      if (lastAction.hasOwnProperty("cardIdActiveAfterMove") && !Utils.isEmpty(lastAction.cardIdActiveAfterMove)) {
        var arrGroupCard = this.listCardOnTable[lastAction.idxSource];
        var listIdCardOfGroup = this.listIdCardOnTable[lastAction.idxSource];
        var card = arrGroupCard[arrGroupCard.length - 1];
        card.resetCard();
        card.idCard = lastAction.cardIdActiveAfterMove;
        this.iHiddenCard++;
        listIdCardOfGroup.splice(listIdCardOfGroup.length - 1, 1);
      }

      switch (lastAction.typeAction) {
        case typeAction.GET_CARD_ON_DECK:
          this.idxCurrentDeck--;

          if (this.listCardDeck.length > 0) {
            this.listCardDeck[this.listCardDeck.length - 1].node.removeFromParent(true);
            this.listCardDeck.splice(this.listCardDeck.length - 1, 1);
          }

          if (this.listCardDeck.length > 2) {
            for (var i = this.listCardDeck.length, j = 0; i > 0; i--, j++) {
              var cardId = this.listIdCardDeck[this.idxCurrentDeck - 1 - j];
              this.listCardDeck[i - 1].setSpriteCard(cardId, this.arrResourcesCard[cardId]);
              this.listCardDeck[i - 1].showCard(false);
            }
          } else if (this.listCardDeck.length > 0) {// this.listCardDeck[this.listCardDeck.length - 1].node.removeFromParent(true);
            // this.listCardDeck.splice(this.listCardDeck.length - 1, 1);
          }

          if (this.listCardDeck.length > 0) {
            this.listCardDeck[this.listCardDeck.length - 1].bTouch = true;
          }

          this.setStateGetCard(true);
          this.btnGetCard.node.active = true;
          break;

        case typeAction.MOVE_CARD_DECK_TO_HOLDER:
          var _card = lastAction.arrCardMove[0];
          this.listIdCardDeck.splice(this.idxCurrentDeck, 0, _card.idCard);
          this.processGetCard(_card.oriPos);
          this.historyAction.splice(this.historyAction.length - 1, 1); // cc.log(this.idxCurrentDeck + " - this.listIdCardDeck undo: " + JSON.stringify(this.listIdCardDeck));

          this.listCardHolder[_card.groupId - 100][this.listCardHolder[_card.groupId - 100].length - 1].node.removeFromParent(true);
          this.listCardHolder[_card.groupId - 100].splice(this.listCardHolder[_card.groupId - 100].length - 1, 1);
          this.listIdCardHolder[_card.groupId - 100].splice(this.listCardHolder[_card.groupId - 100].length - 1, 1);
          break;

        case typeAction.MOVE_CARD_DECK_TO_TABLEGROUP:
          var card2 = lastAction.arrCardMove[0];
          this.listIdCardDeck.splice(this.idxCurrentDeck, 0, card2.idCard); // cc.log(this.idxCurrentDeck + " - this.listIdCardDeck undo: " + JSON.stringify(this.listIdCardDeck));

          this.processGetCard(card2.oriPos);
          this.historyAction.splice(this.historyAction.length - 1, 1);
          this.listCardOnTable[card2.groupId][this.listCardOnTable[card2.groupId].length - 1].node.removeFromParent(true);
          this.listCardOnTable[card2.groupId].splice(this.listCardOnTable[card2.groupId].length - 1, 1);
          this.listIdCardOnTable[card2.groupId].splice(this.listIdCardOnTable[card2.groupId].length - 1, 1);
          break;

        case typeAction.MOVE_CARD_HOLDER_TO_TABLEGROUP:
          this.moveCardToHolder(lastAction.arrCardMove, lastAction.idxSource);
          this.historyAction.splice(this.historyAction.length - 1, 1);
          break;

        case typeAction.MOVE_CARD_TABLE_TO_HOLDER:
          this.moveCardOnTables(lastAction.arrCardMove, lastAction.idxSource);
          this.historyAction.splice(this.historyAction.length - 1, 1);
          break;

        case typeAction.MOVE_CARD_TABLE_TO_TABLE:
          this.moveCardOnTables(lastAction.arrCardMove, lastAction.idxSource);
          this.historyAction.splice(this.historyAction.length - 1, 1);
          break;
      }

      this.historyAction.splice(this.historyAction.length - 1, 1); // cc.log("this.historyAction after:" + this.historyAction.length);
    }
  },
  showCardDeck: function showCardDeck() {
    this.idxCurrentDeck--;

    if (this.idxCurrentDeck > 2 && this.listCardDeck.length > 1) {
      for (var i = 0; i < this.listCardDeck.length; i++) {
        var cardId = this.listIdCardDeck[this.idxCurrentDeck - 3 + i];
        this.listCardDeck[i].setSpriteCard(cardId, this.arrResourcesCard[cardId]);
        this.listCardDeck[i].showCard(false);
        this.listCardDeck[i].bTouch = false;
      }

      var card = this.getCard(this.listIdCardDeck[this.idxCurrentDeck - 1]);
      this.nodeCardContainer.addChild(card.node);
      this.listCardDeck.push(card);
      var posX = this.cardDeckAnchor.getPosition().x + 50 * (this.listCardDeck.length - 1);
      card.oriPos = cc.v2(posX, this.cardDeckAnchor.getPosition().y);
      card.node.position = cc.v2(posX, this.cardDeckAnchor.getPosition().y);
      card.oriIndex = this.listCardDeck.length;
      card.node.zIndex = this.listCardDeck.length;
      card.showCard(false);
      card.bTouch = true;
    } else if (this.listCardDeck.length > 0) {
      this.listCardDeck[this.listCardDeck.length - 1].bTouch = true;
    }
  },
  setStateGetCard: function setStateGetCard(bState) {
    // true: getCard | false: Reload Deck
    this.btnGetCard.node.getComponent(cc.Sprite).spriteFrame = this.arrResourcesCard[bState ? 52 : 53];
  },
  startNewGame: function startNewGame() {
    var _this2 = this;

    this.fbsdk.showInterstitial(); // this.fbsdk.showBannerAds();

    this.resetGame();
    this.bGameStart = true;
    this.btnGetCard.interactable = true;
    this.setStateGetCard(true);
    var newDeck = this.decks.dealCard(); // newDeck = [46,15,47,39,42,2,1,34,41,4,32,16,38,24,14,18,19,28,8,23,37,20,0,33,0,21,4,13,27,8,12,50,7,1,22,17,36,3,10,31,40,48,44,30,43,5,26,9,25,6,51,35];
    // cc.log("newDeck: " + newDeck);

    this.listIdCardDeck = newDeck.slice(0, 24);
    this.idxCurrentDeck = 0;
    var listIdCardOnTable = newDeck.slice(24, newDeck.length);

    var _loop = function _loop(i, _idx) {
      var _loop2 = function _loop2(j) {
        var cardId = listIdCardOnTable[_idx];
        _idx++;

        var card = _this2.getCard(cardId);

        card.node.position = _this2.btnGetCard.node.getPosition();

        _this2.nodeCardContainer.addChild(card.node);

        card.node.zIndex = j;

        var _posX = _this2.cardOnTableAnchor[i].getPosition().x;

        var _posY = _this2.cardOnTableAnchor[i].getPosition().y - 25 * j;

        var _pos = cc.v2(_posX, _posY);

        card.oriPos = _pos;
        card.oriIndex = j;
        card.groupId = i;

        _this2.listCardOnTable[i].push(card);

        cc.tween(card.node).delay(0.3).to(0.3, {
          position: _pos
        }).delay(0.1 * j).call(function () {
          if (i === 0 && j === 0) {
            _this2.gameScene.playEffect(SoundEffect.DEAL_SOUND);
          }

          if (j === i) {
            // if(true) {
            card.showCard(true);

            _this2.listIdCardOnTable[i].push(cardId);
          }
        }).start();
      };

      for (var j = 0; j < i + 1; j++) {
        _loop2(j);
      }

      idx = _idx;
    };

    for (var i = 0, idx = 0; i < 7; i++) {
      _loop(i, idx);
    }
  },
  getCard: function getCard(cardId) {
    var card = this.cardNodePool.get();

    if (Utils.isEmpty(card)) {
      var cardPlus = cc.instantiate(this.prefabCard);
      this.cardNodePool.put(cardPlus);
      card = this.cardNodePool.get();
    }

    card.on(cc.Node.EventType.TOUCH_START, this.onBeginTouch, this, true);
    card.on(cc.Node.EventType.TOUCH_MOVE, this.onMoveCard, this, true);
    card.on(cc.Node.EventType.TOUCH_END, this.onEndTouch, this, true);
    card = card.getComponent(CardPrefab);
    card.setSpriteCard(cardId, this.arrResourcesCard[cardId]);
    return card;
  },
  onBeginTouch: function onBeginTouch(event) {
    this.gameScene.playEffect(SoundEffect.CLICK_SOUND); // this.arrCardMove = [];

    var cardSelect = event.target.getComponent(CardPrefab);

    if (!cardSelect.bTouch || this.arrCardMove.length > 0) {
      cc.log("double click....");

      for (var i = 0; i < this.arrCardMove.length; i++) {
        var card = this.arrCardMove[i];
        card.node.position = card.oriPos;
        card.node.zIndex = card.oriIndex;
      }

      this.arrCardMove = [];
      return;
    }

    if (cardSelect.groupId > -1 && cardSelect.groupId < 100) {
      //Card On Table
      var idxCardSelect = this.listCardOnTable[cardSelect.groupId].indexOf(cardSelect);

      for (var _i2 = idxCardSelect; _i2 < this.listCardOnTable[cardSelect.groupId].length; _i2++) {
        var _card2 = this.listCardOnTable[cardSelect.groupId][_i2];
        this.arrCardMove.push(_card2);
        _card2.node.zIndex = 100 + _i2;
      }
    } else {
      // Card On Deck or Holder
      this.arrCardMove.push(cardSelect);
      cc.log("cardSelect: " + 100);
      cardSelect.node.zIndex = 100;
    } // cc.log("event.target: " + cardSelect.idCard);

  },
  onMoveCard: function onMoveCard(event) {
    this.bMoveCard = true;
    var cardComp = event.target.getComponent(CardPrefab);
    if (!cardComp.bTouch) return;
    var delta = event.touch.getDelta(); // cc.log("onMoveCard: " + event);

    if (this.arrCardMove.length > 0) {
      for (var i = 0; i < this.arrCardMove.length; i++) {
        var card = this.arrCardMove[i];
        card.node.x += delta.x;
        card.node.y += delta.y;
      }
    }
  },
  onEndTouch: function onEndTouch(event) {
    var cardComp = event.target.getComponent(CardPrefab);
    if (!cardComp.bTouch) return; // cc.log("cardComp: " + cardComp.idCard);
    // cc.log("this.arrCardMove: " + this.arrCardMove.length);

    if (this.arrCardMove.length > 0) {
      if (this.bMoveCard && this.checkPosCardMoved(this.arrCardMove)) {} else if (!this.bMoveCard && this.autoMoveCard()) {} else {
        for (var i = 0; i < this.arrCardMove.length; i++) {
          var card = this.arrCardMove[i]; // cc.log("card.oriIndex: " + card.oriIndex)

          card.node.position = card.oriPos;
          card.node.zIndex = card.oriIndex;
          card.shakeCard();
        }
      }
    }

    this.arrCardMove = [];
    this.bMoveCard = false;
  },
  checkPosCardMoved: function checkPosCardMoved(arrCardMoved) {
    if (this.arrCardMove.length < 0) return false;
    var posEndTouch = arrCardMoved[0].node.getPosition();
    var rectCardMove = arrCardMoved[0].node.getBoundingBox(); //bounding Box Holder

    for (var i = 0; i < this.cardHolderAnchor.length; i++) {
      var rectHolder = this.cardHolderAnchor[i].getBoundingBox();

      if (rectHolder.contains(posEndTouch)) {
        var idxHolder = i;

        if (this.checkWithListHolder(arrCardMoved[0].idCard, idxHolder) !== -1 && arrCardMoved.length === 1 && arrCardMoved[0].groupId < 100) {
          this.moveCardToHolder(arrCardMoved[0], idxHolder);
          return true;
        }

        return false;
      }
    } //BoudingBox Group On Table


    for (var j = 0; j < this.listCardOnTable.length; j++) {
      var rectOfLastCard = null;
      if (this.listCardOnTable[j].length === 0) rectOfLastCard = this.cardOnTableAnchor[j].getBoundingBox();else rectOfLastCard = this.listCardOnTable[j][this.listCardOnTable[j].length - 1].node.getBoundingBox();

      if (rectOfLastCard.contains(posEndTouch) && j !== arrCardMoved[0].groupId) {
        var idxGroup = j;

        if (this.checkWithCardOnTable(arrCardMoved[0].idCard, idxGroup) !== -1) {
          this.moveCardOnTables(arrCardMoved, idxGroup);
          return true;
        }

        return false;
      }
    }

    return false;
  },
  autoMoveCard: function autoMoveCard() {
    if (this.arrCardMove.length < 0) return false;
    var holderValidate = this.checkWithListHolder(this.arrCardMove[0].idCard); // cc.log("holderValidate: " + holderValidate);

    var groupValidate = this.checkWithCardOnTable(this.arrCardMove[0].idCard); // cc.log("groupValidate: " + groupValidate);

    if (holderValidate !== -1 && this.arrCardMove.length === 1 && this.arrCardMove[0].groupId < 100) {
      this.moveCardToHolder(this.arrCardMove[0], holderValidate);
      return true;
    } else if (groupValidate !== -1 && groupValidate !== this.arrCardMove[0].groupId) {
      this.moveCardOnTables(this.arrCardMove, groupValidate);
      return true;
    }

    return false;
  },
  moveCardToHolder: function moveCardToHolder(cardMove, idxHolder) {
    var _this3 = this;

    this.gameScene.playEffect(SoundEffect.FLIP_SOUND);
    if (cardMove.groupId > 99) return; //Card FromHolder

    this.iScore += 100;
    this.iMove++;
    var objectHistory = {};
    var bCardFromDeck = cardMove.groupId === -1;

    if (bCardFromDeck) {
      objectHistory.typeAction = typeAction.MOVE_CARD_DECK_TO_HOLDER; // cc.log("this.listCardDeck:" + this.listCardDeck.length);

      objectHistory.idxSource = -1;
    } else {
      objectHistory.typeAction = typeAction.MOVE_CARD_TABLE_TO_HOLDER; // cc.log("this.listCardOnTable[" + cardMove.groupId + "] : " + this.listCardOnTable[cardMove.groupId].length);

      objectHistory.idxSource = cardMove.groupId;
    }

    objectHistory.idxDes = idxHolder;
    objectHistory.arrCardMove = [cardMove];
    var pos = this.cardHolderAnchor[idxHolder].getPosition();
    var iTime = this.bAutoComplete ? 0.1 : 0.2;
    cc.tween(cardMove.node).delay(0.1).to(iTime, {
      position: pos
    }).call(function () {
      cardMove.showEffectCard();
      cardMove.node.zIndex = cardMove.oriIndex;
      if (_this3.bAutoComplete) _this3.autoComplete();

      _this3.checkFinishGame();
    }).start(); // cardMove.node.position = this.cardHolderAnchor[idxHolder].getPosition();

    this.listCardHolder[idxHolder].push(cardMove);
    this.listIdCardHolder[idxHolder].push(cardMove.idCard);
    cardMove.oriIndex = this.listCardHolder[idxHolder].length;
    cardMove.oriPos = this.cardHolderAnchor[idxHolder].getPosition();
    var activeCardInGroup = null;

    if (!bCardFromDeck) {
      this.listCardOnTable[cardMove.groupId] = Utils.removeItemInArray(cardMove, this.listCardOnTable[cardMove.groupId]);
      this.listIdCardOnTable[cardMove.groupId] = Utils.removeItemInArray(cardMove.idCard, this.listIdCardOnTable[cardMove.groupId]);
      activeCardInGroup = this.checkCardActiveInGroup(cardMove.groupId);
    } else {
      this.listCardDeck = Utils.removeItemInArray(cardMove, this.listCardDeck);
      this.listIdCardDeck = Utils.removeItemInArray(cardMove.idCard, this.listIdCardDeck);
      var self = this;
      setTimeout(function () {
        self.showCardDeck();
      }, 200);
    }

    objectHistory.cardIdActiveAfterMove = activeCardInGroup;
    this.historyAction.push(objectHistory);
    cardMove.groupId = 100 + idxHolder;
  },
  moveCardOnTables: function moveCardOnTables(arrSource, idxGroup) {
    var _this4 = this;

    this.gameScene.playEffect(SoundEffect.FLIP_SOUND);
    this.iScore -= 5;
    this.iMove++;
    var objectHistory = {};
    var bCardFromDeck = arrSource[0].groupId === -1;
    var bCardFromHolder = arrSource[0].groupId > 99;

    if (bCardFromDeck) {
      // cc.log("this.listCardDeck:" + this.listCardDeck.length);
      objectHistory.typeAction = typeAction.MOVE_CARD_DECK_TO_TABLEGROUP;
    } else if (bCardFromHolder) {
      objectHistory.typeAction = typeAction.MOVE_CARD_HOLDER_TO_TABLEGROUP;
    } else {
      objectHistory.typeAction = typeAction.MOVE_CARD_TABLE_TO_TABLE;
    }

    objectHistory.idxSource = arrSource[0].groupId;
    objectHistory.idxDes = idxGroup;
    objectHistory.arrCardMove = arrSource; // cc.log("moveCardOnTables to: " + idxGroup)

    var desPos = null;
    if (this.listCardOnTable[idxGroup].length === 0) desPos = this.cardOnTableAnchor[idxGroup].getPosition();else {
      var lastCardPos = this.listCardOnTable[idxGroup][this.listCardOnTable[idxGroup].length - 1].node.getPosition();
      desPos = cc.v2(lastCardPos.x, lastCardPos.y - 50);
    }
    var activeCardInGroup = null;

    var _loop3 = function _loop3(i) {
      var card = arrSource[i]; // cc.log("cardId: " + card.idCard);

      var pos = cc.v2(desPos.x, desPos.y - 50 * i);
      cc.tween(card.node).to(0.2, {
        position: pos
      }).call(function () {
        card.node.zIndex = card.oriIndex;
        if (_this4.bAutoComplete && i === arrSource.length - 1) _this4.autoComplete();
      }).start();

      if (bCardFromDeck) {
        _this4.listCardDeck = Utils.removeItemInArray(card, _this4.listCardDeck);
        _this4.listIdCardDeck = Utils.removeItemInArray(card.idCard, _this4.listIdCardDeck);
        var self = _this4;
        setTimeout(function () {
          self.showCardDeck();
        }, 200);
      } else if (bCardFromHolder) {
        _this4.listCardHolder[card.groupId - 100] = Utils.removeItemInArray(card, _this4.listCardHolder[card.groupId - 100]);
        _this4.listIdCardHolder[card.groupId - 100] = Utils.removeItemInArray(card.idCard, _this4.listIdCardHolder[card.groupId - 100]);
      } else {
        _this4.listCardOnTable[card.groupId] = Utils.removeItemInArray(card, _this4.listCardOnTable[card.groupId]);
        _this4.listIdCardOnTable[card.groupId] = Utils.removeItemInArray(card.idCard, _this4.listIdCardOnTable[card.groupId]);

        if (i === arrSource.length - 1) {
          activeCardInGroup = _this4.checkCardActiveInGroup(card.groupId);
        }
      } // card.node.zIndex = this.listCardOnTable[idxGroup].length + i;


      card.oriIndex = _this4.listCardOnTable[idxGroup].length;

      _this4.listCardOnTable[idxGroup].push(card);

      _this4.listIdCardOnTable[idxGroup].push(card.idCard);

      card.oriPos = pos;
      card.groupId = idxGroup;
    };

    for (var i = 0; i < arrSource.length; i++) {
      _loop3(i);
    }

    objectHistory.cardIdActiveAfterMove = activeCardInGroup;
    this.historyAction.push(objectHistory);
  },
  checkWithListHolder: function checkWithListHolder(cardId, idxHolder) {
    // cc.log("checkWithListHolder: " + cardId + " - " + idxHolder)
    if (!Utils.isEmpty(idxHolder)) {
      if (CardUtils.validateCardHolder(cardId, this.listIdCardHolder[idxHolder])) {
        return idxHolder;
      }
    } else {
      for (var i = 0; i < this.listIdCardHolder.length; i++) {
        if (CardUtils.validateCardHolder(cardId, this.listIdCardHolder[i])) return i;
      }
    }

    return -1;
  },
  checkWithCardOnTable: function checkWithCardOnTable(cardId, idxGroup) {
    if (!Utils.isEmpty(idxGroup)) {
      if (CardUtils.validateCardOnBoard(cardId, this.listIdCardOnTable[idxGroup])) return idxGroup;
    } else {
      for (var i = 0; i < this.listIdCardOnTable.length; i++) {
        if (CardUtils.validateCardOnBoard(cardId, this.listIdCardOnTable[i])) return i;
      }
    }

    return -1;
  },
  checkCardActiveInGroup: function checkCardActiveInGroup(idxGroup) {
    // cc.log("checkCardActiveInGroup: " + idxGroup);
    if (idxGroup > -1 && idxGroup < 8) {
      var arrCard = this.listCardOnTable[idxGroup];
      var card = arrCard[arrCard.length - 1];

      if (arrCard.length > 0 && !card.bActive) {
        card.showCard(true);
        this.listIdCardOnTable[idxGroup].push(card.idCard);
        this.iHiddenCard--;

        if (this.iHiddenCard === 0) {
          // cc.log("You are Victory");
          this.btnAutoComplete.active = true;
        }

        return card.idCard;
      }
    }

    return null;
  },
  //    Check Hint
  checkHint: function checkHint() {
    var _this5 = this;

    this.iScore -= 10; // Check card On Table

    for (var i = 0; i < this.listCardOnTable.length; i++) {
      var listCardOfGroup = this.listCardOnTable[i];

      if (listCardOfGroup.length > 0) {
        //Check lastCard of Group match with Holder
        var idxHolder = this.checkWithListHolder(listCardOfGroup[listCardOfGroup.length - 1].idCard);

        if (idxHolder !== -1) {
          var card = listCardOfGroup[listCardOfGroup.length - 1];
          var pos = this.cardHolderAnchor[idxHolder].getPosition();
          this.moveCardHint([card], pos);
          return;
        } // Check card On Table Match each other
        //If FirstCardOfGroup is King -> return


        if (listCardOfGroup[0].bActive && listCardOfGroup[0].idCard > 47 && listCardOfGroup[0].idCard < 52) continue;
        var arrCard = [];

        for (var j = 0; j < listCardOfGroup.length; j++) {
          if (listCardOfGroup[j].bActive) arrCard.push(listCardOfGroup[j]);
        }

        var idxGroupValidate = this.checkWithCardOnTable(arrCard[0].idCard);

        if (idxGroupValidate !== -1) {
          var desPos = null;
          if (this.listCardOnTable[idxGroupValidate].length === 0) desPos = this.cardOnTableAnchor[idxGroupValidate].getPosition();else {
            var lastCardPos = this.listCardOnTable[idxGroupValidate][this.listCardOnTable[idxGroupValidate].length - 1].node.getPosition();
            desPos = cc.v2(lastCardPos.x, lastCardPos.y - 50);
          }
          this.moveCardHint(arrCard, desPos);
          return;
        }
      }
    } // Check card On Deck


    if (this.listCardDeck.length > 0) {
      var _card3 = this.listCardDeck[this.listCardDeck.length - 1]; //Check card Deck with Holder

      var _idxHolder = this.checkWithListHolder(this.listCardDeck[this.listCardDeck.length - 1].idCard);

      if (_idxHolder !== -1) {
        var posHolder = this.cardHolderAnchor[_idxHolder].getPosition();

        this.moveCardHint([_card3], posHolder);
        return;
      } //Check card Deck with All group


      var _idxGroupValidate = this.checkWithCardOnTable(this.listCardDeck[this.listCardDeck.length - 1].idCard);

      if (_idxGroupValidate !== -1) {
        var _desPos = null;
        if (this.listCardOnTable[_idxGroupValidate].length === 0) _desPos = this.cardOnTableAnchor[_idxGroupValidate].getPosition();else {
          var _lastCardPos = this.listCardOnTable[_idxGroupValidate][this.listCardOnTable[_idxGroupValidate].length - 1].node.getPosition();

          _desPos = cc.v2(_lastCardPos.x, _lastCardPos.y - 50);
        }
        this.moveCardHint([_card3], _desPos);
        return;
      }
    } //Check card On List Card Deck


    if (this.listIdCardDeck.length > 0) {
      for (var _i3 = 0; _i3 < this.listIdCardDeck.length; _i3++) {
        var idxHolderValidate = this.checkWithListHolder(this.listIdCardDeck[_i3]);
        var idxGroupvalidate = this.checkWithCardOnTable(this.listIdCardDeck[_i3]);

        if (idxGroupvalidate !== -1 || idxHolderValidate !== -1) {
          var desPosX = this.cardDeckAnchor.getPosition().x + 50 * (this.listCardDeck.length > 2 ? 2 : this.listCardDeck.length);
          this.srpCardOnDeck.active = true;
          cc.tween(this.srpCardOnDeck).to(0.2, {
            position: cc.v2(desPosX, this.cardDeckAnchor.getPosition().y)
          }).delay(0.2).to(0.2, {
            position: this.btnGetCard.node.getPosition()
          }).call(function () {
            _this5.srpCardOnDeck.active = false;
          }).start();
          return;
        }
      }
    }

    this.gameScene.popup.showPopup(true, "No card can be moved!");
  },
  moveCardHint: function moveCardHint(arrMoved, firstDesPos) {
    var _loop4 = function _loop4(i) {
      var card = arrMoved[i];
      var desPos = cc.v2(firstDesPos.x, firstDesPos.y - 50 * i);
      cc.tween(card.node).call(function () {
        card.showBorder(true);
        card.node.zIndex = 100 + i;
      }).to(0.2, {
        position: desPos
      }).delay(0.2).to(0.2, {
        position: card.oriPos
      }).call(function () {
        card.showBorder(false);
        card.node.zIndex = card.oriIndex;
      }).start();
    };

    for (var i = 0; i < arrMoved.length; i++) {
      _loop4(i);
    }
  },
  //    AutoComplete
  autoComplete: function autoComplete() {
    //Check last card on table
    for (var i = 0; i < this.listCardOnTable.length; i++) {
      var listCardOfGroup = this.listCardOnTable[i];

      if (listCardOfGroup.length > 0) {
        //Check lastCard of Group match with Holder
        var card = listCardOfGroup[listCardOfGroup.length - 1];
        var idxHolder = this.checkWithListHolder(card.idCard);

        if (idxHolder != -1) {
          card.node.zIndex = 100;
          this.moveCardToHolder(card, idxHolder);
          return; // this.autoComplete();
        }
      }
    } //Check last card of list Deck


    if (this.listCardDeck.length > 0) {
      var _card4 = this.listCardDeck[this.listCardDeck.length - 1]; //Check card Deck with Holder

      var _idxHolder2 = this.checkWithListHolder(_card4.idCard);

      if (_idxHolder2 !== -1) {
        _card4.node.zIndex = 100;
        this.moveCardToHolder(_card4, _idxHolder2); // this.autoComplete();

        return;
      }

      var idxGroupValidate = this.checkWithCardOnTable(_card4.idCard);

      if (idxGroupValidate !== -1) {
        _card4.node.zIndex = 100;
        this.moveCardOnTables([_card4], idxGroupValidate);
        return;
      }
    }

    if (this.listIdCardDeck.length > 0) {
      this.btnGetCardClick();
      return;
    }

    this.checkFinishGame();
  },
  checkFinishGame: function checkFinishGame() {
    for (var i = 0; i < this.listCardHolder.length; i++) {
      if (this.listCardHolder[i].length < 13) {
        return;
      }
    }

    this.bGameStart = false;
    this.processEndGame();
  },
  processEndGame: function processEndGame() {
    var _this6 = this;

    var randomPosAnim = arrPosAnim[Math.floor(Math.random() * 3)];

    var _loop5 = function _loop5(i) {
      var _loop6 = function _loop6(j) {
        var card = _this6.listCardHolder[i][j];
        var firstPos = randomPosAnim[(j + i) % randomPosAnim.length];
        var secondPos = randomPosAnim[(j + i + 1) % randomPosAnim.length];
        var thirdPos = randomPosAnim[(j + i + 2) % randomPosAnim.length];
        var fourthPos = randomPosAnim[(j + i + 3) % randomPosAnim.length];
        var fifthPos = randomPosAnim[(j + i + 4) % randomPosAnim.length];
        cc.tween(card.node).delay(2.0 + 0.1 * j + 0.2 * i).to(0.2, {
          position: firstPos
        }).delay(0.2 * (14 - j)).to(0.4, {
          position: secondPos
        }).to(0.4, {
          position: thirdPos
        }).to(0.4, {
          position: fourthPos
        }).to(0.4, {
          position: fifthPos
        }).delay(0.2 * j + 0.1 * i).to(0.2 + 0.01 * j, {
          position: card.oriPos
        }).call(function () {
          if (i === _this6.listCardHolder.length - 1 && j === _this6.listCardHolder[i].length - 1 && !_this6.gameScene.popup.node.active) {
            // cc.log("You are Victory")
            _this6.gameScene.popup.showPopup(true, "You are Victory!");
          }
        }).start();
      };

      for (var j = 0; j < _this6.listCardHolder[i].length; j++) {
        _loop6(j);
      }
    };

    for (var i = 0; i < this.listCardHolder.length; i++) {
      _loop5(i);
    }
  }
});

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9zY3JpcHRzL0dhbWVCb2FyZC5qcyJdLCJuYW1lcyI6WyJEZWNrcyIsInJlcXVpcmUiLCJDYXJkUHJlZmFiIiwiRkJTREsiLCJhcnJQb3NBbmltIiwiY2MiLCJ2MiIsInR5cGVBY3Rpb24iLCJHRVRfQ0FSRF9PTl9ERUNLIiwiTU9WRV9DQVJEX0RFQ0tfVE9fSE9MREVSIiwiTU9WRV9DQVJEX0RFQ0tfVE9fVEFCTEVHUk9VUCIsIk1PVkVfQ0FSRF9IT0xERVJfVE9fVEFCTEVHUk9VUCIsIk1PVkVfQ0FSRF9UQUJMRV9UT19IT0xERVIiLCJNT1ZFX0NBUkRfVEFCTEVfVE9fVEFCTEUiLCJDbGFzcyIsIkNvbXBvbmVudCIsInByb3BlcnRpZXMiLCJidG5BdXRvQ29tcGxldGUiLCJOb2RlIiwiYnRuSGludCIsImJ0blVuZG8iLCJCdXR0b24iLCJwcmVmYWJDYXJkIiwiUHJlZmFiIiwic3JwQ2FyZE9uRGVjayIsImNhcmROb2RlUG9vbCIsImhpZGRlbiIsInR5cGUiLCJOb2RlUG9vbCIsImNhcmRIb2xkZXJBbmNob3IiLCJjYXJkT25UYWJsZUFuY2hvciIsImNhcmREZWNrQW5jaG9yIiwibm9kZUNhcmRDb250YWluZXIiLCJidG5HZXRDYXJkIiwiYXJyUmVzb3VyY2VzQ2FyZCIsIlNwcml0ZUZyYW1lIiwibGlzdENhcmRIb2xkZXIiLCJsaXN0SWRDYXJkSG9sZGVyIiwibGlzdENhcmREZWNrIiwibGlzdElkQ2FyZERlY2siLCJsaXN0Q2FyZE9uVGFibGUiLCJsaXN0SWRDYXJkT25UYWJsZSIsImFyckNhcmRNb3ZlIiwiY3RvciIsImdhbWVTY2VuZSIsImRlY2tzIiwiYkF1dG9Db21wbGV0ZSIsImlTY29yZSIsImlNb3ZlIiwiaGlzdG9yeUFjdGlvbiIsInN0YXJ0IiwiaW5pdCIsInpJbmRleCIsImkiLCJjYXJkIiwiaW5zdGFudGlhdGUiLCJwdXQiLCJyZXNldEdhbWUiLCJmYnNkayIsInJlbW92ZUFsbENoaWxkcmVuIiwiaUhpZGRlbkNhcmQiLCJpZHhDdXJyZW50RGVjayIsImFyckdyb3VwQ2FyZE9uVGFibGUiLCJub2RlIiwiYWN0aXZlIiwiaW50ZXJhY3RhYmxlIiwic2V0U3RhdGVHZXRDYXJkIiwiYkdhbWVTdGFydCIsImJ0bkdldENhcmRDbGljayIsInBsYXlFZmZlY3QiLCJTb3VuZEVmZmVjdCIsIkNMSUNLX1NPVU5EIiwicHJvY2Vzc0dldENhcmQiLCJzb3VyY2VQb3MiLCJsZW5ndGgiLCJGTElQX1NPVU5EIiwicmVtb3ZlRnJvbVBhcmVudCIsIm9iamVjdEhpc3RvcnkiLCJwdXNoIiwiY2FyZElkIiwiY2FyZElkMiIsInNldFNwcml0ZUNhcmQiLCJzaG93Q2FyZCIsInJlc2V0Q2FyZCIsImdldENhcmQiLCJhZGRDaGlsZCIsImJUb3VjaCIsInBvc2l0aW9uIiwiZ2V0UG9zaXRpb24iLCJVdGlscyIsImlzRW1wdHkiLCJwb3NYIiwieCIsInBvcyIsInkiLCJvcmlQb3MiLCJvcmlJbmRleCIsImlUaW1lIiwidHdlZW4iLCJ0byIsImNhbGwiLCJhdXRvQ29tcGxldGUiLCJidG5IaW50Q2xpY2siLCJjaGVja0hpbnQiLCJidG5BdXRvQ29tcGxldGVDbGljayIsImJ0blVuZG9DbGljayIsImxvZyIsImxhc3RBY3Rpb24iLCJoYXNPd25Qcm9wZXJ0eSIsImNhcmRJZEFjdGl2ZUFmdGVyTW92ZSIsImFyckdyb3VwQ2FyZCIsImlkeFNvdXJjZSIsImxpc3RJZENhcmRPZkdyb3VwIiwiaWRDYXJkIiwic3BsaWNlIiwiaiIsImdyb3VwSWQiLCJjYXJkMiIsIm1vdmVDYXJkVG9Ib2xkZXIiLCJtb3ZlQ2FyZE9uVGFibGVzIiwic2hvd0NhcmREZWNrIiwiYlN0YXRlIiwiZ2V0Q29tcG9uZW50IiwiU3ByaXRlIiwic3ByaXRlRnJhbWUiLCJzdGFydE5ld0dhbWUiLCJzaG93SW50ZXJzdGl0aWFsIiwibmV3RGVjayIsImRlYWxDYXJkIiwic2xpY2UiLCJpZHgiLCJfcG9zWCIsIl9wb3NZIiwiX3BvcyIsImRlbGF5IiwiREVBTF9TT1VORCIsImdldCIsImNhcmRQbHVzIiwib24iLCJFdmVudFR5cGUiLCJUT1VDSF9TVEFSVCIsIm9uQmVnaW5Ub3VjaCIsIlRPVUNIX01PVkUiLCJvbk1vdmVDYXJkIiwiVE9VQ0hfRU5EIiwib25FbmRUb3VjaCIsImV2ZW50IiwiY2FyZFNlbGVjdCIsInRhcmdldCIsImlkeENhcmRTZWxlY3QiLCJpbmRleE9mIiwiYk1vdmVDYXJkIiwiY2FyZENvbXAiLCJkZWx0YSIsInRvdWNoIiwiZ2V0RGVsdGEiLCJjaGVja1Bvc0NhcmRNb3ZlZCIsImF1dG9Nb3ZlQ2FyZCIsInNoYWtlQ2FyZCIsImFyckNhcmRNb3ZlZCIsInBvc0VuZFRvdWNoIiwicmVjdENhcmRNb3ZlIiwiZ2V0Qm91bmRpbmdCb3giLCJyZWN0SG9sZGVyIiwiY29udGFpbnMiLCJpZHhIb2xkZXIiLCJjaGVja1dpdGhMaXN0SG9sZGVyIiwicmVjdE9mTGFzdENhcmQiLCJpZHhHcm91cCIsImNoZWNrV2l0aENhcmRPblRhYmxlIiwiaG9sZGVyVmFsaWRhdGUiLCJncm91cFZhbGlkYXRlIiwiY2FyZE1vdmUiLCJiQ2FyZEZyb21EZWNrIiwiaWR4RGVzIiwic2hvd0VmZmVjdENhcmQiLCJjaGVja0ZpbmlzaEdhbWUiLCJhY3RpdmVDYXJkSW5Hcm91cCIsInJlbW92ZUl0ZW1JbkFycmF5IiwiY2hlY2tDYXJkQWN0aXZlSW5Hcm91cCIsInNlbGYiLCJzZXRUaW1lb3V0IiwiYXJyU291cmNlIiwiYkNhcmRGcm9tSG9sZGVyIiwiZGVzUG9zIiwibGFzdENhcmRQb3MiLCJDYXJkVXRpbHMiLCJ2YWxpZGF0ZUNhcmRIb2xkZXIiLCJ2YWxpZGF0ZUNhcmRPbkJvYXJkIiwiYXJyQ2FyZCIsImJBY3RpdmUiLCJsaXN0Q2FyZE9mR3JvdXAiLCJtb3ZlQ2FyZEhpbnQiLCJpZHhHcm91cFZhbGlkYXRlIiwicG9zSG9sZGVyIiwiaWR4SG9sZGVyVmFsaWRhdGUiLCJpZHhHcm91cHZhbGlkYXRlIiwiZGVzUG9zWCIsInBvcHVwIiwic2hvd1BvcHVwIiwiYXJyTW92ZWQiLCJmaXJzdERlc1BvcyIsInNob3dCb3JkZXIiLCJwcm9jZXNzRW5kR2FtZSIsInJhbmRvbVBvc0FuaW0iLCJNYXRoIiwiZmxvb3IiLCJyYW5kb20iLCJmaXJzdFBvcyIsInNlY29uZFBvcyIsInRoaXJkUG9zIiwiZm91cnRoUG9zIiwiZmlmdGhQb3MiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsSUFBSUEsS0FBSyxHQUFHQyxPQUFPLENBQUMsT0FBRCxDQUFuQjs7QUFDQSxJQUFJQyxVQUFVLEdBQUdELE9BQU8sQ0FBQyxZQUFELENBQXhCOztBQUNBLElBQUlFLEtBQUssR0FBR0YsT0FBTyxDQUFDLE9BQUQsQ0FBbkI7O0FBQ0EsSUFBTUcsVUFBVSxHQUFHLENBQUMsQ0FBQ0MsRUFBRSxDQUFDQyxFQUFILENBQU0sQ0FBQyxHQUFQLEVBQVksQ0FBWixDQUFELEVBQWlCRCxFQUFFLENBQUNDLEVBQUgsQ0FBTSxHQUFOLEVBQVcsQ0FBWCxDQUFqQixFQUFnQ0QsRUFBRSxDQUFDQyxFQUFILENBQU0sQ0FBQyxHQUFQLEVBQVksQ0FBQyxHQUFiLENBQWhDLEVBQW1ERCxFQUFFLENBQUNDLEVBQUgsQ0FBTSxHQUFOLEVBQVcsQ0FBQyxHQUFaLENBQW5ELEVBQXFFRCxFQUFFLENBQUNDLEVBQUgsQ0FBTSxDQUFDLEdBQVAsRUFBWSxDQUFDLEdBQWIsQ0FBckUsRUFBd0ZELEVBQUUsQ0FBQ0MsRUFBSCxDQUFNLEdBQU4sRUFBVyxDQUFDLEdBQVosQ0FBeEYsQ0FBRCxFQUNmLENBQUNELEVBQUUsQ0FBQ0MsRUFBSCxDQUFNLENBQU4sRUFBUyxDQUFULENBQUQsRUFBY0QsRUFBRSxDQUFDQyxFQUFILENBQU0sR0FBTixFQUFXLENBQUMsR0FBWixDQUFkLEVBQWdDRCxFQUFFLENBQUNDLEVBQUgsQ0FBTSxDQUFDLEdBQVAsRUFBWSxDQUFDLEdBQWIsQ0FBaEMsRUFBbURELEVBQUUsQ0FBQ0MsRUFBSCxDQUFNLEdBQU4sRUFBVyxDQUFDLEdBQVosQ0FBbkQsRUFBcUVELEVBQUUsQ0FBQ0MsRUFBSCxDQUFNLENBQUMsR0FBUCxFQUFZLENBQUMsR0FBYixDQUFyRSxDQURlLEVBRWYsQ0FBQ0QsRUFBRSxDQUFDQyxFQUFILENBQU0sQ0FBQyxHQUFQLEVBQVksQ0FBQyxHQUFiLENBQUQsRUFBb0JELEVBQUUsQ0FBQ0MsRUFBSCxDQUFNLENBQUMsR0FBUCxFQUFZLENBQUMsR0FBYixDQUFwQixFQUF1Q0QsRUFBRSxDQUFDQyxFQUFILENBQU0sQ0FBTixFQUFTLENBQUMsR0FBVixDQUF2QyxFQUF1REQsRUFBRSxDQUFDQyxFQUFILENBQU0sR0FBTixFQUFXLENBQUMsR0FBWixDQUF2RCxFQUF5RUQsRUFBRSxDQUFDQyxFQUFILENBQU0sR0FBTixFQUFXLENBQUMsR0FBWixDQUF6RSxDQUZlLENBQW5CO0FBR0EsSUFBTUMsVUFBVSxHQUFHO0FBQ2ZDLEVBQUFBLGdCQUFnQixFQUFFLENBREg7QUFFZkMsRUFBQUEsd0JBQXdCLEVBQUUsQ0FGWDtBQUdmQyxFQUFBQSw0QkFBNEIsRUFBRSxDQUhmO0FBSWZDLEVBQUFBLDhCQUE4QixFQUFFLENBSmpCO0FBS2ZDLEVBQUFBLHlCQUF5QixFQUFDLENBTFg7QUFNZkMsRUFBQUEsd0JBQXdCLEVBQUU7QUFOWCxDQUFuQjtBQVNBUixFQUFFLENBQUNTLEtBQUgsQ0FBUztBQUNMLGFBQVNULEVBQUUsQ0FBQ1UsU0FEUDtBQUdMQyxFQUFBQSxVQUFVLEVBQUU7QUFDUkMsSUFBQUEsZUFBZSxFQUFFWixFQUFFLENBQUNhLElBRFo7QUFFUkMsSUFBQUEsT0FBTyxFQUFFZCxFQUFFLENBQUNhLElBRko7QUFHUkUsSUFBQUEsT0FBTyxFQUFFZixFQUFFLENBQUNnQixNQUhKO0FBSVJDLElBQUFBLFVBQVUsRUFBRWpCLEVBQUUsQ0FBQ2tCLE1BSlA7QUFLUkMsSUFBQUEsYUFBYSxFQUFFbkIsRUFBRSxDQUFDYSxJQUxWO0FBTVJPLElBQUFBLFlBQVksRUFBQztBQUNULGlCQUFTLElBREE7QUFFVEMsTUFBQUEsTUFBTSxFQUFFLEtBRkM7QUFHVEMsTUFBQUEsSUFBSSxFQUFFdEIsRUFBRSxDQUFDdUI7QUFIQSxLQU5MO0FBV1JDLElBQUFBLGdCQUFnQixFQUFFLENBQUN4QixFQUFFLENBQUNhLElBQUosQ0FYVjtBQVlSWSxJQUFBQSxpQkFBaUIsRUFBRSxDQUFDekIsRUFBRSxDQUFDYSxJQUFKLENBWlg7QUFhUmEsSUFBQUEsY0FBYyxFQUFDMUIsRUFBRSxDQUFDYSxJQWJWO0FBY1JjLElBQUFBLGlCQUFpQixFQUFFM0IsRUFBRSxDQUFDYSxJQWRkO0FBZVJlLElBQUFBLFVBQVUsRUFBRTVCLEVBQUUsQ0FBQ2dCLE1BZlA7QUFnQlJhLElBQUFBLGdCQUFnQixFQUFFLENBQUM3QixFQUFFLENBQUM4QixXQUFKLENBaEJWO0FBbUJSQyxJQUFBQSxjQUFjLEVBQUUsRUFuQlI7QUFvQlJDLElBQUFBLGdCQUFnQixFQUFDLEVBcEJUO0FBc0JSQyxJQUFBQSxZQUFZLEVBQUUsRUF0Qk47QUF1QlJDLElBQUFBLGNBQWMsRUFBRSxFQXZCUjtBQXlCUkMsSUFBQUEsZUFBZSxFQUFFLEVBekJUO0FBMEJSQyxJQUFBQSxpQkFBaUIsRUFBRSxFQTFCWDtBQTJCUkMsSUFBQUEsV0FBVyxFQUFDO0FBM0JKLEdBSFA7QUFpQ0w7QUFDQUMsRUFBQUEsSUFsQ0ssa0JBa0NDO0FBQ0YsU0FBS0MsU0FBTCxHQUFpQixJQUFqQjtBQUNBLFNBQUtDLEtBQUwsR0FBYSxJQUFJN0MsS0FBSixFQUFiO0FBQ0EsU0FBS29DLGNBQUwsR0FBc0IsQ0FBQyxFQUFELEVBQUksRUFBSixFQUFPLEVBQVAsRUFBVSxFQUFWLENBQXRCO0FBQ0EsU0FBS0MsZ0JBQUwsR0FBd0IsQ0FBQyxFQUFELEVBQUksRUFBSixFQUFPLEVBQVAsRUFBVSxFQUFWLENBQXhCO0FBRUEsU0FBS0MsWUFBTCxHQUFvQixFQUFwQjtBQUNBLFNBQUtDLGNBQUwsR0FBc0IsRUFBdEI7QUFFQSxTQUFLQyxlQUFMLEdBQXVCLEVBQXZCO0FBQ0EsU0FBS0MsaUJBQUwsR0FBeUIsRUFBekI7QUFDQSxTQUFLSyxhQUFMLEdBQXFCLEtBQXJCO0FBRUEsU0FBS0MsTUFBTCxHQUFjLENBQWQ7QUFDQSxTQUFLQyxLQUFMLEdBQWEsQ0FBYjtBQUNBLFNBQUtDLGFBQUwsR0FBcUIsRUFBckI7QUFDSCxHQWxESTtBQW9ETEMsRUFBQUEsS0FwREssbUJBb0RJO0FBQ0wsU0FBS0MsSUFBTDtBQUNILEdBdERJO0FBdURMQSxFQUFBQSxJQXZESyxrQkF1REM7QUFDRixTQUFLM0IsYUFBTCxDQUFtQjRCLE1BQW5CLEdBQTRCLEdBQTVCO0FBQ0EsU0FBSzNCLFlBQUwsR0FBb0IsSUFBSXBCLEVBQUUsQ0FBQ3VCLFFBQVAsRUFBcEI7O0FBQ0EsU0FBSSxJQUFJeUIsQ0FBQyxHQUFHLENBQVosRUFBZUEsQ0FBQyxHQUFHLEVBQW5CLEVBQXVCQSxDQUFDLEVBQXhCLEVBQTJCO0FBQ3ZCLFVBQUlDLElBQUksR0FBR2pELEVBQUUsQ0FBQ2tELFdBQUgsQ0FBZSxLQUFLakMsVUFBcEIsQ0FBWDtBQUNBLFdBQUtHLFlBQUwsQ0FBa0IrQixHQUFsQixDQUFzQkYsSUFBdEI7QUFDSDs7QUFDRCxTQUFLRyxTQUFMO0FBQ0EsU0FBS0MsS0FBTCxHQUFhLElBQUl2RCxLQUFKLEVBQWI7QUFDQSxTQUFLdUQsS0FBTCxDQUFXUCxJQUFYO0FBQ0gsR0FqRUk7QUFrRUxNLEVBQUFBLFNBbEVLLHVCQWtFTTtBQUNQLFNBQUt6QixpQkFBTCxDQUF1QjJCLGlCQUF2QixDQUF5QyxJQUF6QztBQUVBLFNBQUt2QixjQUFMLEdBQXNCLENBQUMsRUFBRCxFQUFJLEVBQUosRUFBTyxFQUFQLEVBQVUsRUFBVixDQUF0QjtBQUNBLFNBQUtDLGdCQUFMLEdBQXdCLENBQUMsRUFBRCxFQUFJLEVBQUosRUFBTyxFQUFQLEVBQVUsRUFBVixDQUF4QjtBQUVBLFNBQUtDLFlBQUwsR0FBb0IsRUFBcEI7QUFDQSxTQUFLQyxjQUFMLEdBQXNCLEVBQXRCO0FBRUEsU0FBS0UsaUJBQUwsR0FBeUIsQ0FBQyxFQUFELEVBQUksRUFBSixFQUFPLEVBQVAsRUFBVSxFQUFWLEVBQWEsRUFBYixFQUFnQixFQUFoQixFQUFtQixFQUFuQixDQUF6QjtBQUNBLFNBQUtELGVBQUwsR0FBdUIsQ0FBQyxFQUFELEVBQUksRUFBSixFQUFPLEVBQVAsRUFBVSxFQUFWLEVBQWEsRUFBYixFQUFnQixFQUFoQixFQUFtQixFQUFuQixDQUF2QjtBQUVBLFNBQUtvQixXQUFMLEdBQW1CLEVBQW5CO0FBQ0EsU0FBS0MsY0FBTCxHQUFzQixDQUF0QjtBQUNBLFNBQUtDLG1CQUFMLEdBQTJCLEVBQTNCLENBZE8sQ0FlUDs7QUFDQSxTQUFLN0IsVUFBTCxDQUFnQjhCLElBQWhCLENBQXFCQyxNQUFyQixHQUE4QixJQUE5QjtBQUNBLFNBQUsvQixVQUFMLENBQWdCZ0MsWUFBaEIsR0FBK0IsS0FBL0I7QUFDQSxTQUFLQyxlQUFMLENBQXFCLEtBQXJCO0FBQ0EsU0FBS0MsVUFBTCxHQUFrQixLQUFsQjtBQUNBLFNBQUtyQixhQUFMLEdBQXFCLEtBQXJCO0FBQ0EsU0FBSzdCLGVBQUwsQ0FBcUIrQyxNQUFyQixHQUE4QixLQUE5QjtBQUNBLFNBQUt4QyxhQUFMLENBQW1Cd0MsTUFBbkIsR0FBNEIsS0FBNUI7QUFDQSxTQUFLakIsTUFBTCxHQUFjLElBQWQ7QUFDQSxTQUFLQyxLQUFMLEdBQWEsQ0FBYixDQXhCTyxDQXlCUDs7QUFDQSxTQUFLQyxhQUFMLEdBQXFCLEVBQXJCO0FBQ0gsR0E3Rkk7QUE4RkxtQixFQUFBQSxlQTlGSyw2QkE4Rlk7QUFDYixRQUFHLEtBQUtELFVBQVIsRUFBbUI7QUFDZixXQUFLdkIsU0FBTCxDQUFleUIsVUFBZixDQUEwQkMsV0FBVyxDQUFDQyxXQUF0QztBQUNBLFdBQUtDLGNBQUw7QUFDSDtBQUNKLEdBbkdJO0FBb0dMQSxFQUFBQSxjQXBHSywwQkFvR1VDLFNBcEdWLEVBb0dvQjtBQUFBOztBQUNyQjtBQUNBO0FBRUEsUUFBRyxLQUFLbEMsY0FBTCxDQUFvQm1DLE1BQXBCLEdBQTZCLENBQWhDLEVBQW1DO0FBQ25DLFNBQUs5QixTQUFMLENBQWV5QixVQUFmLENBQTBCQyxXQUFXLENBQUNLLFVBQXRDO0FBQ0EsU0FBS1QsZUFBTCxDQUFxQixLQUFLTCxjQUFMLEdBQXNCLEtBQUt0QixjQUFMLENBQW9CbUMsTUFBcEIsR0FBNkIsQ0FBeEU7O0FBQ0EsUUFBRyxLQUFLYixjQUFMLEtBQXdCLEtBQUt0QixjQUFMLENBQW9CbUMsTUFBL0MsRUFBdUQ7QUFDbkQsV0FBS2IsY0FBTCxHQUFzQixDQUF0QjtBQUNBLFdBQUtLLGVBQUwsQ0FBcUIsSUFBckI7O0FBQ0EsV0FBSyxJQUFJYixDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHLEtBQUtmLFlBQUwsQ0FBa0JvQyxNQUF0QyxFQUE4Q3JCLENBQUMsRUFBL0MsRUFBbUQ7QUFDL0MsYUFBS2YsWUFBTCxDQUFrQmUsQ0FBbEIsRUFBcUJVLElBQXJCLENBQTBCYSxnQkFBMUIsQ0FBMkMsSUFBM0M7QUFDSDs7QUFDRCxXQUFLdEMsWUFBTCxHQUFvQixFQUFwQjtBQUNILEtBZG9CLENBZXJCOzs7QUFDQSxRQUFJdUMsYUFBYSxHQUFHLEVBQXBCO0FBQ0FBLElBQUFBLGFBQWEsQ0FBQ3RFLFVBQWQsR0FBMkJBLFVBQVUsQ0FBQ0MsZ0JBQXRDO0FBQ0EsU0FBS3lDLGFBQUwsQ0FBbUI2QixJQUFuQixDQUF3QkQsYUFBeEI7QUFDQSxTQUFLN0IsS0FBTDtBQUNBLFNBQUtELE1BQUwsSUFBZSxDQUFmO0FBQ0ksUUFBSU8sSUFBSSxHQUFHLElBQVg7QUFDQSxRQUFJeUIsTUFBTSxHQUFHLEtBQUt4QyxjQUFMLENBQW9CLEtBQUtzQixjQUF6QixDQUFiOztBQUNBLFFBQUcsS0FBS3ZCLFlBQUwsQ0FBa0JvQyxNQUFsQixLQUE2QixDQUFoQyxFQUFrQztBQUM5QixXQUFJLElBQUlyQixFQUFDLEdBQUcsQ0FBWixFQUFlQSxFQUFDLEdBQUcsQ0FBbkIsRUFBc0JBLEVBQUMsRUFBdkIsRUFBMEI7QUFDdEIsWUFBSTJCLE9BQU8sR0FBRyxLQUFLekMsY0FBTCxDQUFvQixLQUFLc0IsY0FBTCxHQUFzQlIsRUFBMUMsQ0FBZDs7QUFDQSxhQUFLZixZQUFMLENBQWtCLElBQUVlLEVBQXBCLEVBQXVCNEIsYUFBdkIsQ0FBcUNELE9BQXJDLEVBQThDLEtBQUs5QyxnQkFBTCxDQUFzQjhDLE9BQXRCLENBQTlDOztBQUNBLGFBQUsxQyxZQUFMLENBQWtCLElBQUVlLEVBQXBCLEVBQXVCNkIsUUFBdkIsQ0FBZ0MsS0FBaEM7QUFDSDs7QUFDRDVCLE1BQUFBLElBQUksR0FBRyxLQUFLaEIsWUFBTCxDQUFrQixDQUFsQixDQUFQO0FBQ0FnQixNQUFBQSxJQUFJLENBQUM2QixTQUFMO0FBQ0E3QixNQUFBQSxJQUFJLENBQUMyQixhQUFMLENBQW1CRixNQUFuQixFQUEyQixLQUFLN0MsZ0JBQUwsQ0FBc0I2QyxNQUF0QixDQUEzQjtBQUNILEtBVEQsTUFTSztBQUNEekIsTUFBQUEsSUFBSSxHQUFHLEtBQUs4QixPQUFMLENBQWFMLE1BQWIsQ0FBUDtBQUNBLFdBQUsvQyxpQkFBTCxDQUF1QnFELFFBQXZCLENBQWdDL0IsSUFBSSxDQUFDUyxJQUFyQzs7QUFDQSxVQUFHLEtBQUt6QixZQUFMLENBQWtCb0MsTUFBbEIsR0FBMkIsQ0FBOUIsRUFBZ0M7QUFDNUIsYUFBS3BDLFlBQUwsQ0FBa0IsS0FBS0EsWUFBTCxDQUFrQm9DLE1BQWxCLEdBQTJCLENBQTdDLEVBQWdEWSxNQUFoRCxHQUF5RCxLQUF6RDtBQUNIOztBQUVELFdBQUtoRCxZQUFMLENBQWtCd0MsSUFBbEIsQ0FBdUJ4QixJQUF2QjtBQUVIOztBQUNEQSxJQUFBQSxJQUFJLENBQUNTLElBQUwsQ0FBVXdCLFFBQVYsR0FBcUIsS0FBS3RELFVBQUwsQ0FBZ0I4QixJQUFoQixDQUFxQnlCLFdBQXJCLEVBQXJCO0FBQ0EsUUFBRyxDQUFDQyxLQUFLLENBQUNDLE9BQU4sQ0FBY2pCLFNBQWQsQ0FBSixFQUNJbkIsSUFBSSxDQUFDUyxJQUFMLENBQVV3QixRQUFWLEdBQXFCZCxTQUFyQjtBQUNKbkIsSUFBQUEsSUFBSSxDQUFDUyxJQUFMLENBQVVYLE1BQVYsR0FBbUIsS0FBS2QsWUFBTCxDQUFrQm9DLE1BQXJDO0FBQ0EsUUFBSWlCLElBQUksR0FBRyxLQUFLNUQsY0FBTCxDQUFvQnlELFdBQXBCLEdBQWtDSSxDQUFsQyxHQUFzQyxNQUFJLEtBQUt0RCxZQUFMLENBQWtCb0MsTUFBbEIsR0FBMkIsQ0FBL0IsQ0FBakQ7QUFDQSxRQUFJbUIsR0FBRyxHQUFHeEYsRUFBRSxDQUFDQyxFQUFILENBQU1xRixJQUFOLEVBQVksS0FBSzVELGNBQUwsQ0FBb0J5RCxXQUFwQixHQUFrQ00sQ0FBOUMsQ0FBVjtBQUNBeEMsSUFBQUEsSUFBSSxDQUFDeUMsTUFBTCxHQUFjRixHQUFkLENBaERpQixDQWlEakI7O0FBQ0F2QyxJQUFBQSxJQUFJLENBQUMwQyxRQUFMLEdBQWdCLEtBQUsxRCxZQUFMLENBQWtCb0MsTUFBbEM7QUFDQSxRQUFJdUIsS0FBSyxHQUFHLEtBQUtuRCxhQUFMLEdBQXFCLElBQXJCLEdBQTRCLEdBQXhDO0FBQ0F6QyxJQUFBQSxFQUFFLENBQUM2RixLQUFILENBQVM1QyxJQUFJLENBQUNTLElBQWQsRUFDS29DLEVBREwsQ0FDUUYsS0FEUixFQUNlO0FBQUNWLE1BQUFBLFFBQVEsRUFBRU07QUFBWCxLQURmLEVBRUtPLElBRkwsQ0FFVSxZQUFJO0FBQ045QyxNQUFBQSxJQUFJLENBQUM0QixRQUFMLENBQWMsSUFBZDtBQUNBLFVBQUcsS0FBSSxDQUFDcEMsYUFBUixFQUNJLEtBQUksQ0FBQ3VELFlBQUw7QUFDUCxLQU5MLEVBT0tuRCxLQVBMO0FBU0EsU0FBS1csY0FBTDtBQUNKLFNBQUs1QixVQUFMLENBQWdCOEIsSUFBaEIsQ0FBcUJDLE1BQXJCLEdBQThCLEVBQUUsS0FBS3pCLGNBQUwsQ0FBb0JtQyxNQUFwQixHQUE2QixDQUEvQixDQUE5QixDQTlEcUIsQ0ErRHJCO0FBQ0E7QUFDQTtBQUNILEdBdEtJO0FBdUtMNEIsRUFBQUEsWUF2S0ssMEJBdUtTO0FBQ1YsU0FBSzFELFNBQUwsQ0FBZXlCLFVBQWYsQ0FBMEJDLFdBQVcsQ0FBQ0MsV0FBdEMsRUFEVSxDQUVWO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0EsUUFBRyxLQUFLSixVQUFSLEVBQ0ksS0FBS29DLFNBQUw7QUFFUCxHQWpMSTtBQWtMTEMsRUFBQUEsb0JBbExLLGtDQWtMaUI7QUFDbEIsUUFBRyxLQUFLckMsVUFBUixFQUFtQjtBQUNmLFdBQUt2QixTQUFMLENBQWV5QixVQUFmLENBQTBCQyxXQUFXLENBQUNDLFdBQXRDO0FBQ0EsV0FBS3pCLGFBQUwsR0FBcUIsSUFBckI7QUFDQSxXQUFLN0IsZUFBTCxDQUFxQitDLE1BQXJCLEdBQThCLEtBQTlCO0FBQ0EsV0FBS3FDLFlBQUw7QUFDSDtBQUVKLEdBMUxJO0FBMkxMSSxFQUFBQSxZQTNMSywwQkEyTFM7QUFDVnBHLElBQUFBLEVBQUUsQ0FBQ3FHLEdBQUgsQ0FBTywyQkFBMkIsS0FBS3pELGFBQUwsQ0FBbUJ5QixNQUFyRDtBQUNBLFNBQUs5QixTQUFMLENBQWV5QixVQUFmLENBQTBCQyxXQUFXLENBQUNDLFdBQXRDOztBQUNBLFFBQUcsS0FBS3RCLGFBQUwsQ0FBbUJ5QixNQUFuQixHQUE0QixDQUE1QixJQUFpQyxLQUFLUCxVQUF6QyxFQUFvRDtBQUNoRCxXQUFLbkIsS0FBTDtBQUNBLFVBQUkyRCxVQUFVLEdBQUcsS0FBSzFELGFBQUwsQ0FBbUIsS0FBS0EsYUFBTCxDQUFtQnlCLE1BQW5CLEdBQTRCLENBQS9DLENBQWpCOztBQUVBLFVBQUdpQyxVQUFVLENBQUNDLGNBQVgsQ0FBMEIsdUJBQTFCLEtBQXNELENBQUNuQixLQUFLLENBQUNDLE9BQU4sQ0FBY2lCLFVBQVUsQ0FBQ0UscUJBQXpCLENBQTFELEVBQTBHO0FBQ3RHLFlBQUlDLFlBQVksR0FBRyxLQUFLdEUsZUFBTCxDQUFxQm1FLFVBQVUsQ0FBQ0ksU0FBaEMsQ0FBbkI7QUFDQSxZQUFJQyxpQkFBaUIsR0FBRyxLQUFLdkUsaUJBQUwsQ0FBdUJrRSxVQUFVLENBQUNJLFNBQWxDLENBQXhCO0FBQ0EsWUFBSXpELElBQUksR0FBR3dELFlBQVksQ0FBQ0EsWUFBWSxDQUFDcEMsTUFBYixHQUFzQixDQUF2QixDQUF2QjtBQUNBcEIsUUFBQUEsSUFBSSxDQUFDNkIsU0FBTDtBQUNBN0IsUUFBQUEsSUFBSSxDQUFDMkQsTUFBTCxHQUFjTixVQUFVLENBQUNFLHFCQUF6QjtBQUNBLGFBQUtqRCxXQUFMO0FBQ0FvRCxRQUFBQSxpQkFBaUIsQ0FBQ0UsTUFBbEIsQ0FBeUJGLGlCQUFpQixDQUFDdEMsTUFBbEIsR0FBMkIsQ0FBcEQsRUFBdUQsQ0FBdkQ7QUFDSDs7QUFFRCxjQUFRaUMsVUFBVSxDQUFDcEcsVUFBbkI7QUFDSSxhQUFLQSxVQUFVLENBQUNDLGdCQUFoQjtBQUNJLGVBQUtxRCxjQUFMOztBQUNBLGNBQUksS0FBS3ZCLFlBQUwsQ0FBa0JvQyxNQUFsQixHQUEyQixDQUEvQixFQUFpQztBQUM3QixpQkFBS3BDLFlBQUwsQ0FBa0IsS0FBS0EsWUFBTCxDQUFrQm9DLE1BQWxCLEdBQTJCLENBQTdDLEVBQWdEWCxJQUFoRCxDQUFxRGEsZ0JBQXJELENBQXNFLElBQXRFO0FBQ0EsaUJBQUt0QyxZQUFMLENBQWtCNEUsTUFBbEIsQ0FBeUIsS0FBSzVFLFlBQUwsQ0FBa0JvQyxNQUFsQixHQUEyQixDQUFwRCxFQUF1RCxDQUF2RDtBQUNIOztBQUNELGNBQUcsS0FBS3BDLFlBQUwsQ0FBa0JvQyxNQUFsQixHQUEyQixDQUE5QixFQUFnQztBQUM1QixpQkFBSSxJQUFJckIsQ0FBQyxHQUFHLEtBQUtmLFlBQUwsQ0FBa0JvQyxNQUExQixFQUFrQ3lDLENBQUMsR0FBRyxDQUExQyxFQUE2QzlELENBQUMsR0FBRyxDQUFqRCxFQUFvREEsQ0FBQyxJQUFJOEQsQ0FBQyxFQUExRCxFQUE4RDtBQUMxRCxrQkFBSXBDLE1BQU0sR0FBRyxLQUFLeEMsY0FBTCxDQUFvQixLQUFLc0IsY0FBTCxHQUFzQixDQUF0QixHQUEwQnNELENBQTlDLENBQWI7QUFDQSxtQkFBSzdFLFlBQUwsQ0FBa0JlLENBQUMsR0FBRyxDQUF0QixFQUF5QjRCLGFBQXpCLENBQXVDRixNQUF2QyxFQUErQyxLQUFLN0MsZ0JBQUwsQ0FBc0I2QyxNQUF0QixDQUEvQztBQUNBLG1CQUFLekMsWUFBTCxDQUFrQmUsQ0FBQyxHQUFHLENBQXRCLEVBQXlCNkIsUUFBekIsQ0FBa0MsS0FBbEM7QUFDSDtBQUNKLFdBTkQsTUFNTSxJQUFJLEtBQUs1QyxZQUFMLENBQWtCb0MsTUFBbEIsR0FBMkIsQ0FBL0IsRUFBaUMsQ0FDbkM7QUFDQTtBQUNIOztBQUNELGNBQUcsS0FBS3BDLFlBQUwsQ0FBa0JvQyxNQUFsQixHQUEyQixDQUE5QixFQUFnQztBQUM1QixpQkFBS3BDLFlBQUwsQ0FBa0IsS0FBS0EsWUFBTCxDQUFrQm9DLE1BQWxCLEdBQTJCLENBQTdDLEVBQWdEWSxNQUFoRCxHQUF5RCxJQUF6RDtBQUNIOztBQUNELGVBQUtwQixlQUFMLENBQXFCLElBQXJCO0FBQ0EsZUFBS2pDLFVBQUwsQ0FBZ0I4QixJQUFoQixDQUFxQkMsTUFBckIsR0FBOEIsSUFBOUI7QUFDQTs7QUFDSixhQUFLekQsVUFBVSxDQUFDRSx3QkFBaEI7QUFDSSxjQUFJNkMsS0FBSSxHQUFHcUQsVUFBVSxDQUFDakUsV0FBWCxDQUF1QixDQUF2QixDQUFYO0FBQ0EsZUFBS0gsY0FBTCxDQUFvQjJFLE1BQXBCLENBQTJCLEtBQUtyRCxjQUFoQyxFQUFnRCxDQUFoRCxFQUFtRFAsS0FBSSxDQUFDMkQsTUFBeEQ7QUFDQSxlQUFLekMsY0FBTCxDQUFvQmxCLEtBQUksQ0FBQ3lDLE1BQXpCO0FBQ0EsZUFBSzlDLGFBQUwsQ0FBbUJpRSxNQUFuQixDQUEwQixLQUFLakUsYUFBTCxDQUFtQnlCLE1BQW5CLEdBQTRCLENBQXRELEVBQXlELENBQXpELEVBSkosQ0FPSTs7QUFDQSxlQUFLdEMsY0FBTCxDQUFvQmtCLEtBQUksQ0FBQzhELE9BQUwsR0FBZSxHQUFuQyxFQUF3QyxLQUFLaEYsY0FBTCxDQUFvQmtCLEtBQUksQ0FBQzhELE9BQUwsR0FBZSxHQUFuQyxFQUF3QzFDLE1BQXhDLEdBQWlELENBQXpGLEVBQTRGWCxJQUE1RixDQUFpR2EsZ0JBQWpHLENBQWtILElBQWxIO0FBQ0EsZUFBS3hDLGNBQUwsQ0FBb0JrQixLQUFJLENBQUM4RCxPQUFMLEdBQWUsR0FBbkMsRUFBd0NGLE1BQXhDLENBQStDLEtBQUs5RSxjQUFMLENBQW9Ca0IsS0FBSSxDQUFDOEQsT0FBTCxHQUFlLEdBQW5DLEVBQXdDMUMsTUFBeEMsR0FBaUQsQ0FBaEcsRUFBbUcsQ0FBbkc7QUFDQSxlQUFLckMsZ0JBQUwsQ0FBc0JpQixLQUFJLENBQUM4RCxPQUFMLEdBQWUsR0FBckMsRUFBMENGLE1BQTFDLENBQWlELEtBQUs5RSxjQUFMLENBQW9Ca0IsS0FBSSxDQUFDOEQsT0FBTCxHQUFlLEdBQW5DLEVBQXdDMUMsTUFBeEMsR0FBaUQsQ0FBbEcsRUFBcUcsQ0FBckc7QUFDQTs7QUFDSixhQUFLbkUsVUFBVSxDQUFDRyw0QkFBaEI7QUFDSSxjQUFJMkcsS0FBSyxHQUFHVixVQUFVLENBQUNqRSxXQUFYLENBQXVCLENBQXZCLENBQVo7QUFDQSxlQUFLSCxjQUFMLENBQW9CMkUsTUFBcEIsQ0FBMkIsS0FBS3JELGNBQWhDLEVBQWdELENBQWhELEVBQW1Ed0QsS0FBSyxDQUFDSixNQUF6RCxFQUZKLENBR0k7O0FBQ0EsZUFBS3pDLGNBQUwsQ0FBb0I2QyxLQUFLLENBQUN0QixNQUExQjtBQUNBLGVBQUs5QyxhQUFMLENBQW1CaUUsTUFBbkIsQ0FBMEIsS0FBS2pFLGFBQUwsQ0FBbUJ5QixNQUFuQixHQUE0QixDQUF0RCxFQUF5RCxDQUF6RDtBQUVBLGVBQUtsQyxlQUFMLENBQXFCNkUsS0FBSyxDQUFDRCxPQUEzQixFQUFvQyxLQUFLNUUsZUFBTCxDQUFxQjZFLEtBQUssQ0FBQ0QsT0FBM0IsRUFBb0MxQyxNQUFwQyxHQUE2QyxDQUFqRixFQUFvRlgsSUFBcEYsQ0FBeUZhLGdCQUF6RixDQUEwRyxJQUExRztBQUNBLGVBQUtwQyxlQUFMLENBQXFCNkUsS0FBSyxDQUFDRCxPQUEzQixFQUFvQ0YsTUFBcEMsQ0FBMkMsS0FBSzFFLGVBQUwsQ0FBcUI2RSxLQUFLLENBQUNELE9BQTNCLEVBQW9DMUMsTUFBcEMsR0FBNkMsQ0FBeEYsRUFBMkYsQ0FBM0Y7QUFDQSxlQUFLakMsaUJBQUwsQ0FBdUI0RSxLQUFLLENBQUNELE9BQTdCLEVBQXNDRixNQUF0QyxDQUE2QyxLQUFLekUsaUJBQUwsQ0FBdUI0RSxLQUFLLENBQUNELE9BQTdCLEVBQXNDMUMsTUFBdEMsR0FBK0MsQ0FBNUYsRUFBK0YsQ0FBL0Y7QUFDQTs7QUFDSixhQUFLbkUsVUFBVSxDQUFDSSw4QkFBaEI7QUFDSSxlQUFLMkcsZ0JBQUwsQ0FBc0JYLFVBQVUsQ0FBQ2pFLFdBQWpDLEVBQThDaUUsVUFBVSxDQUFDSSxTQUF6RDtBQUNBLGVBQUs5RCxhQUFMLENBQW1CaUUsTUFBbkIsQ0FBMEIsS0FBS2pFLGFBQUwsQ0FBbUJ5QixNQUFuQixHQUE0QixDQUF0RCxFQUF5RCxDQUF6RDtBQUNBOztBQUNKLGFBQUtuRSxVQUFVLENBQUNLLHlCQUFoQjtBQUNJLGVBQUsyRyxnQkFBTCxDQUFzQlosVUFBVSxDQUFDakUsV0FBakMsRUFBOENpRSxVQUFVLENBQUNJLFNBQXpEO0FBQ0EsZUFBSzlELGFBQUwsQ0FBbUJpRSxNQUFuQixDQUEwQixLQUFLakUsYUFBTCxDQUFtQnlCLE1BQW5CLEdBQTRCLENBQXRELEVBQXlELENBQXpEO0FBRUE7O0FBQ0osYUFBS25FLFVBQVUsQ0FBQ00sd0JBQWhCO0FBQ0ksZUFBSzBHLGdCQUFMLENBQXNCWixVQUFVLENBQUNqRSxXQUFqQyxFQUE4Q2lFLFVBQVUsQ0FBQ0ksU0FBekQ7QUFDQSxlQUFLOUQsYUFBTCxDQUFtQmlFLE1BQW5CLENBQTBCLEtBQUtqRSxhQUFMLENBQW1CeUIsTUFBbkIsR0FBNEIsQ0FBdEQsRUFBeUQsQ0FBekQ7QUFDQTtBQTFEUjs7QUE2REEsV0FBS3pCLGFBQUwsQ0FBbUJpRSxNQUFuQixDQUEwQixLQUFLakUsYUFBTCxDQUFtQnlCLE1BQW5CLEdBQTRCLENBQXRELEVBQXlELENBQXpELEVBM0VnRCxDQTRFaEQ7QUFDSDtBQUNKLEdBNVFJO0FBNlFMOEMsRUFBQUEsWUE3UUssMEJBNlFTO0FBQ1YsU0FBSzNELGNBQUw7O0FBQ0EsUUFBRyxLQUFLQSxjQUFMLEdBQXNCLENBQXRCLElBQTJCLEtBQUt2QixZQUFMLENBQWtCb0MsTUFBbEIsR0FBMkIsQ0FBekQsRUFBMkQ7QUFDdkQsV0FBSSxJQUFJckIsQ0FBQyxHQUFHLENBQVosRUFBZUEsQ0FBQyxHQUFHLEtBQUtmLFlBQUwsQ0FBa0JvQyxNQUFyQyxFQUE2Q3JCLENBQUMsRUFBOUMsRUFBaUQ7QUFDN0MsWUFBSTBCLE1BQU0sR0FBRyxLQUFLeEMsY0FBTCxDQUFvQixLQUFLc0IsY0FBTCxHQUFzQixDQUF0QixHQUEwQlIsQ0FBOUMsQ0FBYjtBQUNBLGFBQUtmLFlBQUwsQ0FBa0JlLENBQWxCLEVBQXFCNEIsYUFBckIsQ0FBbUNGLE1BQW5DLEVBQTJDLEtBQUs3QyxnQkFBTCxDQUFzQjZDLE1BQXRCLENBQTNDO0FBQ0EsYUFBS3pDLFlBQUwsQ0FBa0JlLENBQWxCLEVBQXFCNkIsUUFBckIsQ0FBOEIsS0FBOUI7QUFDQSxhQUFLNUMsWUFBTCxDQUFrQmUsQ0FBbEIsRUFBcUJpQyxNQUFyQixHQUE4QixLQUE5QjtBQUNIOztBQUNELFVBQUloQyxJQUFJLEdBQUcsS0FBSzhCLE9BQUwsQ0FBYSxLQUFLN0MsY0FBTCxDQUFvQixLQUFLc0IsY0FBTCxHQUFzQixDQUExQyxDQUFiLENBQVg7QUFDQSxXQUFLN0IsaUJBQUwsQ0FBdUJxRCxRQUF2QixDQUFnQy9CLElBQUksQ0FBQ1MsSUFBckM7QUFDQSxXQUFLekIsWUFBTCxDQUFrQndDLElBQWxCLENBQXVCeEIsSUFBdkI7QUFDQSxVQUFJcUMsSUFBSSxHQUFHLEtBQUs1RCxjQUFMLENBQW9CeUQsV0FBcEIsR0FBa0NJLENBQWxDLEdBQXNDLE1BQUksS0FBS3RELFlBQUwsQ0FBa0JvQyxNQUFsQixHQUEyQixDQUEvQixDQUFqRDtBQUNBcEIsTUFBQUEsSUFBSSxDQUFDeUMsTUFBTCxHQUFjMUYsRUFBRSxDQUFDQyxFQUFILENBQU1xRixJQUFOLEVBQVksS0FBSzVELGNBQUwsQ0FBb0J5RCxXQUFwQixHQUFrQ00sQ0FBOUMsQ0FBZDtBQUNBeEMsTUFBQUEsSUFBSSxDQUFDUyxJQUFMLENBQVV3QixRQUFWLEdBQXFCbEYsRUFBRSxDQUFDQyxFQUFILENBQU1xRixJQUFOLEVBQVksS0FBSzVELGNBQUwsQ0FBb0J5RCxXQUFwQixHQUFrQ00sQ0FBOUMsQ0FBckI7QUFDQXhDLE1BQUFBLElBQUksQ0FBQzBDLFFBQUwsR0FBZ0IsS0FBSzFELFlBQUwsQ0FBa0JvQyxNQUFsQztBQUNBcEIsTUFBQUEsSUFBSSxDQUFDUyxJQUFMLENBQVVYLE1BQVYsR0FBbUIsS0FBS2QsWUFBTCxDQUFrQm9DLE1BQXJDO0FBQ0FwQixNQUFBQSxJQUFJLENBQUM0QixRQUFMLENBQWMsS0FBZDtBQUNBNUIsTUFBQUEsSUFBSSxDQUFDZ0MsTUFBTCxHQUFjLElBQWQ7QUFDSCxLQWpCRCxNQWlCTSxJQUFJLEtBQUtoRCxZQUFMLENBQWtCb0MsTUFBbEIsR0FBMkIsQ0FBL0IsRUFBaUM7QUFDbkMsV0FBS3BDLFlBQUwsQ0FBa0IsS0FBS0EsWUFBTCxDQUFrQm9DLE1BQWxCLEdBQTJCLENBQTdDLEVBQWdEWSxNQUFoRCxHQUF5RCxJQUF6RDtBQUVIO0FBQ0osR0FwU0k7QUFxU0xwQixFQUFBQSxlQXJTSywyQkFxU1d1RCxNQXJTWCxFQXFTa0I7QUFBQztBQUNwQixTQUFLeEYsVUFBTCxDQUFnQjhCLElBQWhCLENBQXFCMkQsWUFBckIsQ0FBa0NySCxFQUFFLENBQUNzSCxNQUFyQyxFQUE2Q0MsV0FBN0MsR0FBMkQsS0FBSzFGLGdCQUFMLENBQXNCdUYsTUFBTSxHQUFHLEVBQUgsR0FBUSxFQUFwQyxDQUEzRDtBQUNILEdBdlNJO0FBd1NMSSxFQUFBQSxZQXhTSywwQkF3U1M7QUFBQTs7QUFDVixTQUFLbkUsS0FBTCxDQUFXb0UsZ0JBQVgsR0FEVSxDQUVWOztBQUNBLFNBQUtyRSxTQUFMO0FBQ0EsU0FBS1UsVUFBTCxHQUFrQixJQUFsQjtBQUNBLFNBQUtsQyxVQUFMLENBQWdCZ0MsWUFBaEIsR0FBK0IsSUFBL0I7QUFDQSxTQUFLQyxlQUFMLENBQXFCLElBQXJCO0FBQ0EsUUFBSTZELE9BQU8sR0FBRyxLQUFLbEYsS0FBTCxDQUFXbUYsUUFBWCxFQUFkLENBUFUsQ0FRVjtBQUNBOztBQUNBLFNBQUt6RixjQUFMLEdBQXNCd0YsT0FBTyxDQUFDRSxLQUFSLENBQWMsQ0FBZCxFQUFpQixFQUFqQixDQUF0QjtBQUNBLFNBQUtwRSxjQUFMLEdBQXNCLENBQXRCO0FBQ0EsUUFBSXBCLGlCQUFpQixHQUFHc0YsT0FBTyxDQUFDRSxLQUFSLENBQWMsRUFBZCxFQUFrQkYsT0FBTyxDQUFDckQsTUFBMUIsQ0FBeEI7O0FBWlUsK0JBY0ZyQixDQWRFO0FBQUEsbUNBZUU4RCxDQWZGO0FBZ0JGLFlBQUlwQyxNQUFNLEdBQUd0QyxpQkFBaUIsQ0FBQ3lGLElBQUQsQ0FBOUI7QUFDQUEsUUFBQUEsSUFBRzs7QUFFSCxZQUFJNUUsSUFBSSxHQUFHLE1BQUksQ0FBQzhCLE9BQUwsQ0FBYUwsTUFBYixDQUFYOztBQUNBekIsUUFBQUEsSUFBSSxDQUFDUyxJQUFMLENBQVV3QixRQUFWLEdBQXFCLE1BQUksQ0FBQ3RELFVBQUwsQ0FBZ0I4QixJQUFoQixDQUFxQnlCLFdBQXJCLEVBQXJCOztBQUNBLFFBQUEsTUFBSSxDQUFDeEQsaUJBQUwsQ0FBdUJxRCxRQUF2QixDQUFnQy9CLElBQUksQ0FBQ1MsSUFBckM7O0FBQ0FULFFBQUFBLElBQUksQ0FBQ1MsSUFBTCxDQUFVWCxNQUFWLEdBQW1CK0QsQ0FBbkI7O0FBQ0EsWUFBSWdCLEtBQUssR0FBRyxNQUFJLENBQUNyRyxpQkFBTCxDQUF1QnVCLENBQXZCLEVBQTBCbUMsV0FBMUIsR0FBd0NJLENBQXBEOztBQUNBLFlBQUl3QyxLQUFLLEdBQUcsTUFBSSxDQUFDdEcsaUJBQUwsQ0FBdUJ1QixDQUF2QixFQUEwQm1DLFdBQTFCLEdBQXdDTSxDQUF4QyxHQUE0QyxLQUFHcUIsQ0FBM0Q7O0FBQ0EsWUFBSWtCLElBQUksR0FBR2hJLEVBQUUsQ0FBQ0MsRUFBSCxDQUFNNkgsS0FBTixFQUFhQyxLQUFiLENBQVg7O0FBQ0E5RSxRQUFBQSxJQUFJLENBQUN5QyxNQUFMLEdBQWNzQyxJQUFkO0FBQ0EvRSxRQUFBQSxJQUFJLENBQUMwQyxRQUFMLEdBQWdCbUIsQ0FBaEI7QUFDQTdELFFBQUFBLElBQUksQ0FBQzhELE9BQUwsR0FBZS9ELENBQWY7O0FBQ0EsUUFBQSxNQUFJLENBQUNiLGVBQUwsQ0FBcUJhLENBQXJCLEVBQXdCeUIsSUFBeEIsQ0FBNkJ4QixJQUE3Qjs7QUFDQWpELFFBQUFBLEVBQUUsQ0FBQzZGLEtBQUgsQ0FBUzVDLElBQUksQ0FBQ1MsSUFBZCxFQUNLdUUsS0FETCxDQUNXLEdBRFgsRUFFS25DLEVBRkwsQ0FFUSxHQUZSLEVBRWE7QUFBQ1osVUFBQUEsUUFBUSxFQUFFOEM7QUFBWCxTQUZiLEVBR0tDLEtBSEwsQ0FHVyxNQUFJbkIsQ0FIZixFQUlLZixJQUpMLENBSVUsWUFBSTtBQUNOLGNBQUcvQyxDQUFDLEtBQUssQ0FBTixJQUFXOEQsQ0FBQyxLQUFLLENBQXBCLEVBQXNCO0FBQ2xCLFlBQUEsTUFBSSxDQUFDdkUsU0FBTCxDQUFleUIsVUFBZixDQUEwQkMsV0FBVyxDQUFDaUUsVUFBdEM7QUFDSDs7QUFDRCxjQUFHcEIsQ0FBQyxLQUFLOUQsQ0FBVCxFQUFZO0FBQ1o7QUFDSUMsWUFBQUEsSUFBSSxDQUFDNEIsUUFBTCxDQUFjLElBQWQ7O0FBQ0EsWUFBQSxNQUFJLENBQUN6QyxpQkFBTCxDQUF1QlksQ0FBdkIsRUFBMEJ5QixJQUExQixDQUErQkMsTUFBL0I7QUFDSDtBQUNKLFNBYkwsRUFjSzdCLEtBZEw7QUE5QkU7O0FBZU4sV0FBSSxJQUFJaUUsQ0FBQyxHQUFHLENBQVosRUFBZUEsQ0FBQyxHQUFHOUQsQ0FBQyxHQUFDLENBQXJCLEVBQXdCOEQsQ0FBQyxFQUF6QixFQUE0QjtBQUFBLGVBQXBCQSxDQUFvQjtBQThCM0I7O0FBN0NLO0FBQUE7O0FBY1YsU0FBSSxJQUFJOUQsQ0FBQyxHQUFHLENBQVIsRUFBVzZFLEdBQUcsR0FBRyxDQUFyQixFQUF3QjdFLENBQUMsR0FBRyxDQUE1QixFQUErQkEsQ0FBQyxFQUFoQyxFQUFtQztBQUFBLFlBQTNCQSxDQUEyQixFQUFwQjZFLEdBQW9CO0FBZ0NsQztBQUNKLEdBdlZJO0FBd1ZMOUMsRUFBQUEsT0F4VkssbUJBd1ZHTCxNQXhWSCxFQXdWVTtBQUNYLFFBQUl6QixJQUFJLEdBQUcsS0FBSzdCLFlBQUwsQ0FBa0IrRyxHQUFsQixFQUFYOztBQUNBLFFBQUcvQyxLQUFLLENBQUNDLE9BQU4sQ0FBY3BDLElBQWQsQ0FBSCxFQUF1QjtBQUNuQixVQUFJbUYsUUFBUSxHQUFHcEksRUFBRSxDQUFDa0QsV0FBSCxDQUFlLEtBQUtqQyxVQUFwQixDQUFmO0FBQ0EsV0FBS0csWUFBTCxDQUFrQitCLEdBQWxCLENBQXNCaUYsUUFBdEI7QUFDQW5GLE1BQUFBLElBQUksR0FBRyxLQUFLN0IsWUFBTCxDQUFrQitHLEdBQWxCLEVBQVA7QUFDSDs7QUFDRGxGLElBQUFBLElBQUksQ0FBQ29GLEVBQUwsQ0FBUXJJLEVBQUUsQ0FBQ2EsSUFBSCxDQUFReUgsU0FBUixDQUFrQkMsV0FBMUIsRUFBdUMsS0FBS0MsWUFBNUMsRUFBMEQsSUFBMUQsRUFBZ0UsSUFBaEU7QUFDQXZGLElBQUFBLElBQUksQ0FBQ29GLEVBQUwsQ0FBUXJJLEVBQUUsQ0FBQ2EsSUFBSCxDQUFReUgsU0FBUixDQUFrQkcsVUFBMUIsRUFBc0MsS0FBS0MsVUFBM0MsRUFBdUQsSUFBdkQsRUFBNkQsSUFBN0Q7QUFDQXpGLElBQUFBLElBQUksQ0FBQ29GLEVBQUwsQ0FBUXJJLEVBQUUsQ0FBQ2EsSUFBSCxDQUFReUgsU0FBUixDQUFrQkssU0FBMUIsRUFBcUMsS0FBS0MsVUFBMUMsRUFBc0QsSUFBdEQsRUFBNEQsSUFBNUQ7QUFFQTNGLElBQUFBLElBQUksR0FBR0EsSUFBSSxDQUFDb0UsWUFBTCxDQUFrQnhILFVBQWxCLENBQVA7QUFDQW9ELElBQUFBLElBQUksQ0FBQzJCLGFBQUwsQ0FBbUJGLE1BQW5CLEVBQTJCLEtBQUs3QyxnQkFBTCxDQUFzQjZDLE1BQXRCLENBQTNCO0FBQ0EsV0FBT3pCLElBQVA7QUFDSCxHQXRXSTtBQXVXTHVGLEVBQUFBLFlBdldLLHdCQXVXUUssS0F2V1IsRUF1V2M7QUFDZixTQUFLdEcsU0FBTCxDQUFleUIsVUFBZixDQUEwQkMsV0FBVyxDQUFDQyxXQUF0QyxFQURlLENBRWY7O0FBQ0EsUUFBSTRFLFVBQVUsR0FBR0QsS0FBSyxDQUFDRSxNQUFOLENBQWExQixZQUFiLENBQTBCeEgsVUFBMUIsQ0FBakI7O0FBQ0EsUUFBRyxDQUFDaUosVUFBVSxDQUFDN0QsTUFBWixJQUFzQixLQUFLNUMsV0FBTCxDQUFpQmdDLE1BQWpCLEdBQTBCLENBQW5ELEVBQXNEO0FBQ2xEckUsTUFBQUEsRUFBRSxDQUFDcUcsR0FBSCxDQUFPLGtCQUFQOztBQUNBLFdBQUksSUFBSXJELENBQUMsR0FBRyxDQUFaLEVBQWVBLENBQUMsR0FBRyxLQUFLWCxXQUFMLENBQWlCZ0MsTUFBcEMsRUFBNENyQixDQUFDLEVBQTdDLEVBQWdEO0FBQzVDLFlBQUlDLElBQUksR0FBRyxLQUFLWixXQUFMLENBQWlCVyxDQUFqQixDQUFYO0FBQ0FDLFFBQUFBLElBQUksQ0FBQ1MsSUFBTCxDQUFVd0IsUUFBVixHQUFxQmpDLElBQUksQ0FBQ3lDLE1BQTFCO0FBQ0F6QyxRQUFBQSxJQUFJLENBQUNTLElBQUwsQ0FBVVgsTUFBVixHQUFtQkUsSUFBSSxDQUFDMEMsUUFBeEI7QUFDSDs7QUFDRCxXQUFLdEQsV0FBTCxHQUFtQixFQUFuQjtBQUNBO0FBQ0g7O0FBQ0QsUUFBR3lHLFVBQVUsQ0FBQy9CLE9BQVgsR0FBcUIsQ0FBQyxDQUF0QixJQUEyQitCLFVBQVUsQ0FBQy9CLE9BQVgsR0FBcUIsR0FBbkQsRUFBdUQ7QUFBQztBQUNwRCxVQUFJaUMsYUFBYSxHQUFHLEtBQUs3RyxlQUFMLENBQXFCMkcsVUFBVSxDQUFDL0IsT0FBaEMsRUFBeUNrQyxPQUF6QyxDQUFpREgsVUFBakQsQ0FBcEI7O0FBQ0EsV0FBSSxJQUFJOUYsR0FBQyxHQUFHZ0csYUFBWixFQUEyQmhHLEdBQUMsR0FBRyxLQUFLYixlQUFMLENBQXFCMkcsVUFBVSxDQUFDL0IsT0FBaEMsRUFBeUMxQyxNQUF4RSxFQUFnRnJCLEdBQUMsRUFBakYsRUFBb0Y7QUFDaEYsWUFBSUMsTUFBSSxHQUFHLEtBQUtkLGVBQUwsQ0FBcUIyRyxVQUFVLENBQUMvQixPQUFoQyxFQUF5Qy9ELEdBQXpDLENBQVg7QUFDQSxhQUFLWCxXQUFMLENBQWlCb0MsSUFBakIsQ0FBc0J4QixNQUF0QjtBQUNBQSxRQUFBQSxNQUFJLENBQUNTLElBQUwsQ0FBVVgsTUFBVixHQUFtQixNQUFNQyxHQUF6QjtBQUNIO0FBQ0osS0FQRCxNQU9LO0FBQUM7QUFDRixXQUFLWCxXQUFMLENBQWlCb0MsSUFBakIsQ0FBc0JxRSxVQUF0QjtBQUNBOUksTUFBQUEsRUFBRSxDQUFDcUcsR0FBSCxDQUFPLGlCQUFpQixHQUF4QjtBQUNBeUMsTUFBQUEsVUFBVSxDQUFDcEYsSUFBWCxDQUFnQlgsTUFBaEIsR0FBeUIsR0FBekI7QUFDSCxLQXpCYyxDQTBCZjs7QUFDSCxHQWxZSTtBQW1ZTDJGLEVBQUFBLFVBbllLLHNCQW1ZTUcsS0FuWU4sRUFtWVk7QUFDYixTQUFLSyxTQUFMLEdBQWlCLElBQWpCO0FBQ0EsUUFBSUMsUUFBUSxHQUFHTixLQUFLLENBQUNFLE1BQU4sQ0FBYTFCLFlBQWIsQ0FBMEJ4SCxVQUExQixDQUFmO0FBQ0EsUUFBRyxDQUFDc0osUUFBUSxDQUFDbEUsTUFBYixFQUFxQjtBQUNyQixRQUFJbUUsS0FBSyxHQUFHUCxLQUFLLENBQUNRLEtBQU4sQ0FBWUMsUUFBWixFQUFaLENBSmEsQ0FLYjs7QUFDQSxRQUFHLEtBQUtqSCxXQUFMLENBQWlCZ0MsTUFBakIsR0FBMEIsQ0FBN0IsRUFBK0I7QUFDM0IsV0FBSSxJQUFJckIsQ0FBQyxHQUFHLENBQVosRUFBZUEsQ0FBQyxHQUFHLEtBQUtYLFdBQUwsQ0FBaUJnQyxNQUFwQyxFQUE0Q3JCLENBQUMsRUFBN0MsRUFBZ0Q7QUFDNUMsWUFBSUMsSUFBSSxHQUFHLEtBQUtaLFdBQUwsQ0FBaUJXLENBQWpCLENBQVg7QUFDQUMsUUFBQUEsSUFBSSxDQUFDUyxJQUFMLENBQVU2QixDQUFWLElBQWU2RCxLQUFLLENBQUM3RCxDQUFyQjtBQUNBdEMsUUFBQUEsSUFBSSxDQUFDUyxJQUFMLENBQVUrQixDQUFWLElBQWUyRCxLQUFLLENBQUMzRCxDQUFyQjtBQUNIO0FBQ0o7QUFDSixHQWhaSTtBQWlaTG1ELEVBQUFBLFVBalpLLHNCQWlaTUMsS0FqWk4sRUFpWlk7QUFDYixRQUFJTSxRQUFRLEdBQUdOLEtBQUssQ0FBQ0UsTUFBTixDQUFhMUIsWUFBYixDQUEwQnhILFVBQTFCLENBQWY7QUFDQSxRQUFHLENBQUNzSixRQUFRLENBQUNsRSxNQUFiLEVBQXFCLE9BRlIsQ0FHYjtBQUNBOztBQUNBLFFBQUcsS0FBSzVDLFdBQUwsQ0FBaUJnQyxNQUFqQixHQUEwQixDQUE3QixFQUErQjtBQUMzQixVQUFHLEtBQUs2RSxTQUFMLElBQWtCLEtBQUtLLGlCQUFMLENBQXVCLEtBQUtsSCxXQUE1QixDQUFyQixFQUE4RCxDQUU3RCxDQUZELE1BRU0sSUFBRyxDQUFDLEtBQUs2RyxTQUFOLElBQW1CLEtBQUtNLFlBQUwsRUFBdEIsRUFBMEMsQ0FFL0MsQ0FGSyxNQUVEO0FBQ0QsYUFBSSxJQUFJeEcsQ0FBQyxHQUFHLENBQVosRUFBZUEsQ0FBQyxHQUFHLEtBQUtYLFdBQUwsQ0FBaUJnQyxNQUFwQyxFQUE0Q3JCLENBQUMsRUFBN0MsRUFBZ0Q7QUFDNUMsY0FBSUMsSUFBSSxHQUFHLEtBQUtaLFdBQUwsQ0FBaUJXLENBQWpCLENBQVgsQ0FENEMsQ0FFNUM7O0FBQ0FDLFVBQUFBLElBQUksQ0FBQ1MsSUFBTCxDQUFVd0IsUUFBVixHQUFxQmpDLElBQUksQ0FBQ3lDLE1BQTFCO0FBQ0F6QyxVQUFBQSxJQUFJLENBQUNTLElBQUwsQ0FBVVgsTUFBVixHQUFtQkUsSUFBSSxDQUFDMEMsUUFBeEI7QUFDQTFDLFVBQUFBLElBQUksQ0FBQ3dHLFNBQUw7QUFDSDtBQUNKO0FBQ0o7O0FBQ0QsU0FBS3BILFdBQUwsR0FBbUIsRUFBbkI7QUFDQSxTQUFLNkcsU0FBTCxHQUFpQixLQUFqQjtBQUNILEdBdmFJO0FBd2FMSyxFQUFBQSxpQkF4YUssNkJBd2FhRyxZQXhhYixFQXdhMEI7QUFDM0IsUUFBRyxLQUFLckgsV0FBTCxDQUFpQmdDLE1BQWpCLEdBQTBCLENBQTdCLEVBQWdDLE9BQU8sS0FBUDtBQUNoQyxRQUFJc0YsV0FBVyxHQUFHRCxZQUFZLENBQUMsQ0FBRCxDQUFaLENBQWdCaEcsSUFBaEIsQ0FBcUJ5QixXQUFyQixFQUFsQjtBQUNBLFFBQUl5RSxZQUFZLEdBQUdGLFlBQVksQ0FBQyxDQUFELENBQVosQ0FBZ0JoRyxJQUFoQixDQUFxQm1HLGNBQXJCLEVBQW5CLENBSDJCLENBSTNCOztBQUNBLFNBQUksSUFBSTdHLENBQUMsR0FBRyxDQUFaLEVBQWVBLENBQUMsR0FBRyxLQUFLeEIsZ0JBQUwsQ0FBc0I2QyxNQUF6QyxFQUFpRHJCLENBQUMsRUFBbEQsRUFBcUQ7QUFDakQsVUFBSThHLFVBQVUsR0FBRyxLQUFLdEksZ0JBQUwsQ0FBc0J3QixDQUF0QixFQUF5QjZHLGNBQXpCLEVBQWpCOztBQUNBLFVBQUdDLFVBQVUsQ0FBQ0MsUUFBWCxDQUFvQkosV0FBcEIsQ0FBSCxFQUFvQztBQUNoQyxZQUFJSyxTQUFTLEdBQUdoSCxDQUFoQjs7QUFDQSxZQUFHLEtBQUtpSCxtQkFBTCxDQUF5QlAsWUFBWSxDQUFDLENBQUQsQ0FBWixDQUFnQjlDLE1BQXpDLEVBQWlEb0QsU0FBakQsTUFBZ0UsQ0FBQyxDQUFqRSxJQUFzRU4sWUFBWSxDQUFDckYsTUFBYixLQUF3QixDQUE5RixJQUFtR3FGLFlBQVksQ0FBQyxDQUFELENBQVosQ0FBZ0IzQyxPQUFoQixHQUEwQixHQUFoSSxFQUFvSTtBQUNoSSxlQUFLRSxnQkFBTCxDQUFzQnlDLFlBQVksQ0FBQyxDQUFELENBQWxDLEVBQXVDTSxTQUF2QztBQUNBLGlCQUFPLElBQVA7QUFDSDs7QUFDRCxlQUFPLEtBQVA7QUFDSDtBQUNKLEtBZjBCLENBZ0IzQjs7O0FBQ0EsU0FBSSxJQUFJbEQsQ0FBQyxHQUFHLENBQVosRUFBZUEsQ0FBQyxHQUFHLEtBQUszRSxlQUFMLENBQXFCa0MsTUFBeEMsRUFBZ0R5QyxDQUFDLEVBQWpELEVBQW9EO0FBQ2hELFVBQUlvRCxjQUFjLEdBQUcsSUFBckI7QUFDQSxVQUFHLEtBQUsvSCxlQUFMLENBQXFCMkUsQ0FBckIsRUFBd0J6QyxNQUF4QixLQUFtQyxDQUF0QyxFQUNJNkYsY0FBYyxHQUFHLEtBQUt6SSxpQkFBTCxDQUF1QnFGLENBQXZCLEVBQTBCK0MsY0FBMUIsRUFBakIsQ0FESixLQUdJSyxjQUFjLEdBQUcsS0FBSy9ILGVBQUwsQ0FBcUIyRSxDQUFyQixFQUF3QixLQUFLM0UsZUFBTCxDQUFxQjJFLENBQXJCLEVBQXdCekMsTUFBeEIsR0FBaUMsQ0FBekQsRUFBNERYLElBQTVELENBQWlFbUcsY0FBakUsRUFBakI7O0FBQ0osVUFBR0ssY0FBYyxDQUFDSCxRQUFmLENBQXdCSixXQUF4QixLQUF3QzdDLENBQUMsS0FBSzRDLFlBQVksQ0FBQyxDQUFELENBQVosQ0FBZ0IzQyxPQUFqRSxFQUF5RTtBQUNyRSxZQUFJb0QsUUFBUSxHQUFHckQsQ0FBZjs7QUFDQSxZQUFHLEtBQUtzRCxvQkFBTCxDQUEwQlYsWUFBWSxDQUFDLENBQUQsQ0FBWixDQUFnQjlDLE1BQTFDLEVBQWtEdUQsUUFBbEQsTUFBZ0UsQ0FBQyxDQUFwRSxFQUFzRTtBQUNsRSxlQUFLakQsZ0JBQUwsQ0FBc0J3QyxZQUF0QixFQUFvQ1MsUUFBcEM7QUFDQSxpQkFBTyxJQUFQO0FBQ0g7O0FBQ0QsZUFBTyxLQUFQO0FBQ0g7QUFDSjs7QUFFRCxXQUFPLEtBQVA7QUFDSCxHQTFjSTtBQTJjTFgsRUFBQUEsWUEzY0ssMEJBMmNTO0FBQ1YsUUFBRyxLQUFLbkgsV0FBTCxDQUFpQmdDLE1BQWpCLEdBQTBCLENBQTdCLEVBQWdDLE9BQU8sS0FBUDtBQUNoQyxRQUFJZ0csY0FBYyxHQUFHLEtBQUtKLG1CQUFMLENBQXlCLEtBQUs1SCxXQUFMLENBQWlCLENBQWpCLEVBQW9CdUUsTUFBN0MsQ0FBckIsQ0FGVSxDQUdWOztBQUNBLFFBQUkwRCxhQUFhLEdBQUcsS0FBS0Ysb0JBQUwsQ0FBMEIsS0FBSy9ILFdBQUwsQ0FBaUIsQ0FBakIsRUFBb0J1RSxNQUE5QyxDQUFwQixDQUpVLENBS1Y7O0FBQ0EsUUFBR3lELGNBQWMsS0FBSyxDQUFDLENBQXBCLElBQXlCLEtBQUtoSSxXQUFMLENBQWlCZ0MsTUFBakIsS0FBNEIsQ0FBckQsSUFBMEQsS0FBS2hDLFdBQUwsQ0FBaUIsQ0FBakIsRUFBb0IwRSxPQUFwQixHQUE4QixHQUEzRixFQUErRjtBQUMzRixXQUFLRSxnQkFBTCxDQUFzQixLQUFLNUUsV0FBTCxDQUFpQixDQUFqQixDQUF0QixFQUEyQ2dJLGNBQTNDO0FBQ0EsYUFBTyxJQUFQO0FBQ0gsS0FIRCxNQUdNLElBQUdDLGFBQWEsS0FBSyxDQUFDLENBQW5CLElBQXdCQSxhQUFhLEtBQUssS0FBS2pJLFdBQUwsQ0FBaUIsQ0FBakIsRUFBb0IwRSxPQUFqRSxFQUF5RTtBQUMzRSxXQUFLRyxnQkFBTCxDQUFzQixLQUFLN0UsV0FBM0IsRUFBd0NpSSxhQUF4QztBQUNBLGFBQU8sSUFBUDtBQUNIOztBQUNELFdBQU8sS0FBUDtBQUNILEdBemRJO0FBMGRMckQsRUFBQUEsZ0JBMWRLLDRCQTBkWXNELFFBMWRaLEVBMGRzQlAsU0ExZHRCLEVBMGRnQztBQUFBOztBQUNqQyxTQUFLekgsU0FBTCxDQUFleUIsVUFBZixDQUEwQkMsV0FBVyxDQUFDSyxVQUF0QztBQUNBLFFBQUdpRyxRQUFRLENBQUN4RCxPQUFULEdBQW1CLEVBQXRCLEVBQTBCLE9BRk8sQ0FFQTs7QUFDakMsU0FBS3JFLE1BQUwsSUFBZSxHQUFmO0FBQ0EsU0FBS0MsS0FBTDtBQUNBLFFBQUk2QixhQUFhLEdBQUcsRUFBcEI7QUFDQSxRQUFJZ0csYUFBYSxHQUFJRCxRQUFRLENBQUN4RCxPQUFULEtBQXFCLENBQUMsQ0FBM0M7O0FBRUEsUUFBR3lELGFBQUgsRUFBaUI7QUFDYmhHLE1BQUFBLGFBQWEsQ0FBQ3RFLFVBQWQsR0FBMkJBLFVBQVUsQ0FBQ0Usd0JBQXRDLENBRGEsQ0FFYjs7QUFDQW9FLE1BQUFBLGFBQWEsQ0FBQ2tDLFNBQWQsR0FBMEIsQ0FBQyxDQUEzQjtBQUNILEtBSkQsTUFJTTtBQUNGbEMsTUFBQUEsYUFBYSxDQUFDdEUsVUFBZCxHQUEyQkEsVUFBVSxDQUFDSyx5QkFBdEMsQ0FERSxDQUVGOztBQUNBaUUsTUFBQUEsYUFBYSxDQUFDa0MsU0FBZCxHQUEwQjZELFFBQVEsQ0FBQ3hELE9BQW5DO0FBQ0g7O0FBQ0R2QyxJQUFBQSxhQUFhLENBQUNpRyxNQUFkLEdBQXVCVCxTQUF2QjtBQUNBeEYsSUFBQUEsYUFBYSxDQUFDbkMsV0FBZCxHQUE0QixDQUFDa0ksUUFBRCxDQUE1QjtBQUVBLFFBQUkvRSxHQUFHLEdBQUcsS0FBS2hFLGdCQUFMLENBQXNCd0ksU0FBdEIsRUFBaUM3RSxXQUFqQyxFQUFWO0FBQ0EsUUFBSVMsS0FBSyxHQUFHLEtBQUtuRCxhQUFMLEdBQXFCLEdBQXJCLEdBQTJCLEdBQXZDO0FBQ0F6QyxJQUFBQSxFQUFFLENBQUM2RixLQUFILENBQVMwRSxRQUFRLENBQUM3RyxJQUFsQixFQUNLdUUsS0FETCxDQUNXLEdBRFgsRUFFS25DLEVBRkwsQ0FFUUYsS0FGUixFQUVlO0FBQUNWLE1BQUFBLFFBQVEsRUFBRU07QUFBWCxLQUZmLEVBR0tPLElBSEwsQ0FHVSxZQUFJO0FBQ053RSxNQUFBQSxRQUFRLENBQUNHLGNBQVQ7QUFDQUgsTUFBQUEsUUFBUSxDQUFDN0csSUFBVCxDQUFjWCxNQUFkLEdBQXVCd0gsUUFBUSxDQUFDNUUsUUFBaEM7QUFDQSxVQUFHLE1BQUksQ0FBQ2xELGFBQVIsRUFDSSxNQUFJLENBQUN1RCxZQUFMOztBQUNKLE1BQUEsTUFBSSxDQUFDMkUsZUFBTDtBQUNILEtBVEwsRUFVSzlILEtBVkwsR0F0QmlDLENBa0NqQzs7QUFFQSxTQUFLZCxjQUFMLENBQW9CaUksU0FBcEIsRUFBK0J2RixJQUEvQixDQUFvQzhGLFFBQXBDO0FBQ0EsU0FBS3ZJLGdCQUFMLENBQXNCZ0ksU0FBdEIsRUFBaUN2RixJQUFqQyxDQUFzQzhGLFFBQVEsQ0FBQzNELE1BQS9DO0FBRUEyRCxJQUFBQSxRQUFRLENBQUM1RSxRQUFULEdBQW9CLEtBQUs1RCxjQUFMLENBQW9CaUksU0FBcEIsRUFBK0IzRixNQUFuRDtBQUNBa0csSUFBQUEsUUFBUSxDQUFDN0UsTUFBVCxHQUFrQixLQUFLbEUsZ0JBQUwsQ0FBc0J3SSxTQUF0QixFQUFpQzdFLFdBQWpDLEVBQWxCO0FBRUEsUUFBSXlGLGlCQUFpQixHQUFHLElBQXhCOztBQUNBLFFBQUcsQ0FBQ0osYUFBSixFQUFrQjtBQUNkLFdBQUtySSxlQUFMLENBQXFCb0ksUUFBUSxDQUFDeEQsT0FBOUIsSUFBeUMzQixLQUFLLENBQUN5RixpQkFBTixDQUF3Qk4sUUFBeEIsRUFBa0MsS0FBS3BJLGVBQUwsQ0FBcUJvSSxRQUFRLENBQUN4RCxPQUE5QixDQUFsQyxDQUF6QztBQUNBLFdBQUszRSxpQkFBTCxDQUF1Qm1JLFFBQVEsQ0FBQ3hELE9BQWhDLElBQTJDM0IsS0FBSyxDQUFDeUYsaUJBQU4sQ0FBd0JOLFFBQVEsQ0FBQzNELE1BQWpDLEVBQXlDLEtBQUt4RSxpQkFBTCxDQUF1Qm1JLFFBQVEsQ0FBQ3hELE9BQWhDLENBQXpDLENBQTNDO0FBQ0E2RCxNQUFBQSxpQkFBaUIsR0FBRyxLQUFLRSxzQkFBTCxDQUE0QlAsUUFBUSxDQUFDeEQsT0FBckMsQ0FBcEI7QUFDSCxLQUpELE1BSUs7QUFDRCxXQUFLOUUsWUFBTCxHQUFvQm1ELEtBQUssQ0FBQ3lGLGlCQUFOLENBQXdCTixRQUF4QixFQUFrQyxLQUFLdEksWUFBdkMsQ0FBcEI7QUFDQSxXQUFLQyxjQUFMLEdBQXNCa0QsS0FBSyxDQUFDeUYsaUJBQU4sQ0FBd0JOLFFBQVEsQ0FBQzNELE1BQWpDLEVBQXlDLEtBQUsxRSxjQUE5QyxDQUF0QjtBQUNBLFVBQUk2SSxJQUFJLEdBQUcsSUFBWDtBQUNBQyxNQUFBQSxVQUFVLENBQUMsWUFBVztBQUNsQkQsUUFBQUEsSUFBSSxDQUFDNUQsWUFBTDtBQUNILE9BRlMsRUFFUCxHQUZPLENBQVY7QUFHSDs7QUFFRDNDLElBQUFBLGFBQWEsQ0FBQ2dDLHFCQUFkLEdBQXNDb0UsaUJBQXRDO0FBQ0EsU0FBS2hJLGFBQUwsQ0FBbUI2QixJQUFuQixDQUF3QkQsYUFBeEI7QUFDQStGLElBQUFBLFFBQVEsQ0FBQ3hELE9BQVQsR0FBbUIsTUFBTWlELFNBQXpCO0FBQ0gsR0FyaEJJO0FBc2hCTDlDLEVBQUFBLGdCQXRoQkssNEJBc2hCWStELFNBdGhCWixFQXNoQnVCZCxRQXRoQnZCLEVBc2hCZ0M7QUFBQTs7QUFDakMsU0FBSzVILFNBQUwsQ0FBZXlCLFVBQWYsQ0FBMEJDLFdBQVcsQ0FBQ0ssVUFBdEM7QUFDQSxTQUFLNUIsTUFBTCxJQUFlLENBQWY7QUFDQSxTQUFLQyxLQUFMO0FBQ0EsUUFBSTZCLGFBQWEsR0FBRyxFQUFwQjtBQUNBLFFBQUlnRyxhQUFhLEdBQUlTLFNBQVMsQ0FBQyxDQUFELENBQVQsQ0FBYWxFLE9BQWIsS0FBeUIsQ0FBQyxDQUEvQztBQUNBLFFBQUltRSxlQUFlLEdBQUlELFNBQVMsQ0FBQyxDQUFELENBQVQsQ0FBYWxFLE9BQWIsR0FBdUIsRUFBOUM7O0FBQ0EsUUFBR3lELGFBQUgsRUFBaUI7QUFDYjtBQUNBaEcsTUFBQUEsYUFBYSxDQUFDdEUsVUFBZCxHQUEyQkEsVUFBVSxDQUFDRyw0QkFBdEM7QUFDSCxLQUhELE1BR00sSUFBRzZLLGVBQUgsRUFBbUI7QUFDckIxRyxNQUFBQSxhQUFhLENBQUN0RSxVQUFkLEdBQTJCQSxVQUFVLENBQUNJLDhCQUF0QztBQUNILEtBRkssTUFFRDtBQUNEa0UsTUFBQUEsYUFBYSxDQUFDdEUsVUFBZCxHQUEyQkEsVUFBVSxDQUFDTSx3QkFBdEM7QUFDSDs7QUFDRGdFLElBQUFBLGFBQWEsQ0FBQ2tDLFNBQWQsR0FBMEJ1RSxTQUFTLENBQUMsQ0FBRCxDQUFULENBQWFsRSxPQUF2QztBQUNBdkMsSUFBQUEsYUFBYSxDQUFDaUcsTUFBZCxHQUF1Qk4sUUFBdkI7QUFDQTNGLElBQUFBLGFBQWEsQ0FBQ25DLFdBQWQsR0FBNEI0SSxTQUE1QixDQWpCaUMsQ0FvQmpDOztBQUNBLFFBQUlFLE1BQU0sR0FBRyxJQUFiO0FBQ0EsUUFBRyxLQUFLaEosZUFBTCxDQUFxQmdJLFFBQXJCLEVBQStCOUYsTUFBL0IsS0FBMEMsQ0FBN0MsRUFDSThHLE1BQU0sR0FBRyxLQUFLMUosaUJBQUwsQ0FBdUIwSSxRQUF2QixFQUFpQ2hGLFdBQWpDLEVBQVQsQ0FESixLQUVJO0FBQ0EsVUFBSWlHLFdBQVcsR0FBRyxLQUFLakosZUFBTCxDQUFxQmdJLFFBQXJCLEVBQStCLEtBQUtoSSxlQUFMLENBQXFCZ0ksUUFBckIsRUFBK0I5RixNQUEvQixHQUF3QyxDQUF2RSxFQUEwRVgsSUFBMUUsQ0FBK0V5QixXQUEvRSxFQUFsQjtBQUNBZ0csTUFBQUEsTUFBTSxHQUFHbkwsRUFBRSxDQUFDQyxFQUFILENBQU1tTCxXQUFXLENBQUM3RixDQUFsQixFQUFxQjZGLFdBQVcsQ0FBQzNGLENBQVosR0FBZ0IsRUFBckMsQ0FBVDtBQUNIO0FBQ0QsUUFBSW1GLGlCQUFpQixHQUFHLElBQXhCOztBQTVCaUMsaUNBNkJ6QjVILENBN0J5QjtBQThCN0IsVUFBSUMsSUFBSSxHQUFHZ0ksU0FBUyxDQUFDakksQ0FBRCxDQUFwQixDQTlCNkIsQ0ErQjdCOztBQUNBLFVBQUl3QyxHQUFHLEdBQUd4RixFQUFFLENBQUNDLEVBQUgsQ0FBTWtMLE1BQU0sQ0FBQzVGLENBQWIsRUFBZ0I0RixNQUFNLENBQUMxRixDQUFQLEdBQVcsS0FBR3pDLENBQTlCLENBQVY7QUFDQWhELE1BQUFBLEVBQUUsQ0FBQzZGLEtBQUgsQ0FBUzVDLElBQUksQ0FBQ1MsSUFBZCxFQUNLb0MsRUFETCxDQUNRLEdBRFIsRUFDYTtBQUFDWixRQUFBQSxRQUFRLEVBQUVNO0FBQVgsT0FEYixFQUVLTyxJQUZMLENBRVUsWUFBSTtBQUNOOUMsUUFBQUEsSUFBSSxDQUFDUyxJQUFMLENBQVVYLE1BQVYsR0FBbUJFLElBQUksQ0FBQzBDLFFBQXhCO0FBQ0EsWUFBRyxNQUFJLENBQUNsRCxhQUFMLElBQXVCTyxDQUFDLEtBQUtpSSxTQUFTLENBQUM1RyxNQUFWLEdBQW1CLENBQW5ELEVBQ0ksTUFBSSxDQUFDMkIsWUFBTDtBQUNQLE9BTkwsRUFPS25ELEtBUEw7O0FBU0EsVUFBRzJILGFBQUgsRUFBa0I7QUFDZCxRQUFBLE1BQUksQ0FBQ3ZJLFlBQUwsR0FBb0JtRCxLQUFLLENBQUN5RixpQkFBTixDQUF3QjVILElBQXhCLEVBQThCLE1BQUksQ0FBQ2hCLFlBQW5DLENBQXBCO0FBQ0EsUUFBQSxNQUFJLENBQUNDLGNBQUwsR0FBc0JrRCxLQUFLLENBQUN5RixpQkFBTixDQUF3QjVILElBQUksQ0FBQzJELE1BQTdCLEVBQXFDLE1BQUksQ0FBQzFFLGNBQTFDLENBQXRCO0FBQ0EsWUFBSTZJLElBQUksR0FBRyxNQUFYO0FBQ0FDLFFBQUFBLFVBQVUsQ0FBQyxZQUFXO0FBQ2xCRCxVQUFBQSxJQUFJLENBQUM1RCxZQUFMO0FBQ0gsU0FGUyxFQUVQLEdBRk8sQ0FBVjtBQUlILE9BUkQsTUFRTSxJQUFJK0QsZUFBSixFQUFxQjtBQUN2QixRQUFBLE1BQUksQ0FBQ25KLGNBQUwsQ0FBb0JrQixJQUFJLENBQUM4RCxPQUFMLEdBQWUsR0FBbkMsSUFBMEMzQixLQUFLLENBQUN5RixpQkFBTixDQUF3QjVILElBQXhCLEVBQThCLE1BQUksQ0FBQ2xCLGNBQUwsQ0FBb0JrQixJQUFJLENBQUM4RCxPQUFMLEdBQWUsR0FBbkMsQ0FBOUIsQ0FBMUM7QUFDQSxRQUFBLE1BQUksQ0FBQy9FLGdCQUFMLENBQXNCaUIsSUFBSSxDQUFDOEQsT0FBTCxHQUFlLEdBQXJDLElBQTRDM0IsS0FBSyxDQUFDeUYsaUJBQU4sQ0FBd0I1SCxJQUFJLENBQUMyRCxNQUE3QixFQUFxQyxNQUFJLENBQUM1RSxnQkFBTCxDQUFzQmlCLElBQUksQ0FBQzhELE9BQUwsR0FBZSxHQUFyQyxDQUFyQyxDQUE1QztBQUNILE9BSEssTUFHRDtBQUNELFFBQUEsTUFBSSxDQUFDNUUsZUFBTCxDQUFxQmMsSUFBSSxDQUFDOEQsT0FBMUIsSUFBcUMzQixLQUFLLENBQUN5RixpQkFBTixDQUF3QjVILElBQXhCLEVBQThCLE1BQUksQ0FBQ2QsZUFBTCxDQUFxQmMsSUFBSSxDQUFDOEQsT0FBMUIsQ0FBOUIsQ0FBckM7QUFDQSxRQUFBLE1BQUksQ0FBQzNFLGlCQUFMLENBQXVCYSxJQUFJLENBQUM4RCxPQUE1QixJQUF1QzNCLEtBQUssQ0FBQ3lGLGlCQUFOLENBQXdCNUgsSUFBSSxDQUFDMkQsTUFBN0IsRUFBcUMsTUFBSSxDQUFDeEUsaUJBQUwsQ0FBdUJhLElBQUksQ0FBQzhELE9BQTVCLENBQXJDLENBQXZDOztBQUNBLFlBQUcvRCxDQUFDLEtBQUtpSSxTQUFTLENBQUM1RyxNQUFWLEdBQW1CLENBQTVCLEVBQThCO0FBQzFCdUcsVUFBQUEsaUJBQWlCLEdBQUcsTUFBSSxDQUFDRSxzQkFBTCxDQUE0QjdILElBQUksQ0FBQzhELE9BQWpDLENBQXBCO0FBQ0g7QUFFSixPQTVENEIsQ0E2RDdCOzs7QUFDQTlELE1BQUFBLElBQUksQ0FBQzBDLFFBQUwsR0FBZ0IsTUFBSSxDQUFDeEQsZUFBTCxDQUFxQmdJLFFBQXJCLEVBQStCOUYsTUFBL0M7O0FBQ0EsTUFBQSxNQUFJLENBQUNsQyxlQUFMLENBQXFCZ0ksUUFBckIsRUFBK0IxRixJQUEvQixDQUFvQ3hCLElBQXBDOztBQUNBLE1BQUEsTUFBSSxDQUFDYixpQkFBTCxDQUF1QitILFFBQXZCLEVBQWlDMUYsSUFBakMsQ0FBc0N4QixJQUFJLENBQUMyRCxNQUEzQzs7QUFFQTNELE1BQUFBLElBQUksQ0FBQ3lDLE1BQUwsR0FBY0YsR0FBZDtBQUNBdkMsTUFBQUEsSUFBSSxDQUFDOEQsT0FBTCxHQUFlb0QsUUFBZjtBQW5FNkI7O0FBNkJqQyxTQUFJLElBQUluSCxDQUFDLEdBQUcsQ0FBWixFQUFlQSxDQUFDLEdBQUdpSSxTQUFTLENBQUM1RyxNQUE3QixFQUFzQ3JCLENBQUMsRUFBdkMsRUFBMEM7QUFBQSxhQUFsQ0EsQ0FBa0M7QUF1Q3pDOztBQUNEd0IsSUFBQUEsYUFBYSxDQUFDZ0MscUJBQWQsR0FBc0NvRSxpQkFBdEM7QUFDQSxTQUFLaEksYUFBTCxDQUFtQjZCLElBQW5CLENBQXdCRCxhQUF4QjtBQUNILEdBN2xCSTtBQThsQkx5RixFQUFBQSxtQkE5bEJLLCtCQThsQmV2RixNQTlsQmYsRUE4bEJ1QnNGLFNBOWxCdkIsRUE4bEJpQztBQUNsQztBQUNBLFFBQUcsQ0FBQzVFLEtBQUssQ0FBQ0MsT0FBTixDQUFjMkUsU0FBZCxDQUFKLEVBQTZCO0FBQ3pCLFVBQUdxQixTQUFTLENBQUNDLGtCQUFWLENBQTZCNUcsTUFBN0IsRUFBcUMsS0FBSzFDLGdCQUFMLENBQXNCZ0ksU0FBdEIsQ0FBckMsQ0FBSCxFQUEwRTtBQUN0RSxlQUFPQSxTQUFQO0FBQ0g7QUFDSixLQUpELE1BSUs7QUFDRCxXQUFJLElBQUloSCxDQUFDLEdBQUcsQ0FBWixFQUFlQSxDQUFDLEdBQUcsS0FBS2hCLGdCQUFMLENBQXNCcUMsTUFBekMsRUFBaURyQixDQUFDLEVBQWxELEVBQXFEO0FBQ2pELFlBQUdxSSxTQUFTLENBQUNDLGtCQUFWLENBQTZCNUcsTUFBN0IsRUFBcUMsS0FBSzFDLGdCQUFMLENBQXNCZ0IsQ0FBdEIsQ0FBckMsQ0FBSCxFQUNJLE9BQU9BLENBQVA7QUFDUDtBQUNKOztBQUNELFdBQU8sQ0FBQyxDQUFSO0FBQ0gsR0EzbUJJO0FBNG1CTG9ILEVBQUFBLG9CQTVtQkssZ0NBNG1CZ0IxRixNQTVtQmhCLEVBNG1Cd0J5RixRQTVtQnhCLEVBNG1CaUM7QUFDbEMsUUFBRyxDQUFDL0UsS0FBSyxDQUFDQyxPQUFOLENBQWM4RSxRQUFkLENBQUosRUFBNEI7QUFDeEIsVUFBR2tCLFNBQVMsQ0FBQ0UsbUJBQVYsQ0FBOEI3RyxNQUE5QixFQUFzQyxLQUFLdEMsaUJBQUwsQ0FBdUIrSCxRQUF2QixDQUF0QyxDQUFILEVBQ0ksT0FBT0EsUUFBUDtBQUNQLEtBSEQsTUFHSztBQUNELFdBQUksSUFBSW5ILENBQUMsR0FBRyxDQUFaLEVBQWVBLENBQUMsR0FBRyxLQUFLWixpQkFBTCxDQUF1QmlDLE1BQTFDLEVBQWtEckIsQ0FBQyxFQUFuRCxFQUFzRDtBQUNsRCxZQUFHcUksU0FBUyxDQUFDRSxtQkFBVixDQUE4QjdHLE1BQTlCLEVBQXNDLEtBQUt0QyxpQkFBTCxDQUF1QlksQ0FBdkIsQ0FBdEMsQ0FBSCxFQUNJLE9BQU9BLENBQVA7QUFDUDtBQUNKOztBQUNELFdBQU8sQ0FBQyxDQUFSO0FBQ0gsR0F2bkJJO0FBd25CTDhILEVBQUFBLHNCQXhuQkssa0NBd25Ca0JYLFFBeG5CbEIsRUF3bkIyQjtBQUM1QjtBQUNBLFFBQUdBLFFBQVEsR0FBRyxDQUFDLENBQVosSUFBaUJBLFFBQVEsR0FBRyxDQUEvQixFQUFpQztBQUM3QixVQUFJcUIsT0FBTyxHQUFHLEtBQUtySixlQUFMLENBQXFCZ0ksUUFBckIsQ0FBZDtBQUNBLFVBQUlsSCxJQUFJLEdBQUd1SSxPQUFPLENBQUNBLE9BQU8sQ0FBQ25ILE1BQVIsR0FBaUIsQ0FBbEIsQ0FBbEI7O0FBQ0EsVUFBR21ILE9BQU8sQ0FBQ25ILE1BQVIsR0FBaUIsQ0FBakIsSUFBc0IsQ0FBQ3BCLElBQUksQ0FBQ3dJLE9BQS9CLEVBQXdDO0FBQ3BDeEksUUFBQUEsSUFBSSxDQUFDNEIsUUFBTCxDQUFjLElBQWQ7QUFDQSxhQUFLekMsaUJBQUwsQ0FBdUIrSCxRQUF2QixFQUFpQzFGLElBQWpDLENBQXNDeEIsSUFBSSxDQUFDMkQsTUFBM0M7QUFDQSxhQUFLckQsV0FBTDs7QUFDQSxZQUFJLEtBQUtBLFdBQUwsS0FBcUIsQ0FBekIsRUFBMkI7QUFDdkI7QUFDQSxlQUFLM0MsZUFBTCxDQUFxQitDLE1BQXJCLEdBQThCLElBQTlCO0FBQ0g7O0FBQ0QsZUFBT1YsSUFBSSxDQUFDMkQsTUFBWjtBQUNIO0FBQ0o7O0FBQ0QsV0FBTyxJQUFQO0FBQ0gsR0F6b0JJO0FBNm9CVDtBQUNJVixFQUFBQSxTQTlvQkssdUJBOG9CTTtBQUFBOztBQUNQLFNBQUt4RCxNQUFMLElBQWUsRUFBZixDQURPLENBRVA7O0FBQ0EsU0FBSSxJQUFJTSxDQUFDLEdBQUcsQ0FBWixFQUFlQSxDQUFDLEdBQUcsS0FBS2IsZUFBTCxDQUFxQmtDLE1BQXhDLEVBQWdEckIsQ0FBQyxFQUFqRCxFQUFvRDtBQUNoRCxVQUFJMEksZUFBZSxHQUFHLEtBQUt2SixlQUFMLENBQXFCYSxDQUFyQixDQUF0Qjs7QUFDQSxVQUFHMEksZUFBZSxDQUFDckgsTUFBaEIsR0FBeUIsQ0FBNUIsRUFBOEI7QUFDMUI7QUFDQSxZQUFJMkYsU0FBUyxHQUFHLEtBQUtDLG1CQUFMLENBQXlCeUIsZUFBZSxDQUFDQSxlQUFlLENBQUNySCxNQUFoQixHQUF5QixDQUExQixDQUFmLENBQTRDdUMsTUFBckUsQ0FBaEI7O0FBQ0EsWUFBR29ELFNBQVMsS0FBSyxDQUFDLENBQWxCLEVBQW9CO0FBQ2hCLGNBQUkvRyxJQUFJLEdBQUd5SSxlQUFlLENBQUNBLGVBQWUsQ0FBQ3JILE1BQWhCLEdBQXlCLENBQTFCLENBQTFCO0FBQ0EsY0FBSW1CLEdBQUcsR0FBRyxLQUFLaEUsZ0JBQUwsQ0FBc0J3SSxTQUF0QixFQUFpQzdFLFdBQWpDLEVBQVY7QUFDQSxlQUFLd0csWUFBTCxDQUFrQixDQUFDMUksSUFBRCxDQUFsQixFQUEwQnVDLEdBQTFCO0FBQ0E7QUFDSCxTQVJ5QixDQVMxQjtBQUNBOzs7QUFDQSxZQUFHa0csZUFBZSxDQUFDLENBQUQsQ0FBZixDQUFtQkQsT0FBbkIsSUFBOEJDLGVBQWUsQ0FBQyxDQUFELENBQWYsQ0FBbUI5RSxNQUFuQixHQUE0QixFQUExRCxJQUFnRThFLGVBQWUsQ0FBQyxDQUFELENBQWYsQ0FBbUI5RSxNQUFuQixHQUE0QixFQUEvRixFQUNJO0FBRUosWUFBSTRFLE9BQU8sR0FBRyxFQUFkOztBQUVBLGFBQUksSUFBSTFFLENBQUMsR0FBRyxDQUFaLEVBQWVBLENBQUMsR0FBRzRFLGVBQWUsQ0FBQ3JILE1BQW5DLEVBQTJDeUMsQ0FBQyxFQUE1QyxFQUErQztBQUMzQyxjQUFHNEUsZUFBZSxDQUFDNUUsQ0FBRCxDQUFmLENBQW1CMkUsT0FBdEIsRUFDSUQsT0FBTyxDQUFDL0csSUFBUixDQUFhaUgsZUFBZSxDQUFDNUUsQ0FBRCxDQUE1QjtBQUNQOztBQUNELFlBQUk4RSxnQkFBZ0IsR0FBRyxLQUFLeEIsb0JBQUwsQ0FBMEJvQixPQUFPLENBQUMsQ0FBRCxDQUFQLENBQVc1RSxNQUFyQyxDQUF2Qjs7QUFDQSxZQUFHZ0YsZ0JBQWdCLEtBQUssQ0FBQyxDQUF6QixFQUEyQjtBQUN2QixjQUFJVCxNQUFNLEdBQUcsSUFBYjtBQUNBLGNBQUcsS0FBS2hKLGVBQUwsQ0FBcUJ5SixnQkFBckIsRUFBdUN2SCxNQUF2QyxLQUFrRCxDQUFyRCxFQUNJOEcsTUFBTSxHQUFHLEtBQUsxSixpQkFBTCxDQUF1Qm1LLGdCQUF2QixFQUF5Q3pHLFdBQXpDLEVBQVQsQ0FESixLQUVJO0FBQ0EsZ0JBQUlpRyxXQUFXLEdBQUcsS0FBS2pKLGVBQUwsQ0FBcUJ5SixnQkFBckIsRUFBdUMsS0FBS3pKLGVBQUwsQ0FBcUJ5SixnQkFBckIsRUFBdUN2SCxNQUF2QyxHQUFnRCxDQUF2RixFQUEwRlgsSUFBMUYsQ0FBK0Z5QixXQUEvRixFQUFsQjtBQUNBZ0csWUFBQUEsTUFBTSxHQUFHbkwsRUFBRSxDQUFDQyxFQUFILENBQU1tTCxXQUFXLENBQUM3RixDQUFsQixFQUFxQjZGLFdBQVcsQ0FBQzNGLENBQVosR0FBZ0IsRUFBckMsQ0FBVDtBQUNIO0FBQ0QsZUFBS2tHLFlBQUwsQ0FBa0JILE9BQWxCLEVBQTJCTCxNQUEzQjtBQUNBO0FBQ0g7QUFDSjtBQUNKLEtBdENNLENBdUNQOzs7QUFDQSxRQUFHLEtBQUtsSixZQUFMLENBQWtCb0MsTUFBbEIsR0FBMkIsQ0FBOUIsRUFBZ0M7QUFDNUIsVUFBSXBCLE1BQUksR0FBRyxLQUFLaEIsWUFBTCxDQUFrQixLQUFLQSxZQUFMLENBQWtCb0MsTUFBbEIsR0FBMkIsQ0FBN0MsQ0FBWCxDQUQ0QixDQUU1Qjs7QUFDQSxVQUFJMkYsVUFBUyxHQUFHLEtBQUtDLG1CQUFMLENBQXlCLEtBQUtoSSxZQUFMLENBQWtCLEtBQUtBLFlBQUwsQ0FBa0JvQyxNQUFsQixHQUEyQixDQUE3QyxFQUFnRHVDLE1BQXpFLENBQWhCOztBQUNBLFVBQUdvRCxVQUFTLEtBQUssQ0FBQyxDQUFsQixFQUFvQjtBQUNoQixZQUFJNkIsU0FBUyxHQUFHLEtBQUtySyxnQkFBTCxDQUFzQndJLFVBQXRCLEVBQWlDN0UsV0FBakMsRUFBaEI7O0FBQ0EsYUFBS3dHLFlBQUwsQ0FBa0IsQ0FBQzFJLE1BQUQsQ0FBbEIsRUFBMEI0SSxTQUExQjtBQUNBO0FBQ0gsT0FSMkIsQ0FTNUI7OztBQUNBLFVBQUlELGlCQUFnQixHQUFHLEtBQUt4QixvQkFBTCxDQUEwQixLQUFLbkksWUFBTCxDQUFrQixLQUFLQSxZQUFMLENBQWtCb0MsTUFBbEIsR0FBMkIsQ0FBN0MsRUFBZ0R1QyxNQUExRSxDQUF2Qjs7QUFDQSxVQUFHZ0YsaUJBQWdCLEtBQUssQ0FBQyxDQUF6QixFQUEyQjtBQUN2QixZQUFJVCxPQUFNLEdBQUcsSUFBYjtBQUNBLFlBQUcsS0FBS2hKLGVBQUwsQ0FBcUJ5SixpQkFBckIsRUFBdUN2SCxNQUF2QyxLQUFrRCxDQUFyRCxFQUNJOEcsT0FBTSxHQUFHLEtBQUsxSixpQkFBTCxDQUF1Qm1LLGlCQUF2QixFQUF5Q3pHLFdBQXpDLEVBQVQsQ0FESixLQUVJO0FBQ0EsY0FBSWlHLFlBQVcsR0FBRyxLQUFLakosZUFBTCxDQUFxQnlKLGlCQUFyQixFQUF1QyxLQUFLekosZUFBTCxDQUFxQnlKLGlCQUFyQixFQUF1Q3ZILE1BQXZDLEdBQWdELENBQXZGLEVBQTBGWCxJQUExRixDQUErRnlCLFdBQS9GLEVBQWxCOztBQUNBZ0csVUFBQUEsT0FBTSxHQUFHbkwsRUFBRSxDQUFDQyxFQUFILENBQU1tTCxZQUFXLENBQUM3RixDQUFsQixFQUFxQjZGLFlBQVcsQ0FBQzNGLENBQVosR0FBZ0IsRUFBckMsQ0FBVDtBQUNIO0FBQ0QsYUFBS2tHLFlBQUwsQ0FBa0IsQ0FBQzFJLE1BQUQsQ0FBbEIsRUFBMEJrSSxPQUExQjtBQUNBO0FBQ0g7QUFDSixLQTlETSxDQStEUDs7O0FBQ0EsUUFBRyxLQUFLakosY0FBTCxDQUFvQm1DLE1BQXBCLEdBQTZCLENBQWhDLEVBQWtDO0FBQzlCLFdBQUksSUFBSXJCLEdBQUMsR0FBRyxDQUFaLEVBQWVBLEdBQUMsR0FBRyxLQUFLZCxjQUFMLENBQW9CbUMsTUFBdkMsRUFBK0NyQixHQUFDLEVBQWhELEVBQW1EO0FBQy9DLFlBQUk4SSxpQkFBaUIsR0FBRyxLQUFLN0IsbUJBQUwsQ0FBeUIsS0FBSy9ILGNBQUwsQ0FBb0JjLEdBQXBCLENBQXpCLENBQXhCO0FBQ0EsWUFBSStJLGdCQUFnQixHQUFHLEtBQUszQixvQkFBTCxDQUEwQixLQUFLbEksY0FBTCxDQUFvQmMsR0FBcEIsQ0FBMUIsQ0FBdkI7O0FBQ0EsWUFBRytJLGdCQUFnQixLQUFLLENBQUMsQ0FBdEIsSUFBMkJELGlCQUFpQixLQUFLLENBQUMsQ0FBckQsRUFBdUQ7QUFDbkQsY0FBSUUsT0FBTyxHQUFHLEtBQUt0SyxjQUFMLENBQW9CeUQsV0FBcEIsR0FBa0NJLENBQWxDLEdBQXNDLE1BQUksS0FBS3RELFlBQUwsQ0FBa0JvQyxNQUFsQixHQUEyQixDQUEzQixHQUErQixDQUEvQixHQUFtQyxLQUFLcEMsWUFBTCxDQUFrQm9DLE1BQXpELENBQXBEO0FBQ0EsZUFBS2xELGFBQUwsQ0FBbUJ3QyxNQUFuQixHQUE0QixJQUE1QjtBQUNBM0QsVUFBQUEsRUFBRSxDQUFDNkYsS0FBSCxDQUFTLEtBQUsxRSxhQUFkLEVBQ0syRSxFQURMLENBQ1EsR0FEUixFQUNhO0FBQUNaLFlBQUFBLFFBQVEsRUFBQ2xGLEVBQUUsQ0FBQ0MsRUFBSCxDQUFNK0wsT0FBTixFQUFlLEtBQUt0SyxjQUFMLENBQW9CeUQsV0FBcEIsR0FBa0NNLENBQWpEO0FBQVYsV0FEYixFQUVLd0MsS0FGTCxDQUVXLEdBRlgsRUFHS25DLEVBSEwsQ0FHUSxHQUhSLEVBR2E7QUFBQ1osWUFBQUEsUUFBUSxFQUFFLEtBQUt0RCxVQUFMLENBQWdCOEIsSUFBaEIsQ0FBcUJ5QixXQUFyQjtBQUFYLFdBSGIsRUFJS1ksSUFKTCxDQUlVLFlBQUk7QUFBQyxZQUFBLE1BQUksQ0FBQzVFLGFBQUwsQ0FBbUJ3QyxNQUFuQixHQUE0QixLQUE1QjtBQUFrQyxXQUpqRCxFQUtLZCxLQUxMO0FBTUE7QUFDSDtBQUNKO0FBQ0o7O0FBQ0QsU0FBS04sU0FBTCxDQUFlMEosS0FBZixDQUFxQkMsU0FBckIsQ0FBK0IsSUFBL0IsRUFBcUMsdUJBQXJDO0FBQ0gsR0FodUJJO0FBaXVCTFAsRUFBQUEsWUFqdUJLLHdCQWl1QlFRLFFBanVCUixFQWl1QmtCQyxXQWp1QmxCLEVBaXVCOEI7QUFBQSxpQ0FDdkJwSixDQUR1QjtBQUUzQixVQUFJQyxJQUFJLEdBQUdrSixRQUFRLENBQUNuSixDQUFELENBQW5CO0FBQ0EsVUFBSW1JLE1BQU0sR0FBR25MLEVBQUUsQ0FBQ0MsRUFBSCxDQUFNbU0sV0FBVyxDQUFDN0csQ0FBbEIsRUFBcUI2RyxXQUFXLENBQUMzRyxDQUFaLEdBQWdCLEtBQUd6QyxDQUF4QyxDQUFiO0FBQ0FoRCxNQUFBQSxFQUFFLENBQUM2RixLQUFILENBQVM1QyxJQUFJLENBQUNTLElBQWQsRUFDS3FDLElBREwsQ0FDVSxZQUFJO0FBQ045QyxRQUFBQSxJQUFJLENBQUNvSixVQUFMLENBQWdCLElBQWhCO0FBQ0FwSixRQUFBQSxJQUFJLENBQUNTLElBQUwsQ0FBVVgsTUFBVixHQUFtQixNQUFNQyxDQUF6QjtBQUNILE9BSkwsRUFLSzhDLEVBTEwsQ0FLUSxHQUxSLEVBS2E7QUFBQ1osUUFBQUEsUUFBUSxFQUFDaUc7QUFBVixPQUxiLEVBTUtsRCxLQU5MLENBTVcsR0FOWCxFQU9LbkMsRUFQTCxDQU9RLEdBUFIsRUFPYTtBQUFDWixRQUFBQSxRQUFRLEVBQUNqQyxJQUFJLENBQUN5QztBQUFmLE9BUGIsRUFRS0ssSUFSTCxDQVFVLFlBQUk7QUFDTjlDLFFBQUFBLElBQUksQ0FBQ29KLFVBQUwsQ0FBZ0IsS0FBaEI7QUFDQXBKLFFBQUFBLElBQUksQ0FBQ1MsSUFBTCxDQUFVWCxNQUFWLEdBQW1CRSxJQUFJLENBQUMwQyxRQUF4QjtBQUNILE9BWEwsRUFZSzlDLEtBWkw7QUFKMkI7O0FBQy9CLFNBQUksSUFBSUcsQ0FBQyxHQUFHLENBQVosRUFBZUEsQ0FBQyxHQUFHbUosUUFBUSxDQUFDOUgsTUFBNUIsRUFBb0NyQixDQUFDLEVBQXJDLEVBQXdDO0FBQUEsYUFBaENBLENBQWdDO0FBZ0J2QztBQUNKLEdBbnZCSTtBQW92QlQ7QUFDSWdELEVBQUFBLFlBcnZCSywwQkFxdkJTO0FBQ1Y7QUFDQSxTQUFJLElBQUloRCxDQUFDLEdBQUcsQ0FBWixFQUFlQSxDQUFDLEdBQUcsS0FBS2IsZUFBTCxDQUFxQmtDLE1BQXhDLEVBQWdEckIsQ0FBQyxFQUFqRCxFQUFvRDtBQUNoRCxVQUFJMEksZUFBZSxHQUFHLEtBQUt2SixlQUFMLENBQXFCYSxDQUFyQixDQUF0Qjs7QUFDQSxVQUFHMEksZUFBZSxDQUFDckgsTUFBaEIsR0FBeUIsQ0FBNUIsRUFBOEI7QUFDMUI7QUFDQSxZQUFJcEIsSUFBSSxHQUFHeUksZUFBZSxDQUFDQSxlQUFlLENBQUNySCxNQUFoQixHQUF5QixDQUExQixDQUExQjtBQUNBLFlBQUkyRixTQUFTLEdBQUcsS0FBS0MsbUJBQUwsQ0FBeUJoSCxJQUFJLENBQUMyRCxNQUE5QixDQUFoQjs7QUFDQSxZQUFHb0QsU0FBUyxJQUFJLENBQUMsQ0FBakIsRUFBbUI7QUFDZi9HLFVBQUFBLElBQUksQ0FBQ1MsSUFBTCxDQUFVWCxNQUFWLEdBQW1CLEdBQW5CO0FBQ0EsZUFBS2tFLGdCQUFMLENBQXNCaEUsSUFBdEIsRUFBNEIrRyxTQUE1QjtBQUNBLGlCQUhlLENBSWY7QUFDSDtBQUNKO0FBQ0osS0FmUyxDQWdCVjs7O0FBQ0EsUUFBRyxLQUFLL0gsWUFBTCxDQUFrQm9DLE1BQWxCLEdBQTJCLENBQTlCLEVBQWlDO0FBQzdCLFVBQUlwQixNQUFJLEdBQUcsS0FBS2hCLFlBQUwsQ0FBa0IsS0FBS0EsWUFBTCxDQUFrQm9DLE1BQWxCLEdBQTJCLENBQTdDLENBQVgsQ0FENkIsQ0FFN0I7O0FBQ0EsVUFBSTJGLFdBQVMsR0FBRyxLQUFLQyxtQkFBTCxDQUF5QmhILE1BQUksQ0FBQzJELE1BQTlCLENBQWhCOztBQUNBLFVBQUlvRCxXQUFTLEtBQUssQ0FBQyxDQUFuQixFQUFzQjtBQUNsQi9HLFFBQUFBLE1BQUksQ0FBQ1MsSUFBTCxDQUFVWCxNQUFWLEdBQW1CLEdBQW5CO0FBQ0EsYUFBS2tFLGdCQUFMLENBQXNCaEUsTUFBdEIsRUFBNEIrRyxXQUE1QixFQUZrQixDQUdsQjs7QUFDQTtBQUNIOztBQUNELFVBQUk0QixnQkFBZ0IsR0FBRyxLQUFLeEIsb0JBQUwsQ0FBMEJuSCxNQUFJLENBQUMyRCxNQUEvQixDQUF2Qjs7QUFDQSxVQUFHZ0YsZ0JBQWdCLEtBQUssQ0FBQyxDQUF6QixFQUEyQjtBQUN2QjNJLFFBQUFBLE1BQUksQ0FBQ1MsSUFBTCxDQUFVWCxNQUFWLEdBQW1CLEdBQW5CO0FBQ0EsYUFBS21FLGdCQUFMLENBQXNCLENBQUNqRSxNQUFELENBQXRCLEVBQThCMkksZ0JBQTlCO0FBQ0E7QUFDSDtBQUNKOztBQUNELFFBQUcsS0FBSzFKLGNBQUwsQ0FBb0JtQyxNQUFwQixHQUE2QixDQUFoQyxFQUFrQztBQUM5QixXQUFLTixlQUFMO0FBQ0E7QUFDSDs7QUFDRCxTQUFLNEcsZUFBTDtBQUNILEdBNXhCSTtBQTZ4QkxBLEVBQUFBLGVBN3hCSyw2QkE2eEJZO0FBQ2IsU0FBSSxJQUFJM0gsQ0FBQyxHQUFHLENBQVosRUFBZUEsQ0FBQyxHQUFHLEtBQUtqQixjQUFMLENBQW9Cc0MsTUFBdkMsRUFBK0NyQixDQUFDLEVBQWhELEVBQW9EO0FBQ2hELFVBQUksS0FBS2pCLGNBQUwsQ0FBb0JpQixDQUFwQixFQUF1QnFCLE1BQXZCLEdBQWdDLEVBQXBDLEVBQXVDO0FBQ25DO0FBQ0g7QUFDSjs7QUFDRCxTQUFLUCxVQUFMLEdBQWtCLEtBQWxCO0FBQ0EsU0FBS3dJLGNBQUw7QUFDSCxHQXJ5Qkk7QUFzeUJMQSxFQUFBQSxjQXR5QkssNEJBc3lCVztBQUFBOztBQUNaLFFBQUlDLGFBQWEsR0FBR3hNLFVBQVUsQ0FBQ3lNLElBQUksQ0FBQ0MsS0FBTCxDQUFXRCxJQUFJLENBQUNFLE1BQUwsS0FBYyxDQUF6QixDQUFELENBQTlCOztBQURZLGlDQUVKMUosQ0FGSTtBQUFBLG1DQUdBOEQsQ0FIQTtBQUlKLFlBQUk3RCxJQUFJLEdBQUcsTUFBSSxDQUFDbEIsY0FBTCxDQUFvQmlCLENBQXBCLEVBQXVCOEQsQ0FBdkIsQ0FBWDtBQUNBLFlBQUk2RixRQUFRLEdBQUdKLGFBQWEsQ0FBQyxDQUFDekYsQ0FBQyxHQUFDOUQsQ0FBSCxJQUFNdUosYUFBYSxDQUFDbEksTUFBckIsQ0FBNUI7QUFDQSxZQUFJdUksU0FBUyxHQUFHTCxhQUFhLENBQUMsQ0FBQ3pGLENBQUMsR0FBQzlELENBQUYsR0FBSSxDQUFMLElBQVF1SixhQUFhLENBQUNsSSxNQUF2QixDQUE3QjtBQUNBLFlBQUl3SSxRQUFRLEdBQUdOLGFBQWEsQ0FBQyxDQUFDekYsQ0FBQyxHQUFDOUQsQ0FBRixHQUFJLENBQUwsSUFBUXVKLGFBQWEsQ0FBQ2xJLE1BQXZCLENBQTVCO0FBQ0EsWUFBSXlJLFNBQVMsR0FBR1AsYUFBYSxDQUFDLENBQUN6RixDQUFDLEdBQUM5RCxDQUFGLEdBQUksQ0FBTCxJQUFRdUosYUFBYSxDQUFDbEksTUFBdkIsQ0FBN0I7QUFDQSxZQUFJMEksUUFBUSxHQUFHUixhQUFhLENBQUMsQ0FBQ3pGLENBQUMsR0FBQzlELENBQUYsR0FBSSxDQUFMLElBQVF1SixhQUFhLENBQUNsSSxNQUF2QixDQUE1QjtBQUVBckUsUUFBQUEsRUFBRSxDQUFDNkYsS0FBSCxDQUFTNUMsSUFBSSxDQUFDUyxJQUFkLEVBQ0t1RSxLQURMLENBQ1csTUFBTSxNQUFJbkIsQ0FBVixHQUFjLE1BQUk5RCxDQUQ3QixFQUVLOEMsRUFGTCxDQUVRLEdBRlIsRUFFYTtBQUFDWixVQUFBQSxRQUFRLEVBQUN5SDtBQUFWLFNBRmIsRUFHSzFFLEtBSEwsQ0FHVyxPQUFLLEtBQUtuQixDQUFWLENBSFgsRUFJS2hCLEVBSkwsQ0FJUSxHQUpSLEVBSWE7QUFBQ1osVUFBQUEsUUFBUSxFQUFDMEg7QUFBVixTQUpiLEVBS0s5RyxFQUxMLENBS1EsR0FMUixFQUthO0FBQUNaLFVBQUFBLFFBQVEsRUFBQzJIO0FBQVYsU0FMYixFQU1LL0csRUFOTCxDQU1RLEdBTlIsRUFNYTtBQUFDWixVQUFBQSxRQUFRLEVBQUM0SDtBQUFWLFNBTmIsRUFPS2hILEVBUEwsQ0FPUSxHQVBSLEVBT2E7QUFBQ1osVUFBQUEsUUFBUSxFQUFDNkg7QUFBVixTQVBiLEVBUUs5RSxLQVJMLENBUVcsTUFBSW5CLENBQUosR0FBUSxNQUFJOUQsQ0FSdkIsRUFTSzhDLEVBVEwsQ0FTUSxNQUFPLE9BQUtnQixDQVRwQixFQVN3QjtBQUFDNUIsVUFBQUEsUUFBUSxFQUFDakMsSUFBSSxDQUFDeUM7QUFBZixTQVR4QixFQVVLSyxJQVZMLENBVVUsWUFBSTtBQUNOLGNBQUcvQyxDQUFDLEtBQUssTUFBSSxDQUFDakIsY0FBTCxDQUFvQnNDLE1BQXBCLEdBQTZCLENBQW5DLElBQXdDeUMsQ0FBQyxLQUFLLE1BQUksQ0FBQy9FLGNBQUwsQ0FBb0JpQixDQUFwQixFQUF1QnFCLE1BQXZCLEdBQWdDLENBQTlFLElBQW1GLENBQUMsTUFBSSxDQUFDOUIsU0FBTCxDQUFlMEosS0FBZixDQUFxQnZJLElBQXJCLENBQTBCQyxNQUFqSCxFQUF3SDtBQUNwSDtBQUNBLFlBQUEsTUFBSSxDQUFDcEIsU0FBTCxDQUFlMEosS0FBZixDQUFxQkMsU0FBckIsQ0FBK0IsSUFBL0IsRUFBcUMsa0JBQXJDO0FBQ0g7QUFFSixTQWhCTCxFQWlCS3JKLEtBakJMO0FBWEk7O0FBR1IsV0FBSSxJQUFJaUUsQ0FBQyxHQUFHLENBQVosRUFBZUEsQ0FBQyxHQUFHLE1BQUksQ0FBQy9FLGNBQUwsQ0FBb0JpQixDQUFwQixFQUF1QnFCLE1BQTFDLEVBQWtEeUMsQ0FBQyxFQUFuRCxFQUFzRDtBQUFBLGVBQTlDQSxDQUE4QztBQTJCckQ7QUE5Qk87O0FBRVosU0FBSSxJQUFJOUQsQ0FBQyxHQUFHLENBQVosRUFBZUEsQ0FBQyxHQUFHLEtBQUtqQixjQUFMLENBQW9Cc0MsTUFBdkMsRUFBK0NyQixDQUFDLEVBQWhELEVBQW1EO0FBQUEsYUFBM0NBLENBQTJDO0FBNkJsRDtBQUNKO0FBdDBCSSxDQUFUIiwic291cmNlUm9vdCI6Ii8iLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgRGVja3MgPSByZXF1aXJlKFwiRGVja3NcIik7XG52YXIgQ2FyZFByZWZhYiA9IHJlcXVpcmUoXCJDYXJkUHJlZmFiXCIpO1xudmFyIEZCU0RLID0gcmVxdWlyZShcIkZiU2RrXCIpO1xuY29uc3QgYXJyUG9zQW5pbSA9IFtbY2MudjIoLTIwMCwgMCksIGNjLnYyKDIwMCwgMCksIGNjLnYyKC0yMDAsIC0yNTApLCBjYy52MigyMDAsIC0yNTApLCBjYy52MigtMjAwLCAtNTAwKSwgY2MudjIoMjAwLCAtNTAwKV0sXG4gICAgW2NjLnYyKDAsIDApLCBjYy52MigxNTAsIC01NTApLCBjYy52MigtMzAwLCAtMjUwKSwgY2MudjIoMzAwLCAtMjUwKSwgY2MudjIoLTE1MCwgLTU1MCldLFxuICAgIFtjYy52MigtMzUwLCAtMTAwKSwgY2MudjIoLTIwMCwgLTQ1MCksIGNjLnYyKDAsIC0xMDApLCBjYy52MigyMDAsIC00NTApLCBjYy52MigzNTAsIC0xMDApXV1cbmNvbnN0IHR5cGVBY3Rpb24gPSB7XG4gICAgR0VUX0NBUkRfT05fREVDSzogMCxcbiAgICBNT1ZFX0NBUkRfREVDS19UT19IT0xERVI6IDEsXG4gICAgTU9WRV9DQVJEX0RFQ0tfVE9fVEFCTEVHUk9VUDogMixcbiAgICBNT1ZFX0NBUkRfSE9MREVSX1RPX1RBQkxFR1JPVVA6IDMsXG4gICAgTU9WRV9DQVJEX1RBQkxFX1RPX0hPTERFUjo0LFxuICAgIE1PVkVfQ0FSRF9UQUJMRV9UT19UQUJMRTogNVxufVxuXG5jYy5DbGFzcyh7XG4gICAgZXh0ZW5kczogY2MuQ29tcG9uZW50LFxuXG4gICAgcHJvcGVydGllczoge1xuICAgICAgICBidG5BdXRvQ29tcGxldGU6IGNjLk5vZGUsXG4gICAgICAgIGJ0bkhpbnQ6IGNjLk5vZGUsXG4gICAgICAgIGJ0blVuZG86IGNjLkJ1dHRvbixcbiAgICAgICAgcHJlZmFiQ2FyZDogY2MuUHJlZmFiLFxuICAgICAgICBzcnBDYXJkT25EZWNrOiBjYy5Ob2RlLFxuICAgICAgICBjYXJkTm9kZVBvb2w6e1xuICAgICAgICAgICAgZGVmYXVsdDogbnVsbCxcbiAgICAgICAgICAgIGhpZGRlbjogZmFsc2UsXG4gICAgICAgICAgICB0eXBlOiBjYy5Ob2RlUG9vbFxuICAgICAgICB9LFxuICAgICAgICBjYXJkSG9sZGVyQW5jaG9yOiBbY2MuTm9kZV0sXG4gICAgICAgIGNhcmRPblRhYmxlQW5jaG9yOiBbY2MuTm9kZV0sXG4gICAgICAgIGNhcmREZWNrQW5jaG9yOmNjLk5vZGUsXG4gICAgICAgIG5vZGVDYXJkQ29udGFpbmVyOiBjYy5Ob2RlLFxuICAgICAgICBidG5HZXRDYXJkOiBjYy5CdXR0b24sXG4gICAgICAgIGFyclJlc291cmNlc0NhcmQ6IFtjYy5TcHJpdGVGcmFtZV0sXG5cblxuICAgICAgICBsaXN0Q2FyZEhvbGRlcjogW10sXG4gICAgICAgIGxpc3RJZENhcmRIb2xkZXI6W10sXG5cbiAgICAgICAgbGlzdENhcmREZWNrOiBbXSxcbiAgICAgICAgbGlzdElkQ2FyZERlY2s6IFtdLFxuXG4gICAgICAgIGxpc3RDYXJkT25UYWJsZTogW10sXG4gICAgICAgIGxpc3RJZENhcmRPblRhYmxlOiBbXSxcbiAgICAgICAgYXJyQ2FyZE1vdmU6W10sXG4gICAgfSxcblxuICAgIC8vIExJRkUtQ1lDTEUgQ0FMTEJBQ0tTOlxuICAgIGN0b3IoKXtcbiAgICAgICAgdGhpcy5nYW1lU2NlbmUgPSBudWxsO1xuICAgICAgICB0aGlzLmRlY2tzID0gbmV3IERlY2tzKCk7XG4gICAgICAgIHRoaXMubGlzdENhcmRIb2xkZXIgPSBbW10sW10sW10sW11dO1xuICAgICAgICB0aGlzLmxpc3RJZENhcmRIb2xkZXIgPSBbW10sW10sW10sW11dO1xuXG4gICAgICAgIHRoaXMubGlzdENhcmREZWNrID0gW107XG4gICAgICAgIHRoaXMubGlzdElkQ2FyZERlY2sgPSBbXTtcblxuICAgICAgICB0aGlzLmxpc3RDYXJkT25UYWJsZSA9IFtdO1xuICAgICAgICB0aGlzLmxpc3RJZENhcmRPblRhYmxlID0gW107XG4gICAgICAgIHRoaXMuYkF1dG9Db21wbGV0ZSA9IGZhbHNlO1xuXG4gICAgICAgIHRoaXMuaVNjb3JlID0gMDtcbiAgICAgICAgdGhpcy5pTW92ZSA9IDA7XG4gICAgICAgIHRoaXMuaGlzdG9yeUFjdGlvbiA9IFtdO1xuICAgIH0sXG5cbiAgICBzdGFydCAoKSB7XG4gICAgICAgIHRoaXMuaW5pdCgpO1xuICAgIH0sXG4gICAgaW5pdCgpe1xuICAgICAgICB0aGlzLnNycENhcmRPbkRlY2suekluZGV4ID0gMTAwO1xuICAgICAgICB0aGlzLmNhcmROb2RlUG9vbCA9IG5ldyBjYy5Ob2RlUG9vbCgpO1xuICAgICAgICBmb3IobGV0IGkgPSAwOyBpIDwgNTI7IGkrKyl7XG4gICAgICAgICAgICBsZXQgY2FyZCA9IGNjLmluc3RhbnRpYXRlKHRoaXMucHJlZmFiQ2FyZCk7XG4gICAgICAgICAgICB0aGlzLmNhcmROb2RlUG9vbC5wdXQoY2FyZCk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5yZXNldEdhbWUoKTtcbiAgICAgICAgdGhpcy5mYnNkayA9IG5ldyBGQlNESygpO1xuICAgICAgICB0aGlzLmZic2RrLmluaXQoKTtcbiAgICB9LFxuICAgIHJlc2V0R2FtZSgpe1xuICAgICAgICB0aGlzLm5vZGVDYXJkQ29udGFpbmVyLnJlbW92ZUFsbENoaWxkcmVuKHRydWUpO1xuXG4gICAgICAgIHRoaXMubGlzdENhcmRIb2xkZXIgPSBbW10sW10sW10sW11dO1xuICAgICAgICB0aGlzLmxpc3RJZENhcmRIb2xkZXIgPSBbW10sW10sW10sW11dO1xuXG4gICAgICAgIHRoaXMubGlzdENhcmREZWNrID0gW107XG4gICAgICAgIHRoaXMubGlzdElkQ2FyZERlY2sgPSBbXTtcblxuICAgICAgICB0aGlzLmxpc3RJZENhcmRPblRhYmxlID0gW1tdLFtdLFtdLFtdLFtdLFtdLFtdXTtcbiAgICAgICAgdGhpcy5saXN0Q2FyZE9uVGFibGUgPSBbW10sW10sW10sW10sW10sW10sW11dO1xuXG4gICAgICAgIHRoaXMuaUhpZGRlbkNhcmQgPSAyMTtcbiAgICAgICAgdGhpcy5pZHhDdXJyZW50RGVjayA9IDA7XG4gICAgICAgIHRoaXMuYXJyR3JvdXBDYXJkT25UYWJsZSA9IFtdO1xuICAgICAgICAvLyB0aGlzLmJ0bk5ld0dhbWUubm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgICAgICB0aGlzLmJ0bkdldENhcmQubm9kZS5hY3RpdmUgPSB0cnVlO1xuICAgICAgICB0aGlzLmJ0bkdldENhcmQuaW50ZXJhY3RhYmxlID0gZmFsc2U7XG4gICAgICAgIHRoaXMuc2V0U3RhdGVHZXRDYXJkKGZhbHNlKTtcbiAgICAgICAgdGhpcy5iR2FtZVN0YXJ0ID0gZmFsc2U7XG4gICAgICAgIHRoaXMuYkF1dG9Db21wbGV0ZSA9IGZhbHNlO1xuICAgICAgICB0aGlzLmJ0bkF1dG9Db21wbGV0ZS5hY3RpdmUgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5zcnBDYXJkT25EZWNrLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICB0aGlzLmlTY29yZSA9IDEwMDA7XG4gICAgICAgIHRoaXMuaU1vdmUgPSAwO1xuICAgICAgICAvLyB0aGlzLmJ0blVuZG8uaW50ZXJhY3RhYmxlID0gZmFsc2U7XG4gICAgICAgIHRoaXMuaGlzdG9yeUFjdGlvbiA9IFtdO1xuICAgIH0sXG4gICAgYnRuR2V0Q2FyZENsaWNrKCl7XG4gICAgICAgIGlmKHRoaXMuYkdhbWVTdGFydCl7XG4gICAgICAgICAgICB0aGlzLmdhbWVTY2VuZS5wbGF5RWZmZWN0KFNvdW5kRWZmZWN0LkNMSUNLX1NPVU5EKTtcbiAgICAgICAgICAgIHRoaXMucHJvY2Vzc0dldENhcmQoKTtcbiAgICAgICAgfVxuICAgIH0sXG4gICAgcHJvY2Vzc0dldENhcmQoc291cmNlUG9zKXtcbiAgICAgICAgLy8gY2MubG9nKFwiYnRuR2V0Q2FyZENsaWNrIDpcIiArIHRoaXMubGlzdElkQ2FyZERlY2spO1xuICAgICAgICAvLyBjYy5sb2coXCJ0aGlzLmxpc3RDYXJkRGVjazogXCIgKyB0aGlzLmxpc3RDYXJkRGVjay5sZW5ndGgpO1xuXG4gICAgICAgIGlmKHRoaXMubGlzdElkQ2FyZERlY2subGVuZ3RoIDwgMSkgcmV0dXJuO1xuICAgICAgICB0aGlzLmdhbWVTY2VuZS5wbGF5RWZmZWN0KFNvdW5kRWZmZWN0LkZMSVBfU09VTkQpO1xuICAgICAgICB0aGlzLnNldFN0YXRlR2V0Q2FyZCh0aGlzLmlkeEN1cnJlbnREZWNrIDwgdGhpcy5saXN0SWRDYXJkRGVjay5sZW5ndGggLSAxKTtcbiAgICAgICAgaWYodGhpcy5pZHhDdXJyZW50RGVjayA9PT0gdGhpcy5saXN0SWRDYXJkRGVjay5sZW5ndGgpIHtcbiAgICAgICAgICAgIHRoaXMuaWR4Q3VycmVudERlY2sgPSAwO1xuICAgICAgICAgICAgdGhpcy5zZXRTdGF0ZUdldENhcmQodHJ1ZSk7XG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMubGlzdENhcmREZWNrLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5saXN0Q2FyZERlY2tbaV0ubm9kZS5yZW1vdmVGcm9tUGFyZW50KHRydWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5saXN0Q2FyZERlY2sgPSBbXTtcbiAgICAgICAgfVxuICAgICAgICAvLyB9ZWxzZXtcbiAgICAgICAgbGV0IG9iamVjdEhpc3RvcnkgPSB7fTtcbiAgICAgICAgb2JqZWN0SGlzdG9yeS50eXBlQWN0aW9uID0gdHlwZUFjdGlvbi5HRVRfQ0FSRF9PTl9ERUNLO1xuICAgICAgICB0aGlzLmhpc3RvcnlBY3Rpb24ucHVzaChvYmplY3RIaXN0b3J5KTtcbiAgICAgICAgdGhpcy5pTW92ZSArKztcbiAgICAgICAgdGhpcy5pU2NvcmUgLT0gNTtcbiAgICAgICAgICAgIGxldCBjYXJkID0gbnVsbDtcbiAgICAgICAgICAgIGxldCBjYXJkSWQgPSB0aGlzLmxpc3RJZENhcmREZWNrW3RoaXMuaWR4Q3VycmVudERlY2tdO1xuICAgICAgICAgICAgaWYodGhpcy5saXN0Q2FyZERlY2subGVuZ3RoID09PSAzKXtcbiAgICAgICAgICAgICAgICBmb3IobGV0IGkgPSAxOyBpIDwgMzsgaSsrKXtcbiAgICAgICAgICAgICAgICAgICAgbGV0IGNhcmRJZDIgPSB0aGlzLmxpc3RJZENhcmREZWNrW3RoaXMuaWR4Q3VycmVudERlY2sgLSBpXTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5saXN0Q2FyZERlY2tbMi1pXS5zZXRTcHJpdGVDYXJkKGNhcmRJZDIsIHRoaXMuYXJyUmVzb3VyY2VzQ2FyZFtjYXJkSWQyXSk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubGlzdENhcmREZWNrWzItaV0uc2hvd0NhcmQoZmFsc2UpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBjYXJkID0gdGhpcy5saXN0Q2FyZERlY2tbMl07XG4gICAgICAgICAgICAgICAgY2FyZC5yZXNldENhcmQoKTtcbiAgICAgICAgICAgICAgICBjYXJkLnNldFNwcml0ZUNhcmQoY2FyZElkLCB0aGlzLmFyclJlc291cmNlc0NhcmRbY2FyZElkXSk7XG4gICAgICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgICAgICBjYXJkID0gdGhpcy5nZXRDYXJkKGNhcmRJZCk7XG4gICAgICAgICAgICAgICAgdGhpcy5ub2RlQ2FyZENvbnRhaW5lci5hZGRDaGlsZChjYXJkLm5vZGUpO1xuICAgICAgICAgICAgICAgIGlmKHRoaXMubGlzdENhcmREZWNrLmxlbmd0aCA+IDApe1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmxpc3RDYXJkRGVja1t0aGlzLmxpc3RDYXJkRGVjay5sZW5ndGggLSAxXS5iVG91Y2ggPSBmYWxzZTtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICB0aGlzLmxpc3RDYXJkRGVjay5wdXNoKGNhcmQpO1xuXG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjYXJkLm5vZGUucG9zaXRpb24gPSB0aGlzLmJ0bkdldENhcmQubm9kZS5nZXRQb3NpdGlvbigpO1xuICAgICAgICAgICAgaWYoIVV0aWxzLmlzRW1wdHkoc291cmNlUG9zKSlcbiAgICAgICAgICAgICAgICBjYXJkLm5vZGUucG9zaXRpb24gPSBzb3VyY2VQb3M7XG4gICAgICAgICAgICBjYXJkLm5vZGUuekluZGV4ID0gdGhpcy5saXN0Q2FyZERlY2subGVuZ3RoO1xuICAgICAgICAgICAgbGV0IHBvc1ggPSB0aGlzLmNhcmREZWNrQW5jaG9yLmdldFBvc2l0aW9uKCkueCArIDUwKih0aGlzLmxpc3RDYXJkRGVjay5sZW5ndGggLSAxKTtcbiAgICAgICAgICAgIGxldCBwb3MgPSBjYy52Mihwb3NYLCB0aGlzLmNhcmREZWNrQW5jaG9yLmdldFBvc2l0aW9uKCkueSk7XG4gICAgICAgICAgICBjYXJkLm9yaVBvcyA9IHBvcztcbiAgICAgICAgICAgIC8vIGNjLmxvZyhcImNhcmREZWNrX1wiICsgdGhpcy5saXN0Q2FyZERlY2subGVuZ3RoICsgXCIgaGFzIG9yaVBvczogXCIgKyBwb3MueCArIFwiLVwiICsgcG9zLnkpO1xuICAgICAgICAgICAgY2FyZC5vcmlJbmRleCA9IHRoaXMubGlzdENhcmREZWNrLmxlbmd0aDtcbiAgICAgICAgICAgIGxldCBpVGltZSA9IHRoaXMuYkF1dG9Db21wbGV0ZSA/IDAuMDUgOiAwLjE7XG4gICAgICAgICAgICBjYy50d2VlbihjYXJkLm5vZGUpXG4gICAgICAgICAgICAgICAgLnRvKGlUaW1lLCB7cG9zaXRpb246IHBvc30pXG4gICAgICAgICAgICAgICAgLmNhbGwoKCk9PntcbiAgICAgICAgICAgICAgICAgICAgY2FyZC5zaG93Q2FyZCh0cnVlKTtcbiAgICAgICAgICAgICAgICAgICAgaWYodGhpcy5iQXV0b0NvbXBsZXRlKVxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hdXRvQ29tcGxldGUoKTtcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgIC5zdGFydCgpO1xuXG4gICAgICAgICAgICB0aGlzLmlkeEN1cnJlbnREZWNrKys7XG4gICAgICAgIHRoaXMuYnRuR2V0Q2FyZC5ub2RlLmFjdGl2ZSA9ICEodGhpcy5saXN0SWRDYXJkRGVjay5sZW5ndGggPCAyKTtcbiAgICAgICAgLy8gY2MubG9nKFwiYnRuR2V0Q2FyZENsaWNrIDpcIiArIHRoaXMubGlzdElkQ2FyZERlY2spO1xuICAgICAgICAvLyBjYy5sb2coXCJ0aGlzLmxpc3RDYXJkRGVjazogXCIgKyB0aGlzLmxpc3RDYXJkRGVjay5sZW5ndGgpO1xuICAgICAgICAvLyB9XG4gICAgfSxcbiAgICBidG5IaW50Q2xpY2soKXtcbiAgICAgICAgdGhpcy5nYW1lU2NlbmUucGxheUVmZmVjdChTb3VuZEVmZmVjdC5DTElDS19TT1VORCk7XG4gICAgICAgIC8vIGxldCBzZWxmID0gdGhpcztcbiAgICAgICAgLy8gdGhpcy5mYnNkay5zaG93UmV3YXJkVmlkZW8oKCk9PntcbiAgICAgICAgLy8gICAgIGlmKHNlbGYuYkdhbWVTdGFydClcbiAgICAgICAgLy8gICAgICAgICBzZWxmLmNoZWNrSGludCgpO1xuICAgICAgICAvLyB9KTtcbiAgICAgICAgaWYodGhpcy5iR2FtZVN0YXJ0KVxuICAgICAgICAgICAgdGhpcy5jaGVja0hpbnQoKTtcblxuICAgIH0sXG4gICAgYnRuQXV0b0NvbXBsZXRlQ2xpY2soKXtcbiAgICAgICAgaWYodGhpcy5iR2FtZVN0YXJ0KXtcbiAgICAgICAgICAgIHRoaXMuZ2FtZVNjZW5lLnBsYXlFZmZlY3QoU291bmRFZmZlY3QuQ0xJQ0tfU09VTkQpO1xuICAgICAgICAgICAgdGhpcy5iQXV0b0NvbXBsZXRlID0gdHJ1ZTtcbiAgICAgICAgICAgIHRoaXMuYnRuQXV0b0NvbXBsZXRlLmFjdGl2ZSA9IGZhbHNlO1xuICAgICAgICAgICAgdGhpcy5hdXRvQ29tcGxldGUoKTtcbiAgICAgICAgfVxuICAgICAgICBcbiAgICB9LFxuICAgIGJ0blVuZG9DbGljaygpe1xuICAgICAgICBjYy5sb2coXCI9PT09PT09PT09IFVuZG8gQ2xpY2s6XCIgKyB0aGlzLmhpc3RvcnlBY3Rpb24ubGVuZ3RoKTtcbiAgICAgICAgdGhpcy5nYW1lU2NlbmUucGxheUVmZmVjdChTb3VuZEVmZmVjdC5DTElDS19TT1VORCk7XG4gICAgICAgIGlmKHRoaXMuaGlzdG9yeUFjdGlvbi5sZW5ndGggPiAwICYmIHRoaXMuYkdhbWVTdGFydCl7XG4gICAgICAgICAgICB0aGlzLmlNb3ZlKys7XG4gICAgICAgICAgICBsZXQgbGFzdEFjdGlvbiA9IHRoaXMuaGlzdG9yeUFjdGlvblt0aGlzLmhpc3RvcnlBY3Rpb24ubGVuZ3RoIC0gMV07XG5cbiAgICAgICAgICAgIGlmKGxhc3RBY3Rpb24uaGFzT3duUHJvcGVydHkoXCJjYXJkSWRBY3RpdmVBZnRlck1vdmVcIikgJiYgIVV0aWxzLmlzRW1wdHkobGFzdEFjdGlvbi5jYXJkSWRBY3RpdmVBZnRlck1vdmUpKXtcbiAgICAgICAgICAgICAgICBsZXQgYXJyR3JvdXBDYXJkID0gdGhpcy5saXN0Q2FyZE9uVGFibGVbbGFzdEFjdGlvbi5pZHhTb3VyY2VdO1xuICAgICAgICAgICAgICAgIGxldCBsaXN0SWRDYXJkT2ZHcm91cCA9IHRoaXMubGlzdElkQ2FyZE9uVGFibGVbbGFzdEFjdGlvbi5pZHhTb3VyY2VdO1xuICAgICAgICAgICAgICAgIGxldCBjYXJkID0gYXJyR3JvdXBDYXJkW2Fyckdyb3VwQ2FyZC5sZW5ndGggLSAxXTtcbiAgICAgICAgICAgICAgICBjYXJkLnJlc2V0Q2FyZCgpO1xuICAgICAgICAgICAgICAgIGNhcmQuaWRDYXJkID0gbGFzdEFjdGlvbi5jYXJkSWRBY3RpdmVBZnRlck1vdmU7XG4gICAgICAgICAgICAgICAgdGhpcy5pSGlkZGVuQ2FyZCsrO1xuICAgICAgICAgICAgICAgIGxpc3RJZENhcmRPZkdyb3VwLnNwbGljZShsaXN0SWRDYXJkT2ZHcm91cC5sZW5ndGggLSAxLCAxKTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgc3dpdGNoIChsYXN0QWN0aW9uLnR5cGVBY3Rpb24pe1xuICAgICAgICAgICAgICAgIGNhc2UgdHlwZUFjdGlvbi5HRVRfQ0FSRF9PTl9ERUNLOlxuICAgICAgICAgICAgICAgICAgICB0aGlzLmlkeEN1cnJlbnREZWNrLS07XG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmxpc3RDYXJkRGVjay5sZW5ndGggPiAwKXtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubGlzdENhcmREZWNrW3RoaXMubGlzdENhcmREZWNrLmxlbmd0aCAtIDFdLm5vZGUucmVtb3ZlRnJvbVBhcmVudCh0cnVlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubGlzdENhcmREZWNrLnNwbGljZSh0aGlzLmxpc3RDYXJkRGVjay5sZW5ndGggLSAxLCAxKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBpZih0aGlzLmxpc3RDYXJkRGVjay5sZW5ndGggPiAyKXtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvcihsZXQgaSA9IHRoaXMubGlzdENhcmREZWNrLmxlbmd0aCwgaiA9IDA7IGkgPiAwOyBpLS0sIGorKykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxldCBjYXJkSWQgPSB0aGlzLmxpc3RJZENhcmREZWNrW3RoaXMuaWR4Q3VycmVudERlY2sgLSAxIC0gal07XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5saXN0Q2FyZERlY2tbaSAtIDFdLnNldFNwcml0ZUNhcmQoY2FyZElkLCB0aGlzLmFyclJlc291cmNlc0NhcmRbY2FyZElkXSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5saXN0Q2FyZERlY2tbaSAtIDFdLnNob3dDYXJkKGZhbHNlKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfWVsc2UgaWYgKHRoaXMubGlzdENhcmREZWNrLmxlbmd0aCA+IDApe1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gdGhpcy5saXN0Q2FyZERlY2tbdGhpcy5saXN0Q2FyZERlY2subGVuZ3RoIC0gMV0ubm9kZS5yZW1vdmVGcm9tUGFyZW50KHRydWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gdGhpcy5saXN0Q2FyZERlY2suc3BsaWNlKHRoaXMubGlzdENhcmREZWNrLmxlbmd0aCAtIDEsIDEpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGlmKHRoaXMubGlzdENhcmREZWNrLmxlbmd0aCA+IDApe1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5saXN0Q2FyZERlY2tbdGhpcy5saXN0Q2FyZERlY2subGVuZ3RoIC0gMV0uYlRvdWNoID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldFN0YXRlR2V0Q2FyZCh0cnVlKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5idG5HZXRDYXJkLm5vZGUuYWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSB0eXBlQWN0aW9uLk1PVkVfQ0FSRF9ERUNLX1RPX0hPTERFUjpcbiAgICAgICAgICAgICAgICAgICAgbGV0IGNhcmQgPSBsYXN0QWN0aW9uLmFyckNhcmRNb3ZlWzBdO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmxpc3RJZENhcmREZWNrLnNwbGljZSh0aGlzLmlkeEN1cnJlbnREZWNrLCAwLCBjYXJkLmlkQ2FyZCk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvY2Vzc0dldENhcmQoY2FyZC5vcmlQb3MpXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaGlzdG9yeUFjdGlvbi5zcGxpY2UodGhpcy5oaXN0b3J5QWN0aW9uLmxlbmd0aCAtIDEsIDEpO1xuXG5cbiAgICAgICAgICAgICAgICAgICAgLy8gY2MubG9nKHRoaXMuaWR4Q3VycmVudERlY2sgKyBcIiAtIHRoaXMubGlzdElkQ2FyZERlY2sgdW5kbzogXCIgKyBKU09OLnN0cmluZ2lmeSh0aGlzLmxpc3RJZENhcmREZWNrKSk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubGlzdENhcmRIb2xkZXJbY2FyZC5ncm91cElkIC0gMTAwXVt0aGlzLmxpc3RDYXJkSG9sZGVyW2NhcmQuZ3JvdXBJZCAtIDEwMF0ubGVuZ3RoIC0gMV0ubm9kZS5yZW1vdmVGcm9tUGFyZW50KHRydWUpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmxpc3RDYXJkSG9sZGVyW2NhcmQuZ3JvdXBJZCAtIDEwMF0uc3BsaWNlKHRoaXMubGlzdENhcmRIb2xkZXJbY2FyZC5ncm91cElkIC0gMTAwXS5sZW5ndGggLSAxLCAxKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5saXN0SWRDYXJkSG9sZGVyW2NhcmQuZ3JvdXBJZCAtIDEwMF0uc3BsaWNlKHRoaXMubGlzdENhcmRIb2xkZXJbY2FyZC5ncm91cElkIC0gMTAwXS5sZW5ndGggLSAxLCAxKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSB0eXBlQWN0aW9uLk1PVkVfQ0FSRF9ERUNLX1RPX1RBQkxFR1JPVVA6XG4gICAgICAgICAgICAgICAgICAgIGxldCBjYXJkMiA9IGxhc3RBY3Rpb24uYXJyQ2FyZE1vdmVbMF07XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubGlzdElkQ2FyZERlY2suc3BsaWNlKHRoaXMuaWR4Q3VycmVudERlY2ssIDAsIGNhcmQyLmlkQ2FyZCk7XG4gICAgICAgICAgICAgICAgICAgIC8vIGNjLmxvZyh0aGlzLmlkeEN1cnJlbnREZWNrICsgXCIgLSB0aGlzLmxpc3RJZENhcmREZWNrIHVuZG86IFwiICsgSlNPTi5zdHJpbmdpZnkodGhpcy5saXN0SWRDYXJkRGVjaykpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnByb2Nlc3NHZXRDYXJkKGNhcmQyLm9yaVBvcyk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaGlzdG9yeUFjdGlvbi5zcGxpY2UodGhpcy5oaXN0b3J5QWN0aW9uLmxlbmd0aCAtIDEsIDEpO1xuXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubGlzdENhcmRPblRhYmxlW2NhcmQyLmdyb3VwSWRdW3RoaXMubGlzdENhcmRPblRhYmxlW2NhcmQyLmdyb3VwSWRdLmxlbmd0aCAtIDFdLm5vZGUucmVtb3ZlRnJvbVBhcmVudCh0cnVlKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5saXN0Q2FyZE9uVGFibGVbY2FyZDIuZ3JvdXBJZF0uc3BsaWNlKHRoaXMubGlzdENhcmRPblRhYmxlW2NhcmQyLmdyb3VwSWRdLmxlbmd0aCAtIDEsIDEpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmxpc3RJZENhcmRPblRhYmxlW2NhcmQyLmdyb3VwSWRdLnNwbGljZSh0aGlzLmxpc3RJZENhcmRPblRhYmxlW2NhcmQyLmdyb3VwSWRdLmxlbmd0aCAtIDEsIDEpO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlIHR5cGVBY3Rpb24uTU9WRV9DQVJEX0hPTERFUl9UT19UQUJMRUdST1VQOlxuICAgICAgICAgICAgICAgICAgICB0aGlzLm1vdmVDYXJkVG9Ib2xkZXIobGFzdEFjdGlvbi5hcnJDYXJkTW92ZSwgbGFzdEFjdGlvbi5pZHhTb3VyY2UpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmhpc3RvcnlBY3Rpb24uc3BsaWNlKHRoaXMuaGlzdG9yeUFjdGlvbi5sZW5ndGggLSAxLCAxKTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSB0eXBlQWN0aW9uLk1PVkVfQ0FSRF9UQUJMRV9UT19IT0xERVI6XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubW92ZUNhcmRPblRhYmxlcyhsYXN0QWN0aW9uLmFyckNhcmRNb3ZlLCBsYXN0QWN0aW9uLmlkeFNvdXJjZSk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaGlzdG9yeUFjdGlvbi5zcGxpY2UodGhpcy5oaXN0b3J5QWN0aW9uLmxlbmd0aCAtIDEsIDEpO1xuICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSB0eXBlQWN0aW9uLk1PVkVfQ0FSRF9UQUJMRV9UT19UQUJMRTpcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tb3ZlQ2FyZE9uVGFibGVzKGxhc3RBY3Rpb24uYXJyQ2FyZE1vdmUsIGxhc3RBY3Rpb24uaWR4U291cmNlKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5oaXN0b3J5QWN0aW9uLnNwbGljZSh0aGlzLmhpc3RvcnlBY3Rpb24ubGVuZ3RoIC0gMSwgMSk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICB0aGlzLmhpc3RvcnlBY3Rpb24uc3BsaWNlKHRoaXMuaGlzdG9yeUFjdGlvbi5sZW5ndGggLSAxLCAxKTtcbiAgICAgICAgICAgIC8vIGNjLmxvZyhcInRoaXMuaGlzdG9yeUFjdGlvbiBhZnRlcjpcIiArIHRoaXMuaGlzdG9yeUFjdGlvbi5sZW5ndGgpO1xuICAgICAgICB9XG4gICAgfSxcbiAgICBzaG93Q2FyZERlY2soKXtcbiAgICAgICAgdGhpcy5pZHhDdXJyZW50RGVjay0tO1xuICAgICAgICBpZih0aGlzLmlkeEN1cnJlbnREZWNrID4gMiAmJiB0aGlzLmxpc3RDYXJkRGVjay5sZW5ndGggPiAxKXtcbiAgICAgICAgICAgIGZvcihsZXQgaSA9IDA7IGkgPCB0aGlzLmxpc3RDYXJkRGVjay5sZW5ndGg7IGkrKyl7XG4gICAgICAgICAgICAgICAgbGV0IGNhcmRJZCA9IHRoaXMubGlzdElkQ2FyZERlY2tbdGhpcy5pZHhDdXJyZW50RGVjayAtIDMgKyBpXTtcbiAgICAgICAgICAgICAgICB0aGlzLmxpc3RDYXJkRGVja1tpXS5zZXRTcHJpdGVDYXJkKGNhcmRJZCwgdGhpcy5hcnJSZXNvdXJjZXNDYXJkW2NhcmRJZF0pO1xuICAgICAgICAgICAgICAgIHRoaXMubGlzdENhcmREZWNrW2ldLnNob3dDYXJkKGZhbHNlKTtcbiAgICAgICAgICAgICAgICB0aGlzLmxpc3RDYXJkRGVja1tpXS5iVG91Y2ggPSBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGxldCBjYXJkID0gdGhpcy5nZXRDYXJkKHRoaXMubGlzdElkQ2FyZERlY2tbdGhpcy5pZHhDdXJyZW50RGVjayAtIDFdKTtcbiAgICAgICAgICAgIHRoaXMubm9kZUNhcmRDb250YWluZXIuYWRkQ2hpbGQoY2FyZC5ub2RlKTtcbiAgICAgICAgICAgIHRoaXMubGlzdENhcmREZWNrLnB1c2goY2FyZCk7XG4gICAgICAgICAgICBsZXQgcG9zWCA9IHRoaXMuY2FyZERlY2tBbmNob3IuZ2V0UG9zaXRpb24oKS54ICsgNTAqKHRoaXMubGlzdENhcmREZWNrLmxlbmd0aCAtIDEpO1xuICAgICAgICAgICAgY2FyZC5vcmlQb3MgPSBjYy52Mihwb3NYLCB0aGlzLmNhcmREZWNrQW5jaG9yLmdldFBvc2l0aW9uKCkueSk7XG4gICAgICAgICAgICBjYXJkLm5vZGUucG9zaXRpb24gPSBjYy52Mihwb3NYLCB0aGlzLmNhcmREZWNrQW5jaG9yLmdldFBvc2l0aW9uKCkueSk7XG4gICAgICAgICAgICBjYXJkLm9yaUluZGV4ID0gdGhpcy5saXN0Q2FyZERlY2subGVuZ3RoO1xuICAgICAgICAgICAgY2FyZC5ub2RlLnpJbmRleCA9IHRoaXMubGlzdENhcmREZWNrLmxlbmd0aDtcbiAgICAgICAgICAgIGNhcmQuc2hvd0NhcmQoZmFsc2UpO1xuICAgICAgICAgICAgY2FyZC5iVG91Y2ggPSB0cnVlO1xuICAgICAgICB9ZWxzZSBpZiAodGhpcy5saXN0Q2FyZERlY2subGVuZ3RoID4gMCl7XG4gICAgICAgICAgICB0aGlzLmxpc3RDYXJkRGVja1t0aGlzLmxpc3RDYXJkRGVjay5sZW5ndGggLSAxXS5iVG91Y2ggPSB0cnVlO1xuXG4gICAgICAgIH1cbiAgICB9LFxuICAgIHNldFN0YXRlR2V0Q2FyZChiU3RhdGUpey8vIHRydWU6IGdldENhcmQgfCBmYWxzZTogUmVsb2FkIERlY2tcbiAgICAgICAgdGhpcy5idG5HZXRDYXJkLm5vZGUuZ2V0Q29tcG9uZW50KGNjLlNwcml0ZSkuc3ByaXRlRnJhbWUgPSB0aGlzLmFyclJlc291cmNlc0NhcmRbYlN0YXRlID8gNTIgOiA1M107XG4gICAgfSxcbiAgICBzdGFydE5ld0dhbWUoKXtcbiAgICAgICAgdGhpcy5mYnNkay5zaG93SW50ZXJzdGl0aWFsKCk7XG4gICAgICAgIC8vIHRoaXMuZmJzZGsuc2hvd0Jhbm5lckFkcygpO1xuICAgICAgICB0aGlzLnJlc2V0R2FtZSgpO1xuICAgICAgICB0aGlzLmJHYW1lU3RhcnQgPSB0cnVlO1xuICAgICAgICB0aGlzLmJ0bkdldENhcmQuaW50ZXJhY3RhYmxlID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5zZXRTdGF0ZUdldENhcmQodHJ1ZSk7XG4gICAgICAgIGxldCBuZXdEZWNrID0gdGhpcy5kZWNrcy5kZWFsQ2FyZCgpO1xuICAgICAgICAvLyBuZXdEZWNrID0gWzQ2LDE1LDQ3LDM5LDQyLDIsMSwzNCw0MSw0LDMyLDE2LDM4LDI0LDE0LDE4LDE5LDI4LDgsMjMsMzcsMjAsMCwzMywwLDIxLDQsMTMsMjcsOCwxMiw1MCw3LDEsMjIsMTcsMzYsMywxMCwzMSw0MCw0OCw0NCwzMCw0Myw1LDI2LDksMjUsNiw1MSwzNV07XG4gICAgICAgIC8vIGNjLmxvZyhcIm5ld0RlY2s6IFwiICsgbmV3RGVjayk7XG4gICAgICAgIHRoaXMubGlzdElkQ2FyZERlY2sgPSBuZXdEZWNrLnNsaWNlKDAsIDI0KTtcbiAgICAgICAgdGhpcy5pZHhDdXJyZW50RGVjayA9IDA7XG4gICAgICAgIGxldCBsaXN0SWRDYXJkT25UYWJsZSA9IG5ld0RlY2suc2xpY2UoMjQsIG5ld0RlY2subGVuZ3RoKTtcblxuICAgICAgICBmb3IobGV0IGkgPSAwLCBpZHggPSAwOyBpIDwgNzsgaSsrKXtcbiAgICAgICAgICAgIGZvcihsZXQgaiA9IDA7IGogPCBpKzE7IGorKyl7XG4gICAgICAgICAgICAgICAgbGV0IGNhcmRJZCA9IGxpc3RJZENhcmRPblRhYmxlW2lkeF1cbiAgICAgICAgICAgICAgICBpZHgrKztcblxuICAgICAgICAgICAgICAgIGxldCBjYXJkID0gdGhpcy5nZXRDYXJkKGNhcmRJZCk7XG4gICAgICAgICAgICAgICAgY2FyZC5ub2RlLnBvc2l0aW9uID0gdGhpcy5idG5HZXRDYXJkLm5vZGUuZ2V0UG9zaXRpb24oKTtcbiAgICAgICAgICAgICAgICB0aGlzLm5vZGVDYXJkQ29udGFpbmVyLmFkZENoaWxkKGNhcmQubm9kZSk7XG4gICAgICAgICAgICAgICAgY2FyZC5ub2RlLnpJbmRleCA9IGo7XG4gICAgICAgICAgICAgICAgbGV0IF9wb3NYID0gdGhpcy5jYXJkT25UYWJsZUFuY2hvcltpXS5nZXRQb3NpdGlvbigpLng7XG4gICAgICAgICAgICAgICAgbGV0IF9wb3NZID0gdGhpcy5jYXJkT25UYWJsZUFuY2hvcltpXS5nZXRQb3NpdGlvbigpLnkgLSAyNSpqO1xuICAgICAgICAgICAgICAgIGxldCBfcG9zID0gY2MudjIoX3Bvc1gsIF9wb3NZKTtcbiAgICAgICAgICAgICAgICBjYXJkLm9yaVBvcyA9IF9wb3M7XG4gICAgICAgICAgICAgICAgY2FyZC5vcmlJbmRleCA9IGo7XG4gICAgICAgICAgICAgICAgY2FyZC5ncm91cElkID0gaTtcbiAgICAgICAgICAgICAgICB0aGlzLmxpc3RDYXJkT25UYWJsZVtpXS5wdXNoKGNhcmQpO1xuICAgICAgICAgICAgICAgIGNjLnR3ZWVuKGNhcmQubm9kZSlcbiAgICAgICAgICAgICAgICAgICAgLmRlbGF5KDAuMylcbiAgICAgICAgICAgICAgICAgICAgLnRvKDAuMywge3Bvc2l0aW9uOiBfcG9zfSlcbiAgICAgICAgICAgICAgICAgICAgLmRlbGF5KDAuMSpqKVxuICAgICAgICAgICAgICAgICAgICAuY2FsbCgoKT0+e1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYoaSA9PT0gMCAmJiBqID09PSAwKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdhbWVTY2VuZS5wbGF5RWZmZWN0KFNvdW5kRWZmZWN0LkRFQUxfU09VTkQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgaWYoaiA9PT0gaSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy8gaWYodHJ1ZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNhcmQuc2hvd0NhcmQodHJ1ZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5saXN0SWRDYXJkT25UYWJsZVtpXS5wdXNoKGNhcmRJZCk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIC5zdGFydCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSxcbiAgICBnZXRDYXJkKGNhcmRJZCl7XG4gICAgICAgIGxldCBjYXJkID0gdGhpcy5jYXJkTm9kZVBvb2wuZ2V0KCk7XG4gICAgICAgIGlmKFV0aWxzLmlzRW1wdHkoY2FyZCkpe1xuICAgICAgICAgICAgbGV0IGNhcmRQbHVzID0gY2MuaW5zdGFudGlhdGUodGhpcy5wcmVmYWJDYXJkKTtcbiAgICAgICAgICAgIHRoaXMuY2FyZE5vZGVQb29sLnB1dChjYXJkUGx1cyk7XG4gICAgICAgICAgICBjYXJkID0gdGhpcy5jYXJkTm9kZVBvb2wuZ2V0KCk7XG4gICAgICAgIH1cbiAgICAgICAgY2FyZC5vbihjYy5Ob2RlLkV2ZW50VHlwZS5UT1VDSF9TVEFSVCwgdGhpcy5vbkJlZ2luVG91Y2gsIHRoaXMsIHRydWUpO1xuICAgICAgICBjYXJkLm9uKGNjLk5vZGUuRXZlbnRUeXBlLlRPVUNIX01PVkUsIHRoaXMub25Nb3ZlQ2FyZCwgdGhpcywgdHJ1ZSk7XG4gICAgICAgIGNhcmQub24oY2MuTm9kZS5FdmVudFR5cGUuVE9VQ0hfRU5ELCB0aGlzLm9uRW5kVG91Y2gsIHRoaXMsIHRydWUpO1xuXG4gICAgICAgIGNhcmQgPSBjYXJkLmdldENvbXBvbmVudChDYXJkUHJlZmFiKTtcbiAgICAgICAgY2FyZC5zZXRTcHJpdGVDYXJkKGNhcmRJZCwgdGhpcy5hcnJSZXNvdXJjZXNDYXJkW2NhcmRJZF0pO1xuICAgICAgICByZXR1cm4gY2FyZDtcbiAgICB9LFxuICAgIG9uQmVnaW5Ub3VjaChldmVudCl7XG4gICAgICAgIHRoaXMuZ2FtZVNjZW5lLnBsYXlFZmZlY3QoU291bmRFZmZlY3QuQ0xJQ0tfU09VTkQpO1xuICAgICAgICAvLyB0aGlzLmFyckNhcmRNb3ZlID0gW107XG4gICAgICAgIGxldCBjYXJkU2VsZWN0ID0gZXZlbnQudGFyZ2V0LmdldENvbXBvbmVudChDYXJkUHJlZmFiKTtcbiAgICAgICAgaWYoIWNhcmRTZWxlY3QuYlRvdWNoIHx8IHRoaXMuYXJyQ2FyZE1vdmUubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgY2MubG9nKFwiZG91YmxlIGNsaWNrLi4uLlwiKVxuICAgICAgICAgICAgZm9yKGxldCBpID0gMDsgaSA8IHRoaXMuYXJyQ2FyZE1vdmUubGVuZ3RoOyBpKyspe1xuICAgICAgICAgICAgICAgIGxldCBjYXJkID0gdGhpcy5hcnJDYXJkTW92ZVtpXTtcbiAgICAgICAgICAgICAgICBjYXJkLm5vZGUucG9zaXRpb24gPSBjYXJkLm9yaVBvcztcbiAgICAgICAgICAgICAgICBjYXJkLm5vZGUuekluZGV4ID0gY2FyZC5vcmlJbmRleDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMuYXJyQ2FyZE1vdmUgPSBbXTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBpZihjYXJkU2VsZWN0Lmdyb3VwSWQgPiAtMSAmJiBjYXJkU2VsZWN0Lmdyb3VwSWQgPCAxMDApey8vQ2FyZCBPbiBUYWJsZVxuICAgICAgICAgICAgbGV0IGlkeENhcmRTZWxlY3QgPSB0aGlzLmxpc3RDYXJkT25UYWJsZVtjYXJkU2VsZWN0Lmdyb3VwSWRdLmluZGV4T2YoY2FyZFNlbGVjdCk7XG4gICAgICAgICAgICBmb3IobGV0IGkgPSBpZHhDYXJkU2VsZWN0OyBpIDwgdGhpcy5saXN0Q2FyZE9uVGFibGVbY2FyZFNlbGVjdC5ncm91cElkXS5sZW5ndGg7IGkrKyl7XG4gICAgICAgICAgICAgICAgbGV0IGNhcmQgPSB0aGlzLmxpc3RDYXJkT25UYWJsZVtjYXJkU2VsZWN0Lmdyb3VwSWRdW2ldO1xuICAgICAgICAgICAgICAgIHRoaXMuYXJyQ2FyZE1vdmUucHVzaChjYXJkKTtcbiAgICAgICAgICAgICAgICBjYXJkLm5vZGUuekluZGV4ID0gMTAwICsgaTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfWVsc2V7Ly8gQ2FyZCBPbiBEZWNrIG9yIEhvbGRlclxuICAgICAgICAgICAgdGhpcy5hcnJDYXJkTW92ZS5wdXNoKGNhcmRTZWxlY3QpO1xuICAgICAgICAgICAgY2MubG9nKFwiY2FyZFNlbGVjdDogXCIgKyAxMDApXG4gICAgICAgICAgICBjYXJkU2VsZWN0Lm5vZGUuekluZGV4ID0gMTAwO1xuICAgICAgICB9XG4gICAgICAgIC8vIGNjLmxvZyhcImV2ZW50LnRhcmdldDogXCIgKyBjYXJkU2VsZWN0LmlkQ2FyZCk7XG4gICAgfSxcbiAgICBvbk1vdmVDYXJkKGV2ZW50KXtcbiAgICAgICAgdGhpcy5iTW92ZUNhcmQgPSB0cnVlO1xuICAgICAgICBsZXQgY2FyZENvbXAgPSBldmVudC50YXJnZXQuZ2V0Q29tcG9uZW50KENhcmRQcmVmYWIpO1xuICAgICAgICBpZighY2FyZENvbXAuYlRvdWNoKSByZXR1cm47XG4gICAgICAgIGxldCBkZWx0YSA9IGV2ZW50LnRvdWNoLmdldERlbHRhKCk7XG4gICAgICAgIC8vIGNjLmxvZyhcIm9uTW92ZUNhcmQ6IFwiICsgZXZlbnQpO1xuICAgICAgICBpZih0aGlzLmFyckNhcmRNb3ZlLmxlbmd0aCA+IDApe1xuICAgICAgICAgICAgZm9yKGxldCBpID0gMDsgaSA8IHRoaXMuYXJyQ2FyZE1vdmUubGVuZ3RoOyBpKyspe1xuICAgICAgICAgICAgICAgIGxldCBjYXJkID0gdGhpcy5hcnJDYXJkTW92ZVtpXTtcbiAgICAgICAgICAgICAgICBjYXJkLm5vZGUueCArPSBkZWx0YS54O1xuICAgICAgICAgICAgICAgIGNhcmQubm9kZS55ICs9IGRlbHRhLnk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9LFxuICAgIG9uRW5kVG91Y2goZXZlbnQpe1xuICAgICAgICBsZXQgY2FyZENvbXAgPSBldmVudC50YXJnZXQuZ2V0Q29tcG9uZW50KENhcmRQcmVmYWIpO1xuICAgICAgICBpZighY2FyZENvbXAuYlRvdWNoKSByZXR1cm47XG4gICAgICAgIC8vIGNjLmxvZyhcImNhcmRDb21wOiBcIiArIGNhcmRDb21wLmlkQ2FyZCk7XG4gICAgICAgIC8vIGNjLmxvZyhcInRoaXMuYXJyQ2FyZE1vdmU6IFwiICsgdGhpcy5hcnJDYXJkTW92ZS5sZW5ndGgpO1xuICAgICAgICBpZih0aGlzLmFyckNhcmRNb3ZlLmxlbmd0aCA+IDApe1xuICAgICAgICAgICAgaWYodGhpcy5iTW92ZUNhcmQgJiYgdGhpcy5jaGVja1Bvc0NhcmRNb3ZlZCh0aGlzLmFyckNhcmRNb3ZlKSl7XG5cbiAgICAgICAgICAgIH1lbHNlIGlmKCF0aGlzLmJNb3ZlQ2FyZCAmJiB0aGlzLmF1dG9Nb3ZlQ2FyZCgpKXtcblxuICAgICAgICAgICAgfWVsc2V7XG4gICAgICAgICAgICAgICAgZm9yKGxldCBpID0gMDsgaSA8IHRoaXMuYXJyQ2FyZE1vdmUubGVuZ3RoOyBpKyspe1xuICAgICAgICAgICAgICAgICAgICBsZXQgY2FyZCA9IHRoaXMuYXJyQ2FyZE1vdmVbaV07XG4gICAgICAgICAgICAgICAgICAgIC8vIGNjLmxvZyhcImNhcmQub3JpSW5kZXg6IFwiICsgY2FyZC5vcmlJbmRleClcbiAgICAgICAgICAgICAgICAgICAgY2FyZC5ub2RlLnBvc2l0aW9uID0gY2FyZC5vcmlQb3M7XG4gICAgICAgICAgICAgICAgICAgIGNhcmQubm9kZS56SW5kZXggPSBjYXJkLm9yaUluZGV4O1xuICAgICAgICAgICAgICAgICAgICBjYXJkLnNoYWtlQ2FyZCgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICB0aGlzLmFyckNhcmRNb3ZlID0gW107XG4gICAgICAgIHRoaXMuYk1vdmVDYXJkID0gZmFsc2U7XG4gICAgfSxcbiAgICBjaGVja1Bvc0NhcmRNb3ZlZChhcnJDYXJkTW92ZWQpe1xuICAgICAgICBpZih0aGlzLmFyckNhcmRNb3ZlLmxlbmd0aCA8IDApIHJldHVybiBmYWxzZTtcbiAgICAgICAgbGV0IHBvc0VuZFRvdWNoID0gYXJyQ2FyZE1vdmVkWzBdLm5vZGUuZ2V0UG9zaXRpb24oKTtcbiAgICAgICAgbGV0IHJlY3RDYXJkTW92ZSA9IGFyckNhcmRNb3ZlZFswXS5ub2RlLmdldEJvdW5kaW5nQm94KCk7XG4gICAgICAgIC8vYm91bmRpbmcgQm94IEhvbGRlclxuICAgICAgICBmb3IobGV0IGkgPSAwOyBpIDwgdGhpcy5jYXJkSG9sZGVyQW5jaG9yLmxlbmd0aDsgaSsrKXtcbiAgICAgICAgICAgIGxldCByZWN0SG9sZGVyID0gdGhpcy5jYXJkSG9sZGVyQW5jaG9yW2ldLmdldEJvdW5kaW5nQm94KCk7XG4gICAgICAgICAgICBpZihyZWN0SG9sZGVyLmNvbnRhaW5zKHBvc0VuZFRvdWNoKSl7XG4gICAgICAgICAgICAgICAgbGV0IGlkeEhvbGRlciA9IGk7XG4gICAgICAgICAgICAgICAgaWYodGhpcy5jaGVja1dpdGhMaXN0SG9sZGVyKGFyckNhcmRNb3ZlZFswXS5pZENhcmQsIGlkeEhvbGRlcikgIT09IC0xICYmIGFyckNhcmRNb3ZlZC5sZW5ndGggPT09IDEgJiYgYXJyQ2FyZE1vdmVkWzBdLmdyb3VwSWQgPCAxMDApe1xuICAgICAgICAgICAgICAgICAgICB0aGlzLm1vdmVDYXJkVG9Ib2xkZXIoYXJyQ2FyZE1vdmVkWzBdLCBpZHhIb2xkZXIpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIC8vQm91ZGluZ0JveCBHcm91cCBPbiBUYWJsZVxuICAgICAgICBmb3IobGV0IGogPSAwOyBqIDwgdGhpcy5saXN0Q2FyZE9uVGFibGUubGVuZ3RoOyBqKyspe1xuICAgICAgICAgICAgbGV0IHJlY3RPZkxhc3RDYXJkID0gbnVsbDtcbiAgICAgICAgICAgIGlmKHRoaXMubGlzdENhcmRPblRhYmxlW2pdLmxlbmd0aCA9PT0gMClcbiAgICAgICAgICAgICAgICByZWN0T2ZMYXN0Q2FyZCA9IHRoaXMuY2FyZE9uVGFibGVBbmNob3Jbal0uZ2V0Qm91bmRpbmdCb3goKTtcbiAgICAgICAgICAgIGVsc2VcbiAgICAgICAgICAgICAgICByZWN0T2ZMYXN0Q2FyZCA9IHRoaXMubGlzdENhcmRPblRhYmxlW2pdW3RoaXMubGlzdENhcmRPblRhYmxlW2pdLmxlbmd0aCAtIDFdLm5vZGUuZ2V0Qm91bmRpbmdCb3goKTtcbiAgICAgICAgICAgIGlmKHJlY3RPZkxhc3RDYXJkLmNvbnRhaW5zKHBvc0VuZFRvdWNoKSAmJiBqICE9PSBhcnJDYXJkTW92ZWRbMF0uZ3JvdXBJZCl7XG4gICAgICAgICAgICAgICAgbGV0IGlkeEdyb3VwID0gajtcbiAgICAgICAgICAgICAgICBpZih0aGlzLmNoZWNrV2l0aENhcmRPblRhYmxlKGFyckNhcmRNb3ZlZFswXS5pZENhcmQsIGlkeEdyb3VwKSAhPT0gLTEpe1xuICAgICAgICAgICAgICAgICAgICB0aGlzLm1vdmVDYXJkT25UYWJsZXMoYXJyQ2FyZE1vdmVkLCBpZHhHcm91cCk7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfSxcbiAgICBhdXRvTW92ZUNhcmQoKXtcbiAgICAgICAgaWYodGhpcy5hcnJDYXJkTW92ZS5sZW5ndGggPCAwKSByZXR1cm4gZmFsc2U7XG4gICAgICAgIGxldCBob2xkZXJWYWxpZGF0ZSA9IHRoaXMuY2hlY2tXaXRoTGlzdEhvbGRlcih0aGlzLmFyckNhcmRNb3ZlWzBdLmlkQ2FyZCk7XG4gICAgICAgIC8vIGNjLmxvZyhcImhvbGRlclZhbGlkYXRlOiBcIiArIGhvbGRlclZhbGlkYXRlKTtcbiAgICAgICAgbGV0IGdyb3VwVmFsaWRhdGUgPSB0aGlzLmNoZWNrV2l0aENhcmRPblRhYmxlKHRoaXMuYXJyQ2FyZE1vdmVbMF0uaWRDYXJkKTtcbiAgICAgICAgLy8gY2MubG9nKFwiZ3JvdXBWYWxpZGF0ZTogXCIgKyBncm91cFZhbGlkYXRlKTtcbiAgICAgICAgaWYoaG9sZGVyVmFsaWRhdGUgIT09IC0xICYmIHRoaXMuYXJyQ2FyZE1vdmUubGVuZ3RoID09PSAxICYmIHRoaXMuYXJyQ2FyZE1vdmVbMF0uZ3JvdXBJZCA8IDEwMCl7XG4gICAgICAgICAgICB0aGlzLm1vdmVDYXJkVG9Ib2xkZXIodGhpcy5hcnJDYXJkTW92ZVswXSwgaG9sZGVyVmFsaWRhdGUpO1xuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1lbHNlIGlmKGdyb3VwVmFsaWRhdGUgIT09IC0xICYmIGdyb3VwVmFsaWRhdGUgIT09IHRoaXMuYXJyQ2FyZE1vdmVbMF0uZ3JvdXBJZCl7XG4gICAgICAgICAgICB0aGlzLm1vdmVDYXJkT25UYWJsZXModGhpcy5hcnJDYXJkTW92ZSwgZ3JvdXBWYWxpZGF0ZSk7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfSxcbiAgICBtb3ZlQ2FyZFRvSG9sZGVyKGNhcmRNb3ZlLCBpZHhIb2xkZXIpe1xuICAgICAgICB0aGlzLmdhbWVTY2VuZS5wbGF5RWZmZWN0KFNvdW5kRWZmZWN0LkZMSVBfU09VTkQpO1xuICAgICAgICBpZihjYXJkTW92ZS5ncm91cElkID4gOTkpIHJldHVybjsvL0NhcmQgRnJvbUhvbGRlclxuICAgICAgICB0aGlzLmlTY29yZSArPSAxMDA7XG4gICAgICAgIHRoaXMuaU1vdmUgKys7XG4gICAgICAgIGxldCBvYmplY3RIaXN0b3J5ID0ge307XG4gICAgICAgIGxldCBiQ2FyZEZyb21EZWNrID0gKGNhcmRNb3ZlLmdyb3VwSWQgPT09IC0xKTtcblxuICAgICAgICBpZihiQ2FyZEZyb21EZWNrKXtcbiAgICAgICAgICAgIG9iamVjdEhpc3RvcnkudHlwZUFjdGlvbiA9IHR5cGVBY3Rpb24uTU9WRV9DQVJEX0RFQ0tfVE9fSE9MREVSO1xuICAgICAgICAgICAgLy8gY2MubG9nKFwidGhpcy5saXN0Q2FyZERlY2s6XCIgKyB0aGlzLmxpc3RDYXJkRGVjay5sZW5ndGgpO1xuICAgICAgICAgICAgb2JqZWN0SGlzdG9yeS5pZHhTb3VyY2UgPSAtMTtcbiAgICAgICAgfSBlbHNle1xuICAgICAgICAgICAgb2JqZWN0SGlzdG9yeS50eXBlQWN0aW9uID0gdHlwZUFjdGlvbi5NT1ZFX0NBUkRfVEFCTEVfVE9fSE9MREVSO1xuICAgICAgICAgICAgLy8gY2MubG9nKFwidGhpcy5saXN0Q2FyZE9uVGFibGVbXCIgKyBjYXJkTW92ZS5ncm91cElkICsgXCJdIDogXCIgKyB0aGlzLmxpc3RDYXJkT25UYWJsZVtjYXJkTW92ZS5ncm91cElkXS5sZW5ndGgpO1xuICAgICAgICAgICAgb2JqZWN0SGlzdG9yeS5pZHhTb3VyY2UgPSBjYXJkTW92ZS5ncm91cElkO1xuICAgICAgICB9XG4gICAgICAgIG9iamVjdEhpc3RvcnkuaWR4RGVzID0gaWR4SG9sZGVyO1xuICAgICAgICBvYmplY3RIaXN0b3J5LmFyckNhcmRNb3ZlID0gW2NhcmRNb3ZlXTtcbiAgICAgICAgXG4gICAgICAgIGxldCBwb3MgPSB0aGlzLmNhcmRIb2xkZXJBbmNob3JbaWR4SG9sZGVyXS5nZXRQb3NpdGlvbigpXG4gICAgICAgIGxldCBpVGltZSA9IHRoaXMuYkF1dG9Db21wbGV0ZSA/IDAuMSA6IDAuMjtcbiAgICAgICAgY2MudHdlZW4oY2FyZE1vdmUubm9kZSlcbiAgICAgICAgICAgIC5kZWxheSgwLjEpXG4gICAgICAgICAgICAudG8oaVRpbWUsIHtwb3NpdGlvbjogcG9zfSlcbiAgICAgICAgICAgIC5jYWxsKCgpPT57XG4gICAgICAgICAgICAgICAgY2FyZE1vdmUuc2hvd0VmZmVjdENhcmQoKTtcbiAgICAgICAgICAgICAgICBjYXJkTW92ZS5ub2RlLnpJbmRleCA9IGNhcmRNb3ZlLm9yaUluZGV4O1xuICAgICAgICAgICAgICAgIGlmKHRoaXMuYkF1dG9Db21wbGV0ZSlcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hdXRvQ29tcGxldGUoKTtcbiAgICAgICAgICAgICAgICB0aGlzLmNoZWNrRmluaXNoR2FtZSgpO1xuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5zdGFydCgpO1xuXG4gICAgICAgIC8vIGNhcmRNb3ZlLm5vZGUucG9zaXRpb24gPSB0aGlzLmNhcmRIb2xkZXJBbmNob3JbaWR4SG9sZGVyXS5nZXRQb3NpdGlvbigpO1xuXG4gICAgICAgIHRoaXMubGlzdENhcmRIb2xkZXJbaWR4SG9sZGVyXS5wdXNoKGNhcmRNb3ZlKTtcbiAgICAgICAgdGhpcy5saXN0SWRDYXJkSG9sZGVyW2lkeEhvbGRlcl0ucHVzaChjYXJkTW92ZS5pZENhcmQpO1xuXG4gICAgICAgIGNhcmRNb3ZlLm9yaUluZGV4ID0gdGhpcy5saXN0Q2FyZEhvbGRlcltpZHhIb2xkZXJdLmxlbmd0aDtcbiAgICAgICAgY2FyZE1vdmUub3JpUG9zID0gdGhpcy5jYXJkSG9sZGVyQW5jaG9yW2lkeEhvbGRlcl0uZ2V0UG9zaXRpb24oKTtcblxuICAgICAgICBsZXQgYWN0aXZlQ2FyZEluR3JvdXAgPSBudWxsO1xuICAgICAgICBpZighYkNhcmRGcm9tRGVjayl7XG4gICAgICAgICAgICB0aGlzLmxpc3RDYXJkT25UYWJsZVtjYXJkTW92ZS5ncm91cElkXSA9IFV0aWxzLnJlbW92ZUl0ZW1JbkFycmF5KGNhcmRNb3ZlLCB0aGlzLmxpc3RDYXJkT25UYWJsZVtjYXJkTW92ZS5ncm91cElkXSk7XG4gICAgICAgICAgICB0aGlzLmxpc3RJZENhcmRPblRhYmxlW2NhcmRNb3ZlLmdyb3VwSWRdID0gVXRpbHMucmVtb3ZlSXRlbUluQXJyYXkoY2FyZE1vdmUuaWRDYXJkLCB0aGlzLmxpc3RJZENhcmRPblRhYmxlW2NhcmRNb3ZlLmdyb3VwSWRdKTtcbiAgICAgICAgICAgIGFjdGl2ZUNhcmRJbkdyb3VwID0gdGhpcy5jaGVja0NhcmRBY3RpdmVJbkdyb3VwKGNhcmRNb3ZlLmdyb3VwSWQpO1xuICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgIHRoaXMubGlzdENhcmREZWNrID0gVXRpbHMucmVtb3ZlSXRlbUluQXJyYXkoY2FyZE1vdmUsIHRoaXMubGlzdENhcmREZWNrKTtcbiAgICAgICAgICAgIHRoaXMubGlzdElkQ2FyZERlY2sgPSBVdGlscy5yZW1vdmVJdGVtSW5BcnJheShjYXJkTW92ZS5pZENhcmQsIHRoaXMubGlzdElkQ2FyZERlY2spO1xuICAgICAgICAgICAgbGV0IHNlbGYgPSB0aGlzO1xuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKXtcbiAgICAgICAgICAgICAgICBzZWxmLnNob3dDYXJkRGVjaygpO1xuICAgICAgICAgICAgfSwgMjAwKTtcbiAgICAgICAgfVxuXG4gICAgICAgIG9iamVjdEhpc3RvcnkuY2FyZElkQWN0aXZlQWZ0ZXJNb3ZlID0gYWN0aXZlQ2FyZEluR3JvdXA7XG4gICAgICAgIHRoaXMuaGlzdG9yeUFjdGlvbi5wdXNoKG9iamVjdEhpc3RvcnkpO1xuICAgICAgICBjYXJkTW92ZS5ncm91cElkID0gMTAwICsgaWR4SG9sZGVyO1xuICAgIH0sXG4gICAgbW92ZUNhcmRPblRhYmxlcyhhcnJTb3VyY2UsIGlkeEdyb3VwKXtcbiAgICAgICAgdGhpcy5nYW1lU2NlbmUucGxheUVmZmVjdChTb3VuZEVmZmVjdC5GTElQX1NPVU5EKTtcbiAgICAgICAgdGhpcy5pU2NvcmUgLT0gNTtcbiAgICAgICAgdGhpcy5pTW92ZSArKztcbiAgICAgICAgbGV0IG9iamVjdEhpc3RvcnkgPSB7fTtcbiAgICAgICAgbGV0IGJDYXJkRnJvbURlY2sgPSAoYXJyU291cmNlWzBdLmdyb3VwSWQgPT09IC0xKTtcbiAgICAgICAgbGV0IGJDYXJkRnJvbUhvbGRlciA9IChhcnJTb3VyY2VbMF0uZ3JvdXBJZCA+IDk5KTtcbiAgICAgICAgaWYoYkNhcmRGcm9tRGVjayl7XG4gICAgICAgICAgICAvLyBjYy5sb2coXCJ0aGlzLmxpc3RDYXJkRGVjazpcIiArIHRoaXMubGlzdENhcmREZWNrLmxlbmd0aCk7XG4gICAgICAgICAgICBvYmplY3RIaXN0b3J5LnR5cGVBY3Rpb24gPSB0eXBlQWN0aW9uLk1PVkVfQ0FSRF9ERUNLX1RPX1RBQkxFR1JPVVA7XG4gICAgICAgIH1lbHNlIGlmKGJDYXJkRnJvbUhvbGRlcil7XG4gICAgICAgICAgICBvYmplY3RIaXN0b3J5LnR5cGVBY3Rpb24gPSB0eXBlQWN0aW9uLk1PVkVfQ0FSRF9IT0xERVJfVE9fVEFCTEVHUk9VUDtcbiAgICAgICAgfWVsc2V7XG4gICAgICAgICAgICBvYmplY3RIaXN0b3J5LnR5cGVBY3Rpb24gPSB0eXBlQWN0aW9uLk1PVkVfQ0FSRF9UQUJMRV9UT19UQUJMRTtcbiAgICAgICAgfVxuICAgICAgICBvYmplY3RIaXN0b3J5LmlkeFNvdXJjZSA9IGFyclNvdXJjZVswXS5ncm91cElkO1xuICAgICAgICBvYmplY3RIaXN0b3J5LmlkeERlcyA9IGlkeEdyb3VwO1xuICAgICAgICBvYmplY3RIaXN0b3J5LmFyckNhcmRNb3ZlID0gYXJyU291cmNlO1xuICAgIFxuXG4gICAgICAgIC8vIGNjLmxvZyhcIm1vdmVDYXJkT25UYWJsZXMgdG86IFwiICsgaWR4R3JvdXApXG4gICAgICAgIGxldCBkZXNQb3MgPSBudWxsO1xuICAgICAgICBpZih0aGlzLmxpc3RDYXJkT25UYWJsZVtpZHhHcm91cF0ubGVuZ3RoID09PSAwKVxuICAgICAgICAgICAgZGVzUG9zID0gdGhpcy5jYXJkT25UYWJsZUFuY2hvcltpZHhHcm91cF0uZ2V0UG9zaXRpb24oKTtcbiAgICAgICAgZWxzZXtcbiAgICAgICAgICAgIGxldCBsYXN0Q2FyZFBvcyA9IHRoaXMubGlzdENhcmRPblRhYmxlW2lkeEdyb3VwXVt0aGlzLmxpc3RDYXJkT25UYWJsZVtpZHhHcm91cF0ubGVuZ3RoIC0gMV0ubm9kZS5nZXRQb3NpdGlvbigpO1xuICAgICAgICAgICAgZGVzUG9zID0gY2MudjIobGFzdENhcmRQb3MueCwgbGFzdENhcmRQb3MueSAtIDUwKTtcbiAgICAgICAgfVxuICAgICAgICBsZXQgYWN0aXZlQ2FyZEluR3JvdXAgPSBudWxsO1xuICAgICAgICBmb3IobGV0IGkgPSAwOyBpIDwgYXJyU291cmNlLmxlbmd0aCA7IGkrKyl7XG4gICAgICAgICAgICBsZXQgY2FyZCA9IGFyclNvdXJjZVtpXTtcbiAgICAgICAgICAgIC8vIGNjLmxvZyhcImNhcmRJZDogXCIgKyBjYXJkLmlkQ2FyZCk7XG4gICAgICAgICAgICBsZXQgcG9zID0gY2MudjIoZGVzUG9zLngsIGRlc1Bvcy55IC0gNTAqaSk7XG4gICAgICAgICAgICBjYy50d2VlbihjYXJkLm5vZGUpXG4gICAgICAgICAgICAgICAgLnRvKDAuMiwge3Bvc2l0aW9uOiBwb3N9KVxuICAgICAgICAgICAgICAgIC5jYWxsKCgpPT57XG4gICAgICAgICAgICAgICAgICAgIGNhcmQubm9kZS56SW5kZXggPSBjYXJkLm9yaUluZGV4O1xuICAgICAgICAgICAgICAgICAgICBpZih0aGlzLmJBdXRvQ29tcGxldGUgJiYgKGkgPT09IGFyclNvdXJjZS5sZW5ndGggLSAxKSlcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYXV0b0NvbXBsZXRlKCk7XG4gICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAuc3RhcnQoKTtcblxuICAgICAgICAgICAgaWYoYkNhcmRGcm9tRGVjaykge1xuICAgICAgICAgICAgICAgIHRoaXMubGlzdENhcmREZWNrID0gVXRpbHMucmVtb3ZlSXRlbUluQXJyYXkoY2FyZCwgdGhpcy5saXN0Q2FyZERlY2spO1xuICAgICAgICAgICAgICAgIHRoaXMubGlzdElkQ2FyZERlY2sgPSBVdGlscy5yZW1vdmVJdGVtSW5BcnJheShjYXJkLmlkQ2FyZCwgdGhpcy5saXN0SWRDYXJkRGVjayk7XG4gICAgICAgICAgICAgICAgbGV0IHNlbGYgPSB0aGlzO1xuICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCl7XG4gICAgICAgICAgICAgICAgICAgIHNlbGYuc2hvd0NhcmREZWNrKCk7XG4gICAgICAgICAgICAgICAgfSwgMjAwKTtcblxuICAgICAgICAgICAgfWVsc2UgaWYgKGJDYXJkRnJvbUhvbGRlcikge1xuICAgICAgICAgICAgICAgIHRoaXMubGlzdENhcmRIb2xkZXJbY2FyZC5ncm91cElkIC0gMTAwXSA9IFV0aWxzLnJlbW92ZUl0ZW1JbkFycmF5KGNhcmQsIHRoaXMubGlzdENhcmRIb2xkZXJbY2FyZC5ncm91cElkIC0gMTAwXSk7XG4gICAgICAgICAgICAgICAgdGhpcy5saXN0SWRDYXJkSG9sZGVyW2NhcmQuZ3JvdXBJZCAtIDEwMF0gPSBVdGlscy5yZW1vdmVJdGVtSW5BcnJheShjYXJkLmlkQ2FyZCwgdGhpcy5saXN0SWRDYXJkSG9sZGVyW2NhcmQuZ3JvdXBJZCAtIDEwMF0pO1xuICAgICAgICAgICAgfWVsc2V7XG4gICAgICAgICAgICAgICAgdGhpcy5saXN0Q2FyZE9uVGFibGVbY2FyZC5ncm91cElkXSA9IFV0aWxzLnJlbW92ZUl0ZW1JbkFycmF5KGNhcmQsIHRoaXMubGlzdENhcmRPblRhYmxlW2NhcmQuZ3JvdXBJZF0pO1xuICAgICAgICAgICAgICAgIHRoaXMubGlzdElkQ2FyZE9uVGFibGVbY2FyZC5ncm91cElkXSA9IFV0aWxzLnJlbW92ZUl0ZW1JbkFycmF5KGNhcmQuaWRDYXJkLCB0aGlzLmxpc3RJZENhcmRPblRhYmxlW2NhcmQuZ3JvdXBJZF0pO1xuICAgICAgICAgICAgICAgIGlmKGkgPT09IGFyclNvdXJjZS5sZW5ndGggLSAxKXtcbiAgICAgICAgICAgICAgICAgICAgYWN0aXZlQ2FyZEluR3JvdXAgPSB0aGlzLmNoZWNrQ2FyZEFjdGl2ZUluR3JvdXAoY2FyZC5ncm91cElkKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy8gY2FyZC5ub2RlLnpJbmRleCA9IHRoaXMubGlzdENhcmRPblRhYmxlW2lkeEdyb3VwXS5sZW5ndGggKyBpO1xuICAgICAgICAgICAgY2FyZC5vcmlJbmRleCA9IHRoaXMubGlzdENhcmRPblRhYmxlW2lkeEdyb3VwXS5sZW5ndGg7XG4gICAgICAgICAgICB0aGlzLmxpc3RDYXJkT25UYWJsZVtpZHhHcm91cF0ucHVzaChjYXJkKTtcbiAgICAgICAgICAgIHRoaXMubGlzdElkQ2FyZE9uVGFibGVbaWR4R3JvdXBdLnB1c2goY2FyZC5pZENhcmQpO1xuXG4gICAgICAgICAgICBjYXJkLm9yaVBvcyA9IHBvcztcbiAgICAgICAgICAgIGNhcmQuZ3JvdXBJZCA9IGlkeEdyb3VwO1xuICAgICAgICB9XG4gICAgICAgIG9iamVjdEhpc3RvcnkuY2FyZElkQWN0aXZlQWZ0ZXJNb3ZlID0gYWN0aXZlQ2FyZEluR3JvdXA7XG4gICAgICAgIHRoaXMuaGlzdG9yeUFjdGlvbi5wdXNoKG9iamVjdEhpc3RvcnkpO1xuICAgIH0sXG4gICAgY2hlY2tXaXRoTGlzdEhvbGRlcihjYXJkSWQsIGlkeEhvbGRlcil7XG4gICAgICAgIC8vIGNjLmxvZyhcImNoZWNrV2l0aExpc3RIb2xkZXI6IFwiICsgY2FyZElkICsgXCIgLSBcIiArIGlkeEhvbGRlcilcbiAgICAgICAgaWYoIVV0aWxzLmlzRW1wdHkoaWR4SG9sZGVyKSl7XG4gICAgICAgICAgICBpZihDYXJkVXRpbHMudmFsaWRhdGVDYXJkSG9sZGVyKGNhcmRJZCwgdGhpcy5saXN0SWRDYXJkSG9sZGVyW2lkeEhvbGRlcl0pKXtcbiAgICAgICAgICAgICAgICByZXR1cm4gaWR4SG9sZGVyO1xuICAgICAgICAgICAgfVxuICAgICAgICB9ZWxzZXtcbiAgICAgICAgICAgIGZvcihsZXQgaSA9IDA7IGkgPCB0aGlzLmxpc3RJZENhcmRIb2xkZXIubGVuZ3RoOyBpKyspe1xuICAgICAgICAgICAgICAgIGlmKENhcmRVdGlscy52YWxpZGF0ZUNhcmRIb2xkZXIoY2FyZElkLCB0aGlzLmxpc3RJZENhcmRIb2xkZXJbaV0pKVxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gaTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gLTE7XG4gICAgfSxcbiAgICBjaGVja1dpdGhDYXJkT25UYWJsZShjYXJkSWQsIGlkeEdyb3VwKXtcbiAgICAgICAgaWYoIVV0aWxzLmlzRW1wdHkoaWR4R3JvdXApKXtcbiAgICAgICAgICAgIGlmKENhcmRVdGlscy52YWxpZGF0ZUNhcmRPbkJvYXJkKGNhcmRJZCwgdGhpcy5saXN0SWRDYXJkT25UYWJsZVtpZHhHcm91cF0pKVxuICAgICAgICAgICAgICAgIHJldHVybiBpZHhHcm91cDtcbiAgICAgICAgfWVsc2V7XG4gICAgICAgICAgICBmb3IobGV0IGkgPSAwOyBpIDwgdGhpcy5saXN0SWRDYXJkT25UYWJsZS5sZW5ndGg7IGkrKyl7XG4gICAgICAgICAgICAgICAgaWYoQ2FyZFV0aWxzLnZhbGlkYXRlQ2FyZE9uQm9hcmQoY2FyZElkLCB0aGlzLmxpc3RJZENhcmRPblRhYmxlW2ldKSlcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIC0xO1xuICAgIH0sXG4gICAgY2hlY2tDYXJkQWN0aXZlSW5Hcm91cChpZHhHcm91cCl7XG4gICAgICAgIC8vIGNjLmxvZyhcImNoZWNrQ2FyZEFjdGl2ZUluR3JvdXA6IFwiICsgaWR4R3JvdXApO1xuICAgICAgICBpZihpZHhHcm91cCA+IC0xICYmIGlkeEdyb3VwIDwgOCl7XG4gICAgICAgICAgICBsZXQgYXJyQ2FyZCA9IHRoaXMubGlzdENhcmRPblRhYmxlW2lkeEdyb3VwXTtcbiAgICAgICAgICAgIGxldCBjYXJkID0gYXJyQ2FyZFthcnJDYXJkLmxlbmd0aCAtIDFdO1xuICAgICAgICAgICAgaWYoYXJyQ2FyZC5sZW5ndGggPiAwICYmICFjYXJkLmJBY3RpdmUpIHtcbiAgICAgICAgICAgICAgICBjYXJkLnNob3dDYXJkKHRydWUpO1xuICAgICAgICAgICAgICAgIHRoaXMubGlzdElkQ2FyZE9uVGFibGVbaWR4R3JvdXBdLnB1c2goY2FyZC5pZENhcmQpO1xuICAgICAgICAgICAgICAgIHRoaXMuaUhpZGRlbkNhcmQtLTtcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5pSGlkZGVuQ2FyZCA9PT0gMCl7XG4gICAgICAgICAgICAgICAgICAgIC8vIGNjLmxvZyhcIllvdSBhcmUgVmljdG9yeVwiKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5idG5BdXRvQ29tcGxldGUuYWN0aXZlID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgcmV0dXJuIGNhcmQuaWRDYXJkO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH0sXG5cblxuXG4vLyAgICBDaGVjayBIaW50XG4gICAgY2hlY2tIaW50KCl7XG4gICAgICAgIHRoaXMuaVNjb3JlIC09IDEwO1xuICAgICAgICAvLyBDaGVjayBjYXJkIE9uIFRhYmxlXG4gICAgICAgIGZvcihsZXQgaSA9IDA7IGkgPCB0aGlzLmxpc3RDYXJkT25UYWJsZS5sZW5ndGg7IGkrKyl7XG4gICAgICAgICAgICBsZXQgbGlzdENhcmRPZkdyb3VwID0gdGhpcy5saXN0Q2FyZE9uVGFibGVbaV07XG4gICAgICAgICAgICBpZihsaXN0Q2FyZE9mR3JvdXAubGVuZ3RoID4gMCl7XG4gICAgICAgICAgICAgICAgLy9DaGVjayBsYXN0Q2FyZCBvZiBHcm91cCBtYXRjaCB3aXRoIEhvbGRlclxuICAgICAgICAgICAgICAgIGxldCBpZHhIb2xkZXIgPSB0aGlzLmNoZWNrV2l0aExpc3RIb2xkZXIobGlzdENhcmRPZkdyb3VwW2xpc3RDYXJkT2ZHcm91cC5sZW5ndGggLSAxXS5pZENhcmQpO1xuICAgICAgICAgICAgICAgIGlmKGlkeEhvbGRlciAhPT0gLTEpe1xuICAgICAgICAgICAgICAgICAgICBsZXQgY2FyZCA9IGxpc3RDYXJkT2ZHcm91cFtsaXN0Q2FyZE9mR3JvdXAubGVuZ3RoIC0gMV07XG4gICAgICAgICAgICAgICAgICAgIGxldCBwb3MgPSB0aGlzLmNhcmRIb2xkZXJBbmNob3JbaWR4SG9sZGVyXS5nZXRQb3NpdGlvbigpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLm1vdmVDYXJkSGludChbY2FyZF0sIHBvcyk7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgLy8gQ2hlY2sgY2FyZCBPbiBUYWJsZSBNYXRjaCBlYWNoIG90aGVyXG4gICAgICAgICAgICAgICAgLy9JZiBGaXJzdENhcmRPZkdyb3VwIGlzIEtpbmcgLT4gcmV0dXJuXG4gICAgICAgICAgICAgICAgaWYobGlzdENhcmRPZkdyb3VwWzBdLmJBY3RpdmUgJiYgbGlzdENhcmRPZkdyb3VwWzBdLmlkQ2FyZCA+IDQ3ICYmIGxpc3RDYXJkT2ZHcm91cFswXS5pZENhcmQgPCA1MilcbiAgICAgICAgICAgICAgICAgICAgY29udGludWU7XG5cbiAgICAgICAgICAgICAgICBsZXQgYXJyQ2FyZCA9IFtdO1xuXG4gICAgICAgICAgICAgICAgZm9yKGxldCBqID0gMDsgaiA8IGxpc3RDYXJkT2ZHcm91cC5sZW5ndGg7IGorKyl7XG4gICAgICAgICAgICAgICAgICAgIGlmKGxpc3RDYXJkT2ZHcm91cFtqXS5iQWN0aXZlKVxuICAgICAgICAgICAgICAgICAgICAgICAgYXJyQ2FyZC5wdXNoKGxpc3RDYXJkT2ZHcm91cFtqXSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGxldCBpZHhHcm91cFZhbGlkYXRlID0gdGhpcy5jaGVja1dpdGhDYXJkT25UYWJsZShhcnJDYXJkWzBdLmlkQ2FyZCk7XG4gICAgICAgICAgICAgICAgaWYoaWR4R3JvdXBWYWxpZGF0ZSAhPT0gLTEpe1xuICAgICAgICAgICAgICAgICAgICBsZXQgZGVzUG9zID0gbnVsbDtcbiAgICAgICAgICAgICAgICAgICAgaWYodGhpcy5saXN0Q2FyZE9uVGFibGVbaWR4R3JvdXBWYWxpZGF0ZV0ubGVuZ3RoID09PSAwKVxuICAgICAgICAgICAgICAgICAgICAgICAgZGVzUG9zID0gdGhpcy5jYXJkT25UYWJsZUFuY2hvcltpZHhHcm91cFZhbGlkYXRlXS5nZXRQb3NpdGlvbigpO1xuICAgICAgICAgICAgICAgICAgICBlbHNle1xuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGxhc3RDYXJkUG9zID0gdGhpcy5saXN0Q2FyZE9uVGFibGVbaWR4R3JvdXBWYWxpZGF0ZV1bdGhpcy5saXN0Q2FyZE9uVGFibGVbaWR4R3JvdXBWYWxpZGF0ZV0ubGVuZ3RoIC0gMV0ubm9kZS5nZXRQb3NpdGlvbigpO1xuICAgICAgICAgICAgICAgICAgICAgICAgZGVzUG9zID0gY2MudjIobGFzdENhcmRQb3MueCwgbGFzdENhcmRQb3MueSAtIDUwKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB0aGlzLm1vdmVDYXJkSGludChhcnJDYXJkLCBkZXNQb3MpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIC8vIENoZWNrIGNhcmQgT24gRGVja1xuICAgICAgICBpZih0aGlzLmxpc3RDYXJkRGVjay5sZW5ndGggPiAwKXtcbiAgICAgICAgICAgIGxldCBjYXJkID0gdGhpcy5saXN0Q2FyZERlY2tbdGhpcy5saXN0Q2FyZERlY2subGVuZ3RoIC0gMV07XG4gICAgICAgICAgICAvL0NoZWNrIGNhcmQgRGVjayB3aXRoIEhvbGRlclxuICAgICAgICAgICAgbGV0IGlkeEhvbGRlciA9IHRoaXMuY2hlY2tXaXRoTGlzdEhvbGRlcih0aGlzLmxpc3RDYXJkRGVja1t0aGlzLmxpc3RDYXJkRGVjay5sZW5ndGggLSAxXS5pZENhcmQpO1xuICAgICAgICAgICAgaWYoaWR4SG9sZGVyICE9PSAtMSl7XG4gICAgICAgICAgICAgICAgbGV0IHBvc0hvbGRlciA9IHRoaXMuY2FyZEhvbGRlckFuY2hvcltpZHhIb2xkZXJdLmdldFBvc2l0aW9uKCk7XG4gICAgICAgICAgICAgICAgdGhpcy5tb3ZlQ2FyZEhpbnQoW2NhcmRdLCBwb3NIb2xkZXIpO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIC8vQ2hlY2sgY2FyZCBEZWNrIHdpdGggQWxsIGdyb3VwXG4gICAgICAgICAgICBsZXQgaWR4R3JvdXBWYWxpZGF0ZSA9IHRoaXMuY2hlY2tXaXRoQ2FyZE9uVGFibGUodGhpcy5saXN0Q2FyZERlY2tbdGhpcy5saXN0Q2FyZERlY2subGVuZ3RoIC0gMV0uaWRDYXJkKTtcbiAgICAgICAgICAgIGlmKGlkeEdyb3VwVmFsaWRhdGUgIT09IC0xKXtcbiAgICAgICAgICAgICAgICBsZXQgZGVzUG9zID0gbnVsbDtcbiAgICAgICAgICAgICAgICBpZih0aGlzLmxpc3RDYXJkT25UYWJsZVtpZHhHcm91cFZhbGlkYXRlXS5sZW5ndGggPT09IDApXG4gICAgICAgICAgICAgICAgICAgIGRlc1BvcyA9IHRoaXMuY2FyZE9uVGFibGVBbmNob3JbaWR4R3JvdXBWYWxpZGF0ZV0uZ2V0UG9zaXRpb24oKTtcbiAgICAgICAgICAgICAgICBlbHNle1xuICAgICAgICAgICAgICAgICAgICBsZXQgbGFzdENhcmRQb3MgPSB0aGlzLmxpc3RDYXJkT25UYWJsZVtpZHhHcm91cFZhbGlkYXRlXVt0aGlzLmxpc3RDYXJkT25UYWJsZVtpZHhHcm91cFZhbGlkYXRlXS5sZW5ndGggLSAxXS5ub2RlLmdldFBvc2l0aW9uKCk7XG4gICAgICAgICAgICAgICAgICAgIGRlc1BvcyA9IGNjLnYyKGxhc3RDYXJkUG9zLngsIGxhc3RDYXJkUG9zLnkgLSA1MCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHRoaXMubW92ZUNhcmRIaW50KFtjYXJkXSwgZGVzUG9zKTtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgLy9DaGVjayBjYXJkIE9uIExpc3QgQ2FyZCBEZWNrXG4gICAgICAgIGlmKHRoaXMubGlzdElkQ2FyZERlY2subGVuZ3RoID4gMCl7XG4gICAgICAgICAgICBmb3IobGV0IGkgPSAwOyBpIDwgdGhpcy5saXN0SWRDYXJkRGVjay5sZW5ndGg7IGkrKyl7XG4gICAgICAgICAgICAgICAgbGV0IGlkeEhvbGRlclZhbGlkYXRlID0gdGhpcy5jaGVja1dpdGhMaXN0SG9sZGVyKHRoaXMubGlzdElkQ2FyZERlY2tbaV0pO1xuICAgICAgICAgICAgICAgIGxldCBpZHhHcm91cHZhbGlkYXRlID0gdGhpcy5jaGVja1dpdGhDYXJkT25UYWJsZSh0aGlzLmxpc3RJZENhcmREZWNrW2ldKTtcbiAgICAgICAgICAgICAgICBpZihpZHhHcm91cHZhbGlkYXRlICE9PSAtMSB8fCBpZHhIb2xkZXJWYWxpZGF0ZSAhPT0gLTEpe1xuICAgICAgICAgICAgICAgICAgICBsZXQgZGVzUG9zWCA9IHRoaXMuY2FyZERlY2tBbmNob3IuZ2V0UG9zaXRpb24oKS54ICsgNTAqKHRoaXMubGlzdENhcmREZWNrLmxlbmd0aCA+IDIgPyAyIDogdGhpcy5saXN0Q2FyZERlY2subGVuZ3RoKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zcnBDYXJkT25EZWNrLmFjdGl2ZSA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgIGNjLnR3ZWVuKHRoaXMuc3JwQ2FyZE9uRGVjaylcbiAgICAgICAgICAgICAgICAgICAgICAgIC50bygwLjIsIHtwb3NpdGlvbjpjYy52MihkZXNQb3NYLCB0aGlzLmNhcmREZWNrQW5jaG9yLmdldFBvc2l0aW9uKCkueSl9KVxuICAgICAgICAgICAgICAgICAgICAgICAgLmRlbGF5KDAuMilcbiAgICAgICAgICAgICAgICAgICAgICAgIC50bygwLjIsIHtwb3NpdGlvbjogdGhpcy5idG5HZXRDYXJkLm5vZGUuZ2V0UG9zaXRpb24oKX0pXG4gICAgICAgICAgICAgICAgICAgICAgICAuY2FsbCgoKT0+e3RoaXMuc3JwQ2FyZE9uRGVjay5hY3RpdmUgPSBmYWxzZX0pXG4gICAgICAgICAgICAgICAgICAgICAgICAuc3RhcnQoKTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICB0aGlzLmdhbWVTY2VuZS5wb3B1cC5zaG93UG9wdXAodHJ1ZSwgXCJObyBjYXJkIGNhbiBiZSBtb3ZlZCFcIilcbiAgICB9LFxuICAgIG1vdmVDYXJkSGludChhcnJNb3ZlZCwgZmlyc3REZXNQb3Mpe1xuICAgICAgICBmb3IobGV0IGkgPSAwOyBpIDwgYXJyTW92ZWQubGVuZ3RoOyBpKyspe1xuICAgICAgICAgICAgbGV0IGNhcmQgPSBhcnJNb3ZlZFtpXTtcbiAgICAgICAgICAgIGxldCBkZXNQb3MgPSBjYy52MihmaXJzdERlc1Bvcy54LCBmaXJzdERlc1Bvcy55IC0gNTAqaSk7XG4gICAgICAgICAgICBjYy50d2VlbihjYXJkLm5vZGUpXG4gICAgICAgICAgICAgICAgLmNhbGwoKCk9PntcbiAgICAgICAgICAgICAgICAgICAgY2FyZC5zaG93Qm9yZGVyKHRydWUpXG4gICAgICAgICAgICAgICAgICAgIGNhcmQubm9kZS56SW5kZXggPSAxMDAgKyBpO1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLnRvKDAuMiwge3Bvc2l0aW9uOmRlc1Bvc30pXG4gICAgICAgICAgICAgICAgLmRlbGF5KDAuMilcbiAgICAgICAgICAgICAgICAudG8oMC4yLCB7cG9zaXRpb246Y2FyZC5vcmlQb3N9KVxuICAgICAgICAgICAgICAgIC5jYWxsKCgpPT57XG4gICAgICAgICAgICAgICAgICAgIGNhcmQuc2hvd0JvcmRlcihmYWxzZSk7XG4gICAgICAgICAgICAgICAgICAgIGNhcmQubm9kZS56SW5kZXggPSBjYXJkLm9yaUluZGV4O1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgLnN0YXJ0KCk7XG4gICAgICAgIH1cbiAgICB9LFxuLy8gICAgQXV0b0NvbXBsZXRlXG4gICAgYXV0b0NvbXBsZXRlKCl7XG4gICAgICAgIC8vQ2hlY2sgbGFzdCBjYXJkIG9uIHRhYmxlXG4gICAgICAgIGZvcihsZXQgaSA9IDA7IGkgPCB0aGlzLmxpc3RDYXJkT25UYWJsZS5sZW5ndGg7IGkrKyl7XG4gICAgICAgICAgICBsZXQgbGlzdENhcmRPZkdyb3VwID0gdGhpcy5saXN0Q2FyZE9uVGFibGVbaV07XG4gICAgICAgICAgICBpZihsaXN0Q2FyZE9mR3JvdXAubGVuZ3RoID4gMCl7XG4gICAgICAgICAgICAgICAgLy9DaGVjayBsYXN0Q2FyZCBvZiBHcm91cCBtYXRjaCB3aXRoIEhvbGRlclxuICAgICAgICAgICAgICAgIGxldCBjYXJkID0gbGlzdENhcmRPZkdyb3VwW2xpc3RDYXJkT2ZHcm91cC5sZW5ndGggLSAxXTtcbiAgICAgICAgICAgICAgICBsZXQgaWR4SG9sZGVyID0gdGhpcy5jaGVja1dpdGhMaXN0SG9sZGVyKGNhcmQuaWRDYXJkKTtcbiAgICAgICAgICAgICAgICBpZihpZHhIb2xkZXIgIT0gLTEpe1xuICAgICAgICAgICAgICAgICAgICBjYXJkLm5vZGUuekluZGV4ID0gMTAwO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLm1vdmVDYXJkVG9Ib2xkZXIoY2FyZCwgaWR4SG9sZGVyKTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgICAgICAgICAgICAvLyB0aGlzLmF1dG9Db21wbGV0ZSgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICAvL0NoZWNrIGxhc3QgY2FyZCBvZiBsaXN0IERlY2tcbiAgICAgICAgaWYodGhpcy5saXN0Q2FyZERlY2subGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgbGV0IGNhcmQgPSB0aGlzLmxpc3RDYXJkRGVja1t0aGlzLmxpc3RDYXJkRGVjay5sZW5ndGggLSAxXTtcbiAgICAgICAgICAgIC8vQ2hlY2sgY2FyZCBEZWNrIHdpdGggSG9sZGVyXG4gICAgICAgICAgICBsZXQgaWR4SG9sZGVyID0gdGhpcy5jaGVja1dpdGhMaXN0SG9sZGVyKGNhcmQuaWRDYXJkKTtcbiAgICAgICAgICAgIGlmIChpZHhIb2xkZXIgIT09IC0xKSB7XG4gICAgICAgICAgICAgICAgY2FyZC5ub2RlLnpJbmRleCA9IDEwMDtcbiAgICAgICAgICAgICAgICB0aGlzLm1vdmVDYXJkVG9Ib2xkZXIoY2FyZCwgaWR4SG9sZGVyKTtcbiAgICAgICAgICAgICAgICAvLyB0aGlzLmF1dG9Db21wbGV0ZSgpO1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGxldCBpZHhHcm91cFZhbGlkYXRlID0gdGhpcy5jaGVja1dpdGhDYXJkT25UYWJsZShjYXJkLmlkQ2FyZCk7XG4gICAgICAgICAgICBpZihpZHhHcm91cFZhbGlkYXRlICE9PSAtMSl7XG4gICAgICAgICAgICAgICAgY2FyZC5ub2RlLnpJbmRleCA9IDEwMDtcbiAgICAgICAgICAgICAgICB0aGlzLm1vdmVDYXJkT25UYWJsZXMoW2NhcmRdLCBpZHhHcm91cFZhbGlkYXRlKTtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgaWYodGhpcy5saXN0SWRDYXJkRGVjay5sZW5ndGggPiAwKXtcbiAgICAgICAgICAgIHRoaXMuYnRuR2V0Q2FyZENsaWNrKCk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5jaGVja0ZpbmlzaEdhbWUoKTtcbiAgICB9LFxuICAgIGNoZWNrRmluaXNoR2FtZSgpe1xuICAgICAgICBmb3IobGV0IGkgPSAwOyBpIDwgdGhpcy5saXN0Q2FyZEhvbGRlci5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgaWYgKHRoaXMubGlzdENhcmRIb2xkZXJbaV0ubGVuZ3RoIDwgMTMpe1xuICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICB0aGlzLmJHYW1lU3RhcnQgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5wcm9jZXNzRW5kR2FtZSgpO1xuICAgIH0sXG4gICAgcHJvY2Vzc0VuZEdhbWUoKXtcbiAgICAgICAgbGV0IHJhbmRvbVBvc0FuaW0gPSBhcnJQb3NBbmltW01hdGguZmxvb3IoTWF0aC5yYW5kb20oKSozKV07XG4gICAgICAgIGZvcihsZXQgaSA9IDA7IGkgPCB0aGlzLmxpc3RDYXJkSG9sZGVyLmxlbmd0aDsgaSsrKXtcbiAgICAgICAgICAgIGZvcihsZXQgaiA9IDA7IGogPCB0aGlzLmxpc3RDYXJkSG9sZGVyW2ldLmxlbmd0aDsgaisrKXtcbiAgICAgICAgICAgICAgICBsZXQgY2FyZCA9IHRoaXMubGlzdENhcmRIb2xkZXJbaV1bal07XG4gICAgICAgICAgICAgICAgbGV0IGZpcnN0UG9zID0gcmFuZG9tUG9zQW5pbVsoaitpKSVyYW5kb21Qb3NBbmltLmxlbmd0aF07XG4gICAgICAgICAgICAgICAgbGV0IHNlY29uZFBvcyA9IHJhbmRvbVBvc0FuaW1bKGoraSsxKSVyYW5kb21Qb3NBbmltLmxlbmd0aF07XG4gICAgICAgICAgICAgICAgbGV0IHRoaXJkUG9zID0gcmFuZG9tUG9zQW5pbVsoaitpKzIpJXJhbmRvbVBvc0FuaW0ubGVuZ3RoXTtcbiAgICAgICAgICAgICAgICBsZXQgZm91cnRoUG9zID0gcmFuZG9tUG9zQW5pbVsoaitpKzMpJXJhbmRvbVBvc0FuaW0ubGVuZ3RoXTtcbiAgICAgICAgICAgICAgICBsZXQgZmlmdGhQb3MgPSByYW5kb21Qb3NBbmltWyhqK2krNCklcmFuZG9tUG9zQW5pbS5sZW5ndGhdO1xuXG4gICAgICAgICAgICAgICAgY2MudHdlZW4oY2FyZC5ub2RlKVxuICAgICAgICAgICAgICAgICAgICAuZGVsYXkoMi4wICsgMC4xKmogKyAwLjIqaSlcbiAgICAgICAgICAgICAgICAgICAgLnRvKDAuMiwge3Bvc2l0aW9uOmZpcnN0UG9zfSlcbiAgICAgICAgICAgICAgICAgICAgLmRlbGF5KDAuMiooMTQgLSBqKSlcbiAgICAgICAgICAgICAgICAgICAgLnRvKDAuNCwge3Bvc2l0aW9uOnNlY29uZFBvc30pXG4gICAgICAgICAgICAgICAgICAgIC50bygwLjQsIHtwb3NpdGlvbjp0aGlyZFBvc30pXG4gICAgICAgICAgICAgICAgICAgIC50bygwLjQsIHtwb3NpdGlvbjpmb3VydGhQb3N9KVxuICAgICAgICAgICAgICAgICAgICAudG8oMC40LCB7cG9zaXRpb246ZmlmdGhQb3N9KVxuICAgICAgICAgICAgICAgICAgICAuZGVsYXkoMC4yKmogKyAwLjEqaSlcbiAgICAgICAgICAgICAgICAgICAgLnRvKDAuMiArICgwLjAxKmopLCB7cG9zaXRpb246Y2FyZC5vcmlQb3N9KVxuICAgICAgICAgICAgICAgICAgICAuY2FsbCgoKT0+e1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYoaSA9PT0gdGhpcy5saXN0Q2FyZEhvbGRlci5sZW5ndGggLSAxICYmIGogPT09IHRoaXMubGlzdENhcmRIb2xkZXJbaV0ubGVuZ3RoIC0gMSAmJiAhdGhpcy5nYW1lU2NlbmUucG9wdXAubm9kZS5hY3RpdmUpe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8vIGNjLmxvZyhcIllvdSBhcmUgVmljdG9yeVwiKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZ2FtZVNjZW5lLnBvcHVwLnNob3dQb3B1cCh0cnVlLCBcIllvdSBhcmUgVmljdG9yeSFcIilcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBcbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgLnN0YXJ0KCk7XG5cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cbn0pO1xuIl19