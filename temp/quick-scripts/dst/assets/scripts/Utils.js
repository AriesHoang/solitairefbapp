
                (function() {
                    var nodeEnv = typeof require !== 'undefined' && typeof process !== 'undefined';
                    var __module = nodeEnv ? module : {exports:{}};
                    var __filename = 'preview-scripts/assets/scripts/Utils.js';
                    var __require = nodeEnv ? function (request) {
                        return cc.require(request);
                    } : function (request) {
                        return __quick_compile_project__.require(request, __filename);
                    };
                    function __define (exports, require, module) {
                        if (!nodeEnv) {__quick_compile_project__.registerModule(__filename, module);}"use strict";
cc._RF.push(module, '712fcFcT8tD0JbculpoClOZ', 'Utils');
// scripts/Utils.js

"use strict";

window.Utils = {};

window.Utils.cloneObject = function (objSrc) {
  return JSON.parse(JSON.stringify(objSrc));
};

window.Utils.isEmpty = function (ob) {
  return ob === null || ob === undefined || ob === "";
};

window.Utils.removeItemInArray = function (item, array) {
  var idxItem = array.indexOf(item);
  if (idxItem > -1) array.splice(idxItem, 1);
  return array;
};

window.Utils.convertTime = function (iTime) {
  iTime = Math.floor(iTime);
  var min = Math.floor(iTime / 60);
  var sec = iTime % 60;
  return (min < 10 ? "0" : "") + min + ":" + (sec < 10 ? "0" : "") + sec;
};

window.SoundEffect = {
  DEAL_SOUND: 0,
  FLIP_SOUND: 1,
  CLICK_SOUND: 2,
  DEFEAT_SOUND: 3
};

cc._RF.pop();
                    }
                    if (nodeEnv) {
                        __define(__module.exports, __require, __module);
                    }
                    else {
                        __quick_compile_project__.registerModuleFunc(__filename, function () {
                            __define(__module.exports, __require, __module);
                        });
                    }
                })();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFzc2V0cy9zY3JpcHRzL1V0aWxzLmpzIl0sIm5hbWVzIjpbIndpbmRvdyIsIlV0aWxzIiwiY2xvbmVPYmplY3QiLCJvYmpTcmMiLCJKU09OIiwicGFyc2UiLCJzdHJpbmdpZnkiLCJpc0VtcHR5Iiwib2IiLCJ1bmRlZmluZWQiLCJyZW1vdmVJdGVtSW5BcnJheSIsIml0ZW0iLCJhcnJheSIsImlkeEl0ZW0iLCJpbmRleE9mIiwic3BsaWNlIiwiY29udmVydFRpbWUiLCJpVGltZSIsIk1hdGgiLCJmbG9vciIsIm1pbiIsInNlYyIsIlNvdW5kRWZmZWN0IiwiREVBTF9TT1VORCIsIkZMSVBfU09VTkQiLCJDTElDS19TT1VORCIsIkRFRkVBVF9TT1VORCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQUEsTUFBTSxDQUFDQyxLQUFQLEdBQWUsRUFBZjs7QUFDQUQsTUFBTSxDQUFDQyxLQUFQLENBQWFDLFdBQWIsR0FBMkIsVUFBU0MsTUFBVCxFQUFnQjtBQUN2QyxTQUFPQyxJQUFJLENBQUNDLEtBQUwsQ0FBV0QsSUFBSSxDQUFDRSxTQUFMLENBQWVILE1BQWYsQ0FBWCxDQUFQO0FBQ0gsQ0FGRDs7QUFHQUgsTUFBTSxDQUFDQyxLQUFQLENBQWFNLE9BQWIsR0FBdUIsVUFBVUMsRUFBVixFQUFjO0FBQ2pDLFNBQVFBLEVBQUUsS0FBSyxJQUFQLElBQWVBLEVBQUUsS0FBS0MsU0FBdEIsSUFBbUNELEVBQUUsS0FBSyxFQUFsRDtBQUNILENBRkQ7O0FBR0FSLE1BQU0sQ0FBQ0MsS0FBUCxDQUFhUyxpQkFBYixHQUFpQyxVQUFTQyxJQUFULEVBQWVDLEtBQWYsRUFBcUI7QUFDbEQsTUFBSUMsT0FBTyxHQUFHRCxLQUFLLENBQUNFLE9BQU4sQ0FBY0gsSUFBZCxDQUFkO0FBQ0EsTUFBR0UsT0FBTyxHQUFHLENBQUMsQ0FBZCxFQUNJRCxLQUFLLENBQUNHLE1BQU4sQ0FBYUYsT0FBYixFQUFzQixDQUF0QjtBQUNKLFNBQU9ELEtBQVA7QUFDSCxDQUxEOztBQU1BWixNQUFNLENBQUNDLEtBQVAsQ0FBYWUsV0FBYixHQUEyQixVQUFTQyxLQUFULEVBQWU7QUFDdENBLEVBQUFBLEtBQUssR0FBR0MsSUFBSSxDQUFDQyxLQUFMLENBQVdGLEtBQVgsQ0FBUjtBQUNBLE1BQUlHLEdBQUcsR0FBR0YsSUFBSSxDQUFDQyxLQUFMLENBQVdGLEtBQUssR0FBQyxFQUFqQixDQUFWO0FBQ0EsTUFBSUksR0FBRyxHQUFHSixLQUFLLEdBQUMsRUFBaEI7QUFDQSxTQUFRLENBQUNHLEdBQUcsR0FBRyxFQUFOLEdBQVcsR0FBWCxHQUFpQixFQUFsQixJQUF5QkEsR0FBekIsR0FBK0IsR0FBL0IsSUFBc0NDLEdBQUcsR0FBRyxFQUFOLEdBQVcsR0FBWCxHQUFpQixFQUF2RCxJQUE2REEsR0FBckU7QUFDSCxDQUxEOztBQVFBckIsTUFBTSxDQUFDc0IsV0FBUCxHQUFxQjtBQUNqQkMsRUFBQUEsVUFBVSxFQUFFLENBREs7QUFFakJDLEVBQUFBLFVBQVUsRUFBRSxDQUZLO0FBR2pCQyxFQUFBQSxXQUFXLEVBQUUsQ0FISTtBQUlqQkMsRUFBQUEsWUFBWSxFQUFFO0FBSkcsQ0FBckIiLCJzb3VyY2VSb290IjoiLyIsInNvdXJjZXNDb250ZW50IjpbIndpbmRvdy5VdGlscyA9IHt9O1xud2luZG93LlV0aWxzLmNsb25lT2JqZWN0ID0gZnVuY3Rpb24ob2JqU3JjKXtcbiAgICByZXR1cm4gSlNPTi5wYXJzZShKU09OLnN0cmluZ2lmeShvYmpTcmMpKTtcbn07XG53aW5kb3cuVXRpbHMuaXNFbXB0eSA9IGZ1bmN0aW9uIChvYikge1xuICAgIHJldHVybiAob2IgPT09IG51bGwgfHwgb2IgPT09IHVuZGVmaW5lZCB8fCBvYiA9PT0gXCJcIik7XG59O1xud2luZG93LlV0aWxzLnJlbW92ZUl0ZW1JbkFycmF5ID0gZnVuY3Rpb24oaXRlbSwgYXJyYXkpe1xuICAgIGxldCBpZHhJdGVtID0gYXJyYXkuaW5kZXhPZihpdGVtKTtcbiAgICBpZihpZHhJdGVtID4gLTEpXG4gICAgICAgIGFycmF5LnNwbGljZShpZHhJdGVtLCAxKTtcbiAgICByZXR1cm4gYXJyYXk7XG59XG53aW5kb3cuVXRpbHMuY29udmVydFRpbWUgPSBmdW5jdGlvbihpVGltZSl7XG4gICAgaVRpbWUgPSBNYXRoLmZsb29yKGlUaW1lKTtcbiAgICBsZXQgbWluID0gTWF0aC5mbG9vcihpVGltZS82MCk7XG4gICAgbGV0IHNlYyA9IGlUaW1lJTYwXG4gICAgcmV0dXJuICgobWluIDwgMTAgPyBcIjBcIiA6IFwiXCIpICArIG1pbiArIFwiOlwiICsgKHNlYyA8IDEwID8gXCIwXCIgOiBcIlwiKSArIHNlYyk7XG59XG5cblxud2luZG93LlNvdW5kRWZmZWN0ID0ge1xuICAgIERFQUxfU09VTkQ6IDAsXG4gICAgRkxJUF9TT1VORDogMSxcbiAgICBDTElDS19TT1VORDogMixcbiAgICBERUZFQVRfU09VTkQ6IDNcbn0iXX0=