"use strict";
cc._RF.push(module, 'da8d01jkcRK47ZhDCADmmrf', 'CardPrefab');
// scripts/Model/CardPrefab.js

"use strict";

var arrSuit = ["B", "T", "R", "C"];
var arrRank = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"];
cc.Class({
  "extends": cc.Component,
  properties: {
    btnCard: cc.Button,
    sprCard: cc.Sprite,
    animCard: cc.Animation,
    sprBackCard: cc.SpriteFrame,
    sprBorder: cc.Node,
    effectCard: cc.Animation
  },
  ctor: function ctor() {
    this.idCard = -1;
    this.groupId = -1;
    this.bTouch = false;
    this.oriPos = cc.v2(0, 0);
    this.oriIndex = 0;
    this.spriteFrameCard = null;
  },
  // LIFE-CYCLE CALLBACKS:
  // onLoad () {},
  start: function start() {},
  setSpriteCard: function setSpriteCard(idCard, sprCard) {
    // cc.log("setSpriteCard: " + idCard);
    this.showBorder(false);
    this.bTouch = false;
    this.bActive = false;
    this.idCard = idCard;
    this.spriteFrameCard = sprCard;
    this.btnCard.interactable = false;
  },
  showCard: function showCard(bAnim) {
    var _this = this;

    this.bActive = true;

    if (bAnim) {
      cc.tween(this.node).to(0.2, {
        scaleX: 0
      }).call(function () {
        _this.sprCard.spriteFrame = _this.spriteFrameCard;
        _this.btnCard.interactable = true;
        _this.bTouch = true;
      }).to(0.2, {
        scaleX: 1.0
      }).start();
    } else {
      this.sprCard.spriteFrame = this.spriteFrameCard;
      this.btnCard.interactable = false;
      this.bTouch = false;
    }
  },
  showBorder: function showBorder(bShow) {
    this.sprBorder.active = bShow;
  },
  shakeCard: function shakeCard() {
    this.animCard.play();
  },
  showEffectCard: function showEffectCard(bShow) {
    if (bShow === void 0) {
      bShow = true;
    }

    this.effectCard.node.active = bShow;

    if (bShow) {
      this.effectCard.play();
    }
  },
  endEffectCard: function endEffectCard() {
    this.showEffectCard(false);
  },
  resetCard: function resetCard() {
    this.idCard = -1;
    this.bTouch = false;
    this.bActive = false;
    this.sprCard.spriteFrame = this.sprBackCard;
    this.showBorder(false);
    this.effectCard.node.active = false;
  }
});

cc._RF.pop();