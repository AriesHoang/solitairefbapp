"use strict";
cc._RF.push(module, 'e9959FDf5RBfKVjy79pNx3r', 'GameScene');
// scripts/GameScene.js

"use strict";

var GameBoard = require("GameBoard");

var Popup = require("Popup");

var HelpPopup = require("HelpPopup");

cc.Class({
  "extends": cc.Component,
  properties: {
    btnSound: cc.Sprite,
    arrSprSound: [cc.SpriteFrame],
    audioClips: [cc.AudioClip],
    lblScore: cc.Label,
    lblMoves: cc.Label,
    lblTime: cc.Label,
    btnNewGame: cc.Button,
    gameBoard: GameBoard,
    popup: Popup,
    helpPopup: HelpPopup
  },
  onStart: function onStart() {
    this.iTime = 0;
  },
  onLoad: function onLoad() {
    this.gameBoard.gameScene = this;
    this.popup.gameScene = this;
    this.bMuteSound = false;
    this.backgroundSound = cc.audioEngine.play(this.getComponent(cc.AudioSource).clip, true, 0.5);
    this.popup.showPopup(true, "Start A New Game!");
  },
  btnMuteClick: function btnMuteClick() {
    this.playEffect(SoundEffect.CLICK_SOUND);
    this.bMuteSound = !this.bMuteSound;

    if (this.bMuteSound) {
      this.btnSound.spriteFrame = this.arrSprSound[0];
      cc.audioEngine.pause(this.backgroundSound);
    } else {
      this.btnSound.spriteFrame = this.arrSprSound[1];
      cc.audioEngine.resume(this.backgroundSound);
    }
  },
  btnHelpCliclk: function btnHelpCliclk() {
    this.helpPopup.show(true);
  },
  btnNewGameClick: function btnNewGameClick() {
    this.playEffect(SoundEffect.CLICK_SOUND);
    this.iTime = 0;
    this.gameBoard.startNewGame();
  },
  playEffect: function playEffect(iEffect) {
    if (!this.bMuteSound) {
      cc.audioEngine.playEffect(this.audioClips[iEffect], false, 1);
    }
  },
  update: function update(dt) {
    if (this.gameBoard.bGameStart) {
      this.iTime += dt;
      this.lblTime.string = "Time " + Utils.convertTime(this.iTime);
      this.lblScore.string = "Score:" + this.gameBoard.iScore;
      this.lblMoves.string = "Moves: " + this.gameBoard.iMove;
    } else {
      this.iTime = 0;
    }
  }
});

cc._RF.pop();