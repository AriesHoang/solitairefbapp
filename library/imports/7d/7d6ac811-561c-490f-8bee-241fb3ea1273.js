"use strict";
cc._RF.push(module, '7d6acgRVhxJD4vuJB+z6hJz', 'Popup');
// scripts/Popup.js

"use strict";

cc.Class({
  "extends": cc.Component,
  properties: {
    lblContent: cc.Label
  },
  showPopup: function showPopup(bShow, strContent) {
    var _this = this;

    if (bShow) {
      this.lblContent.string = strContent;
      this.node.opacity = 0;
      this.node.scale = 0;
      this.node.active = true;
      cc.tween(this.node).to(0.2, {
        scale: 1.0,
        opacity: 255
      }).start();
    } else {
      cc.tween(this.node).to(0.2, {
        scale: 0,
        opacity: 0
      }).call(function () {
        _this.node.active = false;
      }).start();
    }
  },
  btnStartNewGame: function btnStartNewGame() {
    this.gameScene.btnNewGameClick();
    this.showPopup(false);
  },
  btnCloseClick: function btnCloseClick() {
    this.showPopup(false);
  },
  ctor: function ctor() {} // update (dt) {},

});

cc._RF.pop();