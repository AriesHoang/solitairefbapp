"use strict";
cc._RF.push(module, 'e0c61rpkHBJH6XQgn736VGo', 'HelpPopup');
// scripts/HelpPopup.js

"use strict";

var arrDesc = ["To win a game, you need to complete 4 pile of the same suit from Ace to King.", "Cards can only be moved to different-color cards ranked one higher.", "Any empty column can only placed by a Kind or a column starting with a King.", "When you cannot move any cards in the deck, you can click the stockpile to deal one or three cards"];
cc.Class({
  "extends": cc.Component,
  properties: {
    nodeContent: cc.Node,
    sprCurrentGuide: cc.Sprite,
    arrSpr: [cc.SpriteFrame],
    arrIdxDesc: [cc.Node],
    btnNext: cc.Button,
    btnPrev: cc.Button,
    lblGuideContent: cc.Label
  },
  ctor: function ctor() {
    this.iCurrentGuide = 0;
  },
  show: function show(bShow) {
    var _this = this;

    if (bShow) {
      this.resetGuide();
      this.node.opacity = 0;
      this.node.scale = 0;
      this.node.active = true;
      cc.tween(this.node).to(0.2, {
        scale: 1.0,
        opacity: 255
      }).start();
    } else {
      cc.tween(this.node).to(0.2, {
        scale: 0,
        opacity: 0
      }).call(function () {
        _this.node.active = false;
      }).start();
    }
  },
  resetGuide: function resetGuide() {
    this.iCurrentGuide = 0;
    this.processGuide();
  },
  processGuide: function processGuide(idxGuide) {
    if (idxGuide === void 0) {
      idxGuide = 0;
    }

    cc.log("processGuide: " + idxGuide);

    for (var i = 0; i < this.arrIdxDesc.length; i++) {
      if (i == idxGuide) {
        this.arrIdxDesc[idxGuide].opacity = 255;
      } else {
        this.arrIdxDesc[i].opacity = 100;
      }
    }

    this.btnNext.node.opacity = this.iCurrentGuide === 3 ? 100 : 255;
    this.btnPrev.node.opacity = this.iCurrentGuide === 0 ? 100 : 255;
    this.sprCurrentGuide.spriteFrame = this.arrSpr[this.iCurrentGuide];
    this.lblGuideContent.string = arrDesc[this.iCurrentGuide];
  },
  btnNextClick: function btnNextClick() {
    if (this.iCurrentGuide < 3) {
      this.iCurrentGuide++;
      this.processGuide(this.iCurrentGuide);
    }
  },
  btnPrevClick: function btnPrevClick() {
    if (this.iCurrentGuide > 0) {
      this.iCurrentGuide--;
      this.processGuide(this.iCurrentGuide);
    }
  },
  btnCloseClick: function btnCloseClick() {
    this.show(false);
  },
  start: function start() {} // update (dt) {},

});

cc._RF.pop();