"use strict";
cc._RF.push(module, 'ef78dkcymJO2onYHMezrQIz', 'CardUtils');
// scripts/CardUtils.js

"use strict";

window.CardUtils = {};

window.CardUtils.getRankCard = function (idxCard) {
  // cc.log(idxCard + " - getRankCard: " + (idxCard - idxCard%4)/4);
  return (idxCard - idxCard % 4) / 4;
};

window.CardUtils.getSuitCard = function (idxCard) {
  // cc.log(idxCard + " - getSuitCard: " + idxCard%4);
  return idxCard % 4;
};

window.CardUtils.checkSameColor = function (idxCard1, idxCard2) {
  return CardUtils.getSuitCard(idxCard1) > 1 && CardUtils.getSuitCard(idxCard2) > 1 || CardUtils.getSuitCard(idxCard1) < 2 && CardUtils.getSuitCard(idxCard2) < 2;
};

window.CardUtils.checkSameSuit = function (idxCard1, idxCard2) {
  return CardUtils.getSuitCard(idxCard1) === CardUtils.getSuitCard(idxCard2);
};

window.CardUtils.validateCardHolder = function (cardId, arrHolder) {
  if (arrHolder.length === 0 && CardUtils.getRankCard(cardId) === 0) return true;

  if (arrHolder.length > 0) {
    var lastCardHolder = arrHolder[arrHolder.length - 1]; //Check same suit and continue rank

    if (CardUtils.checkSameSuit(cardId, lastCardHolder) && CardUtils.getRankCard(cardId) === CardUtils.getRankCard(lastCardHolder) + 1) return true;
  }

  return false;
};

window.CardUtils.validateCardOnBoard = function (idCardSource, arrCard) {
  if (idCardSource > 47 && arrCard.length === 0) return true;
  var lastCardOfGroup = arrCard[arrCard.length - 1]; //Check different color and continue rank

  return !CardUtils.checkSameColor(idCardSource, lastCardOfGroup) && CardUtils.getRankCard(idCardSource) + 1 === CardUtils.getRankCard(lastCardOfGroup);
};

cc._RF.pop();