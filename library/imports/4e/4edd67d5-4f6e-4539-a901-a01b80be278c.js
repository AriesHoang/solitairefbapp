"use strict";
cc._RF.push(module, '4edd6fVT25FOakBoBuAvieM', 'FbSdk');
// scripts/FbSdk.js

"use strict";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html
var ID_FULL = "1339479033125975_1339479883125890";
var ID_REWARD = "1339479033125975_1339479846459227";
var ID_BANNER = "1339479033125975_1339479763125902";
var LoadState = {
  AD_LOADING: "AD_LOADING",
  AD_LOAD_SUCCESS: "AD_LOAD_SUCCESS",
  AD_LOAD_FAIL: "AD_LOAD_FAIL",
  AD_COMPLETE: "AD_COMPLETE"
}; // var linker = require('Linker');

cc.Class({
  "extends": cc.Component,
  properties: {},
  // LIFE-CYCLE CALLBACKS:
  onLoad: function onLoad() {
    // linker.fbSdk = this;
    this.init();
  },
  start: function start() {},
  init: function init() {
    cc.log("init FBSDK");
    this.winCount = 0; // this.adsInterTimeCount = (new Date()).getTime();

    this.adsInterTimeCount = null;
    this.preloadedInterstitial = null;
    this.preloadedRewardedVideo = null;
    this.InterstitialState = null;
    this.RewardedVideoState = null;

    if (typeof FBInstant != 'undefined' && FBInstant != null) {
      //load ads
      // this.showBannerAds();
      this.loadInterstitialAd(); // this.loadRewardVideo();
    } else {
      console.log("FBInstant not loaded");
    }
  },
  showBannerAds: function showBannerAds() {
    // if (cc.winSize.height < 1100) return;
    var isSupportedAdsBanner = FBInstant.getSupportedAPIs().includes('loadBannerAdAsync');
    console.log("isSupportedBanner ", isSupportedAdsBanner);
    if (!isSupportedAdsBanner) return;
    FBInstant.loadBannerAdAsync(ID_BANNER).then(function () {
      console.log('FBInstant.loadBannerAdAsync >> success');
    })["catch"](function (e) {
      console.log('FBInstant.loadBannerAdAsync >> error ', e);
    });
  },
  showInterstitial: function showInterstitial() {
    this.showInterstitialAd();
    this.winCount++;

    if (typeof FBInstant != 'undefined' && FBInstant != null) {
      this.showInterstitialAd();
    }
  },
  showRewardVideo: function showRewardVideo(callback) {
    if (callback === void 0) {
      callback = null;
    }

    if (typeof FBInstant != 'undefined' && FBInstant != null) {
      this.showRewardedVideo(callback);
    } else {
      if (callback) callback();
    }
  },
  loadRewardVideo: function loadRewardVideo() {
    var self = this;
    var supportedAPIs = FBInstant.getSupportedAPIs();

    if (supportedAPIs.includes('getRewardedVideoAsync')) {
      FBInstant.getRewardedVideoAsync(ID_REWARD // Your Ad Placement Id
      ).then(function (rewarded) {
        // Load the Ad asynchronously
        self.preloadedRewardedVideo = rewarded;
        self.RewardedVideoState = LoadState.AD_LOADING;
        return self.preloadedRewardedVideo.loadAsync();
      }).then(function () {
        console.log('Rewarded video preloaded');
        self.RewardedVideoState = LoadState.AD_LOAD_SUCCESS;
      })["catch"](function (err) {
        console.error('Rewarded video failed to preload: ' + err.message);
        self.RewardedVideoState = LoadState.AD_LOAD_FAIL;
      });
    }
  },
  showRewardedVideo: function showRewardedVideo(callback) {
    var self = this;
    var supportedAPIs = FBInstant.getSupportedAPIs();

    if (supportedAPIs.includes('getRewardedVideoAsync')) {
      self.preloadedRewardedVideo.showAsync().then(function () {
        self.RewardedVideoState = LoadState.AD_COMPLETE;
        if (callback) callback();
        self.loadRewardVideo();
        console.log('Rewarded video watched successfully');
      })["catch"](function (e) {
        console.log("reward video watch fail===" + e.message);
        self.loadRewardVideo();
      });
    }
  },
  loadInterstitialAd: function loadInterstitialAd() {
    var self = this;
    var supportedAPIs = FBInstant.getSupportedAPIs();

    if (supportedAPIs.includes('getInterstitialAdAsync')) {
      FBInstant.getInterstitialAdAsync(ID_FULL).then(function (interstitial) {
        self.preloadedInterstitial = interstitial;
        self.InterstitialState = LoadState.AD_LOADING;
        return self.preloadedInterstitial.loadAsync();
      }).then(function () {
        //self.showInterstitialAd();
        // showElement('btn-interstitial');
        self.InterstitialState = LoadState.AD_LOAD_SUCCESS;
      })["catch"](function (err) {
        // displayError
        console.log('Interstitial failed to preload: ' + err.message);
        self.preloadedInterstitial = null;
        self.InterstitialState = LoadState.AD_LOAD_FAIL;
      });
    } else {//  displayError('Ads not supported in this session');
    }
  },
  showInterstitialAd: function showInterstitialAd() {
    var time = new Date().getTime();
    if (this.adsInterTimeCount && Math.floor((time - this.adsInterTimeCount) / 1000) <= 30) return;
    this.adsInterTimeCount = time;
    var self = this;
    var supportedAPIs = FBInstant.getSupportedAPIs();

    if (supportedAPIs.includes('getInterstitialAdAsync')) {
      if (self.preloadedInterstitial != null) {
        self.preloadedInterstitial.showAsync().then(function () {
          self.InterstitialState = LoadState.AD_COMPLETE;
          self.loadInterstitialAd();
        })["catch"](function (e) {
          console.error(e.message);
          self.preloadedInterstitial = null;
          self.loadInterstitialAd();
        });
      } else {
        self.loadInterstitialAd();
      }
    }
  },
  isHaveVideo: function isHaveVideo() {
    if (typeof FBInstant != 'undefined' && FBInstant != null) {
      if (this.RewardedVideoState == LoadState.AD_LOAD_SUCCESS) {
        return true;
      } else {
        return false;
      }
    }

    return false;
  },
  getFriendsId: function getFriendsId() {
    var _this = this;

    this.friendsIDArr = [];
    FBInstant.player.getConnectedPlayersAsync().then(function (players) {
      //Get friend id
      players.forEach(function (player) {
        _this.friendsIDArr.push(player.getID());
      });
    });
  },
  challengeFriend: function challengeFriend(senderId, friendId, imgData, callbackFunction) {
    var _this2 = this;

    if (callbackFunction) {
      FBInstant.context.createAsync(friendId.toString()).then( /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _this2.updateAsync(imgData, senderId, 'leaderboard');

                callbackFunction();

              case 2:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      })))["catch"](function (err) {
        callbackFunction();
      });
    } else this.updateAsync(imgData, senderId, 'end_game');
  },
  inviteFB: function inviteFB(imgData, callback) {
    if (typeof FBInstant != 'undefined' && FBInstant != null) {
      var self = this;
      FBInstant.context.chooseAsync().then(function () {
        var updatePayload = {
          action: 'CUSTOM',
          cta: 'Join The Fight',
          image: imgData,
          text: {
            "default": "Let's play!",
            localizations: {}
          },
          template: 'invite_friends',
          data: {
            isFromInvite: true
          },
          strategy: 'IMMEDIATE',
          notification: 'PUSH'
        };
        if (callback) callback();
        FBInstant.updateAsync(updatePayload).then(function () {
          console.log('Send message success!');
          self.logEvent('ev_invite_done', 1, {});
        })["catch"](function (err) {
          console.log('invite fail ', err);
          self.logEvent('ev_invite_fail', 1, {});
        });
      })["catch"](function (err) {
        if (callback) callback();
      });
    }
  },
  updateAsync: function updateAsync(imgData, senderId, inviteCode) {
    var updatePayload = {
      action: 'CUSTOM',
      cta: 'Join The Fight',
      image: imgData,
      text: {
        "default": "Let's play!",
        localizations: {}
      },
      template: 'invite_friends',
      data: {
        isFromChallenge: true,
        senderId: senderId
      },
      strategy: 'IMMEDIATE',
      notification: 'PUSH'
    };
    FBInstant.updateAsync(updatePayload).then(function () {
      console.log('Send challenge success!');
    });
  },
  checkEntryPoint: function checkEntryPoint() {
    var _this3 = this;

    this.challengingSenderId = null;
    FBInstant.getEntryPointAsync().then(function (entry) {
      if (entry == 'admin_message') _this3.handleAdminMessage();
    });
  },
  handleAdminMessage: function handleAdminMessage() {
    var entryPointData = FBInstant.getEntryPointData();
    if (entryPointData == null) return;
    if (entryPointData.isFromChallenge) this.challengingSenderId = entryPointData.senderId;
  } // update (dt) {},

});

cc._RF.pop();